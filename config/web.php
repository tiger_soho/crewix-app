<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name'=> 'Crewix.com',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\controllers',
    'modules' => [
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'cVne6_pAizT9_o-Mth2RxW2j3MLDYZDV',
            'enableCsrfValidation'=>true,
            'enableCookieValidation'=>false,
            'class' => 'app\components\Request',
            'baseUrl'=>''
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'module/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',

                '<controller:signup>/<action:\w+>' => 'signup/<action>',
                '<controller:settings>/<action:\w+>' => 'settings/<action>',
                '<controller:dashboard>/<action:\w+>' => 'dashboard/<action>',
                '<controller:appsettings>/<action:\w+>' => 'appsettings/<action>',
                'appsettings/<action:\w+>/<edit:\w+>' => 'appsettings/<action>',
                '/id<id:\w+>'=>'site/profile',
                '/<action:\w+>/<id:\w+>'=>'site/<action>',
                '/<action:\w+>/<alias:\w+>'=>'site/<action>',
                'dashboard' => 'dashboard/index',
                'signup' => 'signup/index',
                'settings' => 'settings/index',
                '<action:[-\w]+>' => 'site/<action>',
                
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'paypal' => [
            'class' => 'marciocamello\Paypal',
            'clientId' => 'AcwluMKZfr1HNtMt5ZVan6hKIgO50DBU8D6JGjptJMod0T7ZExx0pe8BxEqwldRK74Ek0D2LWBZu51N3',
            'clientSecret' => 'EBZauRrSVK_fWB_yZAqbwqR5Vju-BEpTZmEFFp9Yw9LJ0AE-N6ZNe8tbQkpdHDyNziXydRz6VTfQWEV4',
            'isProduction' => false,
            // This is config file for the PayPal system
            'config' => [
                'http.ConnectionTimeOut' => 30,
                'http.Retry' => 1,
                'mode' => \marciocamello\Paypal::MODE_SANDBOX, // development (sandbox) or production (live) mode
                'log.LogEnabled' => YII_DEBUG ? 3 : 0,
                'log.FileName' => '@runtime/logs/paypal.log',
                'log.LogLevel' => \marciocamello\Paypal::LOG_LEVEL_FINE,
            ]
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;
