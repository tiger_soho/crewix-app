<?php
namespace app\controllers;


use app\models\CardForm;
use app\models\Faq;
use app\models\FaqCategory;
use app\models\Feedback;
use app\models\CompanyContact;
use app\models\PasswordResetRequestForm;
use app\models\PrivacyPolicy;
use app\models\SeamanDocumentScan;
use app\models\SeamanMedicalDocument;
use app\models\TermsOfService;
use app\models\User;
use app\models\UserFile;
use PayPal\Api\Terms;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\Url;
use app\models\LoginForm;
use app\models\Users;
use app\models\Profile;
use app\models\Countries;
use app\models\Companies;
use app\models\Biometrics;
use app\models\Family;
use app\models\Seaman;
use app\models\ContactForm;
use app\models\SeamanLanguage;
use app\models\Education;
use app\models\SeamanTravelDocument;
use app\models\SeamanCertificateOfCompetency;
use app\models\SeamanCertificateOfProficiency;
use app\models\SeamanSeaService;
use app\models\SeamanReference;
use app\models\CompanyVessel;
use app\models\SeamanNextOfKin;
use app\models\Vacancies;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\Pagination;
use app\models\SeamanTextForm;
use kartik\mpdf\Pdf;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function init()
    {

        parent::init();
        if (!Yii::$app->user->isGuest) {
            $user = Users::findOne(Yii::$app->user->id);
            $user->last_seen = date('Y-m-d H:i:s');
            $user->save(false, ["last_seen"]);
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['logout', 'error'],
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $user = User::findOne(Yii::$app->user->getId());
                            if (!Yii::$app->user->isGuest && !$user->registration_step == null) {
                                return $this->goHome();
                            } else {
                                return true;
                            }
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        if(Yii::$app->user->isGuest):
            $this->layout = 'mainpage';
            return $this->render('index');
        else:
            if (Yii::$app->user->can('seaman')) {
                switch (Yii::$app->user->identity->registration_step):
                    case 1:
                        return $this->redirect(Url::to(['/signup/personal']),302);
                        break;
                    case 2:
                        return $this->redirect(Url::to(['/signup/family']),302);
                        break;
                    case 3:
                        return $this->redirect(Url::to(['/signup/nok']), 302);
                        break;
                    case 4:
                        return $this->redirect(Url::to(['/signup/biometrics']), 302);
                        break;
                    case 5:
                        return $this->redirect(Url::to(['/signup/languages']), 302);
                        break;
                    case 6:
                        return $this->redirect(Url::to(['/signup/education']), 302);
                        break;
                    case 7:
                        return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                        break;
                    case 8:
                        return $this->redirect(Url::to(['/signup/coc']), 302);
                        break;
                    case 9:
                        return $this->redirect(Url::to(['/signup/cop']), 302);
                        break;
                    case 10:
                        return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                        break;
                    case 11:
                        return $this->redirect(Url::to(['/signup/service']), 302);
                        break;
                    case 12:
                        return $this->redirect(Url::to(['/signup/references']), 302);
                        break;
                    default:
                        $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                        return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                        break;
                endswitch;


            } elseif (Yii::$app->user->can('company')) {
                switch (Yii::$app->user->identity->registration_step):
                    case 1:
                        return $this->redirect(Url::to(['/signup/profile']),302);
                        break;
                    case 2:
                        return $this->redirect(Url::to(['/signup/contacts']),302);
                        break;
                    default:
                        $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                        return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                        break;
                endswitch;
            } elseif (Yii::$app->user->can('admin')) {
                return $this->redirect('/dashboard', 302);
            }


        endif;
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'signup';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionComupload()
    {
        if(!empty($_FILES['images'])){
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/companies/logos/';
            $ava = uniqid('', true).'_'.$_FILES['images']['name']['original'];
            move_uploaded_file($_FILES['images']['tmp_name']['huge'], $path.'medium/'.$ava);
            move_uploaded_file($_FILES['images']['tmp_name']['medium'], $path.'small/'.$ava);
            Yii::$app->session->set('file',$ava);
        }
    }

    public function actionAvatarupload()
    {
        if(!empty($_FILES['images'])){
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/seaman/photo/';
            $ava = uniqid('', true).'_'.$_FILES['images']['name']['original'];
            move_uploaded_file($_FILES['images']['tmp_name']['huge'], $path.'medium/'.$ava);
            move_uploaded_file($_FILES['images']['tmp_name']['medium'], $path.'small/'.$ava);
            Yii::$app->session->set('photo',$ava);
        }
    }

    public function actionCvupload()
    {
        if(!empty($_FILES['images'])){
            $path = $_SERVER['DOCUMENT_ROOT'].'images/seaman/docs/';
            $ava = uniqid('', true).'_'.$_FILES['images']['name'];
            move_uploaded_file($_FILES['images']['tmp_name'], $path.$ava);
            Yii::$app->session->set('cv',$ava);
        }
    }

    public function actionDocupload()
    {
        if (!empty($_FILES['src'])) {
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/seaman/docs/scans/';
            $ava = uniqid('', true) . '_' . $_FILES['src']['name'];
            move_uploaded_file($_FILES['src']['tmp_name'], $path . $ava);
            return $ava;
        }
    }

    public function actionEditcont()
    {
    	$id = Companies::find()->where(['user_id'=>Yii::$app->user->id])->one()->id;
    	$info = new CompanyContact();
    	$post = $_POST['Companies'];
        $info->editContacts($id,$post);
    	$country = Countries::find()->where(['id'=>$post['hq_country_id']])->one()->name;
    	$post['country'] = $country;
    	echo json_encode($post);
    }

    public function actionEditabout()
    {
    	$id = Yii::$app->user->id;
    	$post = $_POST['Companies'];
    	$comp = new Companies();

    	$comp->EditAbout($id,$post);

    	echo $post['about'];
    }

    public function actionFormvessel(){
        $this->layout = false;
        $model = new CompanyVessel();
        $id = Companies::find()->where(['user_id'=>Yii::$app->user->id])->one()->id;
        $data = $model->getVessels($id);

        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('CompanyVessel');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach($post['vessel_name'] as $key => $value) {
                $arr[$key]['vessel_name'] = $value;
            }
            foreach($post['imo_number'] as $key => $value) {
                $arr[$key]['imo_number'] = $value;
            }
            foreach($post['vessel_flag_id'] as $key => $value) {
                $arr[$key]['vessel_flag_id'] = $value;
            }
            foreach($post['vessel_type_id'] as $key => $value) {
                $arr[$key]['vessel_type_id'] = $value;
            }
            foreach($post['deadweight'] as $key => $value) {
                $arr[$key]['deadweight'] = $value;
            }
            foreach($post['engine_type_id'] as $key => $value) {
                $arr[$key]['engine_type_id'] = $value;
            }
            foreach($post['engine_power'] as $key => $value) {
                $arr[$key]['engine_power'] = $value;
            }
            foreach($post['year_built'] as $key => $value) {
                $arr[$key]['year_built'] = $value;
            }
            foreach($post['shipowning_company_name'] as $key => $value) {
                $arr[$key]['shipowning_company_name'] = $value;
            }


            $company = Companies::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {

                if(!empty($item['vessel_name'])) {
                    if (isset($item['id'])) {
                        $update_model = CompanyVessel::findOne(['id' => $item['id']]);
                        $update_model->vessel_name = $item['vessel_name'];
                        $update_model->imo_number = $item['imo_number'];
                        $update_model->vessel_flag_id = $item['vessel_flag_id'];
                        $update_model->vessel_type_id = $item['vessel_type_id'];
                        $update_model->deadweight = $item['deadweight'];
                        $update_model->engine_type_id = $item['engine_type_id'];
                        $update_model->engine_power = $item['engine_power'];
                        $update_model->year_built = $item['year_built'];
                        $update_model->shipowning_company_name = $item['shipowning_company_name'];
                        $update_model->save();
                    } else {
                        $model = new CompanyVessel();
                        $model->company_id = $company->id;
                        $model->vessel_name = $item['vessel_name'];
                        $model->imo_number = $item['imo_number'];
                        $model->vessel_flag_id = $item['vessel_flag_id'];
                        $model->vessel_type_id = $item['vessel_type_id'];
                        $model->deadweight = $item['deadweight'];
                        $model->engine_type_id = $item['engine_type_id'];
                        $model->engine_power = $item['engine_power'];
                        $model->year_built = $item['year_built'];
                        $model->shipowning_company_name = $item['shipowning_company_name'];
                        if(!$model->save()){
                            var_dump($model->errors);
                        }
                    }
                }
            }
            return false;
        }

        return $this->render('formvessel',[
            'model'=>$model,
            'data'=>$data,
        ]);
    }

    public function actionVessels(){
        $this->layout = false;

        $data = Companies::getOne(Yii::$app->user->id);

        return $this->render('vessels', [
            'data' => $data,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $this->layout = 'about';

        $data = Companies::find()->limit(6)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('about', [
            'data' => $data
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    
    /************************************Add vacancy form*******************************************/

    public function actionVac()
    {

        if (!Yii::$app->user->can('company')) {
            return $this->goHome();
        }
        $model = new Vacancies();
        $vess = new CompanyVessel();

        $active = $model->find()->where(['company_id'=>Companies::findOne(['user_id'=>Yii::$app->user->id])->id])->count();

        $free = 3 - $active;

        if ($model->load(Yii::$app->request->post())){
            $user = Yii::$app->user->id;
            $post = Yii::$app->request->post('Vacancies');
            $id = Companies::find()->select('id')->where(['user_id'=>Yii::$app->user->id])->one()->id;
            $model->saveVacancy($id,$post);
            if(!empty($post['save_vessel'])){
                $vess->saveVessel($id,$post);
            }
            $vac = Vacancies::find()->where(['company_id'=>$id])->orderBy(['id'=>SORT_DESC])->one()->id;

            /* @var $upd Companies */

            $upd = Companies::findOne($id);
            $curr = $upd->posted_vacancies;
            $upd->posted_vacancies = $curr+1;
            $upd->save(false);

            return $this->redirect('vacancy?id='.$vac,302);
        }
        return $this->render('vac', [
            'model' => $model,
            'free'=>$free,
        ]);
    }


    /************************************Vacancies list**********************************************/

    public function actionVacancies()
    {
        $model = new Vacancies();
        $query = new ActiveQuery(Vacancies::className());
        $query = $query->andWhere(['status' => 1]);
        if (Yii::$app->request->isPjax) {


            if (Yii::$app->request->get('rank')) {
                $query = $query->andWhere(['rank_id' => Yii::$app->request->get('rank')]);
            }
            if (Yii::$app->request->get('salary')) {
                $query = $query->andWhere('>=', 'salary_from',
                    Yii::$app->request->get('rank') * Yii::$app->request->get('period'));
            }
            if (Yii::$app->request->get('vessel')) {
                $query = $query->andWhere(['vessel_type_id' => Yii::$app->request->get('rank')]);
            }

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        return $this->render('vacancies', [
            'models' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionVacancy()
    {
        $id = Yii::$app->request->get('id');
        $model = new Vacancies();

        // add view count
        $model->addCount($id);
        //get data
        $data = $model->getOne($id);
        
        return $this->render('vacancy', [
            'data'=>$data,
        ]);
    }

    public function actionCompanies()
    {
        $model = new Companies();
        $query = new ActiveQuery(Companies::className());


        $query = $query->joinWith('user');
        $query = $query->joinWith('companyContact');
        $query = $query->andWhere(['user.registration_step' => null]);
        $query = $query->andWhere(['user.verified' => 1]);
        $query = $query->andWhere(['user.activated' => 1]);

        if (Yii::$app->request->get('type')) {
            $query = $query->andWhere(['company.type_id' => Yii::$app->request->get('type')]);
        }
        if (Yii::$app->request->get('country')) {
            $query = $query->andWhere(['company_contact.hq_country_id' => Yii::$app->request->get('country')]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        $query = $query->groupBy('user.id');
    	return $this->render('companies',[
            'model' => $model,
            'dataProvider' => $dataProvider,
    	]);
    }

    public function actionSeamen()
    {
        $model = new Seaman();
        $query = new ActiveQuery(Seaman::className());


        $query->joinWith('user user');
        $query->joinWith('service service');
        $query->joinWith('languages languages');
        $query->andWhere(['user.registration_step' => null]);
        $query->andWhere(['user.verified' => 1]);


        if (Yii::$app->request->get('rank')) {
            $query = $query->andWhere(['seaman.primary_rank_id' => Yii::$app->request->get('rank')]);
        }
        if (Yii::$app->request->get('citizenship_id')) {
            $query = $query->andWhere(['seaman.citizenship_id', Yii::$app->request->get('citizenship_id')]);
        }
        if (Yii::$app->request->get('avail')) {
            $query = $query->andWhere(['seaman.status' => Yii::$app->request->get('avail')]);
        }
        if (Yii::$app->request->get('age_from')) {
            $time = time() - (Yii::$app->request->get('seaman.age_from') * 365 * 24 * 60 * 60);
            $date = date("Y-m-d", $time);
            $query = $query->andWhere('<=', 'seaman.date_of_birth', $date);
        }
        if (Yii::$app->request->get('age_to')) {
            $time = time() - (Yii::$app->request->get('age_to') * 365 * 24 * 60 * 60);
            $date = date("Y-m-d", $time);
            $query = $query->andWhere('>=', 'seaman.date_of_birth', $date);
        }
        if (Yii::$app->request->get('sal')) {
            $query = $query->andWhere('<=', 'seaman.salary',
                Yii::$app->request->get('sal') * Yii::$app->request->get('period'));
        }
        if (Yii::$app->request->get('vessel')) {
            $query = $query->andWhere(['service.vessel_type_id' => Yii::$app->request->get('vessel')]);
        }
        if (Yii::$app->request->get('lang')) {
            $query = $query->andWhere(['languages.language_level_id' => Yii::$app->request->get('lang')]);
        }
        $query->groupBy('user.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $this->render('seamen',[
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProfile()
    {
        $uid = Profile::find()->where(['id'=>Yii::$app->request->get('id')])->one()->user_id;
        $type = User::find()->where(['id'=>$uid])->one()->account_type_id;
        if($type==User::ACCOUNT_COMPANY):
            return $this->profileCompany();
        elseif($type==User::ACCOUNT_SEAMAN):
            return $this->profileSeaman();
        else:
            throw new NotFoundHttpException('Page not found');
        endif;
    }

    protected function profileCompany()
    {
        $this->layout = 'userpage';
        $model = new Companies();
        $modvac = new Vacancies();
        $modvess = new CompanyVessel();
        $users = new Users();
        if(!Yii::$app->request->get('id')){
            return $this->goBack();
        }
        if(Yii::$app->request->get('key')) {
            $id = Yii::$app->request->get('id');
            if($id==Yii::$app->user->id&&Yii::$app->request->get('key')==Yii::$app->user->getIdentity()->auth_key){
                $users->activate($id);
            }
        }
        $uid = Profile::find()->select('user_id')->where(['id'=>Yii::$app->request->get('id')])->one()->user_id;
        $id = Companies::findOne(['user_id'=>$uid])->id;

        $data = $model->getOne($uid);

        $vac = $modvac->getByCompany($id);
        $vessels = $modvess->getVessels($id);
        //var_dump($vessels);exit;
        return $this->render('company', [
            'model' => $model,
            'data' => $data,
            'vacancies' => $vac,
            'vessels' => $vessels,
        ]);
    }

    protected function profileSeaman()
    {
        $this->layout = 'userpage';
        if(!Yii::$app->request->get('id')){
            return $this->goBack();
        }

        $model = new Seaman();
        $users = new Users();
        $uid = Yii::$app->request->get('id');
        $card_form = new CardForm();

        $uid = Profile::find()->select('user_id')->where(['id'=>Yii::$app->request->get('id')])->one()->user_id;
        $id = Seaman::findOne(['user_id'=>$uid])->id;
        $data = $model->getOne($id);
        if(Yii::$app->request->get('key')) {
        	if($id==Yii::$app->user->id&&Yii::$app->request->get('key')==Yii::$app->user->getIdentity()->auth_key){
        		$users->activate($id);
        	}
        }
        return $this->render('seaman', [
            'data' => $data,
            'card_form'=>$card_form,
        ]);
    }

    public function actionVacsearch()
    {
        $this->layout = 'empty';
        $model = new Vacancies();

        if(Yii::$app->request->get()){
            $post = Yii::$app->request->get();
            $query = $model->Filter($post);


            $model = new Vacancies();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
            ]);
            return $this->render('vacsearch',[
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionSeamansearch()
    {
        $this->layout = false;
        $model = new Seaman();


        if(Yii::$app->request->get()){

            $post = Yii::$app->request->get();
            $query = $model->Filter($post);
            $countQuery = clone $query;
            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
            $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            return $this->render('seamansearch',[
                'models'=>$models,
                'pages'=>$pages,
            ]);
        }
    }



    public function actionSfedit() //Edit main seaman family
    {
        $this->layout = 'empty';
        $model = new Family();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Family');
            $uid = Yii::$app->user->id;
            $id = Seaman::find()->select('id')->where(['user_id'=>$uid]);
            $model->editFamily($id,$post);
            return false;
        }

        return $this->render('seaman-editfamily', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSmedit() //Edit main seaman info
    {
        $model = new Seaman();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if ($model->load(Yii::$app->request->post())){
            $post=Yii::$app->request->post('Seaman');
            $cv = Yii::$app->session->get('cv');
            $photo = Yii::$app->session->get('photo');

            $bdate = explode('.', $post['date_of_birth']);
            $bdate = $bdate[2].'-'.$bdate[1].'-'.$bdate[0];

            if(!empty($post['availability_date'])){
                $adate = explode('.', $post['availability_date']);
                $adate = $adate[2].'-'.$adate[1].'-'.$adate[0];
            }

            $update_model = Seaman::findOne(['id' => $id]);
            $update_model->first_name = $post['first_name'];
            $update_model->last_name = $post['last_name'];
            $update_model->address = $post['address'];
            $update_model->primary_rank_id = $post['primary_rank_id'];
            $update_model->secondary_rank_id = $post['secondary_rank_id'];
            $update_model->status = $post['status'];
            $update_model->salary = $post['salary'];
            $update_model->salary_currency_id = $post['salary_currency_id'];
            $update_model->date_of_birth = $bdate;
            $update_model->place_of_birth_id = $post['place_of_birth_id'];
            $update_model->country_of_residence_id = $post['country_of_residence_id'];
            $update_model->city_of_residence = $post['city_of_residence'];
            $update_model->citizenship_id = $post['citizenship_id'];
            $update_model->closest_airport = $post['closest_airport'];
            $update_model->primary_phone = $post['primary_phone'];
            $update_model->skype = $post['skype'];
            $update_model->middle_name = $post['middle_name'];
            $update_model->secondary_phone = $post['secondary_phone'];
            if(isset($adate)){
                $update_model->availability_date = $adate;
            }
            $update_model->profile_picture = $photo;
            $update_model->cv_picture = $cv;


            if (!$update_model->save()) {
                $errors = [];
                foreach ($update_model->errors as $key => $value) {
                    $errors['errors'][$key] = $value;
                }
                $errors = json_encode($errors);
                return $errors;
            }

            return false;
        }

        return $this->renderAjax('seaman-editmain', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSkedit() //Edit main seaman nok
    {
        $model = new SeamanNextOfKin();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('SeamanNextOfKin');

            $update_model = SeamanNextOfKin::findOne(['seaman_id' => $id]);
            $update_model->name = $post['name'];
            $update_model->relationship_id = $post['relationship_id'];
            $update_model->phone = $post['phone'];
            $update_model->address = $post['address'];
            $update_model->save();

            return false;
        }

        return $this->renderAjax('seaman-editnok', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSbedit() //Edit main seaman biometrics
    {
        $this->layout = 'empty';
        $model = new Biometrics();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('Biometrics');
            $model->editBio($id,$post);
        }

        return $this->render('seaman-editbio', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSledit() //Edit main seaman biometrics
    {
        $this->layout = 'empty';
       	$model = new SeamanLanguage();
       	$id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanLanguage');
            $arr = [];
            foreach($post['id'] as $key => $value) {
                $arr[$key]['id'] = $value;
            }
            foreach($post['language_id'] as $key => $value) {
                $arr[$key]['language_id'] = $value;
            }
            foreach($post['language_level_id'] as $key => $value) {
                $arr[$key]['language_level_id'] = $value;

            }

            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->id]);


            foreach($arr as $item) {
                if(isset($item['id'])) {
                    $update_model = SeamanLanguage::findOne(['id' => $item['id']]);
                    $update_model->language_id = $item['language_id'];
                    $update_model->language_level_id = $item['language_level_id'];
                    $update_model->save();
                }else{
                    $model = new SeamanLanguage();
                    $model->seaman_id = $seamen->id;
                    $model->language_id = $item['language_id'];
                    $model->language_level_id = $item['language_level_id'];
                    $model->save();
                }

            }
            return false;
        }

        return $this->render('seaman-editlang', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSeduedit() //Edit main seaman biometrics
    {
        $this->layout = 'empty';
       	$model = new Education();
       	$id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('Education');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach($post['institution_name'] as $key => $value) {
                $arr[$key]['institution_name'] = $value;
            }
            foreach($post['degree'] as $key => $value) {
                $arr[$key]['degree'] = $value;
            }
            foreach($post['year_from'] as $key => $value) {
                $arr[$key]['year_from'] = $value;
            }
            foreach($post['year_to'] as $key => $value) {
                $arr[$key]['year_to'] = $value;
            }

            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {
                if(!empty($item['institution_name'])) {
                    if (isset($item['id'])) {
                        $update_model = Education::findOne(['id' => $item['id']]);
                        $update_model->institution_name = $item['institution_name'];
                        $update_model->degree = $item['degree'];
                        $update_model->year_from = $item['year_from'];
                        $update_model->year_to = $item['year_to'];
                        if (!$update_model->save()) {
                            $errors = [];
                            foreach ($update_model->errors as $key => $value) {
                                $errors['errors'][$key] = $value;
                            }
                            $errors = json_encode($errors);
                            return $errors;
                        }
                        //$update_model->save();
                    } else {
                        $model = new Education();
                        $model->seaman_id = $seamen->id;
                        $model->institution_name = $item['institution_name'];
                        $model->degree = $item['degree'];
                        $model->year_from = $item['year_from'];
                        $model->year_to = $item['year_to'];
                        if (!$model->save()) {
                            $errors = [];
                            foreach ($model->errors as $key => $value) {
                                $errors['errors'][$key] = $value;
                            }
                            $errors = json_encode($errors);
                            return $errors;
                        }
                        //$model->save();
                    }
                }
            }
            return false;
        }

        return $this->render('seaman-editedu', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionStredit() //Edit main seaman biometrics
    {
        $this->layout = 'empty';
       	$model = new SeamanTravelDocument();
       	$id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanTravelDocument');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach($post['document_id'] as $key => $value) {
                $arr[$key]['document_id'] = $value;
            }
            foreach($post['number'] as $key => $value) {
                $arr[$key]['number'] = $value;
            }
            foreach($post['place_of_issue_id'] as $key => $value) {
                $arr[$key]['place_of_issue_id'] = $value;
            }
            foreach($post['date_of_issue'] as $key => $value) {
                $issue = explode('.', $value);
                $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
                $arr[$key]['date_of_issue'] = $issue;
            }

            foreach ($post['date_of_expiry'] as $key => $value) {
                if(!empty($value)) {
                    $expiry = explode('.', $value);
                    $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
                    $arr[$key]['date_of_expiry'] = $expiry;
                }
            }


            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {
                if(!empty($item['document_id'])) {
                    if (isset($item['id'])) {
                        $update_model = SeamanTravelDocument::findOne(['id' => $item['id']]);
                        $update_model->document_id = $item['document_id'];
                        $update_model->number = $item['number'];
                        $update_model->place_of_issue_id = $item['place_of_issue_id'];
                        $update_model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $update_model->date_of_expiry = $item['date_of_expiry'];
                        }
                        if (!$update_model->save()) {
                            $errors = [];
                            foreach ($update_model->errors as $key => $value) {
                                $errors['errors'][$key] = $value;
                            }
                            $errors = json_encode($errors);
                            return $errors;
                        }
                    } else {
                        $model = new SeamanTravelDocument();
                        $model->seaman_id = $seamen->id;
                        $model->document_id = $item['document_id'];
                        $model->number = $item['number'];
                        $model->place_of_issue_id = $item['place_of_issue_id'];
                        $model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $model->date_of_expiry = $item['date_of_expiry'];
                        }
                        if (!$model->save()) {
                            $errors = [];
                            foreach ($model->errors as $key => $value) {
                                $errors['errors'][$key] = $value;
                            }
                            $errors = json_encode($errors);
                            return $errors;
                        }
                    }
                }
            }
            return false;
        }

        return $this->render('seaman-edittr', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionStravel()
    {
        $this->layout = false;

        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->render('stravel', [
            'data' => $data,
        ]);
    }

    public function actionScomedit() //Edit main seaman biometrics
    {


        $this->layout = 'empty';
        $model = new SeamanCertificateOfCompetency();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanCertificateOfCompetency');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach($post['rank_id'] as $key => $value) {
                $arr[$key]['rank_id'] = $value;
            }
            foreach($post['document_type_id'] as $key => $value) {
                $arr[$key]['document_type_id'] = $value;
            }
            foreach($post['number'] as $key => $value) {
                $arr[$key]['number'] = $value;
            }
            foreach($post['place_of_issue_id'] as $key => $value) {
                $arr[$key]['place_of_issue_id'] = $value;
            }
            foreach($post['date_of_issue'] as $key => $value) {
                $issue = explode('.', $value);
                $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
                $arr[$key]['date_of_issue'] = $issue;
            }

            foreach ($post['date_of_expiry'] as $key => $value) {
                if(!empty($value)) {
                    $expiry = explode('.', $value);
                    $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
                    $arr[$key]['date_of_expiry'] = $expiry;
                }
            }


            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {

                if(!empty($item['rank_id'])) {
                    if (isset($item['id'])) {
                        $update_model = SeamanCertificateOfCompetency::findOne(['id' => $item['id']]);
                        $update_model->rank_id = $item['rank_id'];
                        $update_model->document_type_id = $item['document_type_id'];
                        $update_model->number = $item['number'];
                        $update_model->place_of_issue_id = $item['place_of_issue_id'];
                        $update_model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $update_model->date_of_expiry = $item['date_of_expiry'];
                        }
                        $update_model->save();
                    } else {
                        $model = new SeamanCertificateOfCompetency();
                        $model->seaman_id = $seamen->id;
                        $model->rank_id = $item['rank_id'];
                        $model->document_type_id = $item['document_type_id'];
                        $model->number = $item['number'];
                        $model->place_of_issue_id = $item['place_of_issue_id'];
                        $model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $model->date_of_expiry = $item['date_of_expiry'];
                        }
                        if(!$model->save()){
                            var_dump($model->errors);
                        }
                    }
                }
            }
            return false;
        }

        return $this->render('seaman-editcom', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSseaedit() //Edit main seaman biometrics
    {
        $model = new SeamanSeaService();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanSeaService');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach($post['rank_id'] as $key => $value) {
                $arr[$key]['rank_id'] = $value;
            }
            foreach($post['vessel_name'] as $key => $value) {
                $arr[$key]['vessel_name'] = $value;
            }
            foreach($post['vessel_type_id'] as $key => $value) {
                $arr[$key]['vessel_type_id'] = $value;
            }
            foreach($post['vessel_flag_id'] as $key => $value) {
                $arr[$key]['vessel_flag_id'] = $value;
            }
            foreach($post['engine_type_id'] as $key => $value) {
                $arr[$key]['engine_type_id'] = $value;
            }
            foreach($post['engine_power'] as $key => $value) {
                $arr[$key]['engine_power'] = $value;
            }
            foreach($post['vessel_deadweight'] as $key => $value) {
                $arr[$key]['vessel_deadweight'] = $value;
            }
            foreach($post['shipowning_company_name'] as $key => $value) {
                $arr[$key]['shipowning_company_name'] = $value;
            }
            foreach($post['crewing_company_name'] as $key => $value) {
                $arr[$key]['crewing_company_name'] = $value;
            }
            foreach($post['date_from'] as $key => $value) {
                $arr[$key]['date_from'] = Yii::$app->formatter->asDate($value, 'y-m-d');
            }

            foreach ($post['date_to'] as $key => $value) {
                $arr[$key]['date_to'] = Yii::$app->formatter->asDate($value, 'y-m-d');
            }


            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {

                if(!empty($item['rank_id'])) {
                    if (isset($item['id'])) {
                        $update_model = SeamanSeaService::findOne(['id' => $item['id']]);
                        $update_model->scenario = 'update';
                        $update_model->rank_id = $item['rank_id'];
                        $update_model->vessel_name = $item['vessel_name'];
                        $update_model->vessel_type_id = $item['vessel_type_id'];
                        $update_model->vessel_flag_id = $item['vessel_flag_id'];
                        $update_model->engine_type_id = $item['engine_type_id'];
                        $update_model->vessel_deadweight = $item['vessel_deadweight'];
                        $update_model->engine_power = $item['engine_power'];
                        $update_model->shipowning_company_name = $item['shipowning_company_name'];
                        $update_model->crewing_company_name = $item['crewing_company_name'];
                        $update_model->date_from = $item['date_from'];
                        $update_model->date_to = $item['date_to'];
                        $update_model->save();
                    } else {
                        $model = new SeamanSeaService();
                        $model->scenario = 'update';
                        $model->seaman_id = $seamen->id;
                        $model->rank_id = $item['rank_id'];
                        $model->vessel_name = $item['vessel_name'];
                        $model->vessel_type_id = $item['vessel_type_id'];
                        $model->vessel_flag_id = $item['vessel_flag_id'];
                        $model->engine_type_id = $item['engine_type_id'];
                        $model->vessel_deadweight = $item['vessel_deadweight'];
                        $model->engine_power = $item['engine_power'];
                        $model->shipowning_company_name = $item['shipowning_company_name'];
                        $model->crewing_company_name = $item['crewing_company_name'];
                        $model->date_from = $item['date_from'];
                        $model->date_to = $item['date_to'];
                        $model->save();
                    }
                }
            }
            return false;
        }

        return $this->renderAjax('seaman-editsea', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSsea(){
        $this->layout = false;

        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->render('ssea', [
            'data' => $data,
        ]);
    }


    public function actionSrefedit() //Edit main seaman biometrics
    {
        $model = new SeamanReference();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanReference');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }
            foreach($post['company_name'] as $key => $value) {
                $arr[$key]['company_name'] = $value;
            }
            foreach($post['phone'] as $key => $value) {
                $arr[$key]['phone'] = $value;
            }
            foreach($post['email'] as $key => $value) {
                $arr[$key]['email'] = $value;
            }
            foreach($post['contact_person'] as $key => $value) {
                $arr[$key]['contact_person'] = $value;
            }


            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {

                if(!empty($item['company_name'])) {
                    if (isset($item['id'])) {
                        $update_model = SeamanReference::findOne(['id' => $item['id']]);
                        $update_model->company_name = $item['company_name'];
                        $update_model->phone = $item['phone'];
                        $update_model->email = $item['email'];
                        $update_model->contact_person = $item['contact_person'];
                        if (!$update_model->save()) {
                            var_dump($update_model->errors);
                        }
                    } else {
                        $model = new SeamanReference();
                        $model->seaman_id = $seamen->id;
                        $model->company_name = $item['company_name'];
                        $model->phone = $item['phone'];
                        $model->email = $item['email'];
                        $model->contact_person = $item['contact_person'];
                        $model->save();

                    }
                }
            }
            return false;
        }

        return $this->renderAjax('seaman-editref', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSref(){
        $this->layout = false;

        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('sref', [
            'data' => $data,
        ]);
    }


    public function actionSprofedit() //Edit seaman COP
    {
        $model = new SeamanCertificateOfProficiency();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanCertificateOfProficiency');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach($post['document_id'] as $key => $value) {
                $arr[$key]['document_id'] = $value;
            }
            foreach($post['number'] as $key => $value) {
                $arr[$key]['number'] = $value;
            }
            foreach($post['place_of_issue_id'] as $key => $value) {
                $arr[$key]['place_of_issue_id'] = $value;
            }
            foreach($post['date_of_issue'] as $key => $value) {
                $issue = explode('.', $value);
                $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
                $arr[$key]['date_of_issue'] = $issue;
            }

            foreach ($post['date_of_expiry'] as $key => $value) {
                if(!empty($value)) {
                    $expiry = explode('.', $value);
                    $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
                    $arr[$key]['date_of_expiry'] = $expiry;
                }
            }


            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {

                if (!empty($item['document_id'])) {
                    if (isset($item['id'])) {
                        $update_model = SeamanCertificateOfProficiency::findOne(['id' => $item['id']]);
                        $update_model->document_id = $item['document_id'];
                        $update_model->number = $item['number'];
                        $update_model->place_of_issue_id = $item['place_of_issue_id'];
                        $update_model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $update_model->date_of_expiry = $item['date_of_expiry'];
                        }
                        $update_model->save();
                    } else {
                        $model = new SeamanCertificateOfProficiency();
                        $model->seaman_id = $seamen->id;
                        $model->document_id = $item['document_id'];
                        $model->number = $item['number'];
                        $model->place_of_issue_id = $item['place_of_issue_id'];
                        $model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $model->date_of_expiry = $item['date_of_expiry'];
                        }
                        $model->save();
                    }
                }
            }
            return false;
        }

        return $this->renderAjax('seaman-editprof', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSprof(){
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('sprof', [
            'data' => $data,
        ]);
    }

    public function actionSmededit() //Edit seaman COP
    {
        $model = new SeamanMedicalDocument();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanMedicalDocument');
            $arr = [];
            if(isset($post['id'])){
                foreach($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach($post['document_id'] as $key => $value) {
                $arr[$key]['document_id'] = $value;
            }
            foreach($post['number'] as $key => $value) {
                $arr[$key]['number'] = $value;
            }
            foreach($post['place_of_issue_id'] as $key => $value) {
                $arr[$key]['place_of_issue_id'] = $value;
            }
            foreach($post['date_of_issue'] as $key => $value) {
                $issue = explode('.', $value);
                $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
                $arr[$key]['date_of_issue'] = $issue;
            }

            foreach ($post['date_of_expiry'] as $key => $value) {
                if(!empty($value)) {
                    $expiry = explode('.', $value);
                    $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
                    $arr[$key]['date_of_expiry'] = $expiry;
                }
            }


            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {
                /* @var $update_model SeamanMedicalDocument */
                if (!empty($item['document_id'])) {
                    if (isset($item['id'])) {
                        $update_model = SeamanMedicalDocument::findOne(['id' => $item['id']]);
                        $update_model->document_id = $item['document_id'];
                        $update_model->number = $item['number'];
                        $update_model->place_of_issue_id = $item['place_of_issue_id'];
                        $update_model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $update_model->date_of_expiry = $item['date_of_expiry'];
                        }
                        if (!$update_model->save()) {
                            $errors = [];
                            foreach ($update_model->errors as $key => $value) {
                                $errors['errors'][$key] = $value;
                            }
                            $errors = json_encode($errors);
                            return $errors;
                        }
                    } else {
                        $model = new SeamanMedicalDocument();
                        $model->seaman_id = $seamen->id;
                        $model->document_id = $item['document_id'];
                        $model->number = $item['number'];
                        $model->place_of_issue_id = $item['place_of_issue_id'];
                        $model->date_of_issue = $item['date_of_issue'];
                        if(!empty($item['date_of_expiry'])){
                            $model->date_of_expiry = $item['date_of_expiry'];
                        }
                        $model->save();
                    }
                }
            }
            return false;
        }

        return $this->renderAjax('seaman-editmed', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSmed(){
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('smed', [
            'data' => $data,
        ]);
    }

    public function actionScvedit()
    {
        $model = new SeamanTextForm();
        $id = Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('SeamanTextForm');
            $update_model = SeamanTextForm::findOne(['id' => $id]);
            $update_model->cv = $post['cv'];
            $update_model->save();

            return false;
        }

        return $this->renderAjax('seaman-editcv', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionScv()
    {
        $id = Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('scv', [
            'data' => $data,
        ]);
    }

    public function actionSnotesedit()
    {
        $model = new SeamanTextForm();
        $id = Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('SeamanTextForm');
            $update_model = SeamanTextForm::findOne(['id' => $id]);
            $update_model->notes = $post['notes'];
            $update_model->save();

            return false;
        }

        return $this->renderAjax('seaman-editnotes', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionSnotes()
    {
        $id = Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('snotes', [
            'data' => $data,
        ]);
    }

    public function actionScom()
    {
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('scom', [
            'data' => $data,
        ]);
    }

    public function actionSmain()
    {
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('smain', [
            'data' => $data,
        ]);
    }

    public function actionSfam()
    {
        $this->layout = 'empty';
        $model = new Seaman();
        $family = new Family();

        $id = Yii::$app->request->get('id');

        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->render('sfam', [
            'data' => $data,
        ]);
    }

    public function actionSnok()
    {
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->renderAjax('snok', [
            'data' => $data,
        ]);
    }

    public function actionSbio()
    {
        $this->layout = 'empty';
        

        $model = new Seaman();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->render('sbio', [
            'data' => $data,
        ]);
    }

    public function actionSlang()
    {
        $this->layout = false;


        $model = new SeamanLanguage();
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->render('slang', [
            'data' => $data,
        ]);
    }

    public function actionSedu()
    {
        $this->layout = false;

        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
        $data = Seaman::getOne($id);

        return $this->render('sedu', [
            'data' => $data,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionPass(){
        var_dump(Yii::$app);die();
    }
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('request-password-reset', [
            'model' => $model,
        ]);
    }



    public function actionDocscans()
    {
        $this->layout = 'empty';
        $model = new UserFile();
        $id = Seaman::find()->select('id')->where(['user_id'=>Yii::$app->user->id])->one()->id;

        $travel_scans = \app\models\SeamanTravelDocument::find()->where(['!=','scan_id',''])->andWhere(['seaman_id'=>$id])->all();
        $coc_scans = SeamanCertificateOfCompetency::find()->where(['!=','scan_id','NULL'])->andWhere(['seaman_id'=>$id])->all();

        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('UserFile');

            foreach($post['doc_id'] as $key => $item){
                //var_dump($post['doc_id']);die;
                if(($data = UserFile::findOne($post['id'][$key])) != null){
                    if(!empty($post['size'][$key])) {
                        $file = json_decode($post['size'][$key]);
                        $data->size = $file->size;
                        $data->src = $file->name;
                        $data->save();
                    }
                }else{
                    $new = new UserFile();
                    if(!empty($post['size'][$key])){

                        $file = json_decode($post['size'][$key]);
                        $new->owner_id = Yii::$app->user->id;
                        $new->size = $file->size;
                        $new->src = $file->name;
                        if ($new->save(false)) {
                            $seaman_doc = new SeamanDocumentScan();
                            $seaman_doc->seaman_id = Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
                            $seaman_doc->scan_file_id = $new->id;
                            if ($seaman_doc->save(false)) {
                                $doc_id = explode('-', $post['doc_id'][$key]);
                                if ($doc_id[0] == 1) {
                                    $doc = SeamanTravelDocument::findOne($doc_id[1]);
                                    $doc->scan_id = $seaman_doc->id;
                                    $doc->save();
                                } elseif ($doc_id[0] == 2) {

                                    $doc = SeamanCertificateOfCompetency::findOne($doc_id[1]);
                                    $doc->scan_id = $seaman_doc->id;
                                    $doc->save();
                                }
                            }else{
                                var_dump($seaman_doc->errors);die;
                            }
                        }else{
                            var_dump($new->errors);die;
                        }
                    }else{
                        var_dump($file);die;
                    }
                }
            }

            return false;


        }

        return $this->render('docscans',[
            'travel_scans'=>$travel_scans,
            'coc_scans'=>$coc_scans,
            'model'=>$model,
        ]);
    }

    public function actionSeamanScans()
    {
        $model = new Seaman();
        $users = new Users();
        //$uid = Yii::$app->request->get('id');

        //$uid = Profile::find()->select('user_id')->where(['id'=>Yii::$app->user->id])->one()->user_id;
        $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;

        $data = $model->getOne($id);

        if(Yii::$app->request->get('key')) {
            if($id==Yii::$app->user->id&&Yii::$app->request->get('key')==Yii::$app->user->getIdentity()->auth_key){
                $users->activate($id);
            }
        }
        return $this->renderAjax('seaman/_scans', [
            'data' => $data,
        ]);
    }

    public function actionFeedback()
    {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->getId();
            $model->likes = '0';
            $model->save();
            $model = new Feedback();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Feedback::find(),
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        return $this->render('feedback', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionFeedbackdelete()
    {
        if (($model = Feedback::findOne(Yii::$app->request->post('id'))) !== null) {
            return $model->delete();
        }
    }

    public function actionFeedbackedit()
    {
        if (Yii::$app->request->isPost) {
            $model = Feedback::findOne(Yii::$app->request->post('id'));
            $model->text = Yii::$app->request->post('text');
            $model->save();
            $this->redirect(['feedback']);
        }
    }

    public function actionTerms()
    {
        $terms=TermsOfService::find()->orderBy(['position'=>SORT_ASC])->all();
        $upd = '0000-00-00';
        if(!empty($terms)){
            $upd = TermsOfService::find()->orderBy(['updated_at'=>SORT_DESC])->one()->updated_at;
        }

        return $this->render('terms',[
            'data'=>$terms,
            'upd'=>$upd,
        ]);
    }

    public function actionPrivacy()
    {
        $terms=PrivacyPolicy::find()->orderBy(['position'=>SORT_ASC])->all();
        $upd = '0000-00-00';
        if(!empty($terms)){
            $upd = PrivacyPolicy::find()->orderBy(['updated_at'=>SORT_DESC])->one()->updated_at;
        }
        return $this->render('terms',[
            'data'=>$terms,
            'upd'=>$upd,
        ]);
    }

    public function actionLoadvessel($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = CompanyVessel::findOne($id);
        return $data;

    }

    public function actionDownloadcv()
    {
        $model = new Seaman();
        //$uid = Profile::find()->select('user_id')->where(['id' => Yii::$app->user->id])->one()->user_id;

        $id = Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
        $data = $model->getOne($id);
        $content = $this->renderPartial('_seaman-pdf',[
            'data'=>$data,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'format'=> Pdf::FORMAT_A4,
            'destination'=>Pdf::DEST_DOWNLOAD,
            'cssFile'=>Yii::$app->basePath.'/web/css/pdf.css',
            'filename'=>Yii::$app->name.' - '.$data->first_name.' '.$data->last_name,
            'options' => [
                'title' => $data->first_name.' '.$data->last_name
            ],
            'methods' => [
                'SetHeader' => ['Generated By: '.Yii::$app->name.' ||Generated On: ' . date("F d, Y")],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }

    public function actionRowScan(){
        $model = new UserFile();
        return $this->renderAjax('row-scan',[
            'model'=>$model,
        ]);
    }

    public function actionFaq()
    {
        $this->layout = 'faq';
        $query = new ActiveQuery(Faq::className());
        $cat = null;
        if (Yii::$app->request->get('alias')) {
            $cat = FaqCategory::findOne(['alias' => Yii::$app->request->get('alias')]);
            $query = $query->andWhere(['faq_category_id' => $cat->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);


        return $this->render('faq',[
            'dataProvider' => $dataProvider,
            'cat' => $cat,
        ]);
    }

    public function actionStats()
    {

    }

    public function actionGetCard()
    {
        $model = new CardForm();
            $post = Yii::$app->request->post('code');
        VarDumper::dump(Yii::$app->request->post('code'));
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'content' => $post,
                'format' => Pdf::FORMAT_A4,
                'destination' => Pdf::DEST_DOWNLOAD,
                //'cssFile' => Yii::$app->basePath . '/web/css/pdf.css',
                'filename' => Yii::$app->name . ' - Business card',
                'options' => [

                ],
                'methods' => [
                    'SetHeader' => ['Generated By: ' . Yii::$app->name . ' ||Generated On: ' . date("F d, Y")],
                    'SetFooter' => ['|Page {PAGENO}|'],
                ]
            ]);
            return $pdf->render();
    }

}
