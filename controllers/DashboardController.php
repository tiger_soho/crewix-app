<?php
namespace app\controllers;

use app\models\ChangeEmail;
use app\models\ChangePassword;
use app\models\Companies;
use app\models\CompanyContact;
use app\models\Profile;
use app\models\Seaman;
use app\models\SeamanCertificateOfCompetency;
use app\models\SeamanCertificateOfProficiency;
use app\models\SeamanDocumentScan;
use app\models\SeamanMedicalDocument;
use app\models\SeamanTravelDocument;
use app\models\TermsForm;
use app\models\TermsOfService;
use app\models\TravelDocument;
use app\models\User;
use app\models\UserFile;
use Yii;
use app\models\Users;
use app\models\Competency;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\ShareSeamanForm;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use app\models\ComposeForm;

/**
 * Site controller
 */
class DashboardController extends Controller
{
    public function init()
    {

        parent::init();
        if (!Yii::$app->user->isGuest) {
            $user = Users::findOne(Yii::$app->user->id);
            $user->last_seen = date('Y-m-d H:i:s');
            $user->save(false, ["last_seen"]);
        }
    }
	 public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
    	$this->layout="dashboard";

    	if(Yii::$app->user->isGuest){
    		return $this->goHome();
    	}
        $labels = [];
        if (Yii::$app->request->get('id') == 2) {
            $data = Users::getChartDataMonth();
            $m = date('m');
            $y = date('Y');
            $days_count = cal_days_in_month(CAL_GREGORIAN, $m, $y);

            for ($i = 1; $i <= $days_count; $i++) {
                $labels[] = $i . '.' . $m;
            }
        } elseif (Yii::$app->request->get('id') == 3) {
            $data = Users::getChartDataYear();
            $labels = [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'Juli',
                'August',
                'September',
                'October',
                'November',
                'December'
            ];
        } elseif (Yii::$app->request->get('id') == 4) {
            $data = Users::getChartDataAll();
            foreach ($data as $item) {
                $labels[] = $item['pick'];
            }
        } else {
            $data = Users::getChartDataWeek();
            $labels = ['Monday', 'Tuesday', 'Wednesday', 'Thursdays', 'Friday', 'Saturday', 'Sunday'];
        }

        return $this->render('index', [
            'data'=>$data,
            'labels' => $labels
        ]);

    }

    public function actionShare()
    {
    	$this->layout="dashboard";
    	$model = new ShareSeamanForm();
        $model_s = new Companies();
    	
    	return $this->render('share',[
    		'model'=>$model,
            'model_s'=>$model_s,
    	]);
    }

    public function actionExport()
    {
        $this->layout = "dashboard";
        $model = new ShareSeamanForm();

        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('ShareSeamanForm');
            $link = str_replace('http://','',$post['link']);
            $link = explode('/',$link);
            $n = count($link);
            $pid = str_replace('id','',$link[$n-1]);
            if (($profile = Profile::findOne($pid)) != null) {
                if (($user = Users::findOne($profile->user_id)) != null && ($user->role == 'seaman')) {
                    $id = $user->id;
                    $data = Seaman::findOne(['user_id'=>$id]);
                    //VarDumper::dump($id);die;
                    $content = $this->renderPartial('_seaman-pdf',[
                        'post'=>$post,
                        'data'=>$data,
                    ]);

                    $pdf = new Pdf([
                        'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                        'content' => $content,
                        'format'=> Pdf::FORMAT_A4,
                        'destination'=>Pdf::DEST_DOWNLOAD,
                        'cssFile'=>Yii::$app->basePath.'/web/css/pdf.css',
                        'filename'=>Yii::$app->name.' - '.$data->first_name.' '.$data->last_name,
                        'options' => [
                            'title' => $data->first_name.' '.$data->last_name
                        ],
                        'methods' => [
                            'SetHeader' => ['Generated By: '.Yii::$app->name.' ||Generated On: ' . date("F d, Y")],
                            'SetFooter' => ['|Page {PAGENO}|'],
                        ]
                    ]);
                    return $pdf->render();
                } else {
                    Yii::$app->session->setFlash('error', 'This user is Company');
                }
            }else{
                Yii::$app->session->setFlash('error', 'This user does not exist');
            }
        }

        return $this->render('seaman-export', [
            'model' => $model,
        ]);
    }


    public function actionLoadseaman()
    {
        $link = str_replace('http://','',Yii::$app->request->get('id'));
        $link = explode('/',$link);
        $n = count($link);
        $pid = str_replace('id','',$link[$n-1]);
        $id = Profile::findOne($pid)->user_id;
        $seaman = Seaman::findOne(['user_id'=>$id])->id;
        $scans = SeamanDocumentScan::findAll(['seaman_id'=>$seaman]);
        $docs = [];
        foreach($scans as $item){
            $coc = SeamanCertificateOfCompetency::getAllByScan($item->id);
            foreach($coc as $c){
                $docs[] = $c;
            }
            $tr = SeamanTravelDocument::getAllByScan($item->id);
            foreach($tr as $t){
                $docs[] = $t;
            }
            $med = SeamanMedicalDocument::getAllByScan($item->id);
            foreach($med as $m){
                $docs[] = $m;
            }
            $prof = SeamanCertificateOfProficiency::getAllByScan($item->id);
            foreach($prof as $p){
                $docs[] = $p;
            }
        }
        if(!empty($docs)) {
            $html = '';
            foreach ($docs as $item) {
                $html .= '<option value="' . $item->scan_id . '">' . $item->docType['name'] . '</option>';
            }
            echo $html;
        }
    }

    public function actionMails()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = CompanyContact::find()->select('email')->andFilterWhere(['like', 'email', Yii::$app->request->get('term')])->all();
        $arr = ArrayHelper::map($data,'email','email');
        return $arr;
    }

    public function actionTerms()
    {
        $this->layout="dashboard";
        $model = new TermsOfService();
        $data = $model->find()->orderBy(['position'=>SORT_ASC])->all();
        return $this->render('terms',[
            'model'=>$model,
            'data'=>$data
        ]);
    }

    public function actionTermsform()
    {
        $model = new TermsOfService();
        $data = '';
        if($model->load(Yii::$app->request->get('id'))){
           $data = $model->findOne(Yii::$app->request->get('id'));
        }
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('TermsOfService');
            if(!empty($post['id'])){
                $updete_model = TermsOfService::findOne($post['id']);
                $updete_model->section_name = $post['section_name'];
                $updete_model->text = $post['text'];
                $updete_model->save();
            }else{
                $model = new TermsOfService();
                $model->section_name = $post['section_name'];
                $model->text = $post['text'];
                $model->save();
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        return $this->renderAjax('terms-form',[
            'model'=>$model,
            'data'=>$data
        ]);
    }

    public function actionSavetermorder()
    {
        $model = new TermsForm();
        $data = Yii::$app->request->get();
        $model->updatePosition($data);
    }

    public function actionGetDocs($id)
    {
        $uid = Profile::findOne($id)->user_id;
        $model = Seaman::findOne(['user_id' => $uid]);
        $options = '';
        $data_key = ['competency', 'travelDocs'];
        foreach ($data_key as $keyname) {
            $keyitem = $model->{$keyname};
            if (is_array($keyitem)) {
                foreach ($keyitem as $item) {
                    if (!empty($item->scan_id)) {
                        $options .= '<option value="' . $item->id . '-' . $item->scan_id . '">' . $item->docType->name . '</option>';
                    }
                }
            } else {
                echo $keyitem->scan_id;
                echo "<br>";
            }
            echo $options;
        }
    }
    public function actionParticulars()
    {
        $this->layout = "dashboard";
        $model = new Seaman();

        $query = new ActiveQuery(Seaman::className());
        $error = null;
        if(Yii::$app->request->get('link')){
            $link = str_replace('http://','',Yii::$app->request->get('link'));
            $link = explode('/',$link);
            $n = count($link);
            $pid = str_replace('id','',$link[$n-1]);
            if (($profile = Profile::findOne($pid)) != null) {
                if (($user = Users::findOne($profile->user_id)) != null && ($user->role == 'seaman')) {
                    $query = $query->andWhere(['user_id' => $profile->user_id]);
                } else {
                    Yii::$app->session->setFlash('error', 'It\'s company');
                    $query = $query->andWhere(['id' => null]);
                }
            }else{
                Yii::$app->session->setFlash('error', 'User id invalid or not exist!');
                $query = $query->andWhere(['id'=>null]);
            }
        }else{
            $query = $query->andWhere(['id'=>null]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

        ]);
        return $this->render('particulars', [
            'model' => $model,
            'dataProvider'=>$dataProvider,
            'error' => $error
        ]);
    }


    /***
     * Methods use in dashboard/users
     */

    /**
     * @return string
     */
    public function actionUsers(){
        $this->layout = 'dashboard';

        $dataProvider_users = new ActiveDataProvider([
            'query' => Users::find()->andWhere(['registration_step' => null]),
            'pagination' => [
                'pageSize' => 100
            ],
            'sort' => ['defaultOrder' => ['signup_date' => SORT_ASC]]
        ]);

        $dataProvider_companies = new ActiveDataProvider([
            'query' => Users::find()->andWhere(['role'=>'company','registration_step'=>null]),
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => ['defaultOrder' => ['signup_date' => SORT_ASC]]
        ]);

        return $this->render('users', [
            'dataProvider_companies' => $dataProvider_companies,
            'dataProvider_users' => $dataProvider_users,
            'changePassword' => new ChangePassword(),
            'changeEmail' => new ChangeEmail(),
            'model' => new Users(),
        ]);
    }

    /**
     * @param $id
     */
    public function actionVerifyAccount($id)
    {
        if(($model = Users::findOne($id)) != null) {
            $model->verified = 1;
            $model->save(false);
        }
    }

    /**
     * @param $id
     *
     * @throws \Exception
     */
    public function actionDeleteAccount()
    {
        if(Yii::$app->request->isPost) {
            $id = Yii::$app->request->post('id');
            Users::findOne($id)->delete();
        };

        $this->redirect(['/dashboard/users']);
    }

    /**
     * @param $id
     */
    public function actionBannedAccount()
    {
        if(Yii::$app->request->isPost) {
            $id = Yii::$app->request->post('id');
            if(($model = Users::findOne($id)) != null) {
                $model->banned = 1;
                $model->save(false);
            }
        }

    }

    /**
     * @param $id
     *
     * @return string
     */
    public function actionInfoAccount($id)
    {
      //  Yii::$app->response = Response::FORMAT_HTML;
        return $this->renderAjax('users/_info', [
            'model' => Users::findOne($id),
        ]);
    }

    public function actionEditAccount()
    {
        if(Yii::$app->request->isPost) {
            $id = Yii::$app->request->post('id');
            if(($model = Users::findOne($id)) != null) {

                $changePassword = new ChangePassword();
                $changeEmail = new ChangeEmail();


                $changeEmail->id = $id;
                $changePassword->id = $id;

                if(Yii::$app->request->post('account_type_id')) {
                    $model->uesr_account_type_id = Yii::$app->request->post('account_type_id');
                }

                if($changePassword->load(Yii::$app->request->post())) {
                    if($changePassword->change()) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return 'ok';
                    }
                    else{
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return $changePassword->getErrors();
                    }
                }
                if($changeEmail->load(Yii::$app->request->post())) {
                    $key = $changeEmail->change();
                    if($key) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $url = Yii::$app->urlManager->createAbsoluteUrl(['/settings/security-verify-email','id'=> $changeEmail->id,'key'=>$key]);

                        $email = \Yii::$app->mailer->compose()
                            ->setTo(Yii::$app->user->getIdentity()->email)
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' no reply'])
                            ->setSubject('Signup Confirmation')
                            ->setHtmlBody("Click this link " . \yii\helpers\Html::a('change you e-mail', $url));


                        $mailer = Yii::$app->get('mailer');
                        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
                        $mailer->getSwiftMailer()->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

                        if (!$email->send()) {
                            echo $logger->dump();
                        }

                        return 'ok';
                    }
                    else{
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return $changeEmail->getErrors();
                    }
                }
            }
            else {
                throw new InvalidParamException;
            }

        }

    }

    /**
     * end dashboard/users
     */

    public function actionCompose()
    {
        $this->layout = 'dashboard';
        $model = new ComposeForm();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('ComposeForm');
            $from = $post->from . '@crewix.com';

            $mailer = \Yii::$app->mailer->compose(['html' => 'Compose-html'], ['message' => $post->message])
                ->setFrom('$from')
                ->setTo($post->to)
                ->setSubject($post->subject);

            if (!empty($post->files)) {
                foreach ($post->files as $file) {
                    $mailer->attach($file);
                }
            }

            return $mailer->send();
        }

        return $this->render('compose', [
            'model' => $model,
        ]);
    }

    public function actionUploadAttachment()
    {
        if (!empty($_FILES['src'])) {
            if ($_FILES['src']['type'] == 'image/jpeg' || $_FILES['src']['type'] == 'image/png' || $_FILES['src']['type'] == 'image/gif' || $_FILES['src']['type'] == 'image/jpg') {
                $icon = 'fa fa-file-image-o';
            } elseif ($_FILES['src']['type'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || $_FILES['src']['type'] == 'application/msword') {
                $icon = 'fa fa-file-word-o';
            } elseif ($_FILES['src']['type'] == 'application/pdf') {
                $icon = 'fa fa-file-pdf-o';
            } elseif ($_FILES['src']['type'] == 'application/vnd.ms-powerpoint' || $_FILES['src']['type'] == 'application/application/vnd.openxmlformats-officedocument.presentationml.presentation') {
                $icon = 'fa fa-file-powerpoint-o';
            } elseif ($_FILES['src']['type'] == 'application/vnd.ms-excel' || $_FILES['src']['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                $icon = 'fa fa-file-excel-o';
            } elseif ($_FILES['src']['type'] == 'application/zip'|| $_FILES['src']['type'] == 'application/x-compressed-zip' || $_FILES['src']['type'] == 'application/x-zip-compressed' || $_FILES['src']['type'] == 'application/x-rar-compressed' || $_FILES['src']['type'] == 'application/octet-stream') {
                $icon = 'fa fa-file-archive-o';
            } else {
                $icon = 'fa fa-file-o';
            }
            $path = $_SERVER['DOCUMENT_ROOT'] . '/images/compose_tmp/';
            $ava = uniqid('', true) . '_' . $_FILES['src']['name'];
            move_uploaded_file($_FILES['src']['tmp_name'], $path . $ava);
            $response = ['file' => $ava, 'ico' => $icon];
            $response = json_encode($response);
            return $response;
        }
    }
}