<?php
namespace app\controllers;

use app\models\Currency;
use app\models\Engines;
use app\models\Faq;
use app\models\FaqCategory;
use app\models\Langs;
use app\models\Languages;
use app\models\PrivacyPolicy;
use app\models\RankGroup;
use app\models\Ranks;
use app\models\TermsForm;
use app\models\TermsOfService;
use app\models\TravelDocument;
use app\models\Users;
use app\models\VarsForm;
use app\models\VesselTypeGroup;
use app\models\VesselTypes;
use Yii;
use app\models\Competency;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessRule;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use app\models\CurrencyRate;
use app\models\Page;
use yii\db\ActiveRecord;

/**
 * Site controller
 */
class AppsettingsController extends Controller
{
    public function init()
    {

        parent::init();
        if (!Yii::$app->user->isGuest) {
            $user = Users::findOne(Yii::$app->user->id);
            $user->last_seen = date('Y-m-d H:i:s');
            $user->save(false, ["last_seen"]);
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $this->layout = "dashboard";
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return $this->redirect('/appsettings/terms');
    }

    public function actionTerms()
    {
        $this->layout = "dashboard";
        $model = new TermsOfService();
        $data = $model->find()->orderBy(['position' => SORT_ASC])->all();
        return $this->render('terms', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionTermsform()
    {
        $model = new TermsOfService();
        $data = '';
        if (!empty(Yii::$app->request->get('id'))) {
            $data = $model->findOne(Yii::$app->request->get('id'));
        }
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('TermsOfService');
            if (!empty($post['id'])) {
                $updete_model = TermsOfService::findOne($post['id']);
                $updete_model->section_name = $post['section_name'];
                $updete_model->text = $post['text'];
                $updete_model->save();
            } else {
                $model = new TermsOfService();
                $model->section_name = $post['section_name'];
                $model->text = $post['text'];
                $model->save();
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        return $this->renderAjax('terms-form', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionTermsData()
    {
        $model = new TermsOfService();
        $data = $model->find()->orderBy(['position' => SORT_ASC])->all();
        return $this->renderAjax('terms-data', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionTermDelete($id)
    {
        $model = TermsOfService::findOne($id);
        $model->delete();
        return false;
    }

    public function actionPrivacy()
    {
        $this->layout = "dashboard";
        $model = new PrivacyPolicy();
        $data = $model->find()->orderBy(['position' => SORT_ASC])->all();
        return $this->render('privacy', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionPrivacyform()
    {
        $model = new PrivacyPolicy();
        $data = '';
        if (!empty(Yii::$app->request->get('id'))) {
            $data = $model->findOne(Yii::$app->request->get('id'));
        }
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('PrivacyPolicy');
            if (!empty($post['id'])) {
                $updete_model = PrivacyPolicy::findOne($post['id']);
                $updete_model->section_name = $post['section_name'];
                $updete_model->text = $post['text'];
                $updete_model->save();
            } else {
                $model = new PrivacyPolicy();
                $model->section_name = $post['section_name'];
                $model->text = $post['text'];
                $model->save();
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        return $this->renderAjax('privacy-form', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionPrivacyData()
    {
        $model = new PrivacyPolicy();
        $data = $model->find()->orderBy(['position' => SORT_ASC])->all();
        return $this->renderAjax('privacy-data', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionPrivacyDelete($id)
    {
        $model = PrivacyPolicy::findOne($id);
        $model->delete();
        return false;
    }

    public function actionSavetermorder()
    {
        $model = new TermsForm();
        $data = Yii::$app->request->get();
        $model->updatePosition($data);
    }

    public function actionSaveprivacyorder()
    {
        $model = new PrivacyPolicy();
        $data = Yii::$app->request->get();
        $model->updatePosition($data);
    }

    public function actionVariables()
    {
        $this->layout = 'dashboard';

        $model = new CurrencyRate();
        $meta_model = new Page();

        $curr = $model->find()->all();
        $meta = $meta_model->find()->all();

        return $this->render('variables', [
            'currencies' => $curr,
            'meta' => $meta
        ]);
    }

    public function actionVarsSave()
    {
        $model = new VarsForm();
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('VarsForm');
            $arr = [];
            if (isset($post['id'])) {
                foreach ($post['id'] as $key => $value) {
                    $arr[$key]['id'] = $value;
                }
            }

            foreach ($post['value'] as $key => $value) {
                $arr[$key]['value'] = $value;
            }
            foreach ($post['meta_description'] as $key => $value) {
                $arr[$key]['meta_description'] = $value;
            }
            foreach ($post['type'] as $key => $value) {
                $arr[$key]['type'] = $value;
            }


            foreach ($arr as $item) {
                if ($item['type'] == '1') {
                    if (!empty($item['value'])) {
                        $model = CurrencyRate::findOne($item['id']);
                        $model->value = $item['value'];
                        $model->save();
                    }
                } else {
                    if (!empty($item['meta_description'])) {
                        $model = Page::findOne($item['id']);
                        $model->meta_description = $item['meta_description'];
                        $model->save();
                    }
                }
            }
        }
        $model1 = new CurrencyRate();
        $meta_model = new Page();

        $curr = $model1->find()->all();
        $meta = $meta_model->find()->all();
        return $this->renderAjax('variables/_content', [
            'currencies' => $curr,
            'meta' => $meta
        ]);
    }

    public function actionVarsForm()
    {
        $model = new VarsForm();
        $model1 = new CurrencyRate();
        $meta_model = new Page();

        $curr = $model1->find()->all();
        $meta = $meta_model->find()->all();
        return $this->renderAjax('variables/_form', [
            'currencies' => $curr,
            'meta' => $meta,
            'model' => $model
        ]);
    }

    /***************************DROPDOWNS*************************/

    public function actionDropdowns($edit)
    {
        if ($edit == 'languages') {
            return $this->listLanguages();
        } elseif ($edit == 'engines') {
            return $this->listEngines();
        } elseif ($edit == 'travel_docs') {
            return $this->listTravelDocuments();
        } elseif ($edit == 'ranks') {
            return $this->listRanks();
        } elseif ($edit == 'vessels') {
            return $this->listVessels();
        }
    }

    /*
     *  LANGUAGES
     * */

    public function listLanguages()
    {
        $this->layout = 'dashboard';
        $model = new Langs();
        $data = Langs::find()->all();


        return $this->render('languages', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionLanguagesForm()
    {
        $model = new Langs();
        $data = Langs::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Langs');

            foreach ($post['name'] as $key => $item) {
                if (!empty($item)) {
                    if (($modelupdate = Langs::findOne($post['id'][$key])) != null) {
                        $modelupdate->name = $item;
                        $modelupdate->save(false);
                    } else {
                        $modelnew = new Langs();
                        $modelnew->name = $item;
                        $modelnew->save();
                    }
                }
            }

            return false;

        }

        return $this->renderAjax('languages/languages-form', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionLangBlock()
    {
        $model = new Langs();
        $data = Langs::find()->all();

        return $this->renderAjax('languages/lang-block', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /*
     * END LANFUAGES
     * */

    /*
     * VESSEL ENGINES
     * */

    public function listEngines()
    {
        $this->layout = 'dashboard';
        $model = new Engines();
        $data = Engines::find()->all();


        return $this->render('engines', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionEnginesForm()
    {
        $model = new Engines();
        $data = Engines::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Engines');

            foreach ($post['name'] as $key => $item) {
                if (!empty($item)) {
                    if (($modelupdate = Engines::findOne($post['id'][$key])) != null) {
                        $modelupdate->name = $item;
                        if (!$modelupdate->save()) {
                            return json_encode($modelupdate->errors);
                        }
                    } else {
                        $modelnew = new Engines();
                        $modelnew->name = $item;
                        if (!$modelnew->save()) {
                            return json_encode($modelnew->errors);
                        }
                        //$modelnew->save();
                    }
                }
            }

            return false;

        }

        return $this->renderAjax('engines/engines-form', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionEnginesBlock()
    {
        $model = new Engines();
        $data = Engines::find()->all();

        return $this->renderAjax('engines/engines-block', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /*
     * END ENGINES
     * */

    /*
     * TRAVEL DOCUMENTS
     * */

    public function listTravelDocuments()
    {
        $this->layout = 'dashboard';
        $model = new TravelDocument();
        $data = $model->find()->orderBy(['position' => SORT_ASC])->all();

        return $this->render('travel-docs', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionTravelForm()
    {
        $model = new TravelDocument();
        $data = TravelDocument::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('TravelDocument');

            $arr = [];
            foreach ($post['id'] as $key => $value) {
                $arr[$key]['id'] = $value;
            }

            foreach ($post['name'] as $key => $value) {
                $arr[$key]['name'] = $value;
            }
            foreach ($post['always_visible'] as $key => $value) {
                $arr[$key]['always_visible'] = $value;
            }


            foreach ($arr as $item) {
                /* @var $modelupdate \app\models\TravelDocument */
                if (($modelupdate = TravelDocument::findOne($item['id'])) != null) {
                    if (!empty($item['name'])) {
                        $modelupdate->name = $item['name'];
                    }
                    $modelupdate->always_visible = $item['always_visible'];
                    if (!$modelupdate->save()) {
                        return json_encode($modelupdate->errors);
                    }
                } else {
                    $modelnew = new TravelDocument();
                    $modelnew->name = $item['name'];
                    $model->always_visible = $item['always_visible'];
                    if (!$modelnew->save()) {
                        return json_encode($modelnew->errors);
                    }
                }
            }
            return false;
        }

        return $this->renderAjax('travel_docs/travel-form', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionTravelBlock()
    {
        $this->layout = 'dashboard';
        $model = new TravelDocument();
        $data = $model->find()->orderBy(['position' => SORT_ASC])->all();

        return $this->renderAjax('travel_docs/travel-block', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionDeleteDoc($id)
    {
        /* @var $model \app\models\TravelDocument */
        if (($model = TravelDocument::findOne($id)) != null) {
            $model->delete();
        }
        return false;
    }

    /*
     * END TRAVEL DOCUMENT
     * */

    /*
     * RANKS
     * */

    public function listRanks()
    {
        $this->layout = 'dashboard';
        $model = new Ranks();
        $group_model = new RankGroup();

        $groups = $group_model->find()->all();
        $data = $model->find()->all();

        return $this->render('ranks', [
            'groups' => $groups,
            'data' => $data
        ]);

    }

    /*
     * END RANKS
     * */

    /*
     * VESSEL TYPES
     * */

    public function listVessels()
    {
        $this->layout = 'dashboard';
        $model = new VesselTypes();
        $vessel_group = new VesselTypeGroup();

        $groups = $vessel_group->find()->orderBy(['position' => SORT_ASC])->all();
        $data = $model->find()->orderBy(['name' => SORT_ASC])->all();

        return $this->render('vessels', [
            'data' => $data,
            'groups' => $groups,
        ]);
    }

    public function actionAddVesselGroup()
    {
        $model = new VesselTypeGroup();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('VesselTypeGroup');
            $model = new VesselTypeGroup();
            $model->name = $post['name'];

            if (!$model->save()) {
                //var_dump($model->errors);die;
            } else {
                $model = new VesselTypes();
                $vessel_group = new VesselTypeGroup();

                $groups = $vessel_group->find()->orderBy(['position' => SORT_ASC])->all();
                $data = $model->find()->orderBy(['name' => SORT_ASC])->all();
                return $this->renderAjax('vessels/_data', [
                    'data' => $data,
                    'groups' => $groups,
                ]);
            }

            //$model->save();

            //return false;
        }

        return $this->renderAjax('vessels/_vessel_group-form', [
            'model' => $model,
        ]);
    }

    public function actionVesselsdata()
    {
        $model = new VesselTypes();
        $vessel_group = new VesselTypeGroup();

        $groups = $vessel_group->find()->orderBy(['position' => SORT_ASC])->all();
        $data = $model->find()->orderBy(['name' => SORT_ASC])->all();
        return $this->renderAjax('vessels/_data', [
            'data' => $data,
            'groups' => $groups,
        ]);
    }

    public function actionVgroupEdit($id = null)
    {
        $model = new VesselTypes();
        $groups = new VesselTypeGroup();

        $group = $groups->findOne($id);
        $data = $model->find()->where(['group_id' => $id])->orderBy(['name' => SORT_ASC])->all();

        if ($model->load(Yii::$app->request->post())) {
            $post_group = Yii::$app->request->post('VesselTypeGroup');
            $post = Yii::$app->request->post('VesselTypes');

            if (!empty($post_group['name'])) {
                $group_upd = VesselTypeGroup::findOne($post_group['id']);
                $group_upd->name = $post_group['name'];
                $group_upd->save();
            }

            if (!empty($post)) {
                foreach ($post['id'] as $key => $item) {

                    $upd = VesselTypes::findOne($post['id'][$key]);
                    if (!empty($post['name'][$key])) {
                        $upd->name = $post['name'][$key];
                    }
                    $upd->group_id = $post['group_id'][$key];
                    $upd->save();
                }
            }
            return false;
        }

        return $this->renderAjax('vessels/_edit-group', [
            'model' => $model,
            'groups' => $groups,
            'group' => $group,
            'data' => $data,
        ]);

    }

    /*
     * END VESSEL TYPES
     * */


    /***********************END DROPDOWNS************************/


    /***********************FAQ***************************/

    public function actionFaq($edit)
    {
        if ($edit == 'categories') {
            return $this->listCategories();
        } elseif ($edit == 'qa') {
            return $this->listQa();
        }
    }

    public function listCategories()
    {
        $this->layout = 'dashboard';
        $model = new FaqCategory();
        $data = $model->find()->all();

        return $this->render('faq-cats', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionEditCat($id = null)
    {
        $model = new FaqCategory();
        $data = $model->findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('FaqCategory');
            /* @var $update \app\models\FaqCategory */
            /* @var $new \app\models\FaqCategory */
            if (isset($post['id'])) {

                $update = FaqCategory::findOne($post['id']);
                $update->name = $post['name'];
                $update->icon = $post['icon'];
                $update->url = $post['url'];
                $update->save();
            } else {
                $new = new FaqCategory();
                $new->name = $post['name'];
                $new->icon = $post['icon'];
                $new->url = $post['url'];
                $new->save();
            }
            return false;
        }

        return $this->renderAjax('faq/categories-form', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionCatList()
    {
        $model = new FaqCategory();
        $data = $model->find()->all();

        return $this->renderAjax('faq/categories-list', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionDelCat($id)
    {
        /* @var $model \app\models\FaqCategory */
        $model = FaqCategory::findOne($id);
        $model->delete();
    }

    public function listQa()
    {
        $this->layout = 'dashboard';
        $model = new Faq();
        $data = $model->find()->all();

        $modelcat = new FaqCategory();
        $cats = $modelcat->find()->all();

        return $this->render('faq-qa', [
            'model' => $model,
            'data' => $data,
            'cats' => $cats
        ]);
    }

    public function actionEditQa($id = null)
    {
        $model = new Faq();
        $data = $model->findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Faq');
            /* @var $update \app\models\Faq */
            /* @var $new \app\models\Faq */
            if (isset($post['id'])) {
                $update = Faq::findOne($post['id']);
                $update->faq_category_id = $post['faq_category_id'];
                $update->question = $post['question'];
                $update->answer = $post['answer'];
                $update->save();
            } else {
                $new = new Faq();
                $new->faq_category_id = $post['faq_category_id'];
                $new->question = $post['question'];
                $new->answer = $post['answer'];
                $new->save();
            }
            return false;
        }

        return $this->renderAjax('faq/qa_form', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionQaList()
    {
        $model = new Faq();
        $data = $model->find()->all();

        $modelcat = new FaqCategory();
        $cats = $modelcat->find()->all();

        return $this->renderAjax('faq-qa', [
            'model' => $model,
            'data' => $data,
            'cats' => $cats
        ]);
    }

    public function actionDelQa($id = null)
    {
        /* @var $model \app\models\FaqCategory */
        $model = Faq::findOne($id);
        $model->delete();
        return false;
    }
}