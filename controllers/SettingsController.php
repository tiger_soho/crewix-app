<?php
namespace app\controllers;

use app\models\ChangeEmail;
use app\models\ChangePassword;
use app\models\Companies;
use app\models\Countries;
use app\models\Notification;
use app\models\NotificationGroup;
use app\models\NotificationType;
use app\models\Payment;
use app\models\Seaman;
use app\models\Token;
use app\models\User;
use app\models\Users;
use marciocamello\Paypal;
use PayPal\Api\Address;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Yii;

use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * Signup controller
 */
class SettingsController extends Controller
{
    public function init()
    {

        parent::init();
        if (!Yii::$app->user->isGuest) {
            $user = Users::findOne(Yii::$app->user->id);
            $user->last_seen = date('Y-m-d H:i:s');
            $user->save(false, ["last_seen"]);
        }
    }

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function actionIndex()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
    	$this->layout = 'settings';	
    	if(Yii::$app->user->isGuest){
    		return $this->goHome();
    	}

  		return $this->render('index');
    }

    public function actionSecurity()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
    	$this->layout = 'settings';

        $changePassword = new ChangePassword();
        $changeEmail = new ChangeEmail();
        $changePassword->id = Yii::$app->user->getId();
        $changeEmail->id = Yii::$app->user->getId();

        if(Yii::$app->request->isAjax){
            if($changePassword->load(Yii::$app->request->post())) {
                if($changePassword->change()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return 'ok';
                }
                else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $changePassword->getErrors();
                }
            }
            if($changeEmail->load(Yii::$app->request->post())) {
                $key = $changeEmail->change();
                if($key) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $url = Yii::$app->urlManager->createAbsoluteUrl(['/settings/security-verify-email','id'=> $changeEmail->id,'key'=>$key]);

                    $email = \Yii::$app->mailer->compose()
                        ->setTo(Yii::$app->user->getIdentity()->email)
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' no reply'])
                        ->setSubject('Signup Confirmation')
                        ->setHtmlBody("Click this link " . \yii\helpers\Html::a('change you e-mail', $url));


                    $mailer = Yii::$app->get('mailer');
                    $logger = new \Swift_Plugins_Loggers_ArrayLogger();
                    $mailer->getSwiftMailer()->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

                    if (!$email->send()) {
                        echo $logger->dump();
                    }

                    return 'ok';
                }
                else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $changeEmail->getErrors();
                }
            }
        }

    		return $this->render('security',[
                'changePassword' => $changePassword,
                'changeEmail' => $changeEmail,
    		]);
    }

    public function actionSecurityVerifyEmail($id, $key)
    {
        if(Yii::$app->user->getId() != $id) {
            throw new ForbiddenHttpException('You can\'t change this e-mail');
        }

        $token = Token::find()->andWhere('user_id = :id',[':id' => $id])
            ->andWhere('value = :key', [':key' => $key])
            ->andWhere('expires_at >= :time',[ ':time' =>  gmdate("Y-m-d H:i:s", time())])
            ->one();
        if($token == null) {
            throw new BadRequestHttpException('verify code or time is incorrect!');
        }

        $user = User::findOne($id);
        $user->email = $user->tmp_email;
        $user->tmp_email = null;
        if($user->save()) {
            $token->value = 'empty';
            $token->expires_at = gmdate("Y-m-d H:i:s", time());
            $token->save();
        }

        $this->redirect(['/settings/security']);

    }


    public function actionNotification()
    {
        $this->layout = 'settings';

    }

    public function actionPayments()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $this->layout = 'settings';

        $model = new Payment();
        $data = $model->find()->where(['user_id' => Yii::$app->user->id])->orderBy(['created_at' => SORT_DESC])->all();

        return $this->render('payments', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    public function actionPay()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new Payment();

        if ($model->load(Yii::$app->request->post())) {
            $apiContext = new ApiContext(new OAuthTokenCredential(
                "AYVFPvuu3wYxDBfJ3M8pbVaoMDokQJQctlpXmRmVUo-ZRsZ7HJi_rM2BVgUyRr_QWAFZ7BFQ-IpInIS6",
                "EGu0FRfnuGp0x9_KYN5Jkor-21-tsQbQkLJ7XWRFkAKLsDiD_CRWbDtk8rcymo8WJsOFTy9bJhvfb1iv"));
            $post = Yii::$app->request->post('Payment');
            $addr = new Address();
            if (Yii::$app->user->can('seaman')) {
                /* @var $user Seaman */
                $user = Seaman::findOne(Yii::$app->user->id);
                $addr->setLine1($user->address);
                $addr->setCity($user->city_of_residence);
                $addr->setCountryCode(Countries::findOne($user->country_of_residence_id)->code_2);
            }


            $payer = new Payer();
            $payer->setPaymentMethod('paypal');


            $amount = new Amount();
            $amount->setCurrency('USD');
            $amount->setTotal($post['amount']);

            $transaction = new Transaction();
            $transaction->setAmount($amount);
            $transaction->setDescription('Add funds to user');

            $payment = new \PayPal\Api\Payment();
            $payment->setIntent('Add funds');
            $payment->setPayer($payer);
            $payment->setTransactions(array($transaction));

            $a = new Paypal();

            return $payment->create($a->_apiContext);

        }

        return $this->renderAjax('payments/_pay_form', [
            'model' => $model
        ]);
    }

    public function actionNotifications()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $this->layout = 'settings';
        $model = new Notification();
        $type_model = new NotificationType();
        $group = new NotificationGroup();
        $types = $type_model->find()->where(['user_type_id' => Yii::$app->user->getIdentity()->account_type_id])->orFilterWhere(['user_type_id' => '0'])->orderBy(['id' => SORT_ASC])->all();
        $data = $model->findAll(Yii::$app->user->id);
        $groups = $group->find()->all();

        if($model->load(Yii::$app->request->post())){
            //VarDumper::dump(Yii::$app->request->post('Notification'));die;
            $post = Yii::$app->request->post('Notification');

            foreach ($post['value'] as $key => $item) {
                if (($modelupdate = Notification::findOne(['id' => $key, 'user_id' => Yii::$app->user->id])) != null) {
                    $modelupdate->value = $item;
                        $modelupdate->save();
                    }else{
                        $modelnew = new Notification();
                        $modelnew->user_id = Yii::$app->user->id;
                    $modelnew->notification_type_id = $key;
                    $modelnew->value = $item;
                        $modelnew->save();
                    }
            }
        }

        return $this->render('notifications', [
            'model' => $model,
            'types' => $types,
            'groups' => $groups,
            'data' => $data,
        ]);
    }
}