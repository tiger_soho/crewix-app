<?php
namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\models\Users;
use app\models\Profile;
use app\models\Countries;
use app\models\Companies;
use app\models\Biometrics;
use app\models\Family;
use app\models\CompanyContact;
use app\models\Seaman;
use app\models\Langs;
use app\models\SeamanLanguage;
use app\models\Education;
use app\models\SeamanTravelDocument;
use app\models\Cloth;
use app\models\Shoes;
use app\models\SeamanCertificateOfCompetency;
use app\models\SeamanCertificateOfProficiency;
use app\models\SeamanMedicalDocument;
use app\models\SeamanSeaService;
use app\models\SeamanReference;
use app\models\SeamanNextOfKin;
use app\models\Token;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\rbac\DbManager;
use yii\helpers\Url;

/**
 * Signup controller
 */
class SignupController extends Controller
{
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    
    public function actionIndex()
    {
        $this->layout = 'signup';

        $model = new Users();

        //$model->scenario = 'register';
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if(!Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){


            $auth_key = $model->generateAuthKey();
            $passwd = $model->password;
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password, 16);

            if($model->save()) {
                $profile = new Profile();
                $profile->user_id = $model->id;
                if($profile->save()) {
                    $autologin = new LoginForm(); // узкое место!!!
                    $autologin->email = $model->email;
                    $autologin->password= $passwd;
                    if($autologin->login()) {
                        return $this->redirect('/signup/welcome', 302);
                    }
                }

            }
            
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionWelcome()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;

            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;

            endswitch;
        }

    	$model = new Users();
    	$token_model = new Token();
    	$key = Yii::$app->user->getIdentity()->auth_key;

    	$key = md5($key.time());
    	$token_type = 2;
    	$user = Yii::$app->user->getId();
    	$token_model->createToken($key,$token_type,$user);
        $rbac = new DbManager();
        $rbac->init();

    	$id = Profile::findOne(['user_id'=>Yii::$app->user->getId()])->id;

        if(Yii::$app->request->post()){
            if(Yii::$app->user->getIdentity()->account_type_id==1):
                return $this->redirect('personal',302);
            elseif(Yii::$app->user->getIdentity()->account_type_id==2):
                return $this->redirect('profile',302);
            endif;
        }

        if(empty($rbac->getAssignments(Yii::$app->user->getId()))):
            if(Yii::$app->user->getIdentity()->account_type_id==1):
                $url = Yii::$app->urlManager->createAbsoluteUrl(['site/seaman','id'=>$id,'key'=>$key]);
                $role = $rbac->getRole('seaman');
                $rbac->assign($role,Yii::$app->user->getId());
                $model->saveRole($role->name,Yii::$app->user->getId());
            elseif(Yii::$app->user->getIdentity()->account_type_id==2):
                $url = Yii::$app->urlManager->createAbsoluteUrl(['site/company','id'=>$id,'key'=>$key]);
                $role = $rbac->getRole('company');

                $rbac->assign($role,Yii::$app->user->getId());
                $model->saveRole($role->name,Yii::$app->user->getId());
            endif;
            $user = Users::findOne(Yii::$app->user->getId());
            $user->registration_step = 1;
            $user->save();

            $email = \Yii::$app->mailer->compose()
                    ->setTo(Yii::$app->user->getIdentity()->email)
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' no reply'])
                    ->setSubject('Signup Confirmation')
                    ->setHtmlBody("Click this link ".\yii\helpers\Html::a('confirm',$url))
                    ->send();
        endif;



    	return $this->render('welcome');
    }

    public function actionPersonal()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):

                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;

            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;

            endswitch;
        }
        $model = new Seaman();
        $countries = new Countries();
        $list = $countries->getList();
        
        if ($model->load(Yii::$app->request->post())){
            $post=Yii::$app->request->post('Seaman');
            $cv = Yii::$app->session->get('cv');
            $photo = Yii::$app->session->get('photo');
            $model->saveData(Yii::$app->user->getId(),$post,$photo,$cv);
            $user = Users::findOne(Yii::$app->user->getId());
            if($user->validate()) {
                $user->registration_step = 2;
                $user->save(false);
            }

            return $this->redirect('family',302);
        }

        return $this->render('personal', [
            'model' => $model,
            'countries'=>$list,
        ]);
    }

    public function actionFamily()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;

                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new Family();
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Family');
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            $model->saveFamily($id,$post);
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 3;
            $user->save();
            return $this->redirect('nok',302);
        }else{
	        return $this->render('family', [
	            'model'=>$model,
	        ]);
        }
            
    }

    public function actionNok()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanNextOfKin();
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('SeamanNextOfKin');
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            $model->saveKin($id,$post);
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 4;
            $user->save();
            return $this->redirect('biometrics',302);
        }
        return $this->render('nok', [
            'model'=>$model,
        ]);
    }

    public function actionBiometrics()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new Biometrics();
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('Biometrics');
			$id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            $model->saveBio($id,$post);
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 5;
            $user->save();
            return $this->redirect('languages',302);
        }
        return $this->render('biometrics', [
            'model'=>$model,
        ]);
    }

    public function actionLanguages()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanLanguage();
        $l_model = new Langs();
        $langs = $l_model->getList();
        if($model->load(Yii::$app->request->post()) && !ActiveForm::validate($model)){
            $post = Yii::$app->request->post('SeamanLanguage');

            $n = sizeof($post['language_id']);
            for ($i=0; $i <= ($n-1); $i++) { 
                $arr[$i]['language_id'] = $post['language_id'][$i];
                $arr[$i]['language_level_id'] = $post['language_level_id'][$i];
            }
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            foreach($arr as $a){
                if(!empty($a['language_id'])&&!empty($a['language_level_id'])){
                	
                    $model->saveLang($id,$a);
                }
            }
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 6;
            $user->save();
            return $this->redirect('education',302);
        }

        return $this->render('languages', [
            'model'=>$model,
            'langs'=>$langs,
        ]);
    }

    public function actionRow()
    {

        $this->layout = false;
        $model = new SeamanLanguage();
        $l_model = new Langs();
        $langs = $l_model->getList();
        return $this->render('row', [
            'model'=>$model,
            'langs'=>$langs
        ]);
    }

    public function actionEducation()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new Education();

        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('Education');
            $n = sizeof($post['institution_name']);
            for ($i=0; $i < $n; $i++) { 
                $arr[$i]['institution_name'] = $post['institution_name'][$i];
                $arr[$i]['degree'] = $post['degree'][$i];
                $arr[$i]['year_from'] = $post['year_from'][$i];
                $arr[$i]['year_to'] = $post['year_to'][$i];
            }
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
            foreach($arr as $a){
                if(!empty($a['institution_name'])&&!empty($a['degree'])&&!empty($a['year_from'])&&!empty($a['year_to'])){
                    $model->saveEdu($id,$a);
                }
            }
            $user = Users::findOne(['id' => Yii::$app->user->id]);
            $user->registration_step = 7;
            $user->save();
            return $this->redirect('travel_docs',302);
        }

        return $this->render('education', [
            'model'=>$model,
        ]);
    }

    public function actionRowed()
    {
        $this->layout = false;
        $model = new Education();
        return $this->render('rowed', [
            'model'=>$model,
        ]);
    }

    public function actionTravel_docs()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanTravelDocument();

        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanTravelDocument');
            $n = sizeof($post['document_id']);
            for ($i=0; $i < $n; $i++) { 
                $arr[$i]['document_id'] = $post['document_id'][$i];
                $arr[$i]['number'] = $post['number'][$i];
                $arr[$i]['place_of_issue_id'] = $post['place_of_issue_id'][$i];
                $arr[$i]['date_of_issue'] = $post['date_of_issue'][$i];
                $arr[$i]['date_of_expiry'] = $post['date_of_expiry'][$i];
            }
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            foreach($arr as $a){
                if(!empty($a['document_id'])&&!empty($a['number'])&&!empty($a['place_of_issue_id'])&&!empty($a['date_of_issue'])){
                    $model->saveDoc($id,$a);
                }
            }
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 8;

            $user->save();
            return $this->redirect('coc',302);
        }

        return $this->render('travel_docs', [
            'model'=>$model,
        ]);
    }

    public function actionRowtr()
    {
        $this->layout = false;
        $model = new SeamanTravelDocument();
        return $this->render('rowtr', [
            'model'=>$model,
        ]);
    }

    public function actionCoc()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanCertificateOfCompetency();
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanCertificateOfCompetency');
            $arr = array();
            foreach($post['rank_id'] as $key => $value) {
                $arr[$key]['rank_id'] = $value;
            }
            foreach($post['document_type_id'] as $key => $value) {
                $arr[$key]['document_type_id'] = $value;
            }
            foreach($post['number'] as $key => $value) {
                $arr[$key]['number'] = $value;
            }
            foreach($post['place_of_issue_id'] as $key => $value) {
                $arr[$key]['place_of_issue_id'] = $value;
            }
            foreach($post['date_of_issue'] as $key => $value) {
                $arr[$key]['date_of_issue'] = $value;
            }
            foreach($post['date_of_expiry'] as $key => $value) {
                $arr[$key]['date_of_expiry'] = $value;
            }

            $seamen = Seaman::findOne(['user_id' => Yii::$app->user->getId()]);


            foreach($arr as $item) {
                $model = new SeamanCertificateOfCompetency();
                $model->seaman_id = $seamen->id;
                $model->rank_id = $item['rank_id'];
                $model->document_type_id = $item['document_type_id'];
                $model->number = $item['number'];
                $model->place_of_issue_id = $item['place_of_issue_id'];
                $doc_issue = explode('.', $item['date_of_issue']);
                $doc_issue = $doc_issue[2].'-'.$doc_issue[1].'-'.$doc_issue[0];
                $model->date_of_issue = $doc_issue;
                $doc_expiry = explode('.', $item['date_of_expiry']);
                $doc_expiry = $doc_expiry[2].'-'.$doc_expiry[1].'-'.$doc_expiry[0];
                $model->date_of_expiry = $doc_expiry;
                if($model->save()){
                    $user = Users::findOne(['id' => Yii::$app->user->getId()]);
                    $user->registration_step = 9;
                    $user->save();
                    return $this->redirect('cop',302);
                } else {
                    var_dump($model->errors);
                    die;
                }
            }

        }

        return $this->render('coc', [
            'model'=>$model,
        ]);
    }
    
	public function actionCocrow()
    {
        $this->layout = false;
        $model = new SeamanCertificateOfCompetency();
        return $this->render('cocrow',[
            'model'=>$model,
        ]);
    }

    public function actionEndor()
    {
        $this->layout = false;
        $model = new SeamanCertificateOfCompetency();
        return $this->render('endor',[
            'model'=>$model,
        ]);
    }

    public function actionCop()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanCertificateOfProficiency();
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanCertificateOfProficiency');
            $n = sizeof($post['document_id']);
            if($n>0){
            	for ($i=0; $i < $n; $i++) { 
	                $arr[$i]['document_id'] = $post['document_id'][$i];
	                $arr[$i]['number'] = $post['number'][$i];
	                $arr[$i]['place_of_issue_id'] = $post['place_of_issue_id'][$i];
	                $arr[$i]['issue'] = $post['date_of_issue'][$i];
                    $arr[$i]['expiry'] = $post['date_of_expiry'][$i];
	            }
                $id = Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
                /*echo "<pre>";
                VarDumper::dump($arr);die;*/
	            foreach($arr as $a){
                    $issue = explode('.', $a['issue']);
                    $issue = $issue[2] . '-' . $issue[1] . '-' . $issue[0];
                    $expiry = explode('.', $a['expiry']);
                    $expiry = $expiry[2] . '-' . $expiry[1] . '-' . $expiry[0];
                    $modelnew = new SeamanCertificateOfProficiency();
                    $modelnew->seaman_id = $id;
                    $modelnew->document_id = $a['document_id'];
                    $modelnew->number = $a['number'];
                    $modelnew->place_of_issue_id = $a['place_of_issue_id'];
                    $modelnew->date_of_issue = $issue;
                    $modelnew->date_of_expiry = $expiry;

                    if (!$modelnew->save()) {
                        var_dump($modelnew->errors);
                        die;
                    }

	            }
            }
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 10;
            $user->save();
            return $this->redirect('medical_docs',302);
        }

        return $this->render('cop', [
            'model'=>$model,
        ]);
    }

    public function actionRowprof()
    {
        $this->layout = false;
        $model = new SeamanCertificateOfProficiency();
        return $this->render('rowprof', [
            'model'=>$model,
        ]);
    }

    public function actionMedical_docs()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanMedicalDocument();
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanMedicalDocument');
            $n = sizeof($post['number']);
            for ($i=0; $i < $n; $i++) { 
                $arr[$i]['document_id'] = $post['document_id'][$i];
                $arr[$i]['number'] = $post['number'][$i];
                $arr[$i]['place_of_issue_id'] = $post['place_of_issue_id'][$i];
                $arr[$i]['date_of_issue'] = $post['date_of_issue'][$i];
                $arr[$i]['date_of_expiry'] = $post['date_of_expiry'][$i];
            }
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            foreach($arr as $a){
                if(!empty($a['document_id'])&&!empty($a['number'])&&!empty($a['place_of_issue_id'])&&!empty($a['date_of_issue'])){
                    $model->saveDoc($id,$a);
                }
            }
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 11;
            $user->save();
            return $this->redirect('scans',302);
        }

        return $this->render('medical_docs', [
            'model'=>$model,
        ]);
    }

    public function actionScans()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
                default:
                    $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                    return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
                default:
                    $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                    return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                    break;
            endswitch;
        }
    	$model = new Users();
    	if(isset($_POST['next'])){
            return $this->redirect('service',302); 
        }
    	return $this->render('scans');
    }
    public function actionService()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanSeaService();
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanSeaService');
            $n = sizeof($post['rank_id']);
            for ($i=0; $i < $n; $i++) { 
                $arr[$i]['rank_id'] = $post['rank_id'][$i];
                $arr[$i]['vessel_name'] = $post['vessel_name'][$i];
                $arr[$i]['vessel_flag_id'] = $post['vessel_flag_id'][$i];
                $arr[$i]['vessel_type_id'] = $post['vessel_type_id'][$i];
                $arr[$i]['vessel_deadweight'] = $post['vessel_deadweight'][$i];
                $arr[$i]['engine_type_id'] = $post['engine_type_id'][$i];
                $arr[$i]['engine_power'] = $post['engine_power'][$i];
                $arr[$i]['measure'] = $post['measure'][$i];
                $arr[$i]['shipowning_company_name'] = $post['shipowning_company_name'][$i];
                $arr[$i]['crewing_company_name'] = $post['crewing_company_name'][$i];
                $arr[$i]['date_from'] = $post['date_from'][$i];
                $arr[$i]['date_to'] = $post['date_to'][$i];
            }
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            foreach($arr as $a){
                if (!empty($a['rank_id']) && !empty($a['vessel_name']) && !empty($a['vessel_flag_id']) && !empty($a['vessel_type_id']) && !empty($a['vessel_deadweight']) && !empty($a['engine_type_id']) && !empty($a['engine_power']) && !empty($a['measure']) && !empty($a['shipowning_company_name']) && !empty($a['date_from'])) {
                    $model->saveServ($id,$a);
                }
            }
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 12;
            $user->save();
            return $this->redirect('references',302);
        }

        return $this->render('service', [
            'model'=>$model,
        ]);
    }



    public function actionRowserv()
    {
        $this->layout = false;
        $model = new SeamanSeaService();
        return $this->render('rowserv', [
            'model'=>$model,
        ]);
    }

    public function actionReferences()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $model = new SeamanReference();
        if($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('SeamanReference');
            $n = sizeof($post['company_name']);
            for ($i=0; $i < $n; $i++) { 
                $arr[$i]['company_name'] = $post['company_name'][$i];
                $arr[$i]['phone'] = $post['phone'][$i];
                $arr[$i]['email'] = $post['email'][$i];
                $arr[$i]['contact_person'] = $post['contact_person'][$i];
            }
            $id = Seaman::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            foreach($arr as $a){
                if(!empty($a['company_name'])&&!empty($a['phone'])&&!empty($a['email'])&&!empty($a['contact_person'])){
                    $model->saveRef($id,$a);
                }
            }
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = null;
            $user->save();
            return $this->redirect('finish',302);
        }

        return $this->render('references', [
            'model'=>$model,
        ]);
    }

    public function actionRowref()
    {
        $this->layout = false;
        $model = new SeamanReference();
        return $this->render('rowref', [
            'model'=>$model,
        ]);
    }

    public function actionFinish()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
    	$model = new Users();
    	if(isset($_POST['next'])){
            Yii::$app->session->remove('m');
            return $this->redirect('/',302); 
        }
    	return $this->render('finish');
    }

    public function actionProfile()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
                default:
                    $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                    return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
            endswitch;
        }
        $m = Yii::$app->session->get('m');
        if(Yii::$app->user->isGuest){
            return $this->goHome();
        }
        $model = new Companies();
        $model2 = new CompanyContact();
        $cont = new CompanyContact();
        $user = new Users();
        if ($model->load(Yii::$app->request->post())){
            $user_data = $user->getByMail(Yii::$app->session->get('m'));

            
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/companies/logos/'.$user_data['id'];
            $post=Yii::$app->request->post('Companies');
            $user_data = $user->getByMail(Yii::$app->session->get('m'));
            $avatar =  Yii::$app->session->get('file');

            Yii::$app->session->set('uid',$user_data['id']);
            $model->saveData(Yii::$app->user->getId(),$post,$avatar);
            $id = Companies::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            $cont->saveData($id,$post);
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = 2;
            $user->save();
            return $this->redirect('contacts',302);
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    public function actionContacts()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
                default:
                    $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                    return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
            endswitch;
        }
        $model = new CompanyContact();
        if ($model->load(Yii::$app->request->post())){
            $post = Yii::$app->request->post('CompanyContact');
            $id = Companies::findOne(['user_id'=>Yii::$app->user->getId()])->id;
            $model->saveContacts($id,$post);
            $user = Users::findOne(['id' => Yii::$app->user->getId()]);
            $user->registration_step = null;
            $user->save();
            return $this->redirect('tips',302);
        }
        return $this->render('contacts', [
            'model'=>$model,
        ]);
    }
    public function actionTips()
    {
        if (Yii::$app->user->can('seaman')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/personal']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/family']), 302);
                    break;
                case 3:
                    return $this->redirect(Url::to(['/signup/nok']), 302);
                    break;
                case 4:
                    return $this->redirect(Url::to(['/signup/biometrics']), 302);
                    break;
                case 5:
                    return $this->redirect(Url::to(['/signup/languages']), 302);
                    break;
                case 6:
                    return $this->redirect(Url::to(['/signup/education']), 302);
                    break;
                case 7:
                    return $this->redirect(Url::to(['/signup/travel_docs']), 302);
                    break;
                case 8:
                    return $this->redirect(Url::to(['/signup/coc']), 302);
                    break;
                case 9:
                    return $this->redirect(Url::to(['/signup/cop']), 302);
                    break;
                case 10:
                    return $this->redirect(Url::to(['/signup/medical_docs']), 302);
                    break;
                case 11:
                    return $this->redirect(Url::to(['/signup/service']), 302);
                    break;
                case 12:
                    return $this->redirect(Url::to(['/signup/references']), 302);
                    break;
                default:
                    $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                    return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                    break;
            endswitch;


        } elseif (Yii::$app->user->can('company')) {
            switch (Yii::$app->user->identity->registration_step):
                case 1:
                    return $this->redirect(Url::to(['/signup/profile']), 302);
                    break;
                case 2:
                    return $this->redirect(Url::to(['/signup/contacts']), 302);
                    break;
                default:
                    $id = Profile::findOne(['user_id' => Yii::$app->user->id])->id;
                    return $this->redirect(Url::to(['profile', 'id' => $id]), 302);
                    break;
            endswitch;
        }
    	$model = new Users();
    	if(isset($_POST['next'])){
            return $this->redirect('finish', 302);
        }
    	return $this->render('tips');
    }

    public function actionCloth() {
        $model = new Cloth();
        $sex = strtolower($_GET['id']);
        echo '<option disabled="disabled" selected>Select clothing size</option>';
        if($sex!=''){
            $sizesCount = Cloth::find()->where(['sex_id'=>$sex])->count();
            $sizes = Cloth::find()->where(['sex_id'=>$sex])->asArray()->all();
            
            if ($sizesCount>0) {
                foreach ($sizes as $size) {
                    echo '<option value="'.$size['id'].'">'.$size['name'].'</option>';
                }
            }
        }else{
            echo 'empty';
        }
    }
    public function actionShoes() {
        $model = new Shoes();
        $sex = $_GET['id'];
        //var_dump($sex);exit();
        echo '<option disabled="disabled" selected>Select shoe size</option>';
        if ($sex!='') {
            $sizesCount = Shoes::find()->where(['sex_id'=>$sex])->count();
            $sizes = Shoes::find()->where(['sex_id'=>$sex])->asArray()->all();
            if ($sizesCount>0) {
                foreach ($sizes as $size) {
                    echo '<option value="'.$size['id'].'">'.$size['name'].'</option>';
                }
            }
        }else{
            echo 'empty';
        }
    }

    public function actionAvatarupload()
    {
        if(!empty($_FILES['images'])){
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/seaman/photo/';
            $ava = uniqid('', true).'_'.$_FILES['images']['name']['original'];
            move_uploaded_file($_FILES['images']['tmp_name']['huge'], $path.'medium/'.$ava);
            move_uploaded_file($_FILES['images']['tmp_name']['medium'], $path.'small/'.$ava);
            Yii::$app->session->set('photo',$ava);
            return $ava;
        }
    }

    public function actionCvupload()
    {
        if(!empty($_FILES['images'])){
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/seaman/docs/';
            $ava = uniqid('', true).'_'.$_FILES['images']['name'];
            move_uploaded_file($_FILES['images']['tmp_name'], $path.$ava);
            Yii::$app->session->set('cv',$ava);
        }
    }
    
    public function actionComupload()
    {
        if(!empty($_FILES['images'])){
            $path = $_SERVER['DOCUMENT_ROOT'].'/images/companies/logos/';
            $ava = uniqid('', true).'_'.$_FILES['images']['name']['original'];
            move_uploaded_file($_FILES['images']['tmp_name']['huge'], $path.'medium/'.$ava);
            move_uploaded_file($_FILES['images']['tmp_name']['medium'], $path.'small/'.$ava);
            Yii::$app->session->set('file',$ava);
            return $ava;
        }
    }
}