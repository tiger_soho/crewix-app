<?php

use yii\db\Migration;

class m160302_100506_create_table_currency_rate extends Migration
{
    public function up()
    {
        $this->createTable('table_currency_rate', [
            'id' => $this->bigPrimaryKey(),
            'from_currency_id' => $this->bigInteger()->notNull()->defaultValue(1),
            'to_currency_id' => $this->bigInteger()->notNull()->defaultValue(1),
            'value' => $this->decimal(19, 2)->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('table_currency_rate');
    }
}
