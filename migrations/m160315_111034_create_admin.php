<?php

use yii\db\Migration;

class m160315_111034_create_admin extends Migration
{
    public function up()
    {
        $this->createTable('admin', [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger(),
            'first_name' => $this->string(64),
            'last_name' => $this->string(64),
            'profile_picture' => $this->string(255),
        ]);
    }

    public function down()
    {
        $this->dropTable('admin');
    }
}