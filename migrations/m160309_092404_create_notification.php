<?php

use yii\db\Migration;

class m160309_092404_create_notification extends Migration
{
    public function up()
    {
        $this->createTable('notification', [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger(),
            'notification_type_id' => $this->bigInteger(),
            'value' => $this->boolean()
        ]);
    }

    public function down()
    {
        $this->dropTable('notification');
    }
}
