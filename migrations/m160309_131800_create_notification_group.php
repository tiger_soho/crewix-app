<?php

use yii\db\Migration;

class m160309_131800_create_notification_group extends Migration
{
    public function up()
    {
        $this->createTable('notification_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    public function down()
    {
        $this->dropTable('notification_group');
    }
}
