<?php

use yii\db\Migration;

class m160229_112010_add_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'user_account_type_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user', 'user_account_type_id');
    }
}
