<?php

use yii\db\Migration;

class m160309_092335_create_notification_type extends Migration
{
    public function up()
    {
        $this->createTable('notification_type', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(),
            'group_id' => $this->bigInteger(),
            'user_type_id' => $this->bigInteger()
        ]);
    }

    public function down()
    {
        $this->dropTable('notification_type');
    }
}
