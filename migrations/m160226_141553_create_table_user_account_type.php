<?php

use yii\db\Migration;

class m160226_141553_create_table_user_account_type extends Migration
{
    public function up()
    {
        $this->createTable('user_account_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('user_account_type');
    }
}
