<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\bootstrap\ActiveForm;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
$current = date('Y');
$from = $current-70;
$to = $current+20;
$range = range($from, $to, 1);
foreach($range as $r){
	$arr[]=['k'=>$r,'y'=>$r];
}
?>
<div class="seamansign">
	<div class="row">
		<div class="wrapper progress-block sign-block wide vis">
			<div class="signup-progress">
				<div class="line" style="width: 100%"></div>
				<div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
				<div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
				<div class="step step-3 active"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
				<div class="step step-4 active"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
				<div class="step step-5 active"><i class="fa fa-check"></i><div class="title">Finish</div></div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="sign-block vis">
			<div class="bt">
				<h2>Finish</h2>
			</div>
			<div class="cont">
				<img alt="Personal details" src="/images/sys/finish.png">
				<div class="text">
					<h4>You’ve signed up!</h4>
					<p>All you have to do now is double check your profile and wait until you’re hired. Good luck!</p>
				</div>
			</div>
			<div class="bot">
				<?php $form = ActiveForm::begin(['id' => 'finish',
                        'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                    ]); ?>
                <input type="submit" name="next" class="next btn btn-primary" value="Finish">
                <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>