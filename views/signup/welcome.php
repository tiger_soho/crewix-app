<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Countries;
use artkost\trumbowyg\Widget;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block vis">
            <div class="bt">
                <h2>Welcome!</h2>
            </div>
            <div class="cont">
                <img src="/images/sys/welcome.png" alt="welcome">
                <div class="text">
                    <h4>Thanks for signing up!</h4>
                    <p>To start using Crewix you need to fill your profile. So let’s begin!</p>
                </div>
            </div>
            <div class="bot">
                <?php $form = ActiveForm::begin(['id' => 'welcome',
                        'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                    ]); ?>
                <?= Html::submitButton('Coninue', ['class' => 'next btn btn-primary', 'name' => 'next', 'id' => 'next']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>