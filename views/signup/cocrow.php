<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanCertificateOfCompetency */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Ranks;
use kartik\file\FileInput;
use dosamigos\datepicker\DatePicker;

?>

<tr>
    <td>Rank</td>
    <td colspan="2">
        <?=Html::activeDropDownList($model,'rank_id[]', ArrayHelper::map(Ranks::find()->all(),'id','name'), ['prompt' => 'Select rank','class'=>'form-rank form-control']);?>
        <?=Html::activeHiddenInput($model, "document_type_id[]", ['value'=>'1']);?>
    </td>
    <td colspan="2"></td>
</tr>
<tr>
    <td>Certificate of competency</td>
    <td>
        <?=Html::activeTextInput($model, 'number[]', ['class'=>'form-rank form-control','placeholder'=>'Enter number'])?>
    </td>
    <td>
        <?=Html::activeDropDownList($model,'place_of_issue_id[]', ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt' => 'Select country','class'=>'form-country form-control']);?>
        
    </td>
    <td><div class="date"><?=Html::activeTextInput($model,'date_of_issue[]',['placeholder'=>'dd.mm.yyyy','class'=>'form-control']);?></div></td>
    <td><div class="date"><?=Html::activeTextInput($model,'date_of_expiry[]',['placeholder'=>'dd.mm.yyyy','class'=>'form-control']);?></div></td>
</tr>