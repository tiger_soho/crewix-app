<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
$current = date('Y');
$from = $current-70;
$to = $current+20;
$range = range($from, $to, 1);
foreach($range as $r){
    $arr[]=['k'=>$r,'y'=>$r];
}
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 42%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3 active"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4 active"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block wide vis">
            <div class="bt">
                <h2>References</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'ref',
                    'options' => ['class'=>'ref_form'],
                    'validateOnBlur' => false,
                ]); ?>
            <div class="col-lg-12">
                <table>
                    <thead>
                        <tr>
                            <td>Company</td>
                            <td>Phone number</td>
                            <td>Email</td>
                            <td>Contact person</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?=$form->field($model,'company_name[]')->textInput(['placeholder'=>'Enter company name'])->label(false);?>
                            </td>
                            <td>
                                <?=$form->field($model,'phone[]', ['options'=>['class'=>'phone']])->textInput(['placeholder'=>'+1 555 555 5555'])->label(false);?>
                            </td>
                            <td>
                                <?=$form->field($model,'email[]')->textInput(['placeholder'=>'Enter email'])->label(false);?>
                            </td>
                            <td>
                                <?=$form->field($model,'contact_person[]')->textInput(['placeholder'=>'Enter full name'])->label(false);?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Reference</span>
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $('.phone input').intlTelInput();
</script>

<script type="text/template" id="new-ref-row">
    <?= $this->render('/signup/rowref', [
        'model' => $model,
    ]); ?>
</script>

<?php
    $this->registerJs('
    $(\'body\').on(\'click\',\'.add\',function(){
        var tpl = $(\'#new-ref-row\').html();

        $(\'#ref table tbody\').append(tpl);
        $(\'#ref select\').each(function(i,el){
            $(el).selectmenu({
                width: \'100%\'
            });
        });

        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
        $(\'.phone input\').each(function(i,el){
            $(el).intlTelInput();
        });

    });
    ', \yii\web\View::POS_READY, 'ref-add');
?>