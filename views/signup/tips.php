<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Countries;
use yii\web\JqueryAsset;
use borales\extensions\phoneInput\PhoneInput;

?>
<div class="seamansign">
    <div class="row">  
        <div class="sign-block vis">
            <div class="bt">
                <h2>Tip</h2>
            </div>
            <div class="cont">
                <img src="/images/sys/tips.png" alt="welcome">
                <div class="text">
                    <h4>One important tip!</h4>
                    <p>Visit Crewix regularly &amp; review applicants as soon as possible.</p>
                </div>
            </div>
            <div class="bot">
                <?php $form = ActiveForm::begin(['id' => 'welcome',
                        'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                    ]); ?>
                <input type="submit" name="next" class="next btn btn-primary" value="Coninue">
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>