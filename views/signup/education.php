<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use kartik\file\FileInput;
use yii\web\View;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
$current = date('Y');
$from = $current-70;
$to = $current+20;
$range = range($from, $to, 1);
foreach($range as $r){
    $arr[]=['k'=>$r,'y'=>$r];
}
?>
<div class="seamansign">
    <div class="sign-block wide vis">
        <div class="row">
            <div class="wrapper progress-block sign-block wide vis">
                <div class="signup-progress">
                    <div class="line" style="width: 23%"></div>
                    <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                    <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                    <div class="step step-3"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                    <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                    <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="sign-block vis">
        <div class="bt">
            <h2>Education</h2>
        </div>
            <?php $form = ActiveForm::begin(['id' => 'edu',
                'options' => ['class'=>'edu_form'],
                'validateOnBlur' => false,
                'validateOnChange' => false,
            ]); ?>
        <div class="col-lg-12">
            <table>
                <thead>
                    <tr>
                        <td>Institution</td>
                        <td>Degree</td>
                        <td>From</td>
                        <td>To</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?=$form->field($model,'institution_name[]')->textInput(['placeholder'=>'Enter college / university name'])->label(false);?>
                        </td>
                        <td>
                            <?=$form->field($model,'degree[]')->textInput(['placeholder'=>'Enter your degree'])->label(false);?>
                        </td>
                        <td>
                            <?= $form->field($model, 'year_from[]')->dropDownList(ArrayHelper::map($arr,'k','y'), ['prompt'=>'Select year'],['placeholder'=>'Select year'])->label(false);?>
                        </td>
                        <td>
                            <?= $form->field($model, 'year_to[]')->dropDownList(ArrayHelper::map($arr,'k','y'), ['prompt'=>'Select year'],['placeholder'=>'Select year'])->label(false);?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Education</span>
        </div>
        <div class="bot">
            <div class="form-group">
                <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    $('.sign-block').first().addClass('vis');
</script>
    <script type="text/template" id="new-education-row">
        <?= $this->render('/signup/rowed', [
            'model' => $model,
        ]); ?>
    </script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-education-row\').html();

    $(\'#edu table tbody\').append(tpl);
    $(\'#edu select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
    });
});
', View::POS_READY, 'asdasasdasdsad');
?>