<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\TravelDocument;

?>


<tr>
    <td><?= Html::activeDropDownList($model, 'document_id[]',
            ArrayHelper::map(TravelDocument::find()->where(['always_visible' => '0'])->orderBy(['position' => SORT_ASC])->all(),
                'id', 'name'), ['prompt' => 'Select document', 'class' => 'form-control']); ?></td>
    
    <td><?=Html::activeTextInput($model,'number[]',['placeholder'=>'Enter number','class'=>'form-control']);?></td>
    
    <td><?=Html::activeDropDownList($model,'place_of_issue_id[]', ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt' => 'Select country','class'=>'form-control']);?></td>
    

	<td><div class="date"><?=Html::activeTextInput($model,'date_of_issue[]',['placeholder'=>'dd.mm.yyyy','class'=>'form-control']);?></div></td>
    <td>
        <div class="cel-block">
            <div class="date"><?= Html::activeTextInput($model, 'date_of_expiry[]',
                    ['placeholder' => 'dd.mm.yyyy', 'class' => 'form-control']); ?></div>
            <div class="span remove"><i class="fa fa-times"></i></div>
        </div>
    </td>
</tr>
