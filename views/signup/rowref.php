<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanReference */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

?>

<tr>
    <td>
        <div class="form-group"><?=Html::activeTextInput($model, 'company_name[]', ['class'=>'form-control','placeholder'=>'Enter company name'])?></div>
    </td>
    <td>
        <div class="form-group phone"><?=Html::activeTextInput($model, 'phone[]', ['class'=>'phone form-control','placeholder'=>'+1 555 555 5555'])?></div>
    </td>
    <td>
        <div class="form-group"><?=Html::activeTextInput($model, 'email[]', ['class'=>'form-control','placeholder'=>'Enter email'])?></div>
    </td>
    <td>
        <div class="cel-block">
            <div class="form-group"><?= Html::activeTextInput($model, 'contact_person[]',
                    ['class' => 'form-control', 'placeholder' => 'Enter full name']) ?></div>
            <div class="span remove"><i class="fa fa-times"></i></div>
        </div>
    </td>
</tr>