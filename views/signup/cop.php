<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanCertificateOfProficiency */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\CertificateOfProficiency;
use dosamigos\datepicker\DatePicker;

$this->title = 'Create account';
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 47%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3 active"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block wide vis">
            <div class="bt">
                <h2>Certificates of proficiency</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'prof',
                    'options' => ['class'=>'prof_form'],
                    'validateOnBlur' => false,
                ]); ?>
            <div class="col-lg-12">
                <table>
                    <thead>
                        <tr>
                            <td>Document</td>
                            <td>Number</td>
                            <td>Place of issue</td>
                            <td>Date of issue</td>
                            <td>Date of expiry</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?= $form->field($model, 'document_id[]')->dropDownList(ArrayHelper::map(CertificateOfProficiency::find()->all(), 'id', 'name'), ['prompt'=>'Select document'],['placeholder'=>'Select document'])->label(false);?>
                            </td>
                            <td>
                                <?=$form->field($model,'number[]')->textInput(['placeholder'=>'Enter number'])->label(false);?>
                            </td>
                            <td>
                                <?= $form->field($model, 'place_of_issue_id[]')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'],['placeholder'=>'Select country'])->label(false);?>
                            </td>
                            <td><?= $form->field($model, 'date_of_issue[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?></td>
                            <td><?= $form->field($model, 'date_of_expiry[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?></td>
                        </tr>
                    </tbody>
                </table>
                <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Document</span>
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="new-prof-row">
    <?= $this->render('/signup/rowprof', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-prof-row\').html();

    $(\'#prof table tbody\').append(tpl);
    $(\'#prof select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'prof-add');
?>