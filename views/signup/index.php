<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create account';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="tit clearfix">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <span class="pull-right"><strong>
            <?=Html::a('Log in',['/login'],['title'=>'Log in']);?>
        </strong></span>
    </div>
        

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'users',
                'validateOnBlur' => false,
                'validateOnChange' => false,
                'enableAjaxValidation' =>true]); ?>
                <?php $model->account_type_id = 1; ?>
                <?= $form->field($model, 'account_type_id')->radioList(array('1'=>'I\'m seaman','2'=>'I\'m company'))->label(false); ?>
            <p class="tip">
                Fill your CV and store it in the cloud. Wait until someone hires you. Be hired, be happy.
            </p>
                <?= $form->field($model, 'email', ['enableAjaxValidation' => true])->textInput(['placeholder'=>'Email'])->label(false); ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false); ?>

                <div class="form-group">
                    <?= Html::submitButton('Get started', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>