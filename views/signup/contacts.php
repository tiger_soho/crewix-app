<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Countries;
use yii\web\JqueryAsset;
use borales\extensions\phoneInput\PhoneInput;

?>
<div class="seamansign">
    <div class="row">  
        <div class="sign-block vis">
            <div class="bt">
                <h2>Contacts</h2>
            </div>
            <div class="cont">
                <img src="/images/sys/cont.png" alt="welcome">
                <div class="text">
                    <h4>Contact information</h4>
                    <p>Let everyone know how to contact your company.</p>
                </div>
            </div>
            <div class="bot">
                <button type="button" class="next btn btn-primary">Coninue</button>
            </div>
        </div>
        <div class="sign-block">
            <div class="bt">
                <h2>Contacts</h2>
            </div>
            <?php $form = ActiveForm::begin(['id' => 'company-cont',
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
            ]); ?>
            <div class="col-lg-8 col-md-10 col-xs-12">
                    <?= $form->field($model, 'email')->textInput(['placeholder'=>'contact@example.com'])->label('Email'); ?>
                    <?= $form->field($model, 'phone')->widget(PhoneInput::className(),[])->label('Phone'); ?>
                    <?= $form->field($model, 'skype', [
                        'template' => '{label}{hint}{input}',
                    ])->textInput(['placeholder' => 'Username'])->hint('It will be visible only to us. We\'ll use it to contact you in case of interview')->label('Skype'); ?>
                    <?= $form->field($model, 'website')->textInput(['placeholder'=>'example.com'])->label('Website'); ?>
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Get started', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'conts']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    $('#cinfo-phone').intlTelInput();
</script>