<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\TravelDocument;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;

$docs = TravelDocument::find()->where(['always_visible' => '1'])->orderBy(['position' => SORT_ASC])->all();

?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 47%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3 active"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="sign-block vis">
        <div class="bt">
            <h2>Documents</h2>
        </div>
        <div class="cont">
            <div class="sign-icon docs-icon"><img src="/images/sys/stack.svg" alt="Personal details"></div>
            <div class="text">
                <h4>Your documents</h4>
                <p>Documents are very important part of your CV. There are several types of documents.<br/>Be sure to add all of your documents. Press <strong>continue</strong> to continue.</p>
            </div>
        </div>
        <div class="bot">
            <button type="button" class="next btn btn-primary">Coninue</button>
        </div>
    </div>
    <div class="sign-block wide">
        <div class="bt">
            <h2>Travel documents</h2>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'travel',
            'options' => ['class'=>'travel_form'],
            'validateOnBlur' => false,
            'validateOnChange' => false,
        ]); ?>
        <div class="col-lg-12">
            <table>
                <thead>
                <tr>
                    <td>Document</td>
                    <td>Number</td>
                    <td>Place of issue</td>
                    <td>Date of issue</td>
                    <td>Date of expiry</td>
                </tr>
                </thead>
                <?php foreach($docs as $doc): ?>
                    <tr>
                        <td class="f-tit lang"><?=$doc->name?>
                            <?=Html::activeHiddenInput($model, "document_id[]", ['value'=>$doc->id]);?>
                        </td>
                        <td>
                            <?= $form->field($model,'number[]')->textInput(['placeholder'=>'Enter number'])->label(false); ?>
                        </td>
                        <td>
                            <?= $form->field($model, 'place_of_issue_id[]')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'])->label(false);?>
                        </td>
                        <td>
                            <?= $form->field($model, 'date_of_issue[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                        </td>
                        <td>
                            <?= $form->field($model, 'date_of_expiry[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </table>
            <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Document</span>
        </div>
        <div class="bot">
            <div class="form-group">
                <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    $('.sign-block').first().addClass('vis');
</script>

<script type="text/template" id="new-travel-row">
    <?= $this->render('/signup/rowtr', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-travel-row\').html();

    $(\'#travel table tbody\').append(tpl);
    $(\'#travel select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'asdasasdasdsad');
?>