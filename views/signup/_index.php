<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create account';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    
    <div class="tit">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
        

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'users',]); ?>
                <?php $model->u_tipe = '1'; ?>
                <?= $form->field($model, 'u_type')->radioList(array('1'=>'I\'m seaman','2'=>'I\'m company'))->label(false); ?>

                <?= $form->field($model, 'mail', ['enableAjaxValidation' => true])->textInput(['placeholder'=>'Email'])->label(false); ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false); ?>

                <div class="form-group">
                    <?= Html::submitButton('Get started', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
