<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanSeaService */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Ranks;
use app\models\VesselTypes;
use app\models\Engines;
use kartik\file\FileInput;
use dosamigos\datepicker\DatePicker;

?>

<tr>
    <td>
        <div class="form-group">
            <?=Html::activeDropDownList($model,'rank_id[]', ArrayHelper::map(Ranks::find()->all(),'id','name'), ['prompt' => 'Select rank','class'=>'form-rank form-control']);?>
        </div>
    </td>
    <td>
        <?=Html::activeTextInput($model, 'vessel_name[]', ['class'=>'form-rank form-control','placeholder'=>'Vessel name'])?>
        <?= Html::activeDropDownList($model,'vessel_flag_id[]', ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt' => 'Select country','class'=>'form-country form-control']); ?>
    </td>
    <td>
        <div class="form-group"><?=Html::activeDropDownList($model,'vessel_type_id[]', ArrayHelper::map(VesselTypes::find()->all(), 'id', 'name'), ['prompt' => 'Vessel type','class'=>'form-country form-control']);?></div>
        <div class="form-group"><?=Html::activeTextInput($model, 'vessel_deadweight[]', ['class'=>'form-rank form-control','placeholder'=>'Enter DWT'])?></div>
    </td>
    <td>
        <div class="form-group"><?=Html::activeDropDownList($model,'engine_type_id[]', ArrayHelper::map(Engines::find()->all(), 'id', 'name'), ['prompt' => 'ME type','class'=>'form-country form-control']);?></div>
        <div class="f_blc">
            <div class="form-group field-service-me_power"><?=Html::activeTextInput($model, 'engine_power[]', ['class'=>'form-rank form-control','placeholder'=>'ME power'])?></div><div class="form-group field-service-measure"><?=Html::activeDropDownList($model,'measure[]', ['kW'=>'kW','BHP'=>'BHP'], ['class'=>'form-country form-control']);?></div>
        </div>
    </td>
    <td>
        <div class="form-group"><?=Html::activeTextInput($model, 'shipowning_company_name[]', ['class'=>'form-rank form-control','placeholder'=>'Chipowner'])?>
        <div class="form-group"><?=Html::activeTextInput($model, 'crewing_company_name[]', ['class'=>'form-rank form-control','placeholder'=>'Crewing agency'])?>
    </td>
    <td>
        <div class="cel-block">
            <div class="date"><?= Html::activeTextInput($model, 'date_from[]',
                    ['placeholder' => 'dd.mm.yyyy', 'class' => 'form-control']); ?></div>
            <div class="date"><?= Html::activeTextInput($model, 'date_to[]',
                    ['placeholder' => 'dd.mm.yyyy', 'class' => 'form-control']); ?></div>
            <div class="span remove"><i class="fa fa-times"></i></div>
        </div>
    </td>
</tr>