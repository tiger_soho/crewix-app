<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Countries;
use kartik\file\FileInput;
use app\models\MaritalStatus;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 23%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="sign-block wide vis">
        <div class="bt">
            <h2>Family</h2>
        </div>
            <?php $form = ActiveForm::begin(['id' => 'family',
                'options' => ['class'=>'family_form'],
                'validateOnBlur' => false,
                'validateOnChange' => false,
            ]); ?>
        <div class="col-lg-12">
            <table>
                <tr>
                    <td class="f-tit"><span class="req">*</span> Marital status</td>
                    <td>
                        <?= $form->field($model, 'marital_status_id')->dropDownList(ArrayHelper::map(MaritalStatus::find()->all(), 'id', 'name'), ['prompt'=>'Marital status'],['placeholder'=>'Marital status'])->label(false);?>
                    </td>
                    <td class="f-tit"><span class="req">*</span> Children</td>
                    <td>
                        <?= $form->field($model, 'children')->dropDownList(['0'=>'No','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'], ['prompt'=>'Do you have children?'],['placeholder'=>'Do you have children?'])->label(false);?>
                    </td>
                </tr>
                <tr>
                    <td class="f-tit">Mother’s name</td>
                    <td>
                        <?= $form->field($model, 'mother')->textInput(['placeholder'=>'Enter full name'])->label(false); ?>
                    </td>
                    <td class="f-tit">Father’s name</td>
                    <td>
                        <?= $form->field($model, 'father')->textInput(['placeholder'=>'Enter full name'])->label(false); ?>
                    </td>
                </tr>
            </table>
                
        </div>
        <div class="bot">
            <div class="form-group">
                <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
    

</div><!-- seamansign -->