<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Ranks;
use kartik\file\FileInput;
use dosamigos\datepicker\DatePicker;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
$current = date('Y');
$from = $current-70;
$to = $current+7;
$range = range($from, $to, 1);
foreach($range as $r){
    $arr[]=['k'=>$r,'y'=>$r];
}
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 47%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3 active"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block wide vis">
            <div class="bt">
                <h2>Certificates of competency</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'competency',
                    'options' => ['class'=>'competency_form'],
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                ]); ?>
            <div class="col-lg-12">
                <table>
                    <thead>
                        <tr>
                            <td>Document</td>
                            <td>Number</td>
                            <td>Place of issue</td>
                            <td>Date of issue</td>
                            <td>Date of expiry</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span class="req">*</span> Highest Rank on Endorsement</td>
                            <td colspan="2">
                                <?=$form->field($model,'rank_id[]')->dropDownList(ArrayHelper::map(Ranks::find()->all(),'id','name'), ['prompt'=>'Select rank'],['placeholder'=>'Select rank'])->label(false);?>
                                <?=Html::activeHiddenInput($model, "document_type_id[]", ['value'=>'1']);?>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td><span class="req">*</span> National License (COC/Endorsement)</td>
                            <td>
                                <?=$form->field($model,'number[]')->textInput(['placeholder'=>'Enter number'])->label(false);?>
                            </td>
                            <td>
                                <?=$form->field($model,'place_of_issue_id[]')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'],['placeholder'=>'Select country?'])->label(false);?>
                            </td>
                            <td>
                                <?= $form->field($model, 'date_of_issue[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                            </td>
                            <td>
                                <?= $form->field($model, 'date_of_expiry[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                            </td>
                        </tr>
                        <!-- <tr>
                            <td>Endorsement</td>
                            <td>
                                <?=$form->field($model,'number[]')->textInput(['placeholder'=>'Enter number'])->label(false);?>
                            </td>
                            <td>
                                <?=$form->field($model,'place_of_issue_id[]')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'],['placeholder'=>'Select country?'])->label(false);?>
                            </td>
                            <td>
                                <?= $form->field($model, 'date_of_issue[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                            </td>
                            <td>
                                <?= $form->field($model, 'date_of_expiry[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                            </td>
                        </tr> -->
                    </tbody>
                </table>
                <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Certificate</span>
                <span class="add-endor btn btn-primary" data-id=""><i class="fa fa-plus"></i> Flag endorsement</span>
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="new-coc-row">
    <?= $this->render('/signup/cocrow', [
        'model' => $model,
    ]); ?>
</script>

<script type="text/template" id="new-endor-row">
    <?= $this->render('/signup/endor', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-coc-row\').html();

    $(\'#competency table tbody\').append(tpl);
    $(\'#competency select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'coc-add');
?>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add-endor\',function(){
    var tpl = $(\'#new-endor-row\').html();

    $(\'#competency table tbody\').append(tpl);
    $(\'#competency select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'endor-add');
?>