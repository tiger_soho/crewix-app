<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\CompanyType;
use vova07\imperavi\Widget;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seamansign">
    <div class="row">
        <div class="sign-block vis">
            <div class="bt">
                <h2>Profile</h2>
            </div>
            <div class="cont">
                <img src="/images/sys/profile.png" alt="welcome">
                <div class="text">
                    <h4>Your profile!</h4>
                    <p>Discover and be discovered. It’s quite simple. Let’s start filling your profile. Press continue.</p>
                </div>
            </div>
            <div class="bot">
                <button type="button" class="next btn btn-primary">Coninue</button>
            </div>
        </div>
        <div class="sign-block">
            <div class="bt">
                <h2>Main information</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'company',
                    'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                ]); ?>
            <input type="hidden" id="csrf" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <div class="col-lg-8 col-md-10 col-xs-12">
                <div class="avatar">
                    <div id="images"><img src="/images/companies/logos/noimage.jpg" alt=""></div>
                    <div class="file-desc">
                        <p><strong>Your logo.</strong> We support JPG and PNG files.</p>
                        <p class="tip">The minimum image size is 300x300 pixels</p>
                        <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                            <div class="upload-btn__txt">Select file</div>
                            <?= $form->field($model,
                                'profile_picture')->fileInput(['accept' => 'image/*'])->label(false); ?>
                        </div>
                        <span class="btn remove">Remove</span>
                    </div>
                </div>

                <?= Html::activeHiddenInput($model, "file", ['value' => '']); ?>
                <?= $form->field($model,
                    'name')->textInput(['placeholder' => 'Company name'])->label('Company name <span class="req">*</span>'); ?>
                <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(CompanyType::find()->all(), 'id',
                    'name'), ['prompt' => 'Company type'])->label('Company type <span class="req">*</span>'); ?>
                <div class="addr">
                    <?php
                    $countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
                    $options = array();
                    foreach ($countries as $key => $value) {
                        $options[$key] = ['data-style' => 'background:url(/images/sys/flags/' . Countries::find()->where(['id' => $key])->one()->code_2 . '.png) 0 center no-repeat'];
                    }
                    ?>
                    <div>
                        <label for="">Headquarters address <span class="req">*</span></label>
                    </div>

                    <?= $form->field($model, 'hq_country_id',
                        ['options' => ['class' => 'countries half']])->dropDownList($countries,
                        ['prompt' => 'Select country', 'options' => $options])->label(false); ?>
                    <?= $form->field($model, 'hq_city',
                        ['options' => ['class' => 'half']])->textInput(['placeholder' => 'City'])->label(false); ?>
                    <?= $form->field($model,
                        'hq_address')->textInput(['placeholder' => 'Street and hous/building number'])->label(false); ?>
                </div>
                <?= $form->field($model, 'about')->widget(Widget::className(), [
                    'settings' => [
                        'minHeight' => 160,
                        'buttons' => ['format', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link']
                    ]
                ])->label('About company');; ?>
                    
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        
    </div>
</div>
    <script>
        window.FileAPI = {
            html5: !!1,
            staticPath: '/js/filapi/',
            flashUrl: '/js/filapi/FileAPI.flash.swf',
            flashImageUrl: '/js/filapi/FileAPI.flash.image.swf'
        };
    </script>
    <script src="/js/fileapi/FileAPI.min.js"></script>
    <script>
        var choose = document.getElementById('companies-profile_picture');
        FileAPI.event.on(choose, 'change', function (evt){
            var files = FileAPI.getFiles(evt); // Retrieve file list

            FileAPI.filterFiles(files, function (file, info/**Object*/){
                if( /^image/.test(file.type) ){
                    return  info.width >= 320 && info.height >= 240;
                }
                return  false;
            }, function (files/**Array*/, rejected/**Array*/){
                if( files.length ){
                    // Make preview 100x100
                    FileAPI.each(files, function (file){
                        FileAPI.Image(file).preview(100).get(function (err, img){
                            if (images.hasChildNodes()) {
                                images.removeChild(images.childNodes[0]);
                            }
                            images.appendChild(img);
                            
                        });
                    });

                    // Uploading Files
                    FileAPI.upload({
                        url: '/signup/comupload',
                        data: { '_csrf': $('#csrf').val() },
                        files: { images: files },
                        imageTransform: {
                            'huge': { maxWidth: 300, maxHeight: 300, preview: true },
                            // crop & resize
                            'medium': { width: 100, height: 100, preview: true },
                        },
                        progress: function (evt){ /* ... */ },
                        complete: function (err, xhr){},
                        filecomplete: function (err, xhr, file){
                            console.log(xhr.response);
                            $('input#companies-file').val(xhr.response);
                        }
                    });
                }
            });
        });
    </script>