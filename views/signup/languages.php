<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use kartik\file\FileInput;
use app\models\LanguageLevel;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 23%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block wide vis">
            <div class="bt">
                <h2>Languages</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'lang',
                    'options' => ['class'=>'lang_form'],
                    'validateOnBlur' => true,
                    'validateOnChange' => true,
                ]); ?>
            <div class="col-lg-12">
                <table>
                    <tr>
                        <td class="f-tit lang"><span class="req">*</span> English
                            <?=Html::activeHiddenInput($model, "language_id[]", ['value'=>'1']);?>
                        </td>
                        <td>
                            <?= $form->field($model, 'language_level_id[]')->dropDownList(ArrayHelper::map(LanguageLevel::find()->all(),'id','name'), ['prompt'=>'Select language level'],['placeholder'=>'Select language level'])->label(false);?>
                        </td>
                    </tr>
                </table>
                <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Language</span>
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="new-lang-row">
    <?= $this->render('/signup/row', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
    $(\'body\').on(\'click\',\'.add\',function(){
        var tpl = $(\'#new-lang-row\').html();

        $(\'#lang table tbody\').append(tpl);
        $(\'#lang select\').each(function(i,el){
            $(el).selectmenu({
                width: \'100%\'
            });
        });

    });
    ', \yii\web\View::POS_READY, 'lang-add');
?>