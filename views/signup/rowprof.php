<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanCertificateOfProficiency */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\CertificateOfProficiency;
use kartik\file\FileInput;
use dosamigos\datepicker\DatePicker;

?>

<tr>
    <td>
        <?=Html::activeDropDownList($model,'document_id[]', ArrayHelper::map(CertificateOfProficiency::find()->all(), 'id', 'name'), ['prompt' => 'Select document','class'=>'form-rank form-control']);?>
    </td>
    <td>
        <?=Html::activeTextInput($model, 'number[]', ['class'=>'form-rank form-control','placeholder'=>'Enter number'])?>
    </td>
    <td>
        <?=Html::activeDropDownList($model,'place_of_issue_id[]', ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt' => 'Select country','class'=>'form-country form-control']);?>
        
    </td>
    <td><div class="date"><?=Html::activeTextInput($model,'date_of_issue[]',['placeholder'=>'dd.mm.yyyy','class'=>'form-control']);?></div></td>
    <td>
        <div class="cel-block">
            <div class="date"><?= Html::activeTextInput($model, 'date_of_expiry[]',
                    ['placeholder' => 'dd.mm.yyyy', 'class' => 'form-control']); ?></div>
            <div class="span remove"><i class="fa fa-times"></i></div>
        </div>
    </td>
</tr>