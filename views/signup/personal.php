<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

/*'template' => '{input}<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>{error}{hint}'*/

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Ranks;
use app\models\Users;
use kartik\file\FileInput;
use dosamigos\datepicker\DatePicker;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;

?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 23%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block vis">
            <div class="bt">
                <h2>Personal details</h2>
            </div>
            <div class="cont">
                <img src="/images/sys/profile2.png" alt="Personal details">
                <div class="text">
                    <h4>Your personal information</h4>
                    <p>Crewix was designed to store all of your data in the cloud. Enter your data once, store it forever.<br>Let’s start filling your CV. First things first: your personal details.</p>
                </div>
            </div>
            <div class="bot">
                <button type="button" class="next btn btn-primary">Coninue</button>
            </div>
        </div>
        <div class="sign-block wide">
            <div class="bt">
                <h2>Main information</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'seaman',
                    'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                ]); ?>
                <input type="hidden" id="csrf" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <div class="col-lg-12">
                <table>
                    <tr>
                        <td class="f-tit">Profile picture</td>
                        <td colspan="3">
                            <div class="avatar ph">
                                <div id="images"><img src="/images/sys/profile2.png" alt=""></div>
                                <div class="file-desc">
                                    <p><strong>Your profile picture.</strong> It will be used on the Crewix website.</p>
                                    <p class="tip">We support JPG files. The minimum image size is 300x300 pixels.</p>
                                    <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                                        <div class="upload-btn__txt">Select file</div>
                                        <?= $form->field($model,'profile_picture')->fileInput(['accept'=>'image/jpg'])->label(false); ?>
                                    </div>
                                    <span class="btn remove">Remove</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">CV picture</td>
                        <td colspan="3">
                            <div class="avatar cv">
                                <div id="imagess"><img src="/images/seaman/nocv.jpg" alt=""></div>
                                <div class="file-desc">
                                    <p><strong>Your 3.5x4.5 cm photo.</strong> It will be used in the PDF version of your CV.</p>
                                    <p class="tip">We support JPG files. The minimum image size is 350x450 pixels.</p>
                                    <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                                        <div class="upload-btn__txt">Select file</div>
                                        <?= $form->field($model,'cv_picture')->fileInput(['accept'=>'image/jpg'])->label(false); ?>
                                    </div>
                                    <span class="btn remove">Remove</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Name</td>
                        <td>
                            <?= $form->field($model, 'first_name', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'Name'])->label(false); ?><!--
                         --><?= $form->field($model, 'last_name', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'Surname'])->label(false); ?>
                        </td>
                        <td class="f-tit">Middle name</td>
                        <td>
                            <?= $form->field($model, 'middle_name')->textInput(['placeholder'=>'Middle name'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Rank</td>
                        <td>
                            <?= $form->field($model, 'primary_rank_id', ['options'=>['class'=>'half']])->dropDownList(ArrayHelper::map(Ranks::find()->all(), 'id', 'name'), ['prompt'=>'Primary rank'],['placeholder'=>'Primary rank'])->label(false);?><!--
                         --><?= $form->field($model, 'secondary_rank_id', ['options'=>['class'=>'half']])->dropDownList(ArrayHelper::map(Ranks::find()->all(), 'id', 'name'), ['prompt'=>'Additional rank'],['placeholder'=>'Additional rank'])->label(false);?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Availability</td>
                        <td>
                            <?= $form->field($model, 'status', ['options'=>['class'=>'half']])->dropDownList(['0'=>'Available from...','1'=>'Not available'], ['prompt'=>'Status'],['placeholder'=>'Additional rank'])->label(false);?><!--
                         --><?= $form->field($model, 'availability_date', ['options'=>['class'=>'half date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Desired salary per month</td>
                        <td>
                            <?= $form->field($model, 'salary', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'Enter number'])->label(false); ?><!--
                         --><?= $form->field($model, 'salary_currency_id', ['options'=>['class'=>'half']])->dropDownList(['1'=>'USD','2'=>'EUR'])->label(false);?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Date and place of birth</td>
                        <td>
                            <?= $form->field($model, 'date_of_birth', ['options'=>['class'=>'half date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?><!--
                         --><?= $form->field($model, 'place_of_birth_id', ['options'=>['class'=>'half']])->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'])->label(false);?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> City and country of residence</td>
                        <td>
                            <?= $form->field($model, 'city_of_residence', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'City'])->label(false); ?><!--
                         --><?= $form->field($model, 'country_of_residence_id', ['options'=>['class'=>'half']])->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'])->label(false);?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Home address</td>
                        <td>
                            <?= $form->field($model, 'address')->textInput(['placeholder'=>'Street and house'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Citizenship</td>
                        <td>
                            <?= $form->field($model, 'citizenship_id')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'])->label(false); ?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Closest airport</td>
                        <td>
                            <?= $form->field($model, 'closest_airport')->textInput(['placeholder'=>'City'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Primary phone</td>
                        <td>
                            <?= $form->field($model, 'primary_phone')->textInput()->label(false); ?>
                        </td>
                        <td class="f-tit">Additional phone</td>
                        <td>
                            <?= $form->field($model, 'secondary_phone')->textInput()->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Skype</td>
                        <td>
                            <?= $form->field($model, 'skype')->textInput(['placeholder'=>'Username'])->label(false); ?>
                        </td>
                        <td class="f-tit">Email</td>
                        <td><?=Yii::$app->user->getIdentity()->email?></td>
                    </tr>
                </table>
                    
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 's_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    

</div><!-- seamansign -->
<script>
    window.FileAPI = {
        html5: !!1,
        staticPath: '/js/filapi/',
        flashUrl: '/js/filapi/FileAPI.flash.swf',
        flashImageUrl: '/js/filapi/FileAPI.flash.image.swf'
    };
</script>
<script src="/js/fileapi/FileAPI.min.js"></script>
<script>
    var choose = document.getElementById('seaman-profile_picture');
    FileAPI.event.on(choose, 'change', function (evt){
        var files = FileAPI.getFiles(evt); // Retrieve file list

        FileAPI.filterFiles(files, function (file, info/**Object*/){
            if( /^image/.test(file.type) ){
                return  info.width >= 300 && info.height >= 300;
            }
            return  false;
        }, function (files/**Array*/, rejected/**Array*/){
            if( files.length ){
                // Make preview 100x100
                FileAPI.each(files, function (file){
                    FileAPI.Image(file).preview(100).get(function (err, img){
                        if (images.hasChildNodes()) {
                            images.removeChild(images.childNodes[0]);
                        }
                        images.appendChild(img);
                        
                    });
                });

                // Uploading Files
                FileAPI.upload({
                    url: 'avatarupload',
                    data: { '_csrf': $('#csrf').val() },
                    files: { images: files },
                    imageTransform: {
                        'huge': { width: 300, height: 300, preview: true },
                        // crop & resize
                        'medium': { width: 100, height: 100, preview: true },
                    },
                    progress: function (evt){ /* ... */ },
                    complete: function (err, xhr){},
                    filecomplete: function (err, xhr, file){
                        $('.field-seaman-profile_picture input[type=hidden]').val(file.name);
                    }
                });
            }
        });
    });
</script>
<script>
    var ch = document.getElementById('seaman-cv_picture');
    FileAPI.event.on(ch, 'change', function (evt){
        var files = FileAPI.getFiles(evt); // Retrieve file list

        FileAPI.filterFiles(files, function (file, info/**Object*/){
            if( /^image/.test(file.type) ){
                return  info.width >= 350 && info.height >= 450;
            }
            return  false;
        }, function (files/**Array*/, rejected/**Array*/){
            if( files.length ){
                // Make preview 100x100
                FileAPI.each(files, function (file){
                    FileAPI.Image(file).preview(100,128).get(function (err, img){
                        if (imagess.hasChildNodes()) {
                            imagess.removeChild(imagess.childNodes[0]);
                        }
                        imagess.appendChild(img);
                        
                    });
                });

                // Uploading Files
                FileAPI.upload({
                    url: 'cvupload',
                    data: { '_csrf': $('#csrf').val() },
                    files: { images: files },
                    imageTransform: {
                        width: 350,
                        height: 450,
                        preview: true
                    },
                    progress: function (evt){ /* ... */ },
                    complete: function (err, xhr){},
                    filecomplete: function (err, xhr, file){
                        $('.field-seaman-cv_picture input[type=hidden]').val(file.name);
                    }
                });
            }
        });
    });
    $('#seaman-primary_phone').intlTelInput();
    $('#seaman-secondary_phone').intlTelInput();
</script>

