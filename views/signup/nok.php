<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\NextOfKinRelationship;
use kartik\file\FileInput;

$this->title = 'Create account';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 23%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block wide vis">
            <div class="bt">
                <h2>Next of kin</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'kin',
                    'options' => ['class'=>'family_form'],
                    'validateOnBlur' => false,
                ]); ?>
            <div class="col-lg-12">
                <table>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Full name</td>
                        <td>
                            <?= $form->field($model, 'name')->textInput(['placeholder'=>'Enter name and surname'])->label(false); ?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Relationship</td>
                        <td>
                            <?= $form->field($model, 'relationship_id')->dropDownList(ArrayHelper::map(NextOfKinRelationship::find()->all(), 'id', 'name'), ['prompt'=>'Select relationship'],['placeholder'=>'Select relationship'])->label(false);?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Phone number</td>
                        <td>
                            <?= $form->field($model, 'phone')->textInput(['placeholder'=>'+1 555 555 5555'])->label(false); ?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Home address</td>
                        <td>
                            <?= $form->field($model, 'address')->textInput(['placeholder'=>'Street, house, city and country'])->label(false); ?>
                        </td>
                    </tr>
                </table>
                    
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $('#seamannextofkin-phone').intlTelInput();
</script>