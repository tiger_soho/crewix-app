<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanSeaService */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Ranks;
use app\models\Engines;
use app\models\VesselTypes;
use dosamigos\datepicker\DatePicker;

$this->title = 'Create account';
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 72%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3 active"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4 active"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block vis">
            <div class="bt">
                <h2>Sea service</h2>
            </div>
            <div class="cont">
                <div class="sign-icon sea-icon"><img src="/images/sys/anchor.svg" alt="Personal details"></div>
                <div class="text">
                    <h4>Your sea service</h4>
                    <p>This is the first thing companies look at. But don’t worry if you don’t have sea service yet.<br/>We’ll be glad to help you to start your career with Crewix.</p>
                </div>
            </div>
            <div class="bot">
                <button type="button" class="next btn btn-primary">Coninue</button>
            </div>
        </div>
        <div class="sign-block wide">
            <div class="bt">
                <h2>Sea service</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'serv',
                    'options' => ['class'=>'serv_form'],
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                ]); ?>
            <div class="col-lg-12">
                <table>
                    <thead>
                        <tr>
                            <td>Rank</td>
                            <td>Vessel name / Flag</td>
                            <td>Vessel type / DWT</td>
                            <td>ME type / ME power</td>
                            <td>Shipowner / Crewing...</td>
                            <td>From / To</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?= $form->field($model, 'rank_id[]')->dropDownList(ArrayHelper::map(Ranks::find()->all(), 'id', 'name'), ['prompt'=>'Select rank'],['placeholder'=>'Select rank'])->label(false);?>
                            </td>
                            <td>
                                <?=$form->field($model,'vessel_name[]')->textInput(['placeholder'=>'Vessel name'])->label(false);?>
                                <?= $form->field($model, 'vessel_flag_id[]')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'],['placeholder'=>'Select country'])->label(false);?>
                            </td>
                            <td>
                                <?= $form->field($model, 'vessel_type_id[]')->dropDownList(ArrayHelper::map(VesselTypes::find()->all(), 'id', 'name'), ['prompt'=>'Vessel type'],['placeholder'=>'Vessel type'])->label(false);?>
                                <?=$form->field($model,'vessel_deadweight[]')->textInput(['placeholder'=>'Enter DWT'])->label(false);?>
                            </td>
                            <td>
                                <?= $form->field($model, 'engine_type_id[]')->dropDownList(ArrayHelper::map(Engines::find()->all(), 'id', 'name'), ['prompt'=>'ME type'],['placeholder'=>'ME type'])->label(false);?>
                                <div class="f_blc">
                                    <?=$form->field($model,'engine_power[]')->textInput(['placeholder'=>'ME power'])->label(false);?><?= $form->field($model, 'measure[]')->dropDownList(['kW'=>'kW','BHP'=>'BHP'])->label(false);?>
                                </div>
                            </td>
                            <td>
                                <?=$form->field($model,'shipowning_company_name[]')->textInput(['placeholder'=>'Shipowner'])->label(false);?>
                                <?=$form->field($model,'crewing_company_name[]')->textInput(['placeholder'=>'Crewing agency'])->label(false);?>
                            </td>
                            <td>
                                <?= $form->field($model, 'date_from[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                                <?= $form->field($model, 'date_to[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Contract</span>
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'm_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="sign-block">
            <div class="bt">
                <h2>Finish</h2>
            </div>
            <div class="cont">
                <img src="/images/sys/finish.png" alt="Personal details">
                <div class="text">
                    <h4>You’ve signed up!</h4>
                    <p>All you have to do now is double check your profile and wait until you’re hired. Good luck!</p>
                </div>
            </div>
            <div class="bot">
                <button type="button" class="finish next btn btn-primary">Coninue</button>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="new-sea-row">
    <?= $this->render('/signup/rowserv', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-sea-row\').html();

    $(\'#serv table tbody\').append(tpl);
    $(\'#serv select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'prof-add');
?>