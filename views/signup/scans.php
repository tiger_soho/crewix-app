<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Countries;
use frontend\models\ProficiencyDocs;
use frontend\models\Ranks;
use frontend\models\Engines;
use frontend\models\VesselTypes;
use dosamigos\datepicker\DatePicker;

$this->title = 'Create account';
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 47%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3 active"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block vis">
            <div class="bt">
                <h2>Document scans</h2>
            </div>
            <div class="cont">
                <div class="sign-icon scans-icon"><img src="/images/sys/cloud-ico.svg" alt="Personal details"></div>
                <div class="text">
                    <h4>Store all of your document scans in the cloud</h4>
                    <p>By having your document scans in the cloud you increase the chances of being hired dramatically.<br/r>You’ll be able to upload the scans once you sign up. But for now just press <strong>continue</strong>.</p>
                </div>
            </div>
            <div class="bot">
                <?php $form = ActiveForm::begin(['id' => 'welcome',
                        'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                    ]); ?>
                <input type="submit" name="next" class="btn btn-primary" value="Coninue">
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
