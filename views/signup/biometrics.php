<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Countries;
use kartik\file\FileInput;
use app\models\Sex;
use app\models\EyeColor;

$this->title = 'Create account';
?>
<div class="seamansign">
    <div class="row">
        <div class="wrapper progress-block sign-block wide vis">
            <div class="signup-progress">
                <div class="line" style="width: 23%"></div>
                <div class="step step-1 active"><i class="fa fa-heart"></i><div class="title">Welcome!</div></div>
                <div class="step step-2 active"><i class="fa fa-newspaper-o"></i><div class="title">Profile</div></div>
                <div class="step step-3"><i class="fa fa-th-large"></i><div class="title">Documents</div></div>
                <div class="step step-4"><i class="fa fa-anchor"></i><div class="title">Sea&nbsp;service</div></div>
                <div class="step step-5"><i class="fa fa-check"></i><div class="title">Finish</div></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="sign-block wide vis">
            <div class="bt">
                <h2>Biometrics</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'bio',
                    'options' => ['class'=>'bio_form'],
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                ]); ?>
            <div class="col-lg-12">
                <table class="bio-table">
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Sex</td>
                        <td>
                            <?= $form->field($model, 'sex_id')->dropDownList(
                                ArrayHelper::map(Sex::find()->all(),'id','name'),
                                ['prompt'=>'Select sex',],
                                ['placeholder'=>'Select sex'])->label(false);?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Height <span class="mes">(cm)</span></td>
                        <td>
                            <?= $form->field($model, 'height')->textInput(['placeholder'=>'Enter number'])->label(false); ?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Clothing size</td>
                        
                        <td>
                            <?= $form->field($model, 'clothing_size_id')->dropDownList([''=>'Select clothing size'], ['disabled'=>'disabled'], ['placeholder'=>'Select clothing size'])->label(false);?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Eye color</td>
                        <td>
                            <?= $form->field($model, 'eyes_color_id')->dropDownList(ArrayHelper::map(EyeColor::find()->all(),'id','name'), ['prompt'=>'Select eye color'],['placeholder'=>'Select eye color'])->label(false);?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Weight <span class="mes">(kg)</span></td>
                        <td>
                            <?= $form->field($model, 'weight')->textInput(['placeholder'=>'Enter number'])->label(false); ?>
                        </td>
                        <td class="f-tit"><span class="req">*</span> Shoe size</td>
                        <td>
                            <?= $form->field($model, 'shoe_size_id')->dropDownList([''=>'Select shoe size'], ['disabled'=>'disabled'],['placeholder'=>'Select shoe size'])->label(false);?>
                        </td>
                    </tr>
                </table>
                    
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>