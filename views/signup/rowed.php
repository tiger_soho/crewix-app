<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Langs;
use yii\helpers\BaseHtml;

$current = date('Y');
$from = $current-70;
$to = $current+7;
$range = range($from, $to, 1);
foreach($range as $r){
    $arr[]=['k'=>$r,'y'=>$r];
}
?>

<tr>
	<td><?=Html::activeTextInput($model,'institution_name[]',['placeholder'=>'Enter college / university name','class'=>'form-control']);?></td>
	<td><?=Html::activeTextInput($model,'degree[]',['placeholder'=>'Enter your degree','class'=>'form-control']);?></td>
	<td><?=Html::activeDropDownList($model,'year_from[]', ArrayHelper::map($arr,'k','y'), ['prompt' => 'Select year','class'=>'form-control']);?></td>
    <td>
        <div class="cel-block">
            <?= Html::activeDropDownList($model, 'year_to[]', ArrayHelper::map($arr, 'k', 'y'),
                ['prompt' => 'Select year level', 'class' => 'form-control']); ?>
            <div class="span remove"><i class="fa fa-times"></i></div>
        </div>
    </td>
</tr>
