<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Langs;
use app\models\LanguageLevel;
use yii\helpers\BaseHtml;
?>

<tr>
	<td><?=Html::activeDropDownList($model,'language_id[]', ArrayHelper::map(Langs::find()->where(['!=','id','1'])->all(), 'id', 'name'), ['prompt' => 'Select language','class'=>'form-control']);?></td>
	<td>
		<div class="cel-block">
			<?= Html::activeDropDownList($model, 'language_level_id[]',
				ArrayHelper::map(LanguageLevel::find()->all(), 'id', 'name'),
				['prompt' => 'Select language level', 'class' => 'form-control half']); ?>
			<div class="span remove"><i class="fa fa-times"></i></div>
		</div>
	</td>
</tr>