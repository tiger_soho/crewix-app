<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $item \app\models\Faq */
/* @var $cat \app\models\FaqCategory */


$this->title = 'Questions & Answers';
$this->registerJsFile('/js/bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="block qa-block clearfix">
    <div class="bt clearfix">
        <h1 class="pull-left">Q&amp;A</h1>
        <span class="edit faq_cats pull-right">New question</span>
    </div>
    <div class="cont">
        <?php if (!empty($data) && !empty($cats)): ?>
            <?php foreach ($cats as $cat): ?>
                <div class="heading"><?= $cat->name ?></div>
                <?php foreach ($data as $item): ?>
                    <?php if($item->faq_category_id==$cat->id): ?>

                        <div class="item lang clearfix">
                            <div class="pull-left">
                                <h5 class="name">
                                    <?= $item->question ?>
                                </h5>
                            </div>
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary edit faq_cats" data-id="<?= $item->id; ?>">Edit
                                    </button>
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" class="delete" data-id="<?= $item->id; ?>">
                                                Delete
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="empty">
                <p>There are no questions yet.</p>
                <span class="edit faq_cats">Add question &raquo;</span>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php
$this->registerJs('
    $(\'body\').on(\'click\',\'.edit\',function(){
        var id = $(this).attr(\'data-id\');
        $.ajax({
            url:\'/appsettings/edit-qa\',
            data:{id:id},
            type:\'GET\',
            success:function(data){
                $(\'.block.qa-block\').html(data);
                $(\'select\').each(function(i,el){
                    $(el).selectmenu({
                        width: \'100%\'
                    });
                });
            }
        });
    });

    $(\'body\').on(\'submit\',\'#faq_qa-form\', function(){
        var data = $(this).serialize();
        $.ajax({
            url:\'/appsettings/edit-qa\',
            data:data,
            type:\'POST\',
            complete: function(){
                $.ajax({
                    url:\'/appsettings/qa-list\',
                    success: function(data){
                        $(\'.block.qa-block\').html(data);
                    }
                });
            }
        });
        return false;
    });

    $(\'body\').on(\'click\',\'.delete\',function(){
        var id = $(this).attr(\'data-id\');
        $.ajax({
            url:\'/appsettings/del-qa\',
            data:{id:id},
            complete:function(){
                $.ajax({
                    url:\'/appsettings/qa-list\',
                    success: function(data){
                        $(\'.block.qa-block\').html(data);
                    }
                });
            }
        });
        return false;
    });

    $(\'body\').on(\'click\',\'.cancel\',function(){
        $.ajax({
            url:\'/appsettings/qa-list\',
            success: function(data){
                $(\'.block.qa-block\').html(data);
            }
        });
    });
', \yii\web\View::POS_READY);
?>
