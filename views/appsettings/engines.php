<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget;

$this->title = 'Vessel engines';
?>
    <div class="block engines-block clearfix">
        <div class="bt clearfix">
            <h1 class="pull-left">Vessel engines</h1>
            <span class="edit engines pull-right">Edit</span>
        </div>

        <?php if (!empty($data)): ?>
            <?php foreach ($data as $item): ?>
                <div class="item lang">
                    <?= $item->name ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="empty">
                <p>There is no data yet.</p>
                <span class="edit engines">Add section »</span>
            </div>
        <?php endif; ?>
    </div>

<?php $this->registerJs('
    $(\'body\').on(\'click\',\'.edit.engines\',function(){
        $.ajax({
            url:\'/appsettings/engines-form\',
            success: function(data){
                $(\'.block.engines-block\').html(data);
            }
        });
    });
', \yii\web\View::POS_END); ?>

<?php
$this->registerJs('
        $(\'body\').on(\'click\',\'.cancel\',function(){
            $.ajax({
                url:\'/appsettings/engines-block\',
                success: function(data){
                    $(\'.block.engines-block\').html(data);
                }
            });
        });

        $(\'body\').on(\'submit\',\'#engines-form\',function(){
            var p = $(this).serialize()
            $.ajax({
                url:\'/appsettings/engines-form\',
                data: p,
                type: \'POST\',
                complete: function(data){
                    console.log(data);
                    var response = data.responseText;
                    if(response!=\'\'){
                        var err = JSON.parse(response);
                        console.log(err.name);
                        $.notify( err.name, \'error\');
                    }
                    /*$.ajax({
                        url:\'/appsettings/engines-block\',
                        success:function(data){
						    $(\'.block.engines-block\').html(data);
					    }
				    });*/
                }
            });
            return false;
        });
    ', \yii\web\View::POS_END);
?>