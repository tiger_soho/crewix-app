<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $data \app\models\Langs */
/* @var $item \app\models\Langs */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;

?>

    <div class="block lang-block clearfix">
        <div class="bt clearfix">
            <h1 class="pull-left">Languages</h1>
            <span class="cancel engines pull-right">Cancel</span>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'engines-form',
            'options' => ['class' => 'engines_form'],
            'validateOnBlur' => false,
            'validateOnChange' => false,
        ]); ?>
        <table class="col-lg-12">
            <?php if (!empty($data)): ?>
                <?php foreach ($data as $item): ?>
                    <tr>
                        <td class="f-tit"><?= $item->name ?></td>
                        <td>
                            <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item->id])->label(false) ?>
                            <?= $form->field($model,
                                'name[]')->textInput(['placeholder' => 'New name'])->label(false) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td class="f-tit">New item</td>
                    <td>
                        <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item->id])->label(false) ?>
                        <?= $form->field($model, 'name[]')->textInput(['placeholder' => 'New name'])->label(false) ?>
                    </td>
                </tr>
            <?php endif; ?>
        </table>
        <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Item</span>
        <div class="bot">
            <div class="form-group">
                <?= Html::submitButton('Save',
                    ['class' => 'btn btn-primary', 'name' => 'save-button', 'id' => 'f_save']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <script type="text/template" id="new-engines-row">
        <?= $this->render('/appsettings/engines/engines-row', [
            'model' => $model,
        ]); ?>
    </script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-engines-row\').html();
    $(\'#engines-form table tbody\').append(tpl);
});
', View::POS_READY);
?>