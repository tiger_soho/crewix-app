<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget;

$this->title = 'Vessel engines';
?>

    <div class="bt clearfix">
        <h1 class="pull-left">Terms of service</h1>
        <span class="edit engines pull-right">Edit</span>
    </div>

<?php if (!empty($data)): ?>
    <?php foreach ($data as $item): ?>
        <div class="item lang">
            <?= $item->name ?>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="empty">
        <p>There is no data yet.</p>
        <span class="edit privacy">Add section »</span>
    </div>
<?php endif; ?>