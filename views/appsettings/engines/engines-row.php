<tr>
    <td class="f-tit">New item</td>
    <td>
        <div class="cel-block">
            <?= \yii\bootstrap\Html::activeHiddenInput($model, 'id[]') ?>
            <?= \yii\bootstrap\Html::activeTextInput($model, 'name[]',
                ['placeholder' => 'New name', 'class' => 'form-control']) ?>
            <div class="span remove"><i class="fa fa-times"></i></div>
        </div>
    </td>
</tr>