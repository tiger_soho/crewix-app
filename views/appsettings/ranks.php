<?php
/* @var $group \app\models\RankGroup */
/* @var $item \app\models\Ranks */



?>

<div class="block ranks">
    <div class="bt clearfix">
        <h1 class="pull-left">Ranks</h1>
        <span class="edit rank pull-right">Edit</span>
    </div>
    <div class="cont">
        <?php if(!empty($data) && !empty($groups)): ?>
            <?php foreach($groups as $group): ?>
                <div class="heading">
                    <?=$group->name?>
                </div>
                <?php $count = \app\models\Ranks::find()->where(['group_id'=>$group->id])->count(); ?>
                <?php if($count>0): ?>
                    <?php foreach($data as $item): ?>
                        <?php if($item->group_id): ?>
                            <div class="item lang clearfix">
                                <div class="name">
                                    <?= $item->name ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="empty">There are no items here yet.</div>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="empty">There is no data</div>
        <?php endif; ?>
    </div>
</div>
