<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */


$this->title = 'Travel Documents';
?>

<div class="bt clearfix">
    <h1 class="pull-left">Travel documents</h1>
    <span class="edit travel pull-right">Edit</span>
</div>

<?php if (!empty($data)): ?>
    <?php foreach ($data as $item): ?>
        <div class="item lang">
            <?= $item->name ?>
            <span class="location">
                        <?php if ($item->locked == 1): ?>
                            (Displayed & hardcoded)
                        <?php else: ?>
                            <?php if ($item->always_visible == 1): ?>
                                (Visible)
                            <?php elseif ($item->always_visible == 0): ?>
                                (In dropdown)
                            <?php endif; ?>
                        <?php endif; ?>
                    </span>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="empty">
        <p>There is no data yet.</p>
        <span class="edit travel">Add section »</span>
    </div>
<?php endif; ?>
