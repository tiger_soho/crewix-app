<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $data \app\models\TravelDocument */
/* @var $item \app\models\TravelDocument */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;

?>

    <div class="block lang-block clearfix">
        <div class="bt clearfix">
            <h1 class="pull-left">Languages</h1>
            <span class="cancel travel_docs pull-right">Cancel</span>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'travel-form',
            'options' => ['class' => 'travel_form'],
            'validateOnBlur' => false,
            'validateOnChange' => false,
        ]); ?>
        <table class="col-lg-12">
            <?php if (!empty($data)): ?>
                <?php foreach ($data as $item): ?>
                    <tr>
                        <td class="f-tit"><?= $item->name ?></td>
                        <td>
                            <div class="pull-left">
                                <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item->id])->label(false) ?>
                                <?= $form->field($model, 'name[]')->textInput(['placeholder' => 'New name'])->label(false) ?>
                                <?php if ($item->locked == 1): ?>
                                    <div class="locket-state">Displayed</div>
                                    <?=$form->field($model,'always_visible[]')->hiddenInput(['value'=>1])->label(false)?>
                                <?php else: ?>
                                    <?= $form->field($model, 'always_visible[]',['options'=>['class'=>'form-group drop']])->dropDownList([
                                        '1' => 'Displayed',
                                        '0' => 'In dropdown'
                                    ],
                                        ['options' => [$item->always_visible => ['Selectes' => 'selected']]])->label(false) ?>
                                    <span class="delete-doc">Delete</span>
                                <?php endif; ?>
                            </div>
                            <div class="move pull-right"><i class="fa fa-bars"></i></div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td class="f-tit">New item</td>
                    <td>
                        <div class="pull-left">
                            <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item->id])->label(false) ?>
                            <?= $form->field($model,
                                'name[]')->textInput(['placeholder' => 'New name'])->label(false) ?>
                            <?= $form->field($model, 'always_visible[]',['options'=>['class'=>'drop']])->dropDownList([
                                '1' => 'Displayed',
                                '0' => 'In dropdown'
                            ], ['options' => [$item->always_visible => ['Selectes' => 'selected']]])->label(false) ?>
                            <span class="delete-doc">Delete</span>
                        </div>
                        <div class="move pull-right"><i class="fa fa-bars"></i></div>
                    </td>
                </tr>
            <?php endif; ?>
        </table>
        <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Item</span>
        <div class="bot">
            <div class="form-group">
                <?= Html::submitButton('Save',
                    ['class' => 'btn btn-primary', 'name' => 'save-button', 'id' => 'f_save']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php $this->registerJs('
    $(\'select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
    });
',View::POS_READY) ?>