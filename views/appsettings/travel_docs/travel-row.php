<tr>
    <td class="f-tit">New item</td>
    <td>
        <div class="form-group">
            <?= \yii\bootstrap\Html::activeHiddenInput($model, 'id[]') ?>
            <?= \yii\bootstrap\Html::activeTextInput($model, 'name[]',
                ['placeholder' => 'New name', 'class' => 'form-control']) ?>
        </div>
        <div class="form-group drop">
            <?= \yii\bootstrap\Html::activeDropDownList($model,'always_visible[]',[
                '1' => 'Displayed',
                '0' => 'In dropdown'
            ],['class'=>'form-control']); ?>
        </div>
        <span class="delete-doc">Delete</span>
        <div class="move pull-right"><i class="fa fa-bars"></i></div>
    </td>
</tr>