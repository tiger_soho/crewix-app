<?php
/**
 * @var $form \yii\bootstrap\ActiveForm;
 * @var $groups \app\models\VesselTypeGroup
 * @var $group \app\models\VesselTypeGroup
 * @var $item \app\models\VesselTypes
 * @var $model \app\models\VesselTypes
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use app\models\VesselTypeGroup;

?>

<div class="bt clearfix">
    <h1 class="pull-left">Vessel types</h1>
    <span class="cancel group-edit pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'edit_group-form',
    'options' => ['class' => 'edit_group_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
<table class="table">
    <tr class="heading">
        <td class="f-tit"><?= $group->name ?></td>
        <td colspan="2">
            <?= $form->field($group, 'id')->hiddenInput(['value' => $group->id])->label(false) ?>
            <?= $form->field($groups, 'name')->textInput(['placeholder' => 'New name'])->label(false) ?>
        </td>
    </tr>
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $item): ?>
            <tr>
                <td class="f-tit">
                    <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item->id])->label(false) ?>
                    <?= $item->name ?>
                </td>
                <td><?= $form->field($model, 'name[]')->textInput(['placeholder' => 'New name'])->label(false) ?></td>
                <td>
                    <?= $form->field($model,
                        'group_id[]')->dropDownList(ArrayHelper::map(VesselTypeGroup::find()->all(), 'id', 'name'),
                        ['options' => [$item->group_id => ['Selected' => 'selected']]])->label(false) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
</table>
<div class="bot">
    <?= Html::submitButton('Save',
        ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
    <span class="btn btn-default group-add cancel">Cancel</span>
</div>
<?php ActiveForm::end(); ?>
