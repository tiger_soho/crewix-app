<?php
/**
 * @var $form \yii\bootstrap\ActiveForm;
 * @var $model \app\models\VesselTypeGroup
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>

<div class="bt clearfix">
    <h1 class="pull-left">Vessel types</h1>
    <span class="cancel group-add pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'vessel_group-form',
    'options' => ['class' => 'vessel_group_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>

<table class="table">
    <tr>
        <td class="f-tit">Group name</td>
        <td>
            <?= $form->field($model,'name')->textInput(['placeholder'=>'Enter group name'])->label(false); ?>
        </td>
    </tr>
</table>
<div class="bot">
    <?= Html::submitButton('Save',
        ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
    <span class="btn btn-default group-add cancel">Cancel</span>
</div>
<?php ActiveForm::end(); ?>
