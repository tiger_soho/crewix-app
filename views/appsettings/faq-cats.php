<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $item \app\models\FaqCategory */


$this->title = 'FAQ Categories';
$this->registerJsFile('/js/bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<div class="block fcats-block clearfix">
    <div class="bt clearfix">
        <h1 class="pull-left">Categories</h1>
        <span class="edit faq_cats pull-right">New category</span>
    </div>

    <?php if (!empty($data)): ?>
        <?php foreach ($data as $item): ?>
            <div class="item lang clearfix">
                <div class="pull-left">
                    <div class="name">
                        <?= $item->name ?>
                    </div>
                    <div class="link">
                        <?= \yii\bootstrap\Html::a(Yii::$app->urlManager->createAbsoluteUrl(['/faq/' . $item->url]),
                            Yii::$app->urlManager->createAbsoluteUrl(['/faq/' . $item->url]), ['target' => '_blank']) ?>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary edit faq_cats" data-id="<?= $item->id; ?>">Edit</button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="delete" data-id="<?= $item->id; ?>">
                                    Delete
                                </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="empty">
            <p>There is no data yet.</p>
            <span class="edit faq_cats" data-id="">Add section »</span>
        </div>
    <?php endif; ?>
</div>

<?php
$this->registerJs('
    $(\'body\').on(\'click\',\'.edit\', function(){
        var id = $(this).attr(\'data-id\');
        $.ajax({
            url:\'/appsettings/edit-cat\',
            data:{id:id},
            type:\'GET\',
            success: function(data){
                $(\'.fcats-block\').html(data);
            }
        });
    });

    $(\'body\').on(\'submit\',\'form#faq_cat-form\', function(){
        var data = $(this).serialize();
        $.ajax({
            url:\'/appsettings/edit-cat\',
            data:data,
            type:\'POST\',
            complete:function(){
                $.ajax({
                    url:\'/appsettings/cat-list\',
                    success:function(data){
                        $(\'.fcats-block\').html(data);
                    }
                });
            }
        });
        return false;
    });

    $(\'body\').on(\'click\',\'.cancel\',function(){
        $.ajax({
            url:\'/appsettings/cat-list\',
            success:function(data){
                $(\'.fcats-block\').html(data);
            }
        });
    });
    $(\'body\').on(\'click\',\'.delete\', function(){
        var id = $(this).attr(\'data-id\');
        $.ajax({
            url:\'/appsettings/del-cat\',
            data:{id:id},
            type:\'GET\',
            complete:function(){
                $.ajax({
                    url:\'/appsettings/cat-list\',
                    success:function(data){
                        $(\'.fcats-block\').html(data);
                    }
                });
            }
        });
        return false;
    });
',\yii\web\View::POS_READY);

?>