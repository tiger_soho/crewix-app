<?php
/**
 * @var $group \app\models\VesselTypeGroup
 * @var $item \app\models\VesselTypes
 * @var $this \yii\web\View
 */

use app\models\VesselTypes;
use yii\web\View;

?>

<div class="block vessels-block clearfix">
    <div class="bt clearfix">
        <h1 class="pull-left">Vessel types</h1>
        <span class="edit add-group pull-right">Add group</span>
    </div>
    <div class="cont">
        <?php if(!empty($groups)): ?>
            <?php foreach($groups as $group): ?>
                <div class="group">
                    <div class="heading clearfix">
                        <span class="pull-left"><?=$group->name?></span>
                        <span class="edit group pull-right" data-id="<?= $group->id ?>">Edit</span>
                    </div>
                    <?php $count = VesselTypes::find()->where(['group_id'=>$group->id])->count(); ?>
                    <?php if($count>0): ?>
                        <?php foreach($data as $item): ?>
                            <?php if($item->group_id==$group->id): ?>
                                <div class="item lang clearfix">
                                    <div class="name">
                                        <?= $item->name ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="empty">There is no vessel types in this group</div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="empty">There is no data yet</div>
        <?php endif; ?>
    </div>
</div>

<?php $this->registerJs('
    $(\'body\').on(\'click\',\'.add-group\',function(){
        $.ajax({
            url:\'/appsettings/add-vessel-group\',
            success: function(data){
                $(\'.vessels-block\').html(data);
            }
        });
    });

    $(\'body\').on(\'submit\',\'#vessel_group-form\',function(){
        var data = $(this).serialize();
        $.ajax({
            url:\'/appsettings/add-vessel-group\',
            data:data,
            type:\'POST\',
            success:function(data){
                $(\'.vessels-block\').html(data);
            }
        });
        return false;
    });

    $(\'body\').on(\'click\',\'.group-add.cancel\',function(){
        $.get(\'/appsettings/vesselsdata\',function(data){
            $(\'.vessels-block\').html(data);
        });
    });

    $(\'body\').on(\'click\',\'.edit.group\',function(){
        var id = $(this).attr(\'data-id\');
        $.ajax({
            url:\'/appsettings/vgroup-edit\',
            data:{id:id},
            success:function(data){
                $(\'.vessels-block\').html(data);
            }
        });
    });

    $(\'body\').on(\'submit\',\'#edit_group-form\',function(){
        var data = $(this).serialize();
        $.ajax({
            url:\'/appsettings/vgroup-edit\',
            data:data,
            type:\'POST\',
            complete:function(data){
                 $.get(\'/appsettings/vesselsdata\',function(data){
                    $(\'.vessels-block\').html(data);
                });
            }
        });
        return false;
    })
',View::POS_READY); ?>
