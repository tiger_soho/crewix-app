<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget;

$this->title = 'Languages';
?>
    <div class="block lang-block clearfix">
        <div class="bt clearfix">
            <h1 class="pull-left">Languages</h1>
            <span class="edit languages pull-right">Edit</span>
        </div>

        <?php if (!empty($data)): ?>
            <?php foreach ($data as $item): ?>
                <div class="item lang">
                    <?= $item->name ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="empty">
                <p>There is no data yet.</p>
                <span class="edit privacy">Add section »</span>
            </div>
        <?php endif; ?>
    </div>

<?php $this->registerJs('
    $(\'body\').on(\'click\',\'.edit.languages\',function(){
        $.ajax({
            url:\'/appsettings/languages-form\',
            success: function(data){
                $(\'.block.lang-block\').html(data);
            }
        });
    });
', \yii\web\View::POS_END); ?>

<?php
$this->registerJs('
        $(\'body\').on(\'click\',\'.cancel\',function(){
            $.ajax({
                url:\'/appsettings/lang-block\',
                success: function(data){
                    $(\'.block.lang-block\').html(data);
                }
            });
        });

        $(\'body\').on(\'submit\',\'#lang-form\',function(){
            var p = $(this).serialize()
            $.ajax({
                url:\'/appsettings/languages-form\',
                data: p,
                type: \'POST\',
                complete: function(){
                    $.ajax({
                        url:\'/appsettings/lang-block\',
                        success:function(data){
						    $(\'.block.lang-block\').html(data);
					    }
				});
                }
            });
            return false;
        });
    ', \yii\web\View::POS_END);
?>