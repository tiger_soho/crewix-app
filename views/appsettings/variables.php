<?php
/*
 *  @var $this yii\web\View
 *  @var $currencies app\models\CurrencyRate
 *  @var $meta app\models\Page
 * */

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\web\View;

$this->title = 'Variables';
?>

<div class="block clearfix" id="variables">
    <div class="bt clearfix">
        <h1 class="pull-left">Variables</h1>
        <span class="edit vars pull-right">Edit</span>
    </div>
    <table class="table">
        <tr>
            <td class="heading" colspan="2">Currencies</td>
        </tr>
        <?php if (!empty($currencies)): ?>
            <?php foreach ($currencies as $item): ?>
                <tr>
                    <td class="f-tit">
                        <?= $item->currencyFrom->code ?>/<?= $item->currencyTo->code ?>
                    </td>
                    <td><?= $item->value ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td class="empty" colspan="2">
                    There is no data yet
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td class="heading">Meta descriptions</td>
        </tr>
        <?php if (!empty($meta)): ?>
            <?php foreach ($meta as $item): ?>
                <tr>
                    <td class="f-tit">
                        <div class="title"><?= $item->name ?></div>
                        <?php if (!empty($item->url)): ?>
                            <div class="url">
                                <?= Html::a('crewix.com' . $item->url,
                                    Yii::$app->urlManager->createAbsoluteUrl([$item->url])) ?>
                            </div>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (!empty($item->meta_description)) { ?>
                            <?= $item->meta_description ?>
                        <?php } else { ?>
                            <span class="no-data">No description</span>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td class="empty" colspan="2">
                    There is no data yet
                </td>
            </tr>
        <?php endif; ?>
    </table>
</div>
<script type="text/template" id="var-form">
    <?= $this->render('/appsettings/variables/_form', [
        'meta' => $meta,
        'currencies' => $currencies,
        'model' => new \app\models\VarsForm(),
    ]); ?>
</script>

<script type="text/template" id="var-content">
    <?= $this->render('/appsettings/variables/_content', [
        'meta' => $meta,
        'currencies' => $currencies,
    ]); ?>
</script>

<?php
$this->registerJs('
        $(\'body\').on(\'click\',\'.edit.vars\',function(){
            $.ajax({
                url: \'/appsettings/vars-form\',
                success: function(data){
                    $(\'#variables\').html(data);
                }
            });
        });
        $(\'body\').on(\'click\',\'.vars-cancel\',function(){
            $.ajax({
                url: \'/appsettings/vars-save\',
                success: function(data){
                    $(\'#variables\').html(data);
                }
            });
        });

        $(\'body\').on(\'submit\',\'#vars-form\',function(){
            var f = $(this).serialize();
            $.ajax({
                url: \'/appsettings/vars-save\',
                data: f,
                type: \'POST\',
                success: function(data){
                    $(\'#variables\').html(data);
                }
            });
            return false;
        });
    ', View::POS_READY);
?>
