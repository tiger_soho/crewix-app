<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget;
$this->title = 'Dashboard';
$this->registerJsFile('/js/bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="block clearfix">
    <div class="bt clearfix">
        <h1 class="pull-left">Terms of service</h1>
        <span class="edit term pull-right">Add section »</span>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'terms',
        'options' => ['class'=>'family_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>
    <?php if(!empty($data)):
        $arr = ArrayHelper::map($data,'id','section_name');
        $row = [];
        foreach ($arr as $item =>$value) {
            $row[] = '<div class="term-name pull-left" data-id="'.$item.'">'.$value.'</div>
                        <div class="btn-group pull-right">
              <button type="button" class="btn btn-primary term-edit">Edit</button>
              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#" class="term-del">delete</a></li>
              </ul>
            </div>';
        }
    ?>
        <div class="sections">

                <?= \yii\jui\Sortable::widget([
                    'items' => $row,
                    'options' => ['tag' => 'ul'],
                    'itemOptions' => ['tag' => 'li','class'=>'clearfix'],
                    'clientOptions' => ['cursor' => 'move','containment'=>'parent'],
                    'clientEvents'=> ['update' => new \yii\web\JsExpression("
                        function(event,ui){
                            $('.ui-sortable').children().each(function (i,el) {
                                console.log(($(el).index()+1),$(el).find('.term-name').attr('data-id'));
                                $.ajax({
                                    url:'/dashboard/savetermorder',
                                    data:{id:$(el).find('.term-name').attr('data-id'),position:($(el).index()+1)},
                                    type:'GET',
                                    success:function(){

                                    }
                                });
                            })
                        }
                    ")]
                ]);?>

        </div>
    <?php else: ?>
        <div class="empty">
            <p>There is no data yet.</p>
            <span class="edit term">Add section »</span>
        </div>
    <?php endif; ?>
</div>


<?php
    $this->registerJs('
        $(\'body\').on(\'click\',\'.edit.term\',function(){
            $.ajax({
                url:\'/appsettings/termsform\',
                success:function(data){
                    $(\'#terms\').parent().html(data);
                }
            });
        });
        $(\'body\').on(\'click\',\'.term-edit\',function(){
            var id = $(this).parents(\'li\').find(\'.term-name\').attr(\'data-id\');
            $.ajax({
                url:\'/appsettings/termsform\',
                type:\'GET\',
                data:{id:id},
                success: function(data){
                    $(\'#terms\').parent().html(data);
                }
            });
        });
        $(\'body\').on(\'submit\',\'form#terms\',function(){
            var data = $(this).serialize();
            $.ajax({
                url:\'/appsettings/termsform\',
                data: data,
                type: \'POST\',
                complete:function(){
                    $.ajax({
                        url:\'/appsettings/terms-data\',
                        success:function(data){
                            $(\'#terms\').parent().html(data);
                        }
                    });
                }
            });
            return false;
        });
        $(\'body\').on(\'click\',\'.terms-cancel\',function(){
            $.ajax({
                url:\'/appsettings/terms-data\',
                success:function(data){
                    $(\'#terms\').parent().html(data);
                }
            });
        });
    ',\yii\web\View::POS_READY);
?>