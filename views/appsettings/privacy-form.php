<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;
$this->title = 'Dashboard';
?>
<div class="bt">
    <?php if(!empty($data)): ?>
        <h1><?=$data->section_name?></h1>
    <?php else: ?>
        <h1>New section</h1>
    <?php endif; ?>

</div>
<?php $form = ActiveForm::begin(['id' => 'privacy',
    'options' => ['class'=>'family_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>

<?php
if(!empty($data)){
    $model->section_name = $data->section_name;
    $model->text = $data->text;
}
?>
<table class="table">
    <tr>
        <td class="heading" colspan="2">Section</td>
    </tr>
    <tr>
        <td class="f-tit">Name</td>
        <td>
            <?php if(!empty($data->id)): ?>
                <?=$form->field($model,'id')->hiddenInput(['value'=>$data->id])->label(false)?>
            <?php endif; ?>
                <?=$form->field($model, 'section_name')->textInput(['placeholder'=>'Enter section name'])->label(false)?>
        </td>
    </tr>
    <tr>
        <td class="heading" colspan="2">Text</td>
    </tr>
    <tr>
        <td class="" colspan="2">
            <?=$form->field($model, 'text')->widget(Widget::className(), [
                'settings' => [
                    'minHeight' => 200,
                    'buttons' => ['format', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link']
                ]
            ])->label(false);?>
        </td>
    </tr>
</table>
<div class="bot">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
    <span class="btn btn-default privacy-cancel">Cancel</span>
</div>


