<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;

$this->title = 'Dashboard';
?>
    <div class="bt">
        <h1>New section</h1>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'terms',
        'options' => ['class'=>'family_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>

    <?php
        if(!empty($data)){
            $model->section_name = $data->section_name;
            $model->text = $data->text;
        }
    ?>
        <table class="table">
            <tr>
                <td class="heading" colspan="2">Section</td>
            </tr>
            <tr>
                <td class="f-tit">Name</td>
                <td>
                    <?php if(isset($data->id)): ?>
                        <?=$form->field($model,'id')->hiddenInput(['value'=>$data->id])->label(false)?>
                    <?php endif; ?>
                    <?=$form->field($model, 'section_name')->textInput(['placeholder'=>'Enter section name'])->label(false)?>
                </td>
            </tr>
            <tr>
                <td class="heading" colspan="2">Text</td>
            </tr>
            <tr>
                <td class="" colspan="2">
                    <?=$form->field($model, 'text')->widget(Widget::className(), [
                        'settings' => [
                            'minHeight' => 200,
                            'buttons' => ['format', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link']
                        ]
                    ])->label(false);?>
                </td>
            </tr>
        </table>
        <div class="bot">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
            <span class="btn btn-default terms-cancel">Cancel</span>
        </div>


