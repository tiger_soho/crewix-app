<?php

/* @var $this yii\web\View */
/* @var $form ActiveForm */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Currency;

//$curr = Currency::findAll(['!=','id','1']);

?>

    <div class="bt clearfix">
        <h1 class="pull-left">Variables</h1>
        <span class="vars-cancel pull-right">Cancel</span>
    </div>
<?php $form = ActiveForm::begin([
    'id' => 'vars-form',
    'options' => ['class' => 'vars_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
    <table class="table">
        <tr>
            <td class="heading" colspan="2">Currencies</td>
        </tr>
        <?php if (!empty($currencies)): ?>
            <?php foreach ($currencies as $item): ?>
                <tr>
                    <td class="f-tit"><?= $item->currencyFrom->code ?>/<?= $item->currencyTo->code ?></td>
                    <td>
                        <?= $form->field($model, 'type[]')->hiddenInput(['value' => '1'])->label(false) ?>
                        <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item->id])->label(false) ?>
                        <?= $form->field($model, 'value[]')->textInput([
                            'placeholder' => 'Enter value',
                            'value' => $item->value
                        ])->label(false) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        <tr>
            <td class="heading">Meta descriptions</td>
        </tr>
        <?php if (!empty($meta)): ?>
            <?php foreach ($meta as $item): ?>
                <tr>
                    <td class="f-tit">
                        <div class="title"><?= $item->name ?></div>
                        <?php if (!empty($item->url)): ?>
                            <div class="url">
                                <?= Html::a('crewix.com' . $item->url,
                                    Yii::$app->urlManager->createAbsoluteUrl([$item->url])) ?>
                            </div>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?= $form->field($model, 'type[]')->hiddenInput(['value' => '2'])->label(false) ?>
                        <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item->id])->label(false) ?>
                        <?= $form->field($model,
                            'meta_description[]')->textarea(['value' => $item->meta_description])->label(false) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
    <div class="bot">
        <?= Html::submitButton('Save',
            ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
        <span class="btn btn-default vars-cancel">Cancel</span>
    </div>
<?php ActiveForm::end(); ?>