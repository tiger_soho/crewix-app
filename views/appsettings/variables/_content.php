<?php

use yii\bootstrap\Html;

?>

<div class="bt clearfix">
    <h1 class="pull-left">Variables</h1>
    <span class="edit vars pull-right">Edit</span>
</div>
<table class="table">
    <tr>
        <td class="heading" colspan="2">Currecies</td>
    </tr>
    <?php if (!empty($currencies)): ?>
        <?php foreach ($currencies as $item): ?>
            <tr>
                <td class="f-tit">
                    <?= $item->currencyFrom->code ?>/<?= $item->currencyTo->code ?>
                </td>
                <td><?= $item->value ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td class="empty" colspan="2">
                There is no data yet
            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <td class="heading">Meta descriptions</td>
    </tr>
    <?php if (!empty($meta)): ?>
        <?php foreach ($meta as $item): ?>
            <tr>
                <td class="f-tit">
                    <div class="title"><?= $item->name ?></div>
                    <?php if (!empty($item->url)): ?>
                        <div class="url">
                            <?= Html::a('crewix.com' . $item->url,
                                Yii::$app->urlManager->createAbsoluteUrl([$item->url])) ?>
                        </div>
                    <?php endif; ?>
                </td>
                <td>
                    <?php if (!empty($item->meta_description)) { ?>
                        <?= $item->meta_description ?>
                    <?php } else { ?>
                        <span class="no-data">No description</span>
                    <?php } ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td class="empty" colspan="2">
                There is no data yet
            </td>
        </tr>
    <?php endif; ?>
</table>

