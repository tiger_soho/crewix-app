<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $item \app\models\TravelDocument */



$this->title = 'Travel documents';
?>
    <div class="block travel-block clearfix">
        <div class="bt clearfix">
            <h1 class="pull-left">Travel documents</h1>
            <span class="edit travel pull-right">Edit</span>
        </div>

        <?php if (!empty($data)): ?>
            <?php foreach ($data as $item): ?>
                <div class="item lang">
                    <?= $item->name ?>
                    <span class="location">
                        <?php if ($item->locked == 1): ?>
                            (Displayed & hardcoded)
                        <?php else: ?>
                            <?php if ($item->always_visible == 1): ?>
                                (Visible)
                            <?php elseif ($item->always_visible == 0): ?>
                                (In dropdown)
                            <?php endif; ?>
                        <?php endif; ?>
                    </span>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="empty">
                <p>There is no data yet.</p>
                <span class="edit travel">Add section »</span>
            </div>
        <?php endif; ?>
    </div>

<?php $this->registerJs('
    $(\'body\').on(\'click\',\'.edit.travel\',function(){
        $.ajax({
            url:\'/appsettings/travel-form\',
            success: function(data){
                $(\'.block.travel-block\').html(data);
            }
        });
    });
', \yii\web\View::POS_END); ?>

<?php
$this->registerJs('
        $(\'body\').on(\'click\',\'.cancel\',function(){
            $.ajax({
                url:\'/appsettings/travel-block\',
                success: function(data){
                    $(\'.block.travel-block\').html(data);
                }
            });
        });

        $(\'body\').on(\'submit\',\'#travel-form\',function(){
            var p = $(this).serialize()
            $.ajax({
                url:\'/appsettings/travel-form\',
                data: p,
                type: \'POST\',
                complete: function(data){
                    console.log(data);
                    var response = data.responseText;
                    if(response!=\'\'){
                        var err = JSON.parse(response);
                        console.log(err.name);
                        $.notify( err.name, \'error\');
                    }else{
                        $.ajax({
                            url:\'/appsettings/travel-block\',
                            success:function(data){
                                $(\'.block.travel-block\').html(data);
                            }
                        });
				    }
                }
            });
            return false;
        });

        $(\'body\').on(\'click\',\'.delete-doc\',function(){
            var id = $(this).parents(\'td\').find(\'input[type=hidden]\').val();
            if(id!=\'\'){
                $.ajax({
                    url:\'/appsettings/delete-doc\',
                    data:{id:id},
                    type:\'GET\',
                    success: function(data){

                    }
                });
            }
            $(this).parents(\'tr\').remove();
        })

    ', \yii\web\View::POS_END);
?>
<script type="text/template" id="new-travel-row">
    <?= $this->render('/appsettings/travel_docs/travel-row', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-travel-row\').html();
    $(\'#travel-form table tbody\').append(tpl);
});
', \yii\web\View::POS_READY);
?>
