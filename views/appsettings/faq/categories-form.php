<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\FaqCategory */
/* @var $data \app\models\FaqCategory */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>

<div class="bt clearfix">
    <h1 class="pull-left">
        <?php if (!empty($data)) { ?>
            <?= $data->name ?>
        <?php }else{ ?>
            New category
        <?php } ?>
    </h1>
    <span class="cancel faq-cat pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin([
    'id' => 'faq_cat-form',
    'options' => ['class' => 'faq_cat_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
<table class="col-lg-12">
    <?php if (!empty($data)) { ?>
        <tr>
            <td class="f-tit"><span class="req">*</span> Category name</td>
            <td>
                <?= $form->field($model, 'id')->hiddenInput(['value' => $data->id])->label(false) ?>
                <?= $form->field($model, 'name')->textInput([
                    'placeholder' => 'Enter category name',
                    'value' => $data->name
                ])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Category icon</td>
            <td>
                <?= $form->field($model, 'icon')->textInput([
                    'placeholder' => 'Enter icon name',
                    'value' => $data->icon
                ])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> URL</td>
            <td>
                <?= $form->field($model, 'url', [
                    'template' => '<span class="input-group-addon" id="basic-addon3">crewix.com/faq/</span> {input}',
                    'options' => ['class' => 'input-group'],
                ])->textInput([
                    'placeholder' => 'Enter users link',
                    'aria-describedby' => 'basic-addon3',
                    'value' => $data->url
                ]); ?>
            </td>
        </tr>
    <?php } else { ?>
        <tr>
            <td class="f-tit"><span class="req">*</span> Category name</td>
            <td>
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Enter category name'])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Category icon</td>
            <td>
                <?= $form->field($model, 'icon')->textInput(['placeholder' => 'Enter icon name'])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> URL</td>
            <td>
                <?= $form->field($model, 'url', [
                    'template' => '<span class="input-group-addon" id="basic-addon3">crewix.com/faq/</span> {input}',
                    'options' => ['class' => 'input-group'],
                ])->textInput([
                    'placeholder' => 'Enter users link',
                    'aria-describedby' => 'basic-addon3'
                ])->label(false); ?>
            </td>
        </tr>
    <?php } ?>
</table>
<div class="bot">
    <?= Html::submitButton('Save',
        ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
    <span class="btn btn-default cats cancel">Cancel</span>

</div>
<?php ActiveForm::end(); ?>
