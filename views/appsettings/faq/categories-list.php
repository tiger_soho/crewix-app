<div class="bt clearfix">
    <h1 class="pull-left">Categories</h1>
    <span class="edit faq_cats pull-right">New category</span>
</div>

<?php if (!empty($data)): ?>
    <?php foreach ($data as $item): ?>
        <div class="item lang clearfix">
            <div class="pull-left">
                <div class="name">
                    <?= $item->name ?>
                </div>
                <div class="link">
                    <?=\yii\bootstrap\Html::a(\yii\helpers\Url::to('/faq',['alias'=>$item->url]),\yii\helpers\Url::to('/faq',['alias'=>$item->url]),['target'=>'_blank'])?>
                </div>
            </div>
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary edit faq_cats" data-id="<?= $item->id; ?>">Edit</button>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="delete" data-id="<?= $item->id; ?>">
                                Delete
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="empty">
        <p>There is no data yet.</p>
        <span class="edit faq_cats" data-id="">Add section »</span>
    </div>
<?php endif; ?>