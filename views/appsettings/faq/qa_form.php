<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Faq */
/* @var $data \app\models\Faq */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget;
use yii\helpers\VarDumper;

?>

    <div class="bt clearfix">
        <h1 class="pull-left">
            <?php if (!empty($data)) { ?>
                Edit Question
            <?php } else { ?>
                New Question
            <?php } ?>
        </h1>
        <span class="cancel faq-cat pull-right">Cancel</span>
    </div>
<?php $form = ActiveForm::begin([
    'id' => 'faq_qa-form',
    'options' => ['class' => 'faq_qa_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
<?php if (!empty($data)) { ?>
    <?php $model->question = $data->question; ?>
    <?php $model->answer = $data->answer; ?>
    <div class="heading">Category</div>
    <div class="form-line">
        <?= $form->field($model, 'id')->hiddenInput(['value' => $data->id])->label(false) ?>
        <?= $form->field($model,
            'faq_category_id')->dropDownList(ArrayHelper::map(\app\models\FaqCategory::find()->all(), 'id', 'name'),
            ['options' => [$data->faq_category_id => ['Selected' => 'selected']]])->label(false); ?>
    </div>
    <div class="heading">Question</div>
    <div class="form-line">
        <?= $form->field($model, 'question')->widget(Widget::className(), [
            'settings' => [
                'minHeight' => 200,
                'buttons' => ['bold', 'italic', 'unorderedlist', 'orderedlist']
            ],
            'value' => $data->question,
        ])->label(false); ?>
    </div>
    <div class="heading">Answer</div>
    <div class="form-line">
        <?= $form->field($model, 'answer')->widget(Widget::className(), [
            'settings' => [
                'minHeight' => 200,
                'buttons' => ['bold', 'italic', 'unorderedlist', 'orderedlist']
            ],
            'value' => $data->answer,
        ])->label(false); ?>
    </div>
<?php } else { ?>
    <div class="heading">Category</div>
    <div class="form-line">
        <?= $form->field($model,
            'faq_category_id')->dropDownList(ArrayHelper::map(\app\models\FaqCategory::find()->all(), 'id',
            'name'))->label(false); ?>
    </div>
    <div class="heading">Question</div>
    <div class="form-line">
        <?= $form->field($model, 'question')->widget(Widget::className(), [
            'settings' => [
                'minHeight' => 200,
                'buttons' => ['bold', 'italic', 'unorderedlist', 'orderedlist']
            ],
        ])->label(false); ?>
    </div>
    <div class="heading">Answer</div>
    <div class="form-line">
        <?= $form->field($model, 'answer')->widget(Widget::className(), [
            'settings' => [
                'minHeight' => 200,
                'buttons' => ['bold', 'italic', 'unorderedlist', 'orderedlist']
            ],
        ])->label(false); ?>
    </div>
<?php } ?>
    <div class="bot">
        <?= Html::submitButton('Save',
            ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
        <span class="btn btn-default qa cancel">Cancel</span>

    </div>
<?php ActiveForm::end(); ?>