<div class="bt clearfix">
    <h1 class="pull-left">Q&amp;A</h1>
    <span class="edit faq_cats pull-right">New question</span>
</div>
<div class="cont">
    <?php if (!empty($data) && !empty($cats)): ?>
    <?php foreach ($cats as $cat): ?>
    <div class="heading"><?= $cat->name ?></div>
    <?php foreach ($data as $item): ?>
    <div class="item lang clearfix">
        <div class="pull-left">
            <h3 class="name">
                <?= $item->name ?>
            </h3>
        </div>
        <div class="pull-right">
            <div class="btn-group">
                <button type="button" class="btn btn-primary edit faq_cats" data-id="<?= $item->id; ?>">Edit
                </button>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" class="delete" data-id="<?= $item->id; ?>">
                            Delete
                    </a></li>
                </ul>
            </div>
        </div>
        <?php endforeach; ?>
        <?php endforeach; ?>
        <?php else: ?>
            <div class="empty">
                <p>There are no questions yet.</p>
                <span class="edit faq_cats">Add question &raquo;</span>
            </div>
        <?php endif; ?>
    </div>
</div>