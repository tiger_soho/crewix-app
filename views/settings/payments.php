<?php

/* @var $this yii\web\View */
/* @var $item \app\models\Payment */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'Security settings';
?>

    <div class="block payments top-left clearfix">
        <div class="bt">
            <h2>Balance</h2>
        </div>
        <div class="cont">
            <div class="icon balance-ico">
                <img src="/images/sys/dollar.svg" alt="Ballance"/>
            </div>
            <div class="info">
                <div class="current-balance">
                    You have: <span class="balance">$<?= Yii::$app->user->identity->balance ?></span>
                </div>
                <div class="text">You can add funds to your account to pay for Crewix services.</div>
                <?= \yii\bootstrap\Html::button('Add funds', ['class' => 'btn btn-primary pay']); ?>
            </div>
        </div>
    </div>

    <div class="block top-left clearfix">
        <div class="bt">
            <h2>Billing history</h2>
        </div>
        <table class="bill col-lg-12 table">
            <thead>
            <tr>
                <td>Amount</td>
                <td>Description</td>
                <td>Date</td>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($data)): ?>
                <?php foreach ($data as $item): ?>
                    <tr>
                        <td>$<?= $item->amount ?></td>
                        <td><?= $item->type->name ?></td>
                        <td><?= Yii::$app->formatter->asDate($item->created_at, 'MMMM dd, y') ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td class="empdy" colspan="3">
                        You have no billing history
                    </td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

<?php
$this->registerJs('
        $(\'body\').on(\'click\',\'button.pay\',function(){
            $.ajax({
                url:\'/settings/pay\',
                success:function(data){
                    $(\'.block.payments\').next(\'.block\').remove();
                    $(\'.block.payments\').html(data);
                }
            });
        })
    ', \yii\web\View::POS_READY);
?>