<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $changePassword app\models\ChangePassword */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;



$this->title = 'Security settings';
?>

<div class="block top-left">
	<div class="bt">
		<h2>Change email</h2>
	</div>
	<?php $form = ActiveForm::begin(['id' => 'change-email']); ?>
	<?php /*echo $form->errorSummary($changePassword); */?>
	<div class="cont">
		<?= $form->field($changeEmail, 'new')->textInput(['class' => 'form-control']); ?>
	</div>
	<div class="bot">
		<?= Html::submitButton('Save changes', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>
<div class="block">
	<div class="bt">
		<h2>Change password</h2>
	</div>
    <?php $form = ActiveForm::begin(['id' => 'change-password']); ?>
    <?php /*echo $form->errorSummary($changePassword); */?>
	<div class="cont">
		<?= $form->field($changePassword, 'current')->passwordInput(['class' => 'form-control']); ?>
        <?= $form->field($changePassword, 'new')->passwordInput(['class' => 'form-control']); ?>
	</div>
	<div class="bot">
		<?= Html::submitButton('Save changes', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
	</div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJs('
$(\'body\').on(\'submit\', \'form#change-email\', function(e) {
var form = $(this);
    $.ajax({
        url: \'/settings/security\',
        method: \'POST\',
        dataType: \'JSON\',
        data:form.serialize(),
        success: function (result) {
        	 $(\'.top-left\').notify({
    			message: { text: result.new }
  }).show();
            //alert(result);
        }
    });

    return false;
});

$(\'body\').on(\'submit\', \'form#change-password\', function(e) {
$(\'#loading\').modal(\'show\');
var form = $(this);
    $.ajax({
        url: \'/settings/security\',
        method: \'POST\',
        dataType: \'JSON\',
        data:form.serialize(),
        success: function (result) {
        	$(\'#loading\').modal(\'hide\');
            if(typeof(result.current) != \'undefined\') {
                $(\'.field-' . Html::getInputId($changePassword, 'current'). '\').removeClass(\'has-success\').addClass(\'has-error\');
            }

        }
    });

    return false;
});

', \yii\web\View::POS_READY, 'send-data');?>
