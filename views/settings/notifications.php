<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>

<div class="block payments top-left clearfix">
    <div class="bt">
        <h2>Notifications</h2>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'notifications-form',
        'options' => ['class' => 'notifications_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>
    <div class="cont">

        <?php if (!empty($groups)): ?>
            <?php foreach ($groups as $group): ?>
                <div class="line">
                    <div class="name"><?= $group->name ?></div>
                    <?php foreach ($types as $type): ?>
                        <?php if ($group->id == $type->group_id): ?>

                            <?php if (($option = \app\models\Notification::findOne([
                                    'user_id' => Yii::$app->user->id,
                                    'id' => $type->id
                                ])) != null
                            ): ?>

                                <?= $form->field($model, 'value[' . $type->id . ']')
                                    ->checkbox([
                                        'value' => '1',
                                        'label' => $type->name,
                                        'Checked' => $option->value == '1' ? 'checked' : false,
                                        'id' => 'notification-value' . $type->id
                                    ]); ?>

                            <?php else: ?>
                                <?= $form->field($model, 'value[' . $type->id . ']')->checkbox([
                                    'value' => '1',
                                    'label' => $type->name,
                                    'id' => 'notification-value' . $type->id
                                ]); ?>
                        <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>

        <?php endif; ?>

    </div>
    <div class="bot">
        <div class="">
            <?= Html::submitButton('Save changes',
                ['class' => 'btn btn-primary', 'name' => 'save-button', 'id' => 'notificanions_save']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>