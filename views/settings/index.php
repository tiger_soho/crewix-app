<?php

/* @var $this yii\web\View */

$this->title = 'Account settings';

$this->registerJsFile('/js/bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="block">
	<div class="bt">
		<h1>Account settings</h1>
	</div>
	<div class="cont">
		<div class="acc-type">
			<strong>Account type:</strong> <a href="#" class="type" data-toggle="popover" data-trigger="focus" data-content="Some content" data-placement="bottom">Free</a>
		</div>
		<p>You can post up to 5 vacancies per month. You don’t have access to our advanced search filters so you can’t really search for seamen. But here is the thing: post vacancies and we’ll find someone for you. </p>
		<p>If you hire enough people we will offer you a partnership. To become our partner you’ll need to sign a contract between your company and Crewix.  After becoming our Partner you’ll be able to post unlimited amount of vacancies and use all of our filters to find someone you need instantly.</p>
		<p>Keep hiring!</p>
	</div>
</div>