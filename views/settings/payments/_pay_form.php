<?php
/* @var $form ActiveForm */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>


    <div class="bt clearfix">
        <h2 class="pull-left">Balance</h2>
        <span class="cancel pay-cancel pull-right">Cancel</span>
    </div>
    <div class="icon balance-ico">
        <img src="/images/sys/dollar.svg" alt="Ballance"/>
    </div>
<?php $form = ActiveForm::begin([
    'id' => 'pay-form',
    'options' => ['class' => 'pay_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
    <div class="info">
        <div class="current-balance">
            You have: <span class="balance">$<?= Yii::$app->user->identity->balance ?></span>
        </div>
        <div class="text">You can add funds to your account &mdash; just <strong>enter the amount</strong> and <strong>press
                pay</strong>.
        </div>
        <?= $form->field($model, 'amount', [
            'template' => '<span class="input-group-addon money" id="basic-addon3">$</span> {input}'
        ])->textInput(['placeholder' => 'Amount'])->label(false); ?>
    </div>
    <div class="bot clearfix">
        <div class="pull-left">
            <?= Html::submitButton('<i class="fa fa-lock"></i>&nbsp;&nbsp;Pay',
                ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>