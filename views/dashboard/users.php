<?php

use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\web\View;

/* @var $changePassword app\models\ChangeEmail */
/* @var $changeEmail app\models\ChangePassword */
?>

<div class="block">
    <div class="new-company">
        <h2 class="pull-left">Companies</h2>
    </div>
    <?php Pjax::begin(['id' => 'companies', 'enablePushState' => true]); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider_companies,
        'id' => 'user-block',
        'itemOptions' => ['class' => 'item clearfix'],
        'summary' => '',
        'itemView' => 'users/_item',
    ]) ?>
    <?php Pjax::end(); ?>
</div>

<div class="block admin-block-2">
    <div class="bt">
        <h2 class="pull-left">Users</h2>
        <div class="btn-group admin-s-p">
            <p class="admin-search-btn">
                Search
            </p>
            <ul class="dropdown-menu a-drop">
                <?php $form = \yii\bootstrap\ActiveForm::begin(['id' => 'user-search',
                    'options' => ['class' => 'a-form'],
                    'validateOnBlur' => false,
                    'validateOnChange' => false,]) ?>
                    <?=$form->field($model,'name')->textInput()->label('Name')?>
                    <?=$form->field($model,'email')->textInput()->label('Email')?>
                    <?=$form->field($model,'verified')->dropDownList(['1'=>'active','0'=>'Not verified'],['prompt'=>'Select status'])->label('Account status')?>


                <?php \yii\bootstrap\ActiveForm::end(); ?>
                <div class="col-md-6 text-center"><button type="button" class="btn btn-primary a-open-c">Apply</button></div><div class="col-md-6 text-center"><button type="button" class="btn btn-default">Clear</button></div>
            </ul>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'users', 'enablePushState' => true]); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider_users,
        'id' => 'user-block',
        'itemOptions' => ['class' => 'item clearfix'],
        'summary' => '',
        'itemView' => 'users/_item',
        'pager' => [

            'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
            'maxButtonCount' => 3,

            // Customzing options for pager container tag
            'options' => [
                'tag' => 'div',
                'class' => 'pagination admin-pag',
                'id' => 'pager-container',
            ],

            // Customzing CSS class for pager link
            'linkOptions' => ['class' => 'admin-pag-a'],
            'activePageCssClass' => 'active',
            'disabledPageCssClass' => 'disable',

            // Customzing CSS class for navigating link
            'prevPageCssClass' => 'left',
            'nextPageCssClass' => 'right',
        ],
    ]) ?>
    <?php Pjax::end(); ?>
</div>


<div class="modal-block">
    <?= $this->render('users/_modal_edit',[
        'changePassword' => $changePassword,
        'changeEmail' => $changeEmail,
]); ?>

    <?= $this->render('users/_modal_info'); ?>
    <?= $this->render('users/_modal_verify'); ?>
    <?= $this->render('users/_modal_delete'); ?>
    <?= $this->render('users/_modal_delete_confirm'); ?>
    <?= $this->render('users/_modal_delete_ban'); ?>
    <?= $this->render('users/_modal_delete_ban_confirm'); ?>
</div>




<?php $this->registerJS('
var g_item = null;
$(document).on(\'click\', \'.db-show-info\', function(e) {
    var user_id = $(this).data(\'user-id\');
    $.get(\'/dashboard/info-account\', {id: user_id}, function(data){
        $(\'#view_account .modal-body\').html(\'\');
        $(\'#view_account .modal-body\').html(data);
    });
    $(\'#view_account\').modal(\'show\');
return false;
});

$(document).on(\'click\', \'.edit-user\', function(e) {
    var user_id = $(this).data(\'user-id\');
    /*$.get(\'/dashboard/info-account\', {id: user_id}, function(data){
        $(\'#view_account .modal-body\').html(\'\');
        $(\'#view_account .modal-body\').html(data);
    });*/
    $(\'#edit_account\').modal(\'show\');
return false;
});

$(document).on(\'click\', \'.verify-user\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
    $(\'.btn-verify\').data(\'user-id\',user_id);
    var item = el.parent().parent().parent().parent();
    var tmp = item.find(\'.user-none\').html();

        $(\'#verify .modal-body\').html(\'\');
        $(\'#verify .modal-body\').html(\'<h5>Are you sure you want to <b>verify this user?</b></h5>\');
        $(\'#verify .modal-body\').append(tmp);

    $(\'#verify\').modal(\'show\');
return false;
});

$(document).on(\'click\', \'.btn-verify\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
       $.get(\'/dashboard/verify-account\', {id: user_id});

    $(\'#verify\').modal(\'hide\');
return false;
});

$(document).on(\'click\', \'.delete-user\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
    $(\'.btn-delete-user\').data(\'user-id\',user_id);
    var item = el.parent().parent().parent().parent();
    g_item = item;
    var tmp = item.find(\'.user-none\').html();

        $(\'#delete .modal-body\').html(\'\');
        $(\'#delete .modal-body\').html(\'<h5>Are you sure you want to <b>delete this user?</b></h5>\');
        $(\'#delete .modal-body\').append(tmp);
    $(\'#delete\').modal(\'show\');
return false;
});

$(document).on(\'click\', \'.btn-delete-user\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
   $(\'.btn-confirm-delete-user\').data(\'user-id\',user_id);
   var item = g_item;
    var tmp = item.find(\'.user-none\').html();

        $(\'#delete_confirm .modal-body\').html(\'\');
        $(\'#delete_confirm .modal-body\').html(\'<h5>You are about to <b>delete this user.</b> Are you really sure about this?</h5>\');
        $(\'#delete_confirm .modal-body\').append(tmp);


    $(\'#delete_confirm\').modal(\'show\');
return false;
});

$(document).on(\'click\', \'.btn-confirm-delete-user\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
    console.log(user_id);
       $.post(\'/dashboard/delete-account\', {id: user_id});

    $(\'#delete_confirm\').modal(\'hide\');
    $(\'#delete\').modal(\'hide\');
return false;
});

$(document).on(\'click\', \'.delete-ban-user\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
    $(\'.btn-delete-ban-user\').data(\'user-id\',user_id);
    var item = el.parent().parent().parent().parent();
    g_item = item;
    var tmp = item.find(\'.user-none\').html();

        $(\'#delete_ban .modal-body\').html(\'\');
        $(\'#delete_ban .modal-body\').html(\'<h5>Are you sure you want to <b>delete and ban this user?</b></h5>\');
        $(\'#delete_ban .modal-body\').append(tmp);
    $(\'#delete_ban\').modal(\'show\');
return false;
});

$(document).on(\'click\', \'.btn-delete-ban-user\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
   $(\'.btn-confirm-delete-ban-user\').data(\'user-id\',user_id);
   var item = g_item;
    var tmp = item.find(\'.user-none\').html();

        $(\'#delete_ban_confirm .modal-body\').html(\'\');
        $(\'#delete_ban_confirm .modal-body\').html(\'<h5>You are about to <b>delete and ban this user.</b> Are you really sure about this?</h5>\');
        $(\'#delete_ban_confirm .modal-body\').append(tmp);


    $(\'#delete_ban_confirm\').modal(\'show\');
return false;
});

$(document).on(\'click\', \'.btn-confirm-delete-ban-user\', function(e) {
    var el = $(this);
    var user_id = $(this).data(\'user-id\');
    console.log(user_id);
       $.post(\'/dashboard/banned-account\', {id: user_id});

    $(\'#delete_ban_confirm\').modal(\'hide\');
    $(\'#delete_ban\').modal(\'hide\');
return false;
});
', View::POS_READY, 'dashboard-user-modal'); ?>
