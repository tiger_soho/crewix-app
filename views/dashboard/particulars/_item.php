<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Seaman */
/* @var $item \app\models\SeamanTravelDocument */
?>
<ol>
    <li><strong>Rank:</strong> <?=$model->primaryRank->name?></li>
    <li><strong>Surname:</strong> <?=$model->last_name ?></li>
    <li><strong>Name:</strong> <?=$model->first_name?></li>
    <li><strong>Date of Birth:</strong> <?=Yii::$app->formatter->asDate($model->date_of_birth,'php:d.m.Y')?></li>
    <li><strong>Place of Birth:</strong> <?=\app\models\Countries::findOne($model->place_of_birth_id)->name?></li>
    <li><strong>Nationality:</strong> <?=\app\models\Countries::findOne($model->citizenship_id)->name?></li>

    <?php foreach ($model->travelDocs as $item): ?>
        <?php if ($item->document_id == 1): ?>
            <li><strong>International Passport</strong><br>
                No.: <?= $item->number ?><br>
                DOI: <?= $item->date_of_issue ?><br>
                DOE: <?= ($item->date_of_expiry != null) ? $item->date_of_expiry : 'Unlimited' ?><br>
                POI: <?= \app\models\Countries::findOne($item->place_of_issue_id)->name ?>
            </li>
        <?php elseif ($item->document_id == 2): ?>
            <li><strong>Seaman's Identification Card</strong><br>
                No.: <?= $item->number ?><br>
                DOI: <?= $item->date_of_issue ?><br>
                DOE: <?= ($item->date_of_expiry != null) ? $item->date_of_expiry : 'Unlimited' ?><br>
                POI: <?= \app\models\Countries::findOne($item->place_of_issue_id)->name ?>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>


</ol>