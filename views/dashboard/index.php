<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Users;
use kartik\file\FileInput;
use dosamigos\chartjs\ChartJs;
use yii\db\Query;
use yii\db\QueryBuilder;
use yii\widgets\Pjax;

$this->title = 'Dashboard';
$this->registerJsFile('/js/bootstrap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="stats">
	<div class="item"></div>
	<div class="item"></div>
	<div class="item"></div>
	<div class="item"></div>
</div>
<div class="block graph">
    <div class="bt clearfix">
        <h1 class="pull-left">User growth rate</h1>
        <div class="dropdown pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
                This week
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu chart-view" aria-labelledby="dropdownMenu1">
                <li><a href="#" data-id="1">This week</a></li>
                <li><a href="#" data-id="2">This month</a></li>
                <li><a href="#" data-id="3">This year</a></li>
                <li><a href="#" data-id="4">All time</a></li>
            </ul>
        </div>
    </div>

    <div class="chart">
        <?php Pjax::begin(['id' => 'chart', 'enablePushState' => false]); ?>
        <?php
        $month = [];
        $pick = [];
        foreach ($data as $item) {
            $month[] = $item['count_month'];
            $pick[] = $item['pick'];
        }

        ?>
        <?= ChartJs::widget([
            'type' => 'Line',
            'options' => [
                'height' => 400,
                'width' => 800,

            ],
            'data' => [
                'labels' => $labels,
                'datasets' => [
                    [
                        'fillColor' => "rgba(151,187,205,0.5)",
                        'strokeColor' => "rgba(151,187,205,1)",
                        'pointColor' => "rgba(151,187,205,1)",
                        'pointStrokeColor' => "#fff",
                        'data' => $month,

                    ]
                ]
            ]
        ]); ?>
        <?php Pjax::end() ?>
    </div>

</div>

<?php
$this->registerJs('
        $(\'body\').on(\'click\',\'.chart-view li a\',function(){
            var $el = $(this);
            var id = $el.attr(\'data-id\');
            $.pjax({
                url:\'/dashboard\',
				container: \'#chart\',
        		push: false,
				data: {id:id},
            }).done(function(){
                $(\'.dropdown-toggle\').html($el.text()+\'<span class="caret"></span>\').attr(\'aria-expanded\',false);
                $(\'.dropdown\').removeClass(\'open\');
            });
            return false;
        });
    ', \yii\web\View::POS_READY);
?>