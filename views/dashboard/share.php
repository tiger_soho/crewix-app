<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title = 'Dashboard';


?>
<div class="block">
    <div class="bt">
        <h1>Share seaman</h1>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'share',
        'options' => ['class'=>'family_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>


        <table class="col-lg-12">
            <tr>
                <td colspan="2" class="heading">
                    Seaman
                </td>
            </tr>
            <tr>
                <td class="f-tit">User</td>
                <td>
                    <div class="info">
                        <div class="img"><img src="/images/sys/profile2.png" alt=""></div>
                        <div class="data">
                            <div class="name"></div>
                            <div class="rank"></div>
                        </div>
                    </div>
                    <?= $form->field($model, 'link', [
                        'template' => '<span class="input-group-addon" id="basic-addon3">http://crewix.com/id</span> {input}',
                        'options' => ['class'=>'input-group'],
                    ])->textInput(['placeholder'=>'Enter users link','aria-describedby'=>'basic-addon3']); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="heading">
                    Details
                </td>
            </tr>
            <tr>
                <td class="f-tit">Send to</td>
                <td>
                    <?= $form->field($model, 'to')->widget(\yii\jui\AutoComplete::classname(), [
                        'options' => [
                            'class'=>'form-control'
                        ],
                        'clientOptions' => [
                            'source' => \yii\helpers\Url::to('/dashboard/mails'),
                        ]
                    ])->label(false) ?>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Rank</td>
                <td>
                    <?= $form->field($model, 'primary_rank_id')->dropDownList(ArrayHelper::map(\app\models\Ranks::find()->all(),'id','name'), ['prompt'=>'Select rank'])->label(false);?>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Vessel type</td>
                <td>
                    <?= $form->field($model, 'vessel_type_id')->dropDownList(ArrayHelper::map(\app\models\VesselTypes::find()->all(),'id','name'), ['prompt'=>'Select vessel type'])->label(false);?>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="heading">
                    Options
                </td>
            </tr>
            <tr>
                <td class="f-tit">View</td>
                <td>
                    <?php $model->view=1; ?>
                    <?=$form->field($model,'view')->radioList(['1'=>'Long view','2'=>'Short view'])->label(false)?>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Units of power</td>
                <td>
                    <?php $model->power=1; ?>
                    <?=$form->field($model,'power')->radioList(['1'=>'kW','2'=>'BHP'])->label(false)?>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Document scans</td>
                <td><?=$form->field($model,'scans')->dropDownList(['0'=>'No document scans'])->label(false)?></td>
            </tr>
            <tr>
                <td class="f-tit">Attach 3.5x4.5cm photo</td>
                <td>
                    <div class="checkbox checkbox-primary">
                        <?=Html::checkbox(false,'photo',['class'=>'styled','name'=>'photo','id'=>'photo']);?>
                        <?=Html::label('Attach seaman\'s photo as JPG','photo')?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Contacts</td>
                <td>
                    <div class="checkbox checkbox-primary">
                        <?=Html::checkbox(false,'contacts',['class'=>'styled','name'=>'contacts','id'=>'contacts']);?>
                        <?=Html::label('Include contacts in PDF','contacts')?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Notes</td>
                <td>
                    <div class="checkbox checkbox-primary">
                        <?=Html::checkbox(false,'notes',['class'=>'styled','name'=>'notes','id'=>'notes']);?>
                        <?=Html::label('Include notes in PDF','notes')?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="f-tit">CV</td>
                <td>
                    <div class="checkbox checkbox-primary">
                        <?=Html::checkbox(false,'cv',['class'=>'styled','name'=>'cv','id'=>'cv']);?>
                        <?=Html::label('Include CV in PDF','cv')?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Send a copy to me</td>
                <td>
                    <div class="checkbox checkbox-primary">
                        <?=Html::checkbox(false,'copy_to_admin',['class'=>'styled','name'=>'copy_to_admin','id'=>'copy_to_admin']);?>
                        <?=Html::label('Send a copy to '.Yii::$app->user->identity->email,'copy_to_admin')?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="f-tit">Notify seaman</td>
                <td>
                    <div class="checkbox checkbox-primary">
                        <?=Html::checkbox(false,'notify',['class'=>'styled','name'=>'notify','id'=>'notify']);?>
                        <?=Html::label('Send a notification to seaman','notify')?>
                    </div>
                </td>
            </tr>
        </table>

    <div class="bot">
        <div class="form-group">
            <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


<script>
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
          _renderItem: function( ul, item ) {
            var li = $( "<li>", { text: item.label } );
     
            if ( item.disabled ) {
              li.addClass( "ui-state-disabled" );
            }
     
            $( "<span>", {
              style: item.element.attr( "data-style" ),
              "class": "ui-icon " + item.element.attr( "data-class" )
            })
              .appendTo( li );
     
            return li.appendTo( ul );
          }
        });
    $('select').each(function(i,el){
        if(!$(el).parent().hasClass('countries')){
            $(el).selectmenu({
                width: '100%'
            });
        }else{
            $(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
        }
    });
    $('.date input').datepicker({
            format: 'dd.mm.yyyy',
        });
</script>