<?php
/* @var $this yii\web\View */
/* @var $model \app\models\ComposeForm */
/* @var $form ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;

$this->title = 'Compose message';

?>

<div class="block clearfix">
    <div class="bt clearfix">
        <h1>Compose message</h1>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'compose',
        'options' => ['class' => 'compose_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>

    <table class="compose table">
        <tr>
            <td class="heading" colspan="2">Headers</td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> To</td>
            <td>
                <?= $form->field($model, 'to')->textInput(['placeholder' => 'Enter recipient’s email'])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> From</td>
            <td>
                <?= $form->field($model, 'from', [
                    'template' => '{input} <span class="input-group-addon" id="basic-addon3">@crewix.com</span>',
                    'options' => ['class' => 'input-group'],
                ])->textInput(['placeholder' => 'Enter username', 'aria-describedby' => 'basic-addon3']); ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Subject</td>
            <td>
                <?= $form->field($model, 'subject')->textInput(['placeholder' => 'Enter subject'])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="heading" colspan="2">Message</td>
        </tr>
        <tr>
            <td colspan="2">
                <?= $form->field($model, 'message')->widget(Widget::className(), [
                    'settings' => [
                        'minHeight' => 200,
                        'buttons' => ['format', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link']
                    ]
                ])->label(false); ?>
            </td>
        </tr>
        <tr>
            <td class="heading" colspan="2">
                Attachments
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="attachments">
                    <div class="item">
                        <div class="img">
                            <div class="images">
                                <i class="fa fa-plus-circle"></i>
                                <svg class="loader" viewBox="0 0 100 100">
                                    <circle class="one" r="40" cx="50" cy="50"/>
                                    <circle class="two" r="40" cx="50" cy="50"/>
                                </svg>
                            </div>
                            <?= $form->field($model, 'files[]',
                                ['options' => ['class' => 'attach']])->fileInput()->label(false); ?>
                        </div>
                        <div class="filename"></div>
                        <div class="remove-file">Remove</div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class="bot">
        <?= Html::submitButton('Send', ['class' => 'btn btn-primary', 'name' => 'compose-button', 'id' => 'compose-but']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script type="text/template" id="new-file">
    <?= $this->render('/dashboard/new-file', [
        'model' => $model,
    ]); ?>
</script>

<script>
    $("select").selectmenu({
        width: '100%'
    });
    window.FileAPI = {
        html5: !!1,
        staticPath: '/js/filapi/',
        flashUrl: '/js/filapi/FileAPI.flash.swf',
        flashImageUrl: '/js/filapi/FileAPI.flash.image.swf'
    };
</script>
<script src="/js/fileapi/FileAPI.html5.js"></script>
<script>
    $('body').on('change','.attach', function (evt) {
        $(this).parent().find('.images').find('i').hide();
        $(this).parent().find('.images').find('svg').show();
        var f = evt.target.files[0];
        var reader = new FileReader();
        reader.readAsText(f);
        reader.onload = function (theFile) {
            //console.log(theFile);
            return function (e) {
                console.log('>>>>>>>>>>>');
                if (theFile.size > 25000000) {
                    alert('Images size more then 25MB');
                    return false;
                }
            };
        };


        var i = $(this);

        FileAPI.upload({
            url: '/dashboard/upload-attachment',
            files: {src: f},
            data: {'_csrf': '<?=Yii::$app->request->getCsrfToken()?>'},
            imageTransform: {
                width: 350,
                height: 450,
                preview: true
            },
            progress: function (evt) {
                var a = (evt.loaded / evt.total) * 360;
                i.parents('.item').find('.images').find('svg').find('.two').css({'stroke-dasharray': '' + a + ' 300'});
            },
            complete: function (err, xhr) {
                var res = JSON.parse(xhr.responseText)
            },
            filecomplete: function (err, xhr, file) {
                var res = JSON.parse(xhr.responseText);
                FileAPI.Image(f).preview(100, 100).get(function (err, img) {
                    setTimeout(function () {
                        i.parents('.item').find('.img').css({'border': '3px solid #4f8ec4'});
                        i.parents('.item').find('.images').find('svg').hide();
                        i.hide();
                        var n = file.name.length;
                        if(n>13){
                            var ext = file.name.split('.').pop();
                            var name = file.name.replace(/\.[^/.]+$/, '');
                            var nname = name.slice(0,(0-n+13));
                            var filename = nname+'... .'+ext
                            console.log(filename);
                        }else{
                            var filename = file.name;
                        }
                        i.parents('.item').find('.filename').text(filename);
                        i.parents('.item').find('.remove-file').show();
                        i.parents('.item').find('.images').append('<i class="' + res.ico + ' file-ico"></i>');
                    }, 300);
                    var fileinfo = '{"name":"' + res.file + '","size":"' + file.size + '"}';
                    i.parents('.item').find('.file-info').find('input').val(fileinfo);
                    var c = $('.attachments .item').length;
                    if(c<5){
                        var tpl = $('#new-file').html();
                        $('.attachments').append(tpl);
                    }
                });
            }
        });
        reader.readAsDataURL;
    });
</script>