<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */

?>

<?php Modal::begin([
    'id' => 'view_account',
    'header' => 'User info',
    'footer' => '<button class="btn btn-primary" data-dismiss="modal">Close</button>',
]); ?>

<?php Modal::end(); ?>

<?php
$this->registerCss('
#view_account .modal-body{
padding: 0px;
}

#view_account .modal-footer{
    background-color: rgba(232, 230, 230, 0.47);
}
');
?>
