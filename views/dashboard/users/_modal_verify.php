<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */

?>

<?php Modal::begin([
    'id' => 'verify',
    'header' => 'Verify this user?',
    'footer' => '<button class="btn btn-primary btn-verify" data-user-id="">Verify user</button><button class="btn btn-default"  data-dismiss="modal">Cancel</button>',
]); ?>

<?php Modal::end(); ?>