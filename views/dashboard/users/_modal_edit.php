<?php

use app\models\UserAccountType;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $changePassword app\models\ChangeEmail */
/* @var $changeEmail app\models\ChangePassword */


?>
<?php $form = ActiveForm::begin(['id' => 'form-edit-account']); ?>
<?php Modal::begin([
    'id' => 'edit_account',
    'header' => 'Edit user',
    'footer' => '<button type="submit" class="btn btn-primary">Save changes</button><button type="button" class="btn cancel" data-dismiss="modal">Cancel</button>',
]); ?>


<?php // echo Html::dropDownList('user_account_type', null, UserAccountType::getAccountType(), ['class' => 'form-control']); ?>
        <?= $form->field($changeEmail, 'new')->textInput(['class' => 'form-control']); ?>
<?= $form->field($changePassword, 'new')->passwordInput(['class' => 'form-control'])->label('Password'); ?>
<?= $form->field($changePassword, 'new2')->passwordInput(['class' => 'form-control'])->label(false); ?>


<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>