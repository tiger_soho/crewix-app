<?php
use yii\helpers\Html;

/* @var $model app\models\Users */
?>

<?php
$profile_picture_url = null;
$email = null;
$fullname = null;


if ($model->account_type_id == 1) {
    $profile_picture_url = !empty($model->seaman->profile_picture) ? "/images/seaman/photo/small/" . $model->seaman->profile_picture : "/images/seaman/nocv.png";
    $email = $model->email;
    $fullname = $model->seaman->first_name . ' ' . $model->seaman->last_name;
} elseif ($model->account_type_id == 2) {
    $profile_picture_url = !empty($model->seaman->profile_picture) ? "/images/companies/logos/small/" . $model->company->profile_picture : "/images/companies/logos/noimage.jpg";
    $email = $model->email;
    $fullname = $model->company->name;
}

?>

<table class="table-bordered">
    <tr>
        <th>User</th>
        <td>
            <div class="img company-logo">
                <img src="<?= $profile_picture_url; ?>" alt="<?= $fullname; ?>">
            </div>
            <div class="detales">
                <h3 class="full-name"><?= $fullname; ?></h3>
                <div class="mail"><?= $email; ?></div>
            </div>
        </td>
    </tr>
    <tr>
        <th>User ID</th>
        <td>
            <?= $model->id; ?>
        </td>
    </tr>
    <tr>
        <th>Account type</th>
        <td>
            <?= !is_null($model->user_account_type_id) ? $model->userAccountType->name : ''; ?>
        </td>
    </tr>
    <tr>
        <th>Last seen</th>
        <td class="last-seen">
            <?php $time = floor((time() - strtotime($model->last_seen))/60); ?>
            <?= strtotime($model->last_seen); ?>
        </td>
    </tr>
    <tr>
        <th>Balance</th>
        <td><?= $model->balance; ?></td>
    </tr>
</table>

<?php
$this->registerCss('
.table-bordered{
width: 100%;
}
.table-bordered th{
    padding: 15px;
    text-align: right;
}
');

$this->registerJs('
    if (navigator.cookieEnabled)
       a = parseInt(- new Date().getTimezoneOffset());
    var t = parseInt($(\'.last-seen\').text());
    var m = moment(t,\'X\').fromNow();
    $(\'.last-seen\').text(m);
', \yii\web\View::POS_READY)
 ?>
