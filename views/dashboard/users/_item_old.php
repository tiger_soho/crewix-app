<?php
use yii\helpers\Url;
use app\models\Currency;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Users';
?>

<?php if($model->account_type_id==1): ?>
    <div class="info pull-left">
        <div class="img profile-img">
            <?php if(!empty($model->seaman->profile_picture)): ?>
                <img src="/images/seaman/photo/small/<?=$model->seaman->profile_picture?>" alt="<?=$model->seaman->first_name?> <?=$model->seaman->last_name?>">
            <?php else: ?>
                <img src="/images/seaman/nocv.jpg" alt="">
            <?php endif; ?>
        </div>
        <div class="detales">
            <h3 class="full-name"><?=$model->seaman->first_name?> <?=$model->seaman->last_name?></h3>
            <div class="mail"><?=$model->email;?></div>
        </div>
    </div>


<?php elseif($model->account_type_id==2): ?>
    <div class="info pull-left">
        <div class="img company-logo">
            <?php if(!empty($model->company->profile_picture)): ?>
                <img src="/images/companies/logos/small/<?=$model->company->profile_picture?>" alt="<?=$model->company->name?>?>">
            <?php else: ?>
                <img src="/images/companies/logos/noimage.jpg" alt="">
            <?php endif; ?>
        </div>
        <div class="detales">
            <h3 class="full-name"><?=$model->company->name?></h3>
            <div class="mail"><?=$model->email;?></div>
        </div>
    </div>
<?php elseif($model->account_type_id==0): ?>

<?php endif; ?>

