<?php
use yii\helpers\Url;
use app\models\Currency;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Users';
?>


<?php
$profile_picture_url = null;
$email = null;
$fullname = null;


if ($model->role=='seaman') {
    $profile_picture_url = !empty($model->seaman->profile_picture) ? "/images/seaman/photo/small/" . $model->seaman->profile_picture : "/images/seaman/nocv.jpg";
    $email = $model->email;
    $fullname = $model->seaman->first_name . ' ' . $model->seaman->last_name;
} elseif ($model->role=='company') {
    $profile_picture_url = !empty($model->seaman->profile_picture) ? "/images/companies/logos/small/" . $model->company->profile_picture : "/images/companies/logos/noimage.jpg";
    $email = $model->email;
    $fullname = $model->company->name;
}

?>

<div class="info pull-left">
    <span class="user-none">
    <div class="img company-logo user-image <?php if($model->role=='seaman'): ?>img-circle<?php endif; ?>">
        <img src="<?= $profile_picture_url; ?>" alt="<?= $fullname; ?>">
    </div>
    <div class="detales user-details">
        <h3 class="full-name"><?= $fullname; ?></h3>
        <div class="mail"><?= $email; ?></div>
    </div>
    </span>
    <div class="btn-group btn-admin">
        <button type="button" class="btn btn-primary db-show-info" data-user-id="<?= $model->id; ?>">View</button>
        <?php if($model->role != 'admin'): ?>
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu">
                <?php if($model->role == 'seaman'): ?>
                <li><a href="#" class="edit-user" data-user-id="<?= $model->id; ?>">
                        Edit user
                </a></li>
                <?php else: ?>
                    <li><a href="#" class="verify-user" data-user-id="<?= $model->id; ?>">
                        Verify user
                    </a></li>
                <?php endif; ?>
                <li><a href="#" class="delete-user" data-user-id="<?= $model->id; ?>">Delete user</a></li>
                <li><a href="#" class="delete-ban-user" data-user-id="<?= $model->id; ?>">Delete & ban user</a></li>
            </ul>
        <?php endif; ?>
    </div>
</div>


