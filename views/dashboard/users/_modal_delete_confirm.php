<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */

?>

<?php Modal::begin([
    'id' => 'delete_confirm',
    'header' => 'Warning',
    'footer' => '<button class="btn btn-danger btn-confirm-delete-user">Delete user</button><button class="btn btn-default" data-dismiss="modal">Close</button>',
]); ?>

<?php Modal::end(); ?>