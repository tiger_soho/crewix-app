<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */

?>

<?php Modal::begin([
    'id' => 'delete',
    'header' => 'Delete this user?',
    'footer' => '<button class="btn btn-danger btn-delete-user">Delete user</button><button class="btn btn-default" data-dismiss="modal">Close</button>',
]); ?>

<?php Modal::end(); ?>