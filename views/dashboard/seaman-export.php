<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Dashboard';


?>
<div class="block">
    <div class="bt">
        <h1>Export seaman</h1>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'export',
        'options' => ['class' => 'family_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>


    <table class="col-lg-12">
        <tr>
            <td colspan="2" class="heading">
                Seaman
            </td>
        </tr>
        <tr>
            <td class="f-tit">User</td>
            <td>
                <div class="info">
                    <div class="img"><img src="/images/sys/profile2.png" alt=""></div>
                    <div class="data">
                        <div class="name"></div>
                        <div class="rank"></div>
                    </div>
                </div>
                <?= $form->field($model, 'link', [
                    'template' => '<span class="input-group-addon" id="basic-addon3">http://crewix.com/id</span> {input}',
                    'options' => ['class' => 'input-group'],
                ])->textInput(['placeholder' => 'Enter users link', 'aria-describedby' => 'basic-addon3']); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="heading">
                Options
            </td>
        </tr>
        <tr>
            <td>Document scans</td>
            <td>
                <?= $form->field($model, 'documents[]')->dropDownList([],
                    ['prompt' => 'Select vessel type'])->label(false); ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit">View</td>
            <td>
                <?php $model->view = 1; ?>
                <?= $form->field($model, 'view')->radioList(['1' => 'Long view', '2' => 'Short view'])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit">Units of power</td>
            <td>
                <?php $model->power = 1; ?>
                <?= $form->field($model, 'power')->radioList(['1' => 'kW', '2' => 'BHP'])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit">Contacts</td>
            <td>
                <div class="checkbox checkbox-primary">
                    <?= Html::checkbox('contacts',false,
                        ['class' => 'styled', 'name' => 'contacts', 'id' => 'contacts','label'=>'Include contacts in PDF']); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="f-tit">Notes</td>
            <td>
                <div class="checkbox checkbox-primary">
                    <?= Html::checkbox('notes', false, ['class' => 'styled', 'name' => 'notes', 'id' => 'notes','label'=>'Include notes in PDF']); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td class="f-tit">CV</td>
            <td>
                <div class="checkbox checkbox-primary">
                    <?= Html::checkbox('cv', false, ['class' => 'styled', 'name' => 'cv', 'id' => 'cv','label'=>'Include CV in PDF']); ?>
                </div>
            </td>
        </tr>
    </table>

    <div class="bot">
        <div class="form-group">
            <?= Html::submitButton('Save & continue',
                ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


<script>
    $.widget("custom.iconselectmenu", $.ui.selectmenu, {
        _renderItem: function (ul, item) {
            var li = $("<li>", {text: item.label});

            if (item.disabled) {
                li.addClass("ui-state-disabled");
            }

            $("<span>", {
                style: item.element.attr("data-style"),
                "class": "ui-icon " + item.element.attr("data-class")
            })
                .appendTo(li);

            return li.appendTo(ul);
        }
    });
    $('select').each(function (i, el) {
        if (!$(el).parent().hasClass('countries')) {
            $(el).selectmenu({
                width: '100%'
            });
        } else {
            $(el).iconselectmenu({width: '100%'}).iconselectmenu("menuWidget").addClass("ui-menu-icons avatar");
        }
    });
    $('.date input').datepicker({
        format: 'dd.mm.yyyy',
    });
</script>