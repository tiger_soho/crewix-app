<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = 'Dashboard';


?>
<div class="block">
    <div class="bt">
        <h1>Share seaman</h1>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'particulars',
        'options' => ['class' => 'family_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>
    <table class="col-lg-12">
        <tr>
            <td colspan="2" class="heading">
                User
            </td>
        </tr>
        <tr>
            <td class="f-tit">User</td>
            <td>
                <div class="info">
                    <div class="img"><img src="/images/sys/profile2.png" alt=""></div>
                    <div class="data">
                        <div class="name"></div>
                        <div class="rank"></div>
                    </div>
                </div>
                <?= $form->field($model, 'link')->textInput(['placeholder' => 'Enter users link',['class'=>'link']])->label(false); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="heading">
                Particulars
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="particulars-info">
                    <?php Pjax::begin(['id' => 'particulars-block', 'enablePushState' => false]); ?>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'id' => 'part',
                        'emptyText' => '',
                        'emptyTextOptions' => ['class' => 'empty-val'],
                        'itemOptions' => ['class' => 'item'],
                        'summary' => '',
                        'itemView' => 'particulars/_item',
                    ]) ?>
                    <?php Pjax::end(); ?>
                </div>
            </td>
        </tr>
    </table>
    <div class="bot">
        <div class="form-group">
            <?= Html::Button('Generate',
                ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'generate']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
    $this->registerJs('
        $(document).on(\'click\',\'#generate\',function(){
            var val = $(\'#seaman-link\').val();
            $.pjax({
				url:\'/dashboard/particulars\',
				container: \'#particulars-block\',
        		fragment: \'#part\',
				data: {link:val},
				push:false,
			}).done(function(data) {
			    console.log(data);
			    if(data==\'<title>Dashboard</title>                    <div id="part" class="list-view"><div class="empty-val"></div></div>                    \'){
			        $(\'#particulars-block\').addClass(\'disabled\');
			        $.notify(\'User does not exists or this user is Company\', "error");
			    }else{
			        $(\'#particulars-block\').removeClass(\'disabled\')
			    }

			});
			return false;
        })
    ',\yii\web\View::POS_READY);
?>