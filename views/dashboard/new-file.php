<?php
    use yii\bootstrap\Html;
?>

<div class="item">
    <div class="img">
        <div class="images">
            <i class="fa fa-plus-circle"></i>
            <svg class="loader" viewBox="0 0 100 100">
                <circle class="one" r="40" cx="50" cy="50"/>
                <circle class="two" r="40" cx="50" cy="50"/>
            </svg>
        </div>
        <div class="attach field-composeform-files">
            <?=Html::activeFileInput($model,'files[]',['class' => 'attach'])?>
        </div>

    </div>
    <div class="filename"></div>
    <div class="remove-file">Remove</div>
</div>