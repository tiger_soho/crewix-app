<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use app\models\Seaman;
use app\models\Companies;
use kartik\sidenav\SideNav;
use yii\bootstrap\Modal;
use app\models\Admin;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->endBody() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <nav role="navigation" class="navbar-inverse navbar-top navbar" id="w1">
        <div class="container">
            <div class="navbar-header">
                <button data-target="#w0-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo Url::toRoute('/'); ?>" class="navbar-brand"><img src="/images/sys/logo-rectangle.svg" alt="Crewix"></a>
            </div>
            <div class="collapse navbar-collapse" id="w0-collapse">
                <ul class="navbar-nav navbar-left nav">
                    <li><a href="<?php echo Url::toRoute('/vacancies'); ?>">Vacancies</a></li>
                    <li><a href="<?php echo Url::toRoute('/seamen'); ?>">Seamen</a></li>
                    <li><a href="<?php echo Url::toRoute('/companies'); ?>">Companies</a></li>
                </ul>
                <?php if(Yii::$app->user->isGuest): ?>
                    <ul class="navbar-nav navbar-right nav">
                        <li><a href="<?php echo Url::toRoute('/login'); ?>">Log in</a></li>
                        <li><a href="<?php echo Url::toRoute('/signup'); ?>">Create account</a></li>
                    </ul>
                <?php else: ?>
                    <div class="user-menu">
                        <?php if(Yii::$app->user->can('seaman')): ?>
                            <?php $c =  Seaman::find()->where(['user_id'=>Yii::$app->user->id])->count(); ?>
                            <?php if($c>0): ?>
                                <?php $img = Seaman::find()->select('profile_picture_thumb')->where(['user_id'=>Yii::$app->user->id])->one()->profile_picture_thumb; ?>
                                <?php if(!empty($img)): ?>
                                    <img src="/images/seaman/photo/small/<?=$img?>" alt="">
                                <?php else: ?>
                                    <img src="/images/sys/profile2.png" alt="">
                                <?php endif; ?>
                                <div class="name"><?=$img = Seaman::find()->select('first_name')->where(['user_id'=>Yii::$app->user->id])->one()->first_name;?> <i class="fa fa-caret-down"></i></div>
                                <ul>
                                    <li><?= Html::a('<i class="fa fa-power-off"></i>Log out', ['/site/logout'], ['data-method'=>'post']); ?></li>
                                </ul>
                            <?php else: ?>
                                <img src="/images/sys/profile2.png" alt="">
                                <div class="name">Seamen <i class="fa fa-caret-down"></i></div>
                                <ul>
                                    <li><?= Html::a('<i class="fa fa-power-off"></i>Log out', ['/site/logout'], ['data-method'=>'post']); ?></li>
                                </ul>
                            <?php endif; ?>
                            <?php $id = \app\models\Profile::find()->select('id')->where(['user_id'=>Yii::$app->user->id])->one()->id ?>
                            <ul>
                                <li><a href="<?= Url::to(['/profile', 'id' => $id]) ?>"><i class="fa fa-user"></i>My
                                        profile</a></li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to('/settings') ?>"><i class="fa fa-cog"></i></i>Settings</a></li>
                                <li><?= Html::a('<i class="fa fa-power-off"></i>Log out', ['/site/logout'], ['data-method'=>'post']); ?></li>
                            </ul>
                        <?php elseif(Yii::$app->user->can('company')): ?>
                            <?php $c =  Companies::find()->where(['user_id'=>Yii::$app->user->id])->count(); ?>
                            <?php if($c>0): ?>

                                <?php $img = Companies::find()->select('profile_picture_thumb')->where(['user_id'=>Yii::$app->user->id])->one()->profile_picture_thumb; ?>
                                <?php if(!empty($img)): ?>
                                    <img src="/images/companies/logos/small/<?=$img?>" alt="">
                                <?php else: ?>
                                    <img src="/images/sys/profile2.png" alt="">
                                <?php endif; ?>
                                <div class="name"><?=$img = Companies::find()->select('name')->where(['user_id'=>Yii::$app->user->id])->one()->name;?> <i class="fa fa-caret-down"></i></div>
                            <?php else: ?>
                                <img src="/images/sys/profile2.png" alt="">
                                <div class="name">Company <i class="fa fa-caret-down"></i></div>

                            <?php endif; ?>
                            <?php $id = \app\models\Profile::find()->select('id')->where(['user_id'=>Yii::$app->user->id])->one()->id ?>
                            <ul>
                                <li><a href="<?= Url::to('/vac') ?>"><i class="fa fa-plus"></i>New vacancy</a></li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to(['/profile', 'id' => $id]) ?>"><i class="fa fa-user"></i>My
                                        profile</a></li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to('/settings') ?>"><i class="fa fa-cog"></i></i>Settings</a></li>
                                <li><?= Html::a('<i class="fa fa-power-off"></i>Log out', ['/site/logout'], ['data-method'=>'post']); ?></li>

                            </ul>
                        <?php elseif (Yii::$app->user->can('admin')): ?>
                            <?php $c = Admin::find()->where(['user_id' => Yii::$app->user->id])->count(); ?>
                            <?php if ($c > 0): ?>

                                <?php $img = Companies::find()->select('profile_picture_thumb')->where(['user_id' => Yii::$app->user->id])->one()->profile_picture_thumb; ?>
                                <?php if (!empty($img)): ?>
                                    <img src="/images/admin/photo/small/<?= $img ?>" alt="">
                                <?php else: ?>
                                    <img src="/images/sys/profile2.png" alt="">
                                <?php endif; ?>
                                <div
                                    class="name"><?= $img = Admin::find()->select('name')->where(['user_id' => Yii::$app->user->id])->one()->name; ?>
                                    <i class="fa fa-caret-down"></i></div>
                            <?php else: ?>
                                <img src="/images/sys/profile2.png" alt="">
                                <div class="name">Admin <i class="fa fa-caret-down"></i></div>
                            <?php endif; ?>
                            <ul>
                                <li><a href="<?= Url::to('/new/payment') ?>"><i class="fa fa-plus"></i>New payment</a>
                                </li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to(['/payments']) ?>"><i
                                            class="fa fa-credit-card"></i>Payments</a></li>
                                <li><a href="<?= Url::to('/stats') ?>"><i class="fa fa-bar-chart"></i></i>Statistics</a>
                                </li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to('/settings') ?>"><i class="fa fa-cog"></i></i>Settings</a></li>
                                <li><?= Html::a('<i class="fa fa-power-off"></i>Log out', ['/site/logout'],
                                        ['data-method' => 'post']); ?></li>

                            </ul>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>


    

    <div class="container settings">
        <div class="alert abs">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
        <div class="side-menu-settings">
            <div class="balance_block">
                <?php if(Yii::$app->user->can('seaman')): ?>
                    <?php $c = Seaman::find()->where(['user_id' => Yii::$app->user->id])->count(); ?>
                    <div class="img">
                        <?php if ($c > 0): ?>

                            <?php $img = Seaman::find()->select('profile_picture_thumb')->where(['user_id' => Yii::$app->user->id])->one()->profile_picture_thumb; ?>
                            <?php if (!empty($img)): ?>
                                <img src="/images/companies/logos/small/<?= $img ?>" alt="">
                            <?php else: ?>
                                <img src="/images/sys/profile2.png" alt="">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="/images/sys/profile2.png" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="info">
                        <div
                            class="name"><?= Seaman::find()->where(['user_id' => Yii::$app->user->id])->one()->first_name ?> <?= Seaman::find()->where(['user_id' => Yii::$app->user->id])->one()->last_name ?></div>
                        <div class="balance">
                            $ <?= Yii::$app->user->getIdentity()->balance ?>
                        </div>
                    </div>
                <?php elseif(Yii::$app->user->can('company')): ?>
                    <?php $c =  Companies::find()->where(['user_id'=>Yii::$app->user->id])->count(); ?>
                    <div class="img">
                        <?php if($c>0): ?>
                               
                            <?php $img = Companies::find()->select('profile_picture')->where(['user_id'=>Yii::$app->user->id])->one()->profile_picture; ?>
                            <?php if(!empty($img)): ?>
                                <img src="/images/companies/logos/small/<?=$img?>" alt="">
                            <?php else: ?>
                                <img src="/images/companies/logos/noimage.jpg" alt="">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="/images/sys/profile2.png" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="info">
                        <div class="name"><?=Companies::find()->where(['user_id'=>Yii::$app->user->id])->one()->name?></div>
                        <div class="balance">
                            $ <?=Yii::$app->user->getIdentity()->balance?>
                        </div>
                    </div>      
                <?php elseif(Yii::$app->user->can('admin')): ?>
                <?php endif; ?>
            </div>
            <?php 
                echo SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'heading' => false,
                    'items' => [
                        [
                            'url' => '/settings',
                            'label' => 'Account',
                            'active'=>(Yii::$app->urlManager->parseRequest(Yii::$app->request)[0] == 'settings/index'),
                            'icon' => 'user'
                        ],
                        [
                            'url' => '/settings/security',
                            'label' => 'Security',
                            'active'=>(Yii::$app->urlManager->parseRequest(Yii::$app->request)[0] == 'settings/security'),
                            'icon' => 'lock'
                        ],
                        [
                            'url' => '/settings/payments',
                            'label' => 'Payments',
                            'active' => (Yii::$app->urlManager->parseRequest(Yii::$app->request)[0] == 'settings/payments'),
                            'icon' => 'credit-card'
                        ],
                        [
                            'url' => '/settings/notifications',
                            'label' => 'Notifications',
                            'active'=>(Yii::$app->urlManager->parseRequest(Yii::$app->request)[0] == 'settings/notifications'),
                            'icon' => 'bell'
                        ],
                    ],
                ]);
            ?>
        </div>
        <div class="content"><?= $content ?></div>
        
    </div>
</div>

<?php
Modal::begin([
    'id' => 'loading',
    'header' => 'loading',
]); ?>

<p>Please wait...</p>

<?php Modal::end(); ?>
<footer class="footer">
    <div class="container">
        <div class="f-menu">
            <a href="<?=Url::to('/about')?>">About</a><!--
         --><a href="<?=Url::to('/terms')?>">Terms</a>
        </div>
        <div class="f-logo"><img src="/images/sys/logo-small-circle.svg" alt=""></div>
        <div class="f-menu">
            <a href="<?=Url::to('/faq')?>">FAQ</a><!--
         --><a href="<?=Url::to('/feedback')?>">Feedback</a>
        </div>
    </div>
</footer>


</body>
</html>
<?php $this->endPage() ?>
