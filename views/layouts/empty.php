<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\Alert;
AppAsset::register($this);
?>
    <?php $this->endBody() ?>
<body class="sign">
<?php $this->beginBody() ?>
                <?= $content ?>

</body>
<?php $this->endPage() ?>