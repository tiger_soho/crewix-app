<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
//use app\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <nav role="navigation" class="navbar-inverse navbar-top navbar main" id="w0">
        <div class="container">
            <div class="navbar-header">
                <button data-target="#w0-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo Url::toRoute('/'); ?>" class="navbar-brand"><img src="/images/sys/logo-rectangle.svg" alt="Crewix"></a>
            </div>
            <div class="collapse navbar-collapse" id="w0-collapse">
                <ul class="navbar-nav navbar-left nav">
                    <li><a href="<?php echo Url::toRoute('/vacancies'); ?>">Vacancies</a></li>
                    <li><a href="<?php echo Url::toRoute('/seamen'); ?>">Seamen</a></li>
                    <li><a href="<?php echo Url::toRoute('/companies'); ?>">Companies</a></li>
                </ul>
                <?php if(Yii::$app->user->isGuest){ ?>
                    <ul class="navbar-nav navbar-right nav">
                        <li><a href="<?php echo Url::toRoute('/login'); ?>">Log in</a></li>
                        <li><a href="<?php echo Url::toRoute('/signup'); ?>">Create account</a></li>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </nav>


    
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>

<footer class="footer">
    <div class="container">
        <div class="f-menu">
            <a href="<?=Url::to('/about')?>">About</a><!--
         --><a href="<?=Url::to('/terms')?>">Terms</a>
        </div>
        <div class="f-logo"><img src="/images/sys/logo-small-circle.svg" alt=""></div>
        <div class="f-menu">
            <a href="<?=Url::to('/faq')?>">FAQ</a><!--
         --><a href="<?=Url::to('/feedback')?>">Feedback</a>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>
