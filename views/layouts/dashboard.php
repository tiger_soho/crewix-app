<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Alert;
use app\models\Seamans;
use app\models\Companies;
use kartik\sidenav\SideNav;
use yii\widgets\Pjax;
use app\models\Admin;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->endBody() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <nav role="navigation" class="navbar-inverse navbar-top navbar" id="w1">
        <div class="container">
            <div class="navbar-header">
                <button data-target="#w0-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo Url::toRoute('/'); ?>" class="navbar-brand"><img src="/images/sys/logo-rectangle.svg" alt="Crewix"></a>
            </div>
            <div class="collapse navbar-collapse" id="w0-collapse">
                <ul class="navbar-nav navbar-left nav">
                    <li><a href="<?php echo Url::toRoute('/vacancies'); ?>">Vacancies</a></li>
                    <li><a href="<?php echo Url::toRoute('/seamen'); ?>">Seamen</a></li>
                    <li><a href="<?php echo Url::toRoute('/companies'); ?>">Companies</a></li>
                </ul>
                <?php if(Yii::$app->user->isGuest): ?>
                    <ul class="navbar-nav navbar-right nav">
                        <li><a href="<?php echo Url::toRoute('/login'); ?>">Log in</a></li>
                        <li><a href="<?php echo Url::toRoute('/signup'); ?>">Create account</a></li>
                    </ul>
                <?php else: ?>
                    <div class="user-menu">
                        <?php if (Yii::$app->user->can('seaman')): ?>
                            <?php $c =  \app\models\Seaman::find()->where(['user_id'=>Yii::$app->user->getIdentity()->id])->count(); ?>
                            <?php if($c>0): ?>
                                <?php $img = \app\models\Seaman::find()->select('profile_picture_thumb')->where(['user_id'=>Yii::$app->user->getIdentity()->id])->one()->profile_picture_thumb; ?>
                                <?php if(!empty($img)): ?>
                                    <img src="/images/seaman/photo/small/<?=$img?>" alt="">
                                <?php else: ?>
                                    <img src="/images/sys/profile2.png" alt="">
                                <?php endif; ?>
                                <div class="name"><?=$img = \app\models\Seaman::find()->select('first_name')->where(['user_id'=>Yii::$app->user->getIdentity()->id])->one()->first_name;?> <i class="fa fa-caret-down"></i></div>
                                <ul>
                                    <li><a href="<?=Url::toRoute('/site/logout')?>" data-method="post">Log out</a></li>
                                </ul>
                            <?php else: ?>
                                <img src="/images/sys/profile2.png" alt="">
                                <div class="name">Seamen <i class="fa fa-caret-down"></i></div>
                                <ul>
                                    <li><a href="<?=Url::toRoute('/site/logout')?>" data-method="post">Log out</a></li>
                                </ul>
                            <?php endif; ?>
                        <?php elseif (Yii::$app->user->can('company')): ?>
                            <?php $c =  Companies::find()->where(['user_id'=>Yii::$app->user->getIdentity()->id])->count(); ?>
                            <?php if($c>0): ?>

                                <?php $img = Companies::find()->select('profile_picture_thumb')->where(['user_id'=>Yii::$app->user->getIdentity()->id])->one()->profile_picture_thumb; ?>
                                <?php if(!empty($img)): ?>
                                    <img src="/images/companies/logos/small/<?=$img?>" alt="">
                                <?php else: ?>
                                    <img src="/images/sys/profile2.png" alt="">
                                <?php endif; ?>
                                <div class="name"><?=$img = Companies::find()->select('name')->where(['user_id'=>Yii::$app->user->getIdentity()->id])->one()->name;?> <i class="fa fa-caret-down"></i></div>
                            <?php else: ?>
                                <img src="/images/sys/profile2.png" alt="">
                                <div class="name">Company <i class="fa fa-caret-down"></i></div>

                            <?php endif; ?>
                            <?php $id = \app\models\Profile::find()->select('id')->where(['user_id'=>Yii::$app->user->id])->one()->id ?>
                            <ul>
                                <li><a href="<?= Url::to('/vac') ?>"><i class="fa fa-plus"></i>New vacancy</a></li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to(['/profile', 'id' => $id]) ?>"><i class="fa fa-user"></i>My
                                        profile</a></li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to('/settings') ?>"><i class="fa fa-cog"></i></i>Settings</a></li>
                                <li><a href="<?=Url::toRoute('/site/logout')?>" data-method="post"><i class="fa fa-power-off"></i>Log out</a></li>
                            </ul>
                        <?php elseif (Yii::$app->user->can('admin')): ?>
                            <?php $c = Admin::find()->where(['user_id' => Yii::$app->user->id])->count(); ?>
                            <?php if ($c > 0): ?>

                                <?php $img = Companies::find()->select('profile_picture_thumb')->where(['user_id' => Yii::$app->user->id])->one()->profile_picture_thumb; ?>
                                <?php if (!empty($img)): ?>
                                    <img src="/images/admin/photo/small/<?= $img ?>" alt="">
                                <?php else: ?>
                                    <img src="/images/sys/profile2.png" alt="">
                                <?php endif; ?>

                                <div class="name"><?= $img = Admin::find()->select('name')->where(['user_id' => Yii::$app->user->id])->one()->name; ?>
                                    <i class="fa fa-caret-down"></i></div>
                            <?php else: ?>
                                <img src="/images/sys/profile2.png" alt="">
                                <div class="name">Admin <i class="fa fa-caret-down"></i></div>
                            <?php endif; ?>
                            <ul>
                                <li><a href="<?= Url::to('/new/payment') ?>"><i class="fa fa-plus"></i>New payment</a>
                                </li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to(['/payments']) ?>"><i
                                            class="fa fa-credit-card"></i>Payments</a></li>
                                <li><a href="<?= Url::to('/stats') ?>"><i class="fa fa-bar-chart"></i></i>Statistics</a>
                                </li>
                                <li class="devider"></li>
                                <li><a href="<?= Url::to('/settings') ?>"><i class="fa fa-cog"></i></i>Settings</a></li>
                                <li><?= Html::a('<i class="fa fa-power-off"></i>Log out', ['/site/logout'],
                                        ['data-method' => 'post']); ?></li>

                            </ul>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>




    <div class="container dash">
        <div class="side-menu">
            <?php
                echo SideNav::widget([
                    'type' => SideNav::TYPE_PRIMARY,
                    'heading' => false,
                    'indItem' => '',
                    'indMenuOpen' => '<i class="fa fa-angle-up"></i>',
                    'indMenuClose' => '<i class="fa fa-angle-down"></i>',
                    'iconPrefix' => 'fa fa-',
                    'items' => [
                        [
                            'url' => '/dashboard',
                            'label' => 'Dashboard',
                            'icon' => 'tachometer'
                        ],
                        [
                            'url' => '/dashboard/timeline',
                            'label' => 'Timeline',
                            'icon' => 'bars'
                        ],
                        [
                            'label' => 'Manage',
                            'icon' => 'share-alt',
                            'items' => [
                                ['label' => 'Share seaman', 'url' => '/dashboard/share'],
                                ['label' => 'Export seaman', 'url' => '/dashboard/export'],
                                [
                                    'label' => 'Seaman’s particulars',
                                    'url' => '/dashboard/particulars'
                                ],
                            ],
                        ],
                        [
                            'url' => '/dashboard/users',
                            'label' => 'Browse users',
                            'icon' => 'users'
                        ],
                        [
                            'url' => '/dashboard/compose',
                            'label' => 'Compose message',
                            'icon' => 'paper-plane'
                        ],
                        [
                            'label' => 'Crewix settings',
                            'icon' => 'cog',
                            'items' => [
                                ['label' => 'Accounts', 'url' => '/appsettings/accounts'],
                                ['label' => 'Variables', 'url' => '/appsettings/variables'],
                                [
                                    'label' => 'Dropdowns',
                                    'items' => [
                                        [
                                            'label' => 'Languages',
                                            'url' => Url::to(['/appsettings/dropdowns', 'edit' => 'languages'])
                                        ],
                                        [
                                            'label' => 'Vessel types',
                                            'url' => Url::to(['/appsettings/dropdowns', 'edit' => 'vessels'])
                                        ],
                                        [
                                            'label' => 'Vessel engines',
                                            'url' => Url::to(['/appsettings/dropdowns', 'edit' => 'engines'])
                                        ],
                                        [
                                            'label' => 'Travel documents',
                                            'url' => Url::to(['/appsettings/dropdowns', 'edit' => 'travel_docs'])
                                        ],
                                    ],
                                ],
                                [
                                    'label' => 'FAQ',
                                    'items' => [
                                        [
                                            'label' => 'Categories',
                                            'url' => Url::to(['/appsettings/faq', 'edit' => 'categories'])
                                        ],
                                        [
                                            'label' => 'Q&A',
                                            'url' => Url::to(['/appsettings/faq', 'edit' => 'qa'])
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'label' => 'Legal',
                            'icon' => 'dot-circle-o',
                            'items' => [
                                ['label' => 'Terms', 'url' => '/appsettings/terms'],
                                ['label' => 'Privacy', 'url' => '/appsettings/privacy'],
                            ],
                        ],
                    ],
                ]);
            ?>
        </div>
        <div class="content"><?= $content ?></div>

    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="f-menu">
            <a href="<?=Url::to('/about')?>">About</a><!--
         --><a href="<?=Url::to('/terms')?>">Terms</a>
        </div>
        <div class="f-logo"><img src="/images/sys/logo-small-circle.svg" alt=""></div>
        <div class="f-menu">
            <a href="<?=Url::to('/faq')?>">FAQ</a><!--
         --><a href="<?=Url::to('/feedback')?>">Feedback</a>
        </div>
    </div>
</footer>


</body>
</html>
<?php $this->endPage() ?>
