<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */
/* @var $prof \app\models\SeamanCertificateOfProficiency */
/* @var $data \app\models\Seaman */

use app\models\Users;
use app\models\Countries;
use app\models\Ranks;
use app\models\Docs;
use app\models\Langs;
use app\models\SeamanTravelDocument;
use app\models\TravelDocument;
use app\models\VesselTypes;
use app\models\SeamanCertificateOfCompetency;
use app\models\ProficiencyDocs;
use app\models\Engines;
use app\models\SeamanReference;
use app\models\MaritalStatus;
use app\models\NextOfKinRelationship;
use app\models\Sex;
use app\models\EyeColor;
use app\models\Cloth;
use app\models\Shoes;
use app\models\LanguageLevel;

$this->title = $data->first_name . ' ' . $data->last_name;

$references = SeamanReference::find()->where(['seaman_id' => $data->id])->all();
$travel_scans = SeamanTravelDocument::find()->where(['!=', 'scan_id', ''])->andWhere(['seaman_id' => $data->id])->all();
$coc_scans = SeamanCertificateOfCompetency::find()->where([
    '!=',
    'scan_id',
    ''
])->andWhere(['seaman_id' => $data->id])->all();

$a = 0;
foreach ($data->service as $s) {
    $n = strtotime($s->date_to) - strtotime($s->date_from);
    $a = $a + $n;
}
?>
    <div class="profile">
        <div class="wrapper">
            <div class="img">
                <?php if (!empty($data->profile_picture)): ?>
                    <img class="img-circle" src="/images/seaman/photo/medium/<?= $data->profile_picture ?>"
                         alt="<?= $data->first_name ?> <?= $data->last_name ?>"/>
                <?php else: ?>
                    <img class="img-circle" src="/images/sys/profile.png" alt=""/>
                <?php endif; ?>
                <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                    <div class="dl-cv text-center">
                        <?= \yii\bootstrap\Html::a('Download CV', ['downloadcv'], [
                            'class' => 'cv-profile btn btn-primary',
                            'title' => 'Download CV',
                            'target' => '_blank'
                        ]) ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="info">
                <div class="top">
                    <h1><?= $data->first_name ?> <?= $data->last_name ?></h1>
                </div>
                <div class="bot">
                    <div class="stat time">
                        <div class="digit"><?= $data->date_of_birth ?></div>
                        years old
                    </div><!--
			 -->
                    <div class="stat active">
                        <div class="digit">0</div>
                        contacts
                    </div><!--
			 -->
                    <div class="stat contracts">
                        <div class="digit"><?= round($a / 60 / 60 / 24) ?></div>
                        days at sea
                    </div>
                </div>
                <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                    <div class="update-notification text-center">
                        Don’t forget to keep your CV up to date.
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <ul class="tabs user">
            <li class="tab active" id="prof"><i class="fa fa-newspaper-o"></i> Profile</li>
            <li class="tab" id="serv"><i class="fa fa-anchor"></i> Sea service</li>
            <li class="tab" id="scans"><i class="fa fa-file-image-o"></i> Document scans</li>
            <li class="tab" id="cv-block"><i class="fa fa-file-text-o"></i> CV</li>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                <li class="tab" id="card"><i class="">@</i> Business card</li>
            <?php endif; ?>
        </ul>
    </div>
    <div class="wrapper company">
        <div class="tab_cont prof active">
            <div class="block main-info">
                <div class="bt">
                    <h2>Main information</h2>
                    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                        <span class="edit scont">Edit</span>
                    <?php endif; ?>
                </div>
                <table>
                    <tr class="first">
                        <td>
                            <?php if (!empty($data->cv_picture)): ?>
                                <img src="/images/seaman/docs/<?= $data->cv_picture ?>" alt="">
                            <?php else: ?>
                                <img src="/images/seaman/nocv.jpg" alt="">
                            <?php endif; ?>
                        </td>
                        <td colspan="2">
                            <h2><?= $data->first_name ?><?php if (!empty($data->middle_name)): ?> <?= $data->middle_name ?><?php endif ?> <?= $data->last_name ?></h2>
                            <div class="rank"><?= Ranks::find()->where(['id' => $data->primary_rank_id])->one()->name ?>
                                <?php if (!empty($data->secondary_rank_id)): ?>
                                    / <?= Ranks::find()->where(['id' => $data->secondary_rank_id])->one()->name ?>
                                <?php endif; ?>
                            </div>
                            <?= \yii\bootstrap\Html::a(Yii::$app->urlManager->createAbsoluteUrl(['/id' . Yii::$app->request->get('id')]),
                                Yii::$app->urlManager->createAbsoluteUrl(['/id' . Yii::$app->request->get('id')]),
                                ['class' => 'link']) ?>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="f-tit">Rank</td>
                        <td><?= Ranks::find()->where(['id' => $data->primary_rank_id])->one()->name ?></td>
                        <td class="f-tit">Availability</td>
                        <td>
                            <?php if ($data->status == 0): ?>
                                Available from
                                <?php if ($data->availability_date != '0000-00-00'): ?>
                                    <?= Yii::$app->formatter->asDate($data->availability_date, 'dd.MM.yyyy') ?>
                                <?php endif; ?>
                            <?php else: ?>
                                Not available
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Desired salary per month</td>
                        <td><?= \app\models\Currency::findOne(['id' => $data->salary_currency_id])->symbol ?>
                            &nbsp;<?= Yii::$app->formatter->asInteger($data->salary) ?></td>
                        <td class="f-tit">Date and place of birth</td>
                        <td>
                            <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                                <?= Yii::$app->formatter->asDate($data->date_of_birth, "php:d.m.Y") ?>
                            <?php else: ?>
                                <?= Yii::$app->formatter->asDate($data->date_of_birth, "php:Y") ?>
                            <?php endif; ?>
                            &nbsp;/&nbsp;<?= Countries::find()->where(['id' => $data->place_of_birth_id])->one()->name ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">City and country of residence</td>
                        <td><?= $data->city_of_residence ?>
                            , <?= Countries::find()->where(['id' => $data->country_of_residence_id])->one()->name ?></td>
                        <td class="f-tit">Home address</td>
                        <td>
                            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin')): ?>
                                <?= $data->address ?>
                            <?php else: ?>
                                *****
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Citizenship</td>
                        <td><?= Countries::find()->where(['id' => $data->citizenship_id])->one()->name ?></td>
                        <td class="f-tit">Closest airport</td>
                        <td><?= $data->closest_airport ?></td>
                    </tr>
                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                        <tr>
                            <td class="f-tit">Primary phone</td>
                            <td><?= $data->primary_phone ?></td>
                            <td class="f-tit">Additional phone</td>
                            <td><?= $data->secondary_phone ?></td>
                        </tr>
                        <tr>
                            <td class="f-tit">Email</td>
                            <td><?= Users::find()->where(['id' => $data->user_id])->asArray()->one()['email'] ?></td>
                            <td class="f-tit">Skype</td>
                            <td><?= $data->skype ?></td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
            <div class="block family-info">
                <div class="bt">
                    <h2>Family</h2>
                    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                        <span class="edit family">Edit</span>
                    <?php endif; ?>
                </div>
                <table>
                    <tr>
                        <td class="f-tit">Marital status</td>
                        <td><?= MaritalStatus::find()->where(['id' => $data->family->marital_status_id])->one()->name ?></td>
                        <td class="f-tit">Children</td>
                        <td>
                            <?= $data->family->children ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Mother’s name</td>
                        <td>
                            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin')): ?>
                                <?= $data->family->mother ?>
                            <?php else: ?>
                                *****
                            <?php endif; ?>
                        </td>
                        <td class="f-tit">Father’s name</td>
                        <td>
                            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin')): ?>
                                <?= $data->family->father ?>
                            <?php else: ?>
                                *****
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                <div class="block nok-info">
                    <div class="bt">
                        <h2>Next of kin</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit kin">Edit</span>
                        <?php endif; ?>
                    </div>
                    <table>
                        <tr>
                            <td class="f-tit">Full name</td>
                            <td><?= $data->nok['name'] ?></td>
                            <td class="f-tit">Relationship</td>

                            <td><?= NextOfKinRelationship::find()->where(['id' => $data->nok['relationship_id']])->one()->name ?></td>
                        </tr>
                        <tr>
                            <td class="f-tit">Phone number</td>
                            <td><?= $data->nok['phone'] ?></td>
                            <td class="f-tit">Home address</td>
                            <td><?= $data->nok['address'] ?></td>
                        </tr>
                    </table>
                </div>
            <?php endif; ?>
            <div class="block biometrics">
                <div class="bt">
                    <h2>Biometrics</h2>
                    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                        <span class="edit bio" data-id="<?= $data->user_id ?>">Edit</span>
                    <?php endif; ?>
                </div>
                <table>
                    <tr>
                        <td class="f-tit">Sex</td>
                        <td><?= Sex::find()->where(['id' => $data->biometrics->sex_id])->one()->name ?></td>
                        <td class="f-tit">Eye color</td>
                        <td><?= EyeColor::find()->where(['id' => $data->biometrics->eyes_color_id])->one()->name ?></td>
                    </tr>
                    <tr>
                        <td class="f-tit">Height</td>
                        <td><?= $data->biometrics->height ?></td>
                        <td class="f-tit">Clothing size</td>
                        <td><?= Cloth::find()->where(['id' => $data->biometrics->clothing_size_id])->one()->name ?></td>
                    </tr>
                    <tr>
                        <td class="f-tit">Weight</td>
                        <td><?= $data->biometrics->weight ?></td>
                        <td class="f-tit">Shoe size</td>
                        <td><?= Shoes::find()->where(['id' => $data->biometrics->shoe_size_id])->one()->name ?></td>
                    </tr>
                </table>
            </div>
            <div class="block lang-info">
                <div class="bt">
                    <h2>Languages</h2>
                    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                        <span class="edit lang">Edit</span>
                    <?php endif; ?>
                </div>
                <table class="langs">
                    <?php foreach ($data->languages as $lang) { ?>
                        <tr>
                            <td class="f-tit"><?= Langs::find()->where(['id' => $lang->language_id])->one()->name ?></td>
                            <td colspan="3"><?= LanguageLevel::find()->where(['id' => $lang->language_level_id])->one()->name ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || !empty($data->education)): ?>
                <div class="block education-info">
                    <div class="bt">
                        <h2>Education</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit edu">Edit</span>
                        <?php endif; ?>
                    </div>
                    <table class="education">
                        <thead>
                        <tr>
                            <td>Institution</td>
                            <td>Degree</td>
                            <td>From</td>
                            <td>To</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($data->education as $edu) { ?>
                            <tr>
                                <td class="f-tit">
                                    <?php if (\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin')): ?>
                                    <?= $edu->institution_name ?></td>
                                <?php else: ?>
                                    *****
                                <?php endif; ?>
                                <td><?= $edu->degree ?></td>
                                <td><?= $edu->year_from ?></td>
                                <td><?= $edu->year_to ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php endif; ?>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || !empty($data->travelDocs)): ?>
                <div class="block travel-info">
                    <div class="bt">
                        <h2>Travel documents</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit tr">Edit</span>
                        <?php endif; ?>
                    </div>
                    <table class="five">
                        <thead>
                        <tr>
                            <td>Document</td>
                            <td>Number</td>
                            <td>Place of issue</td>
                            <td>Date of issue</td>
                            <td>Date of expiry</td>
                        </tr>
                        </thead>
                        <?php foreach ($data->travelDocs as $tr) { ?>
                            <tr>
                                <td class="f-tit"><?= TravelDocument::find()->where(['id' => $tr->document_id])->one()->name ?></td>
                                <td>
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                    <?= $tr->number ?></td>
                                <?php else: ?>
                                    *****
                                <?php endif; ?>
                                </td>
                                <td><?= Countries::find()->where(['id' => $tr->place_of_issue_id])->one()->name ?></td>
                                <td class="date"><?= Yii::$app->formatter->asDate($tr->date_of_issue,
                                        "php:d.m.Y") ?></td>
                                <?php if ($tr->date_of_expiry != null): ?>
                                    <td class="date">
                                        <?= Yii::$app->formatter->asDate($tr->date_of_expiry, "php:d.m.Y") ?>
                                    </td>
                                <?php else: ?>
                                    <td>
                                        Unlimitted
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php endif; ?>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || !empty($data->competency)): ?>
                <div class="block competency-info">
                    <div class="bt">
                        <h2>Certificates of competency</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit comp">Edit</span>
                        <?php endif; ?>
                    </div>
                    <table class="five">
                        <thead>
                        <tr>
                            <td>Document</td>
                            <td>Number</td>
                            <td>Place of issue</td>
                            <td>Date of issue</td>
                            <td>Date of expiry</td>
                        </tr>
                        </thead>
                        <?php foreach ($data->competency as $com) { ?>
                            <tr>
                                <td class="f-tit">Rank</td>
                                <td colspan="4"><?= Ranks::find()->where(['id' => $com['rank_id']])->one()->name ?></td>
                            </tr>
                            <tr>
                                <td class="f-tit"><?= \app\models\CertificateOfCompetencyType::find()->where(['id' => $com['document_type_id']])->one()->name ?></td>
                                <td>
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                    <?= $com['number'] ?></td>
                                <?php else: ?>
                                    *****
                                <?php endif; ?>
                                </td>
                                <td><?= Countries::find()->where(['id' => $com['place_of_issue_id']])->one()->name ?></td>
                                <td class="date"><?= Yii::$app->formatter->asDate($com['date_of_issue'],
                                        "php:d.m.Y") ?></td>
                                <td class="date"><?= Yii::$app->formatter->asDate($com['date_of_expiry'],
                                        "php:d.m.Y") ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php endif; ?>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || !empty($data->proficiency)): ?>
                <div class="block proficiency">
                    <div class="bt">
                        <h2>Certificates of proficiency</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit prof">Edit</span>
                        <?php endif; ?>
                    </div>
                    <table class="five">
                        <thead>
                        <tr>
                            <td>Document</td>
                            <td>Number</td>
                            <td>Place of issue</td>
                            <td>Date of issue</td>
                            <td>Date of expiry</td>
                        </tr>
                        </thead>
                        <?php foreach ($data->proficiency as $prof) { ?>
                            <tr>
                                <td class="f-tit"><?= \app\models\CertificateOfProficiency::find()->where(['id' => $prof->document_id])->one()->name ?></td>
                                <td>
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                    <?= $prof->number ?></td>
                                <?php else: ?>
                                    *****
                                <?php endif; ?>
                                </td>
                                <td><?= Countries::find()->where(['id' => $prof->place_of_issue_id])->one()->name ?></td>
                                <td class="date"><?= Yii::$app->formatter->asDate($prof->date_of_issue,
                                        "php:d.m.Y") ?></td>
                                <td class="date"><?= Yii::$app->formatter->asDate($prof->date_of_expiry,
                                        "php:d.m.Y") ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php endif; ?>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || !empty($data->medical)): ?>
                <div class="block medical">
                    <div class="bt">
                        <h2>Medical documents</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit med">Edit</span>
                        <?php endif; ?>
                    </div>
                    <table class="five">
                        <thead>
                        <tr>
                            <td>Document</td>
                            <td>Number</td>
                            <td>Place of issue</td>
                            <td>Date of issue</td>
                            <td>Date of expiry</td>
                        </tr>
                        </thead>
                        <?php foreach ($data->medical as $med) { ?>
                            <tr>
                                <td class="f-tit"><?= \app\models\MedicalDocument::find()->where(['id' => $med->document_id])->one()->name ?></td>
                                <td>
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                    <?= $med->number ?></td>
                                <?php else: ?>
                                    *****
                                <?php endif; ?>
                                </td>
                                <td><?= Countries::find()->where(['id' => $med->place_of_issue_id])->one()->name ?></td>
                                <td class="date"><?= Yii::$app->formatter->asDate($med->date_of_issue,
                                        "php:d.m.Y") ?></td>
                                <td class="date"><?= Yii::$app->formatter->asDate($med->date_of_expiry,
                                        "php:d.m.Y") ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            <?php endif; ?>

            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || !empty($data->notes)): ?>
                <div class="block notes-info">
                    <div class="bt">
                        <h2>Notes</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit notes pull-right">Edit</span>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($data->notes)): ?>
                        <div class="text">
                            <?= $data->notes ?>
                        </div>
                    <?php else: ?>
                        <div class="empty text-center">
                            <p>You can add notes for your employers here.</p>
                            <span class="edit notes">Add notes &raquo;</span>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="tab_cont serv">
            <div class="block sea-services">
                <div class="bt">
                    <h2>Sea service</h2>
                    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                        <span class="edit sea">Edit</span>
                    <?php endif; ?>
                </div>
                <table class="services">
                    <thead>
                    <tr>
                        <td>Rank</td>
                        <td>Vessel name / Flag</td>
                        <td>Vessel type / DWT</td>
                        <td>ME type / ME power</td>
                        <td>Shipowner / Crewing agency</td>
                        <td>From / To</td>
                    </tr>
                    </thead>
                    <?php foreach ($data->service as $serv): ?>
                        <tr>
                            <td class="f-tit"><?= Ranks::find()->where(['id' => $serv->rank_id])->one()->name ?></td>
                            <td><?= $serv->vessel_name ?>
                                &nbsp;/ <?= Countries::find()->where(['id' => $serv->vessel_flag_id])->one()->name ?></td>
                            <td><?= VesselTypes::find()->where(['id' => $serv->vessel_type_id])->one()->name ?>
                                &nbsp;/ <?= Yii::$app->formatter->asInteger($serv->vessel_deadweight) ?></td>
                            <td><?= Engines::find()->where(['id' => $serv->engine_type_id])->one()->name ?>&nbsp;/
                                <?php if ($serv->measure == 'kW'): ?>
                                    <?= Yii::$app->formatter->asInteger($serv->engine_power) ?>
                                <?php else: ?>
                                    <?= Yii::$app->formatter->asInteger($serv->engine_power * 1.34) ?>
                                <?php endif; ?>&nbsp;kW
                            </td>
                            <td>
                                <?= $serv->shipowning_company_name ?>
                                <?php if (!empty($serv->crewing_company_name)): ?>
                                    &nbsp;- <?= $serv->crewing_company_name ?>
                                <?php endif; ?>
                            </td>
                            <td><?= Yii::$app->formatter->asDate($serv->date_from, "php:d.m.Y") ?>
                                &nbsp;/ <?= Yii::$app->formatter->asDate($serv->date_to, "php:d.m.Y") ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin')): ?>
                <div class="block references">
                    <div class="bt">
                        <h2>References</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit ref">Edit</span>
                        <?php endif; ?>
                    </div>
                    <table>
                        <thead>
                        <tr>
                            <td>Company</td>
                            <td>Phone number</td>
                            <td>Email</td>
                            <td>Contact person</td>
                        </tr>
                        </thead>
                        <?php foreach ($data->references as $ref): ?>
                            <tr>
                                <td class="f-tit"><?= $ref->company_name ?></td>
                                <td><?= $ref->phone ?></td>
                                <td><?= $ref->email ?></td>
                                <td>
                                    <?php if (!empty($ref->contact_person)): ?>
                                        <?= $ref->contact_person ?>
                                    <?php else: ?>
                                        &mdash;
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            <?php endif; ?>
            <!--<div class="block">
                <div class="bt">
                    <h2>Statistics</h2>
                    <?php /*if (\app\models\User::allowForCurrentUser($data->user_id)): */ ?>
                        <span class="edit ref">Edit</span>
                    <?php /*endif; */ ?>
                </div>
                <table>
                </table>
            </div>-->
        </div>
        <div class="tab_cont cv-block">
            <?php if (\app\models\User::allowForCurrentUser($data->user_id) || !empty($data->cv)): ?>
                <div class="block cv-info">
                    <div class="bt">
                        <h2>CV</h2>
                        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                            <span class="edit cv pull-right">Edit</span>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($data->cv)): ?>
                        <div class="text">
                            <?= $data->cv ?>
                        </div>
                        <div class="clearfix"></div>
                    <?php else: ?>
                        <div class="empty text-center">
                            <p>Offshore CVs are not the same as onshore ones. To solve this, we made this section. You
                                can
                                add your offshore CV here.</p>
                            <span class="edit cv">Add CV &raquo;</span>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="tab_cont scans">
            <div class="block">
                <div class="bt">
                    <h2>Document scans</h2>
                    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                        <span class="edit add-scan">Edit</span>
                    <?php endif; ?>
                </div>
                <div class="docs">
                    <?php if (!empty($travel_scans)): ?>
                        <div class="bt">
                            <h2>Travel documents</h2>
                        </div>
                        <?php $base = __DIR__ . '/../../../'; ?>

                        <?php foreach ($travel_scans as $scan): ?>
                            <?php $filedata = exif_read_data(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src) ?>
                            <?php $size = getimagesize(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src) ?>
                            <div class="item">
                                <div class="img">
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                        <img
                                            src="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src ?>"
                                            alt="">
                                    <?php else: ?>
                                        <div class="hide-img"><img src="/images/sys/cloud-ico.svg" alt=""/></div>
                                    <?php endif; ?>
                                </div>
                                <div class="info">
                                    <div class="docname">
                                        <h3><?= TravelDocument::find()->where(['id' => $scan->document_id])->one()->name ?></h3>
                                    </div>
                                    <div class="additional">Issued
                                        in <?= Countries::find()->where(['id' => $scan->place_of_issue_id])->one()->name ?>
                                        / <?= round($filedata['FileSize'] / 1024 / 1024, 2) ?> MB / <?= $size[0] ?>
                                        x<?= $size[1] ?></div>
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                        <div class="buttons">
                                            <a class="view_doc btn btn-primary"
                                               href="/images/seaman/docs/scans/<?= \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src ?>"><i
                                                    class="fa fa-search-plus"></i> View
                                                document</a> <a
                                                href="/images/seaman/docs/scans/<?= \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src ?>"
                                                class="dl btn btn-primary" target="_blank"><i
                                                    class="fa fa-download"></i> Download</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (!empty($coc_scans)): ?>
                        <div class="bt">
                            <h2>Certificate of Competency</h2>
                        </div>
                        <?php $base = __DIR__ . '/../../';
                        ?>

                        <?php foreach ($coc_scans as $sc): ?>
                            <?php $filedata = exif_read_data(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src) ?>
                            <?php $size = getimagesize(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src) ?>
                            <div class="item">
                                <div class="img">
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                        <img
                                            src="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src ?>"
                                            alt="">
                                    <?php else: ?>
                                        <div class="hide-img"><img src="/images/sys/cloud-ico.svg" alt=""/></div>
                                    <?php endif; ?>
                                </div>
                                <div class="info">
                                    <div class="docname">
                                        <h3><?= \app\models\CertificateOfCompetencyType::findOne($sc->document_type_id)->name ?></h3>
                                    </div>
                                    <div class="additional">Issued
                                        in <?= Countries::find()->where(['id' => $sc->place_of_issue_id])->one()->name ?>
                                        / <?= round($filedata['FileSize'] / 1024 / 1024, 2) ?> MB / <?= $size[0] ?>
                                        x<?= $size[1] ?></div>
                                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                                        <div class="buttons">
                                            <a class="view_doc btn btn-primary"
                                               href="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src ?>"><i
                                                    class="fa fa-search-plus"></i> View
                                                document</a> <a
                                                href="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src ?>"
                                                class="dl btn btn-primary" target="_blank"><i
                                                    class="fa fa-download"></i> Download</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (empty($travel_scans) && empty($coc_scans)): ?>
                        <div class="empty">
                            <p>Upload document scans to increase the chances of being hired dramatically.</p>
                            <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                                <a class="add-scan">Upload scans &raquo;</a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?= $this->render('seaman/_modal_view_scan'); ?>
        </div>
        <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
            <div class="tab_cont card">
                <div class="block clearfix">
                    <div class="bt">
                        <h2>Business card</h2>
                    </div>
                    <div class="card-block clearfix" data-card="one-side">
                        <div class="card-wrap pull-left col-lg-6 one-side">
                            <div class="card-front"
                                 style="height: 240px;background: #e9e9e9;padding: 5px;border-radius: 3px;">
                                <div class="card-cont clearfix"
                                     style="background: #4f8ec4;border-radius: 3px;height: 100%;">
                                    <div class="card-info pull-left col-lg-6"
                                         style="color:#fff; padding: 32px 15px 32px 25px;position: relative;">
                                        <div class="name"
                                             style="text-transform: uppercase; font-size: 14px; font-weight: bold;">
                                            <?= $data->first_name ?> <?= $data->last_name ?>
                                        </div>
                                        <div class="rank" style="text-transform: uppercase;font-size: 12px;">
                                            <?= Ranks::findOne($data->primary_rank_id)->name ?>
                                        </div>
                                        <div class="card-contacts" style="margin-top: 30px; font-size: 10px;">
                                            <div style="margin-bottom: 10px;"><?= $data->primary_phone ?></div>
                                            <div
                                                style="margin-bottom: 10px;"><?= Yii::$app->user->identity->email ?></div>
                                            <?php if (!empty($data->skype)): ?>
                                                <div style="margin-bottom: 10px;"><?= $data->skype ?></div>
                                            <?php endif; ?>
                                            <div
                                                style="margin-bottom: 10px;"><?= Yii::$app->urlManager->createAbsoluteUrl(['/id' . Yii::$app->request->get('id')]); ?></div>
                                        </div>
                                        <div class="dev"
                                             style="position: absolute;width: 4px;height: 190px;background: #fff;top: 0;bottom: 0;right: 0;margin: auto;"></div>
                                    </div>
                                    <div class="card-logo pull-right col-lg-6" style="padding: 93px 15px;">
                                        <img
                                            src="<?= Yii::$app->urlManager->createAbsoluteUrl('images') ?>/sys/logo-rectangle.png"
                                            alt="" style="width: 100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-wrap pull-left col-lg-6 two-side hidden">
                            <div class="card-front"
                                 style="height: 240px;background: #e9e9e9;padding: 5px;border-radius: 3px;">
                                <div class="card-cont clearfix"
                                     style="background: #4f8ec4;border-radius: 3px;height: 100%;padding: 70px 35px;">
                                    <img
                                        src="<?= Yii::$app->urlManager->createAbsoluteUrl('images') ?>/sys/logo-rectangle.png"
                                        alt="" style="width: 100%">
                                </div>
                            </div>
                            <div class="card-back"
                                 style="height: 240px;background: #e9e9e9;padding: 5px;border-radius: 3px;margin-top: 20px;">
                                <div class="card-cont clearfix"
                                     style="background: #4f8ec4;border-radius: 3px;height: 100%;">
                                    <div class="card-info pull-left col-lg-6"
                                         style="color:#fff; padding: 32px 15px 32px 25px;position: relative;">
                                        <div class="name"
                                             style="text-transform: uppercase; font-size: 14px; font-weight: bold;">
                                            <?= $data->first_name ?> <?= $data->last_name ?>
                                        </div>
                                        <div class="rank" style="text-transform: uppercase;font-size: 12px;">
                                            <?= Ranks::findOne($data->primary_rank_id)->name ?>
                                        </div>
                                        <div class="card-contacts" style="margin-top: 30px; font-size: 10px;">
                                            <div style="margin-bottom: 10px;"><?= $data->primary_phone ?></div>
                                            <div
                                                style="margin-bottom: 10px;"><?= Yii::$app->user->identity->email ?></div>
                                            <?php if (!empty($data->skype)): ?>
                                                <div style="margin-bottom: 10px;"><?= $data->skype ?></div>
                                            <?php endif; ?>
                                            <div
                                                style="margin-bottom: 10px;"><?= Yii::$app->urlManager->createAbsoluteUrl(['/id' . Yii::$app->request->get('id')]); ?></div>
                                        </div>
                                    </div>
                                    <div class="card-logo pull-right col-lg-6" style="padding: 0;">
                                        <div class="circle-bg"
                                             style="background: #fff;width: 100%;height: 230px;border-radius: 120px 0 0 120px;padding: 60px 0;">
                                            <div class="img-bg"
                                                 style="background: #4f8ec4;border-radius: 50%;width: 107px;margin: 0 auto 0 65px;">
                                                <img
                                                    src="<?= Yii::$app->urlManager->createAbsoluteUrl('images') ?>/sys/logo-small-circle.png"
                                                    alt="" style="width: 100%">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cart-form pull-right col-lg-6">
                            <?php $form = \yii\bootstrap\ActiveForm::begin([
                                'id' => 'edit_group-form',
                                'options' => ['class' => 'edit_group_form'],
                                'validateOnBlur' => false,
                                'validateOnChange' => false,
                                'action' => '/get-card',
                            ]); ?>
                            <label>Make the right impression</label>
                            <p class="tip">Hand over a stunning business card to a crewing
                                company and they will likely keep it to hire you later.</p>

                            <p class="tip">Choose card type, color and press download.</p>
                            <?php $card_form->card_type = 1 ?>
                            <?php $card_form->color = '#4f8ec4' ?>
                            <?= $form->field($card_form, 'card_type',
                                ['options' => ['class' => 'clearfix']])->radioList([
                                '1' => 'One-sided',
                                '2' => 'Two-sided'
                            ], ['class' => 'clearfix'])->label('Card type'); ?>

                            <?= $form->field($card_form, 'color')->radioList([
                                '#4f8ec4' => '<span style="background: #4f8ec4"></span>',
                                '#1eb19a' => '<span style="background: #1eb19a"></span>',
                                '#e2b153' => '<span style="background: #e2b153"></span>',
                                '#da5e5b' => '<span style="background: #da5e5b"></span>',
                                '#8878aa' => '<span style="background: #8878aa"></span>',
                                '#28292b' => '<span style="background: #28292b"></span>'
                            ], ['class' => 'clearfix']) ?>
                            <label>Tip</label>
                            <p class="tip">After you download the card, go ahead and visit a local print </p>
                            <?= $form->field($card_form, 'code')->hiddenInput()->label(false); ?>
                            <?= \yii\bootstrap\Html::submitButton('<i class="fa fa-download"></i> Download',
                                [
                                    'class' => 'btn btn-primary save-about',
                                    'name' => 'signup-button',
                                    'id' => 'c_sign'
                                ]) ?>
                            <?php \yii\bootstrap\ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>

<?php
$this->registerJs('
        $(\'.view_doc\').on(\'click\', function(){
            var data = \'<div class="text-center"><img src="\'+$(this).attr(\'href\')+\'" /></div>\';
            $(\'#scan_view .modal-header\').html($(this).parents(\'.info\').find(\'.docname\').text());
            $(\'#scan_view .modal-body\').html(data);
            $(\'#scan_view\').modal(\'show\');
            return false;
        });
        $(\'body\').on(\'change\',\'#cardform-color input\', function(){
            var color = $(this).val();
            $(\'.card-cont, .img-bg\').css(\'background\', color);
        });
        $(\'body\').on(\'change\',\'#cardform-card_type input\', function(){
            if($(this).val()==1){
                var code = $(\'.one-side\').html();
                $(\'.one-side\').removeClass(\'hidden\');
                $(\'.two-side\').addClass(\'hidden\');
                $(\'.card-block\').attr(\'data-card\',\'one-side\');
                $(\'#cardform-code\').val(code);
            }else{
                var code = $(\'.two-side\').html();
                $(\'.one-side\').addClass(\'hidden\');
                $(\'.two-side\').removeClass(\'hidden\');
                $(\'.card-block\').attr(\'data-card\',\'two-side\');
                $(\'#cardform-code\').val(code);
            }
        });

//        $(\'body\').on(\'submit\',\'#edit_group-form\',function(){
//            var act = $(\'.card-block\').attr(\'data-card\');
//            var data = $(\'.\'+act).html();;
//            $.ajax({
//                url:\'/get-card\',
//                data:{code:data},
//                type:\'POST\',
//                success:function(data){
//                    //console.log(data);
//                }
//            });
//            return false;
//        });
    ', \yii\web\View::POS_READY);
?>
<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add.travel-doc\',function(){
    var tpl = $(\'#new-travel-row\').html();

    $(\'#stravel table tbody\').append(tpl);
    $(\'#stravel select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
    });
    $(\'.date\').each(function(j,e){
        $(e).find(\'input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });

});
$(\'body\').on(\'click\',\'.add.institute\',function(){
    var tpl = $(\'#new-education-row\').html();

    $(\'#sedu table tbody\').append(tpl);
    $(\'#sedu select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
    });
});
', \yii\web\View::POS_READY, 'row');
?>
