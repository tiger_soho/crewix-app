<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */
/* @var $prof \app\models\SeamanCertificateOfProficiency */

use app\models\Countries;
use app\models\Ranks;
use app\models\SeamanCertificateOfCompetency;

?>

<div class="bt">
    <h2>Medical documents</h2>
    <?php if(\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit med">Edit</span>
    <?php endif; ?>
</div>
<table class="five">
    <thead>
    <tr>
        <td>Document</td>
        <td>Number</td>
        <td>Place of issue</td>
        <td>Date of issue</td>
        <td>Date of expiry</td>
    </tr>
    </thead>
    <?php foreach ($data->medical as $med) { ?>
        <tr>
            <td class="f-tit"><?= \app\models\MedicalDocument::find()->where(['id' => $med->document_id])->one()->name ?></td>
            <td>
                <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                <?= $med->number ?></td>
            <?php else: ?>
                *****
            <?php endif; ?>
            </td>
            <td><?= Countries::find()->where(['id' => $med->place_of_issue_id])->one()->name ?></td>
            <td class="date"><?= Yii::$app->formatter->asDate($med->date_of_issue, "php:d.m.Y") ?></td>
            <td class="date"><?= Yii::$app->formatter->asDate($med->date_of_expiry, "php:d.m.Y") ?></td>
        </tr>
    <?php } ?>
</table>
