<?php
use yii\helpers\Url;
use app\models\Currency;
use app\models\Ranks;
use app\models\Countries;
use app\models\SeamanLanguage;
use app\models\Profile;
use app\models\CompanyType;
use app\models\Vacancies;

/* @var $this yii\web\View */
/* @var $model app\models\Seaman */

$this->title = 'Seamen';
?>


<div class="img">
    <?php if (!empty($model->logo)): ?>
        <img src="images/companies/logos/small/<?= $model->logo ?>" alt="<?= $model->name ?>"/>
    <?php else: ?>
        <img src="images/companies/logos/noimage.jpg" alt="">
    <?php endif; ?>
</div>
<div class="info">
    <h2><a href="<?= Url::to([
            'profile',
            'id' => Profile::find()->where(['user_id' => $model->user_id])->one()->id
        ]) ?>"><?= $model->name ?></a></h2>
    <div class="detales">
        <div class="ctype"><i
                class="fa fa-university"></i> <?= CompanyType::find()->select('name')->where(['id' => $model->type_id])->one()->name ?>
        </div>
        <div class="country"><i
                class="fa fa-globe "></i> <?= @Countries::find()->select('name')->where(['id' => $model->companyContact->hq_country_id])->one()->name ?>
        </div>
    </div>
</div>
<div class="vac_count">
    <div class="qty">
        <?= Vacancies::find()->where(['status' => 1])->andWhere(['company_id' => $model->id])->count(); ?>
    </div>
    <div class="period">vacansies</div>
</div>
