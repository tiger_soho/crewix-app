<?php

use app\models\Users;
use app\models\Countries;
use app\models\Ranks;
use app\models\Langs;
use app\models\TravelDocument;
use app\models\VesselTypes;
use app\models\Engines;
use app\models\MaritalStatus;
use app\models\NextOfKinRelationship;
use app\models\Sex;
use app\models\EyeColor;
use app\models\Cloth;
use app\models\Shoes;
use app\models\LanguageLevel;

?>

<div class="wrapper company">
    <div class="prof active">
        <div class="block main-info">
            <table>
                <tr class="first">
                    <td style="text-align: center;">
                        <?php if (!empty($data->cv_picture)): ?>
                            <img style="width: 150px;" src="<?=Yii::$app->basePath?>/web/images/seaman/docs/<?= $data->cv_picture ?>" alt="">
                        <?php else: ?>
                            <img style="width: 150px;" src="<?=Yii::$app->basePath?>/web/images/seaman/nocv.jpg" alt="">
                        <?php endif; ?>
                    </td>
                    <td colspan="2" style="text-align: center;">
                        <h2><?= $data->first_name ?><?php if (!empty($data->middle_name)): ?> <?= $data->middle_name ?><?php endif ?> <?= $data->last_name ?></h2>
                        <div class="rank"><?= Ranks::find()->where(['id' => $data->primary_rank_id])->one()->name ?>
                            / <?= Ranks::find()->where(['id' => $data->secondary_rank_id])->one()->name ?></div>

                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Rank</td>
                    <td><?= Ranks::find()->where(['id' => $data->primary_rank_id])->one()->name ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Availability</td>
                    <td>
                        <?php if($data->status==0): ?>
                            Available from
                            <?php if($data->availability_date!='0000-00-00'): ?>
                                <?=Yii::$app->formatter->asDate($data->availability_date,'dd.MM.yyyy')?>
                            <?php endif; ?>
                        <?php else: ?>
                            Not available
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Desired salary per month</td>
                    <td><?=\app\models\Currency::findOne(['id'=>$data->salary_currency_id])->symbol?>&nbsp;<?= Yii::$app->formatter->asInteger($data->salary) ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Date and place of birth</td>
                    <td>
                        <?= Yii::$app->formatter->asDate($data->date_of_birth, "php:d.m.Y") ?>
                        &nbsp;/&nbsp;<?= Countries::find()->where(['id' => $data->place_of_birth_id])->one()->name ?>
                    </td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">City and country of residence</td>
                    <td><?= $data->city_of_residence ?>
                        , <?= Countries::find()->where(['id' => $data->country_of_residence_id])->one()->name ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Home address</td>
                    <td>
                        <?= $data->address ?>
                    </td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Citizenship</td>
                    <td><?= Countries::find()->where(['id' => $data->citizenship_id])->one()->name ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Closest airport</td>
                    <td><?= $data->closest_airport ?></td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Primary phone</td>
                    <td><?= $data->primary_phone ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Additional phone</td>
                    <td><?= $data->secondary_phone ?></td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Email</td>
                    <td><?= Users::find()->where(['id' => $data->user_id])->asArray()->one()['email'] ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Skype</td>
                    <td><?= $data->skype ?></td>
                </tr>
            </table>
        </div>
        <div class="block family-info">
            <div class="bt">
                <h2>Family</h2>
            </div>
            <table>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Marital status</td>
                    <td><?= MaritalStatus::find()->where(['id' => $data->family->marital_status_id])->one()->name ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Children</td>
                    <td>
                        <?= $data->family->children ?>
                    </td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Mother’s name</td>
                    <td>
                        <?= $data->family->mother ?>
                    </td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Father’s name</td>
                    <td>
                        <?= $data->family->father ?>
                    </td>
                </tr>
            </table>
        </div>
            <div class="block nok-info">
                <div class="bt">
                    <h2>Next of kin</h2>
                </div>
                <table>
                    <tr>
                        <td class="f-tit" style="font-weight: bold;text-align: right;">Full name</td>
                        <td><?= $data->nok['name']?></td>
                        <td class="f-tit" style="font-weight: bold;text-align: right;">Relationship</td>

                        <td><?= NextOfKinRelationship::find()->where(['id' => $data->nok['relationship_id']])->one()->name ?></td>
                    </tr>
                    <tr>
                        <td class="f-tit" style="font-weight: bold;text-align: right;">Phone number</td>
                        <td><?= $data->nok['phone']?></td>
                        <td class="f-tit" style="font-weight: bold;text-align: right;">Home address</td>
                        <td><?= $data->nok['address']?></td>
                    </tr>
                </table>
            </div>
        <div class="block biometrics">
            <div class="bt">
                <h2>Biometrics</h2>
            </div>
            <table>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Sex</td>
                    <td><?= Sex::find()->where(['id' => $data->biometrics->sex_id])->one()->name ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Eye color</td>
                    <td><?= EyeColor::find()->where(['id' => $data->biometrics->eyes_color_id])->one()->name ?></td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Height</td>
                    <td><?= $data->biometrics->height ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Clothing size</td>
                    <td><?= Cloth::find()->where(['id' => $data->biometrics->clothing_size_id])->one()->name ?></td>
                </tr>
                <tr>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Weight</td>
                    <td><?= $data->biometrics->weight ?></td>
                    <td class="f-tit" style="font-weight: bold;text-align: right;">Shoe size</td>
                    <td><?= Shoes::find()->where(['id' => $data->biometrics->shoe_size_id])->one()->name ?></td>
                </tr>
            </table>
        </div>
        <div class="block lang-info">
            <div class="bt">
                <h2>Languages</h2>
            </div>
            <table class="langs">
                <?php foreach ($data->languages as $lang) { ?>
                    <tr>
                        <td class="f-tit" style="font-weight: bold;text-align: right;"><?= Langs::find()->where(['id' => $lang->language_id])->one()->name ?></td>
                        <td colspan="3"><?= LanguageLevel::find()->where(['id' => $lang->language_level_id])->one()->name ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <?php if (!empty($data->education)): ?>
            <div class="block education-info">
                <div class="bt">
                    <h2>Education</h2>
                </div>
                <table class="education">
                    <thead>
                    <tr>
                        <td>Institution</td>
                        <td>Degree</td>
                        <td>From</td>
                        <td>To</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data->education as $edu) { ?>
                        <tr>
                            <td class="f-tit" style="font-weight: bold;text-align: right;">
                                <?php if (\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin')): ?>
                                <?= $edu->institution_name ?></td>
                            <?php else: ?>
                                *****
                            <?php endif; ?>
                            <td><?= $edu->degree ?></td>
                            <td><?= $edu->year_from ?></td>
                            <td><?= $edu->year_to ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
        <?php if (!empty($data->travelDocs)): ?>
            <div class="block travel-info">
                <div class="bt">
                    <h2>Travel documents</h2>
                </div>
                <table class="five">
                    <thead>
                    <tr>
                        <td>Document</td>
                        <td>Number</td>
                        <td>Place of issue</td>
                        <td>Date of issue</td>
                        <td>Date of expiry</td>
                    </tr>
                    </thead>
                    <?php foreach ($data->travelDocs as $tr) { ?>
                        <tr>
                            <td class="f-tit" style="border: 1px solid #a6a6a6;font-weight: bold;text-align: right;padding: 10px;"><?= TravelDocument::find()->where(['id' => $tr->document_id])->one()->name ?></td>
                            <td style="border: 1px solid #a6a6a6;padding: 10px;">
                                <?= $tr->number ?></td>
                            </td>
                            <td style="border: 1px solid #a6a6a6;"><?= Countries::find()->where(['id' => $tr->place_of_issue_id])->one()->name ?></td>
                            <td class="date" style="border: 1px solid #a6a6a6;padding: 10px;"><?= Yii::$app->formatter->asDate($tr->date_of_issue, "php:d.m.Y") ?></td>
                            <?php if ($tr->date_of_expiry != null): ?>
                                <td class="date" style="border: 1px solid #a6a6a6;padding: 10px;">
                                    <?= Yii::$app->formatter->asDate($tr->date_of_expiry, "php:d.m.Y") ?>
                                </td>
                            <?php else: ?>
                                <td>
                                    Unlimitted
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php endif; ?>
        <?php if (!empty($data->competency)): ?>
            <div class="block competency-info">
                <div class="bt">
                    <h2>Certificates of competency</h2>
                </div>
                <table class="five">
                    <thead>
                    <tr>
                        <td>Document</td>
                        <td>Number</td>
                        <td>Place of issue</td>
                        <td>Date of issue</td>
                        <td>Date of expiry</td>
                    </tr>
                    </thead>
                    <?php foreach ($data->competency as $com) { ?>
                        <tr>
                            <td class="f-tit" style="font-weight: bold;text-align: right;">Rank</td>
                            <td colspan="4"><?= Ranks::find()->where(['id' => $com['rank_id']])->one()->name ?></td>
                        </tr>
                        <tr>
                            <td class="f-tit" style="font-weight: bold;text-align: right;"><?= \app\models\CertificateOfCompetencyType::find()->where(['id' => $com['document_type_id']])->one()->name ?></td>
                            <td>
                                <?= $com['number'] ?></td>
                            </td>
                            <td><?= Countries::find()->where(['id' => $com['place_of_issue_id']])->one()->name ?></td>
                            <td class="date"><?= Yii::$app->formatter->asDate($com['date_of_issue'], "php:d.m.Y") ?></td>
                            <td class="date"><?= Yii::$app->formatter->asDate($com['date_of_expiry'], "php:d.m.Y") ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php endif; ?>
        <?php if (!empty($data->proficiency)): ?>
            <div class="block proficiency">
                <div class="bt">
                    <h2>Certificates of proficiency</h2>
                </div>
                <table class="five">
                    <thead>
                    <tr>
                        <td>Document</td>
                        <td>Number</td>
                        <td>Place of issue</td>
                        <td>Date of issue</td>
                        <td>Date of expiry</td>
                    </tr>
                    </thead>
                    <?php foreach ($data->proficiency as $prof) { ?>
                        <tr>
                            <td class="f-tit" style="font-weight: bold;text-align: right;"><?= \app\models\CertificateOfProficiency::find()->where(['id' => $prof->document_id])->one()->name ?></td>
                            <td>
                                <?= $prof->number ?></td>
                            </td>
                            <td><?= Countries::find()->where(['id' => $prof->place_of_issue_id])->one()->name ?></td>
                            <td class="date"><?= Yii::$app->formatter->asDate($prof->date_of_issue, "php:d.m.Y") ?></td>
                            <td class="date"><?= Yii::$app->formatter->asDate($prof->date_of_expiry, "php:d.m.Y") ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php endif; ?>
        <?php if (!empty($data->cv)): ?>
            <div class="block cv-info">
                <div class="bt">
                    <h2>CV</h2>
                </div>
                    <div class="text">
                        <?= $data->cv ?>
                    </div>

            </div>
        <?php endif; ?>
        <?php if (!empty($data->notes)): ?>
            <div class="block notes-info">
                <div class="bt">
                    <h2>Notes</h2>
                </div>
                    <div class="text">
                        <?= $data->notes ?>
                    </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="serv">
        <?php if(!empty($data->service)): ?>
        <div class="block sea-services">
            <div class="bt">
                <h2>Sea service</h2>
            </div>
            <table class="services">
                <thead>
                <tr>
                    <td>Rank</td>
                    <td>Vessel name / Flag</td>
                    <td>Vessel type / DWT</td>
                    <td>ME type / ME power</td>
                    <td>Shipowner / Crewing agency</td>
                    <td>From / To</td>
                </tr>
                </thead>
                <?php foreach ($data->service as $serv): ?>
                    <tr>
                        <td class="f-tit" style="font-weight: bold;text-align: right;"><?= Ranks::find()->where(['id' => $serv->rank_id])->one()->name ?></td>
                        <td><?= $serv->vessel_name ?>
                            &nbsp;/ <?= Countries::find()->where(['id' => $serv->vessel_flag_id])->one()->name ?></td>
                        <td><?= VesselTypes::find()->where(['id' => $serv->vessel_type_id])->one()->name ?>
                            &nbsp;/ <?= Yii::$app->formatter->asInteger($serv->vessel_deadweight) ?></td>
                        <td><?= Engines::find()->where(['id' => $serv->engine_type_id])->one()->name ?>&nbsp;/
                            <?php if ($serv->measure == 'kW'): ?>
                                <?= Yii::$app->formatter->asInteger($serv->engine_power) ?>
                            <?php else: ?>
                                <?= Yii::$app->formatter->asInteger($serv->engine_power * 1.34) ?>
                            <?php endif; ?>&nbsp;kW
                        </td>
                        <td>
                            <?= $serv->shipowning_company_name ?>
                            <?php if (!empty($serv->crewing_company_name)): ?>
                                &nbsp;- <?= $serv->crewing_company_name ?>
                            <?php endif; ?>
                        </td>
                        <td><?= Yii::$app->formatter->asDate($serv->date_from, "php:d.m.Y") ?>
                            &nbsp;/ <?= Yii::$app->formatter->asDate($serv->date_to, "php:d.m.Y") ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
        <?php if (!empty($data->references)): ?>
            <div class="block references">
                <div class="bt">
                    <h2>References</h2>
                </div>
                <table>
                    <thead>
                    <tr>
                        <td style="font-weight: bold;">Company</td>
                        <td style="font-weight: bold;">Phone number</td>
                        <td style="font-weight: bold;">Email</td>
                        <td style="font-weight: bold;">Contact person</td>
                    </tr>
                    </thead>
                    <?php foreach ($data->references as $ref): ?>
                        <tr>
                            <td class="f-tit" style="font-weight: bold;text-align: right;"><?= $ref->company_name ?></td>
                            <td><?= $ref->phone ?></td>
                            <td><?= $ref->email ?></td>
                            <td>
                                <?php if (!empty($ref->contact_person)): ?>
                                    <?= $ref->contact_person ?>
                                <?php else: ?>
                                    &mdash;
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>

    </div>
</div>