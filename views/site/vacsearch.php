<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Ranks;
use app\models\vesselTypes;
use app\models\Currency;
use yii\widgets\Pjax;
use yii\widgets\ListView;
$this->title = 'Vacancies';
?>
<?php Pjax::begin(['id' => 'vacancies']); ?>
<?= ListView::widget([
	'dataProvider' => $dataProvider,
	'itemOptions' => ['class' => 'item'],
	'summary' => '',
	'itemView' => 'vacancies/_item',
]) ?>
<?php Pjax::end(); ?>