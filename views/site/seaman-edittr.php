<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
?>

<div class="bt">
    <h2>Travel documents</h2>
    <span class="cancel travel pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'stravel',
    'options' => ['class'=>'travel_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
<div class="">
    <table>
        <thead>
        <tr>
            <td>Document</td>
            <td>Number</td>
            <td>Place of issue</td>
            <td>Date of issue</td>
            <td>Date of expiry</td>
        </tr>
        </thead>
        <?php foreach($data->travelDocs as $item): ?>
            <tr>
                <td>
                    <?=$form->field($model,'id[]')->hiddenInput(['value'=>$item['id']])->label(false)?>
                    <?= $form->field($model, 'document_id[]')
                        ->dropDownList(ArrayHelper::map(\app\models\TravelDocument::find()->all(),'id','name'), ['options'=> [$item['document_id']=>['Selected'=>'selected']]],['placeholder'=>'Select year'])->label(false);?>
                </td>
                <td><?=$form->field($model,'number[]')->textInput(['placeholder'=>'Enter number','value'=>$item['number']])->label(false)?></td>
                <td>
                    <?= $form->field($model, 'place_of_issue_id[]')
                        ->dropDownList(ArrayHelper::map(Countries::find()->all(),'id','name'), ['options'=> [$item['place_of_issue_id']=>['Selected'=>'selected']]],['placeholder'=>'Select year'])->label(false);?>
                </td>
                <td>
                    <div class="date">
                        <?=$form->field($model,'date_of_issue[]')->textInput(['placeholder'=>'dd.mm.yyyy', 'value'=>Yii::$app->formatter->asDate($item['date_of_issue'],'dd.MM.Y')])->label(false)?>
                    </div>
                </td>
                <td>
                    <div class="date">
                        <?= $form->field($model, 'date_of_expiry[]',
                            ['options' => ['enableAjaxValidation' => true]])->textInput([
                            'placeholder' => 'dd.mm.yyyy',
                            'value' => Yii::$app->formatter->asDate($item['date_of_expiry'], 'dd.MM.y')
                        ])->label(false) ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>

    </table>
    <span class="add travel-doc btn btn-primary" data-id=""><i class="fa fa-plus"></i> Document</span>
</div>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
    <script type="text/template" id="new-travel-row">
        <?= $this->render('/signup/rowtr', [
            'model' => $model,
        ]); ?>
    </script>

