<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Langs;
use app\models\Competency;
$lang_list = Langs::find()->all();
?>
<div class="bt">
    <h2>Languages</h2>
    <span class="cancel languages pull-right">Cancel</span>
</div>
    <?php $form = ActiveForm::begin(['id' => 'sledit',
        'options' => ['class'=>'lang_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>
    <table>
        <?php foreach ($data->languages as $lang) { ?>
            <tr>
                <td class="f-tit lang">
                <?=Html::activeHiddenInput($model, "id[]", ['value'=>$lang->id]);?>
                <?php if($lang['language_id']==1): ?>
                    <span class="req">*</span> English
                    <?=Html::activeHiddenInput($model, "language_id[]", ['value'=>$lang['language_id']]);?>
                <?php else: ?>
                    <?=$form->field($model,'language_id[]')->dropDownList(ArrayHelper::map(Langs::find()->where(['!=','id','1'])->all(),'id','name'), ['options'=>[$lang['language_id']=>['Selected'=>'selected']]])->label(false)?>

                <?php endif; ?>
                </td>
                <td colspan="3">
                    <?= $form->field($model, 'language_level_id[]')->dropDownList(ArrayHelper::map(\app\models\LanguageLevel::find()->all(),'id','name'), ['prompt'=>'Select language level','options'=>[$lang['language_level_id']=>['Selected'=>'selected']]])->label(false);?></td>
            </tr> 
        <?php } ?>
    </table>
    <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Language</span>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    
</div>
<?php ActiveForm::end(); ?>
<script>
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
          _renderItem: function( ul, item ) {
            var li = $( "<li>", { text: item.label } );
     
            if ( item.disabled ) {
              li.addClass( "ui-state-disabled" );
            }
     
            $( "<span>", {
              style: item.element.attr( "data-style" ),
              "class": "ui-icon " + item.element.attr( "data-class" )
            })
              .appendTo( li );
     
            return li.appendTo( ul );
          }
        });
    $('select').each(function(i,el){
        if(!$(el).parent().hasClass('countries')){
            $(el).selectmenu({
                width: '100%'
            });
        }else{
            $(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
        }
    });
</script>

<script type="text/template" id="new-lang-row">
    <?= $this->render('/signup/row', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
    $(\'body\').on(\'click\',\'.add\',function(){
        var tpl = $(\'#new-lang-row\').html();

        $(\'#sledit table tbody\').append(tpl);
        $(\'#sledit select\').each(function(i,el){
            $(el).selectmenu({
                width: \'100%\'
            });
        });

    });
    ', \yii\web\View::POS_READY, 'lang-add');
?>