<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Crewix';
?>

<div id="edge">
    <div class="mid">
        <div class="title">Welcome to Crewix.</div>
        <div class="subtitle">Cloud-based crewing platform for seamen and crewing companies.</div>
        <a href="<?= Url::to('signup') ?>" class="learn">Get started</a>
        <a href="#" class="learn">Learn more</a>
    </div>
    <div id="Stage1" class="EDGE-1462813254"></div>
    <div id="Stage" class="EDGE-1455169207"></div>
</div>

<div class="nav-follower">
    <div class="wrapper">
        <a href="#" id="info">Overview</a>
        <a href="#" id="seamen">For a seaman</a>
        <a href="#" id="com">For a company</a>
    </div>

</div>

<div class="site-index">
    <div class="wrapper">
        <div class="cont info">
            <div class="item">
                <div class="img">
                    <img src="/images/sys/icanchr.svg" alt=""/>
                </div>
                <div class="tit">Build your career</div>
                <div class="txt">Fill your CV and store it in the cloud. Search and apply for vacancies. Be hired. It’s
                    that simple.
                </div>
            </div><!--
    	 -->
            <div class="item">
                <div class="img">
                    <img src="/images/sys/icslmn.svg" alt=""/>
                </div>
                <div class="tit">Powerful enough for anyone</div>
                <div class="txt">Crewix is packed with tons of features that take crewing business to a whole new
                    level.
                </div>
            </div><!--
    	 -->
            <div class="item">
                <div class="img">
                    <img src="/images/sys/iccloud.svg" alt=""/>
                </div>
                <div class="tit">Cloud-based platform</div>
                <div class="txt">With Crewix, you always have what’s most important to you on whatever device you have
                    in hand.
                </div>
            </div>
        </div>
        <div class="cont seamen">
            <h2>Your career, your way</h2>
            <h5 class="text-center">Take control of your career with Crewix. Make your cloud CV work for you, just the
                way you
                need it to.</h5>
            <div class="list">
                <div class="item">
                    <div class="ico"><img src="/images/sys/icon_document_certificate.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Build your career</div>
                        <p>Grow your career or start one with Crewix. When you deliver great work to your employers,
                            success will follow.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/icon_document_cloud.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Create your cloud CV</div>
                        <p>Create and manage your cloud CV. Your profile will have a unique link which you can share
                            with everyone.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/chat.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Upload your documents</div>
                        <p>You'll be able to download and print a stunning business card designed by Crewix for
                            free.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/profile.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Personal business card</div>
                        <p>Most CVs are country dependent. Crewix is global, and that's why our CV was designed to
                            be simple and versatile.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/icon_document_checked.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Universal CV</div>
                        <p>At Crewix, your trust means everything to us. That’s why we respect your personal
                            information and protect it.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/icon-lock.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Privacy</div>
                        <p>Once you’ve connected with seaman, you’ll be able to download his CV. Hire him to be able
                            to download his document scans.</p>
                    </div>
                </div>
            </div>

        </div>

        <div class="cont com">
            <h2>Features for company</h2>
            <div class="list">
                <div class="item">
                    <div class="ico"><img src="/images/sys/case.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Post vacancies</div>
                        <p>Post up to 5 vacancies per month for free.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/search.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Find the perfect match fast</div>
                        <p>ind the best seamen instantly with our advanced search. </p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/chat.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Interview the candidates</div>
                        <p>You can ask for interview with any seaman you like.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/icon-certificate.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Work with someone skilled</div>
                        <p>Hire seamen with confidence, always knowing their work experience.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/icon_clipboard_list.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Be focused and productive</div>
                        <p>Intuitive design, advanced search and email notifications ensure that nothing gets lost.</p>
                    </div>
                </div>
                <div class="item">
                    <div class="ico"><img src="/images/sys/icanchr.svg" alt=""></div>
                    <div class="text">
                        <div class="tit">Become our partner</div>
                        <p>Our experts will search for the best candidates out there for your vacancies and send you
                            their CVs for your approval. </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs('
        $(\'.nav-follower a\').on(\'click\',function(){
            var id = $(this).attr(\'id\');
            var h = $(\'.nav-follower a\').outerHeight();
            var ot = $(\'.cont.\'+id).offset().top;
            console.log(ot,h,(ot-h));
            $(window).scrollTo((ot-h-h)+\'px\',400);
        });
    ', \yii\web\View::POS_READY);
?>