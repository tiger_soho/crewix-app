<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Vacancies;
use app\models\Countries;
use app\models\Ranks;
use app\models\Docs;
use app\models\Langs;
use app\models\Languages;
use app\models\Education;
use app\models\TravelDocs;
use app\models\VesselTypes;
use app\models\Competency;
use app\models\Proficiency;
use app\models\ProficiencyDocs;
use app\models\Service;
use app\models\Engines;
use app\models\References;

?>

<div class="bt">
	<h2>Biometrics</h2>
	<?php if(\app\models\User::allowForCurrentUser($data->user_id)): ?>
		<span class="edit bio">Edit</span>
	<?php endif; ?>
</div>
<table>
	<tr>
		<td class="f-tit">Sex</td>
		<td><?=\app\models\Sex::find()->where(['id'=>$data->biometrics->sex_id])->one()->name?></td>
		<td class="f-tit">Eye color</td>
		<td><?=\app\models\EyeColor::find()->where(['id'=>$data->biometrics->eyes_color_id])->one()->name?></td>
	</tr>
	<tr>
		<td class="f-tit">Height</td>
		<td><?=$data->biometrics->height?></td>
		<td class="f-tit">Clothing size</td>
		<td><?=\app\models\Cloth::find()->where(['id'=>$data->biometrics->clothing_size_id])->one()->name?></td>
	</tr>
	<tr>
		<td class="f-tit">Weight</td>
		<td><?=$data->biometrics->weight?></td>
		<td class="f-tit">Shoe size</td>
		<td><?=\app\models\Shoes::find()->where(['id'=>$data->biometrics->shoe_size_id])->one()->name?></td>
	</tr>
</table>