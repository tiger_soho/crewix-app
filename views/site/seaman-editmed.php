<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Seaman */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;

$countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
$options = array();
foreach ($countries as $key => $value) {
    $options[$key] = ['data-style'=>'background:url(/images/sys/flags/'.Countries::find()->where(['id'=>$key])->one()->code_2.'.png) 0 center no-repeat'];
}
?>

<div class="bt">
    <h2>Medical documents</h2><span class="cancel med pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'st-med',
    'options' => ['class'=>'travel_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
<table>
    <thead>
    <tr>
        <td>Document</td>
        <td>Number</td>
        <td>Place of issue</td>
        <td>Date of issue</td>
        <td>Date of expiry</td>
    </tr>
    </thead>

    <?php foreach($data->medical as $item): ?>
        <tr>
            <td>
                <?= $form->field($model, 'id[]')->hiddenInput(['value' => $item['id']])->label(false) ?>
                <?= $form->field($model, 'document_id[]')->dropDownList(ArrayHelper::map(\app\models\MedicalDocument::find()->all(), 'id', 'name'), ['prompt'=>'Select document','options'=>[$item['id']=>['Selected'=>'selected']]])->label(false);?>
            </td>
            <td>
                <?=$form->field($model,'number[]')->textInput(['placeholder'=>'Enter number','value'=>$item['number']])->label(false);?>
            </td>
            <td>
                <?= $form->field($model, 'place_of_issue_id[]')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country','options'=>[$item['place_of_issue_id']=>['Selected'=>'selected']]])->label(false);?>
            </td>
            <td><?= $form->field($model, 'date_of_issue[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy','value'=>Yii::$app->formatter->asDate($item['date_of_issue'],'dd.MM.y')])->label(false) ?></td>
            <td><?= $form->field($model, 'date_of_expiry[]', ['options'=>['class'=>'date','enableAjaxValidation' => true]])->textInput(['placeholder'=>'dd.mm.yyyy','value'=>Yii::$app->formatter->asDate($item['date_of_expiry'],'dd.MM.y')])->label(false) ?></td>
        </tr>

    <?php endforeach; ?>
    <?php if(empty($data->medical)): ?>
        <tr>
            <td>
                <?= $form->field($model, 'document_id[]')->dropDownList(ArrayHelper::map(\app\models\CertificateOfProficiency::find()->all(), 'id', 'name'), ['prompt'=>'Select document'])->label(false);?>
            </td>
            <td>
                <?=$form->field($model,'number[]')->textInput(['placeholder'=>'Enter number'])->label(false);?>
            </td>
            <td>
                <?= $form->field($model, 'place_of_issue_id[]', ['options'=>['class'=>'countries']])->dropDownList($countries, ['prompt'=>'Select country','options'=>$options])->label(false);?>
            </td>
            <td><?= $form->field($model, 'date_of_issue[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?></td>
            <td><?= $form->field($model, 'date_of_expiry[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?></td>
        </tr>
    <?php endif; ?>

</table>
<!--<span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Document</span>-->
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script>
    $('.date input').datepicker({
        format: 'dd.mm.yyyy',
    });
</script>