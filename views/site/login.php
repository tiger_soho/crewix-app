<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="tit">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <span class="pull-right"><strong>
            <?=Html::a('Create account',['/signup'],['title'=>'Create new account']);?>
        </strong></span>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <!-- <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div> -->

                <div class="form-group">
                    <?= Html::submitButton('Log in', ['class' => 'btn btn-primary pull-left', 'name' => 'login-button']) ?>
                    <span class="fgt pull-right">
                        <strong>
                            <?=Html::a('Forgot password?',['/request-password-reset'],['title'=>'Forgot my password'])?>
                        </strong>
                    </span>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
