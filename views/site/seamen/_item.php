<?php
use yii\helpers\Url;
use app\models\Currency;
use app\models\Ranks;
use app\models\Countries;
use app\models\SeamanLanguage;
use app\models\Profile;

/* @var $this yii\web\View */
/* @var $model app\models\Seaman */

$this->title = 'Seamen';
?>


<div class="img"><img src="images/companies/logos/noimage.jpg" alt=""></div>
<div class="info">
    <?php $id = Profile::find()->select('id')->where(['user_id' => $model->user_id])->one()->id; ?>
    <h2><a href="<?= Url::to([
            'profile',
            'id' => $id
        ]) ?>">
            <?= $model->first_name ?>
            &mdash;
            <?= $model->primaryRank->name ?>

            <?php if (!empty($model->secondary_rank_id)): ?>
                /
                <?= $model->secondaryRank->name ?>

            <?php endif; ?></a>
    </h2>
    <div class="detales">
        <div class="bdate"><i class="fa fa-clock-o"></i> <span><?= $model->date_of_birth ?></span></div>
        <div class="duration"><i
                class="fa fa-globe"></i> <?= Countries::find()->where(['id' => $model->citizenship_id])->one()->name ?>
        </div>
        <div class="lang"><i
                class="fa fa-commenting-o"></i> <?= SeamanLanguage::find()->where(['seaman_id' => $model->id])->andWhere(['language_id' => '1'])->one()->language_level_id ?>
        </div>
    </div>
</div>
<div class="sallary">
    <div class="summ"><?= Currency::find()->where(['id' => $model->salary_currency_id])->one()->symbol ?>
        &nbsp;<?= Yii::$app->formatter->asInteger($model->salary) * 30 ?></div>
    <div class="period">per month</div>
</div>

<?php
$this->registerJs('
        $(\'.bdate\').each(function(i,el){
			var d = $(el).find(\'span\').text();
			var age = moment().diff(d,\'years\');
			$(el).find(\'span\').text(age+\' years\')
		});
',\yii\web\View::POS_LOAD,'date')
?>

