<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Ranks;
use app\models\Countries;
use app\models\Vacancies;
use app\models\CompanyType;
use app\models\Profile;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = 'Companies';
?>
<div class="wrapper vacancies">
	<div class="filters" id="companies_filter">
		<?= Html::beginForm('/filter-form-companies', 'GET', ['id' => 'filter-form-companies']); ?>
		<div class="line">
			<label for="type">Company type</label>
			<?= Html::dropDownList('type', null, ArrayHelper::map(CompanyType::find()->all(), 'id', 'name'),
				['id' => 'type', 'class' => 'form-control', 'prompt' => 'Any type']); ?>
		</div>
		<div class="line">
			<label for="country">Country</label>
			<?=Html::dropDownList('country', null, ArrayHelper::map(Countries::find()->orderBy(['name'=>SORT_ASC])->all(), 'id', 'name'),['id'=>'country', 'class'=>'form-control','prompt'=>'Any country']);?>
		</div>
		<?=Html::endForm();?>
	</div>
    <div class="vac_list">
    	<div class="bt">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
		<?php Pjax::begin(['id' => 'companies', 'enablePushState' => false]); ?>
		<?= ListView::widget([
			'dataProvider' => $dataProvider,
			'id' => 'companies-list',
			'itemOptions' => ['class' => 'item'],
			'summary' => '',
			'itemView' => 'companies/_item',
			'pager' => [

				'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
				'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
				'maxButtonCount' => 3,

				// Customzing options for pager container tag
				'options' => [
					'tag' => 'div',
					'class' => 'pagination page-pag',
					'id' => 'pager-container',
				],

				// Customzing CSS class for pager link
				'linkOptions' => ['class' => 'admin-pag-a'],
				'activePageCssClass' => 'active',
				'disabledPageCssClass' => 'disable',

				// Customzing CSS class for navigating link
				'prevPageCssClass' => 'left',
				'nextPageCssClass' => 'right',
			],
		]) ?>
		<?php Pjax::end(); ?>
    </div>
</div>
<?php
$this->registerJs(
	'
		$(\'#companies_filter form\').on(\'change\',\'input\',function(){
			var p = $(\'#filter-form-vacancies\').serialize();
			$.pjax({
				url:\'/companies\',
				container: \'#companies\',
        		fragment: \'#companies-list\',
				data: p,
			});
			return false;
		});

		$(\'#companies_filter form select\').on(\'selectmenuchange\',function(){
			var p = $(\'#filter-form-companies\').serialize();
			$.pjax({
				url:\'/companies\',
				container: \'#companies\',
        		fragment: \'#companies-list\',
        		push: false,
				data: p,
			});
		});
		$(\'#clear\').on(\'click\',function(){
			$(\'#filter-form-companies\').trigger(\'reset\');
			var p = $(\'#filter-form-companies\').serialize();
			$.pjax({
				url:\'/companies\',
				container: \'#companies\',
        		fragment: \'#companies-list\',
				data: p,
				push: false,
				success:function(data){
					$(\'.vac_list\').remove();
					$(\'#vac_filter\').after(data);
				}
			});
		});

 //   });', \yii\web\View::POS_READY, 'companies-list-pjax');
?>