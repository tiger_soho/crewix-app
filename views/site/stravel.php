<div class="bt">
    <h2>Travel documents</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit tr">Edit</span>
    <?php endif; ?>
</div>
<table class="five">
    <thead>
    <tr>
        <td>Document</td>
        <td>Number</td>
        <td>Place of issue</td>
        <td>Date of issue</td>
        <td>Date of expiry</td>
    </tr>
    </thead>
    <?php foreach ($data->travelDocs as $tr) { ?>
        <tr>
            <td class="f-tit"><?= \app\models\TravelDocument::find()->where(['id' => $tr->document_id])->one()->name ?></td>
            <td>
                <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                <?= $tr->number ?></td>
            <?php else: ?>
                *****
            <?php endif; ?>
            </td>
            <td><?= \app\models\Countries::find()->where(['id' => $tr->place_of_issue_id])->one()->name ?></td>
            <td class="date"><?= Yii::$app->formatter->asDate($tr->date_of_issue, "php:d.m.Y") ?></td>
            <?php if ($tr->date_of_expiry != null): ?>
                <td class="date">
                    <?= Yii::$app->formatter->asDate($tr->date_of_expiry, "php:d.m.Y") ?>
                </td>
            <?php else: ?>
                <td>
                    Unlimitted
                </td>
            <?php endif; ?>
        </tr>
    <?php } ?>
</table>
</div>