<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="col-lg-12 clearfix terms">
    <div class="content pull-left">
        <div class="block">
            <div class="bt">
                <h1>Terms of Service</h1>
            </div>
            <?php if(!empty($data)): ?>
                <?php foreach($data as $item): ?>
                    <div class="heading">
                        <h2><?=$item->section_name?></h2>
                    </div>
                    <div class="text">
                        <?=$item->text?>
                    </div>
                <?php endforeach; ?>
                <div class="upd">
                    <div class="text">
                        Last updated <?=Yii::$app->formatter->asDate($upd,'MMMM d, y')?>
                    </div>
                </div>
            <?php else: ?>
                <div class="empty">
                    There is no text yet
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="sidebar pull-right">
        <div class="side-block clearfix">
            <div class="img lock"><img src="/images/sys/lock.svg" alt=""></div>
            <p>Looking for <a href="<?=Url::to('/privacy')?>">Privacy policy</a>?</p>
        </div>
        <div class="side-block clearfix">
            <div class="img safe"><img src="/images/sys/support.svg" alt=""></div>
            <p>Looking for <a href="<?=Url::to('/faq')?>">help</a>? Check out our FAQ.</p>
        </div>
    </div>
</div>
