<?php

use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\widgets\ListView;

?>

<div class="block faq">
    <?php Pjax::begin(['id' => 'faq', 'enablePushState' => true]); ?>
    <div class="bt">
        <?php if ($cat != null) { ?>
            <h1><?= $cat->name ?></h1>
        <?php } else { ?>
            <h1>FAQ</h1>
        <?php } ?>
    </div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'faq-list',
        'itemOptions' => ['class' => 'item'],
        'summary' => '',
        'itemView' => 'faq/_item',
        'pager' => [

            'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
            'maxButtonCount' => 3,

            // Customzing options for pager container tag
            'options' => [
                'tag' => 'div',
                'class' => 'pagination page-pag',
                'id' => 'pager-container',
            ],

            // Customzing CSS class for pager link
            'linkOptions' => ['class' => 'admin-pag-a'],
            'activePageCssClass' => 'active',
            'disabledPageCssClass' => 'disable',

            // Customzing CSS class for navigating link
            'prevPageCssClass' => 'left',
            'nextPageCssClass' => 'right',
        ],
    ]) ?>
    <?php Pjax::end(); ?>
</div>
