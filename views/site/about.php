<?php

/* @var $this yii\web\View */
/* @var $item \app\models\Companies */

use yii\helpers\Html;

$this->title = 'About';
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 about-main-fullwidth">
    <p class="about-main-text"><span class="about-span-one">Welcome to Crewix.</span><br>
        <span class="about-span-two">Cloud-based crewing platform for seamen and crewing companies.</span><br>
        <button type="button" class="btn btn-secondary about-button-learnmore">Learn more</button>
    </p>

</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 about-who-we">
    <p class="about-main-text"><span class="about-who-one">Who we are</span><br>
        <span class="about-who-two">We are Crewix — a team of crewing experts & developers who are passionate about helping our users reach their goals.<br>
We’re determined to craft unique user experience and highly functional product.</span><br></p>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 about-what-we">
    <p class="about-main-text"><span class="about-what-one">What we do</span><br>
        <span class="about-what-two">We help seamen find their dream job. We also help employers hire the best employees they can find.</span><br>
    </p>
</div>
<?php if (!empty($data)): ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 about-working">
        <div class="about-main-text"><span class="about-who-one">We are working with</span><br>
            <div class="container about-padding-work">
                <?php foreach ($data as $item): ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 item">
                        <div class="img">
                            <?php if (!empty($item->profile_picture)): ?>
                                <img src="/images/companies/logos/medium/<?= $item->profile_picture ?>">
                            <?php else: ?>
                                <img src="/images/companies/logos/noimage.jpg">
                            <?php endif; ?>
                        </div>
                        <span class="about-working-name"><?= $item->name ?></span><br>
                        <span
                            class="about-working-country"><?= \app\models\Countries::findOne($item->companyContact->hq_country_id)->name ?></span>
                    </div>
                <?php endforeach; ?>


            </div>
        </div>
    </div>
<?php endif; ?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 about-soc">
    <div class="about-main-text"><span class="about-who-one">Follow us</span></div>
    <div class="item"><a href="#"><img src="/images/sys/facebook.svg" alt=""></a></div>
    <div class="item"><a href="#"><img src="/images/sys/vk.svg" alt=""></a></div>
    <div class="item"><a href="#"><img src="/images/sys/google.svg" alt=""></a></div>
    <div class="item"><a href="#"><img src="/images/sys/twitter.svg" alt=""></a></div>
    <div class="item"><a href="#"><img src="/images/sys/telegram.svg" alt=""></a></div>
</div>
