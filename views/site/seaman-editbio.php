<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="bt">
    <h2>Biometrics</h2>
    <span class="cancel bio pull-right">Cancel</span>
</div>
    <?php $form = ActiveForm::begin(['id' => 'sbedit',
        'options' => ['class'=>'bio_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>

    <?php 
        $model->sex_id = $data->biometrics->sex_id;
        $model->height = $data->biometrics->height;
        $model->clothing_size_id = $data->biometrics->clothing_size_id;
        $model->eyes_color_id = $data->biometrics->eyes_color_id;
        $model->weight = $data->biometrics->weight;
        $model->shoe_size_id = $data->biometrics->shoe_size_id;
    ?>
    <table class="bio-table">
        <tr>
            <td class="f-tit"><span class="req">*</span> Sex</td>
            <td>
                <?= $form->field($model, 'sex_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\app\models\Sex::find()->all(),'id','name'),
                    ['prompt'=>'Select sex',],
                    ['placeholder'=>'Select sex'])->label(false);?>
            </td>
            <td class="f-tit"><span class="req">*</span> Height <span class="mes">(cm)</span></td>
            <td>
                <?= $form->field($model, 'height')->textInput(['placeholder'=>'Enter number'])->label(false); ?>
            </td>
            <td class="f-tit"><span class="req">*</span> Clothing size</td>
            
            <td>
                <?= $form->field($model,
                    'clothing_size_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Cloth::find()->where(['sex_id' => $data->biometrics->sex_id])->all(),
                    'id', 'name'), ['placeholder' => 'Select clothing size'])->label(false); ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Eye color</td>
            <td>
                <?= $form->field($model, 'eyes_color_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\EyeColor::find()->all(),'id','name'), ['prompt'=>'Select eye color'],['placeholder'=>'Select eye color'])->label(false);?>
            </td>
            <td class="f-tit"><span class="req">*</span> Weight <span class="mes">(kg)</span></td>
            <td>
                <?= $form->field($model, 'weight')->textInput(['placeholder'=>'Enter number'])->label(false); ?>
            </td>
            <td class="f-tit"><span class="req">*</span> Shoe size</td>
            <td>
                <?= $form->field($model,
                    'shoe_size_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Shoes::find()->where(['sex_id' => $data->biometrics->sex_id])->all(),
                    'id', 'name'), ['placeholder' => 'Select shoe size'])->label(false); ?>
            </td>
        </tr>
    </table>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script>
    $('#biometrics-sex_id').change();
    $( "#biometrics-sex_id" ).on( "selectmenuchange", function( event, ui ){
        console.log(ui.item.value);
        $.post("/signup/shoes?id="+ui.item.value, function(data){

            if(data=="empty"){

                $("select#biometrics-shoe_size_id").attr("disabled","disabled");
            }else{
                console.log(data);
                $("select#biometrics-shoe_size_id").html(data);
                $("select#biometrics-shoe_size_id").removeAttr("disabled");
                $("select#biometrics-shoe_size_id").selectmenu( "refresh" );

            }
        });
        $.post("/signup/cloth?id="+ui.item.value, function(data){
            if(data=="empty"){
                $("select#biometrics-clothing_size_id").attr("disabled","disabled");
            }else{
                $("select#biometrics-clothing_size_id").html(data);
                $("select#biometrics-clothing_size_id").removeAttr("disabled");
                $("select#biometrics-clothing_size_id").selectmenu( "refresh" );
            }
        });
    });
</script>
