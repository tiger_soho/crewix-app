<?php
use yii\helpers\Url;
use app\models\Currency;

/* @var $this yii\web\View */
/* @var $model app\models\Vacancies */

$this->title = 'Vacancies';
?>


<div class="img"><img src="images/companies/logos/noimage.jpg" alt=""></div>
<div class="info">
    <h2><a href="<?= Url::to(['vacancy', 'id' => $model->id]) ?>"><?= $model->ranks->name ?>
            on <?= $model->vesselTypes->name ?></a></h2>
    <div class="detales">
        <div class="date"><i class="fa fa-calendar-o"></i> <?= Yii::$app->formatter->asDate($model->joining_date_from,
                "php:d.m.Y") ?></div>
        <div class="duration"><i
                class="fa fa-clock-o"></i> <?= $model->duration_of_contract ?> &plusmn; <?= $model->duration_of_contract_offset ?>
        </div>
        <div class="vessel_type"><i
                class="fa fa-ship"></i> <?= Yii::$app->formatter->asInteger($model->vessel_deadweight) ?> MT
        </div>

    </div>
</div>
<div class="sallary">
    <div class="summ">
        <?= Currency::find()->where(['id' => $model->salary_currency_id])->one()->symbol ?>
        <?= round($model->salary_from / $model->payroll_period) ?>
    </div>
    <div class="period">
        <?php if ($model->payroll_period == 1): ?>
            per day
        <?php elseif ($model->payroll_period == 7): ?>
            per week
        <?php elseif ($model->payroll_period == 30): ?>
            per month
        <?php endif; ?>
    </div>
</div>

