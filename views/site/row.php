<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Langs;
use yii\helpers\BaseHtml;
?>

<tr>
	<td><?=Html::activeDropDownList($model,'lang[]', ArrayHelper::map(Langs::find()->where(['!=','id','1'])->all(), 'id', 'lang'), ['prompt' => 'Select language','class'=>'form-control']);?></td>
	<td><?=Html::activeDropDownList($model,'level[]', ['Elementary'=>'Elementary','Lower Intermediate'=>'Lower Intermediate','Intermediate'=>'Intermediate','Upper Intermediate'=>'Upper Intermediate','Advanced'=>'Advanced','Fluent'=>'Fluent'], ['prompt' => 'Select language level','class'=>'form-control half']);?></td>
</tr>