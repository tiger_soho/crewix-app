<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Seaman */

use app\models\Countries;
use app\models\Ranks;
use app\models\VesselTypes;
use app\models\Competency;
use app\models\Engines;
?>

<div class="bt">
    <h2>References</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit ref">Edit</span>
    <?php endif; ?>
</div>
<table>
    <thead>
    <tr>
        <td>Company</td>
        <td>Phone number</td>
        <td>Email</td>
        <td>Contact person</td>
    </tr>
    </thead>
    <?php foreach ($data->references as $ref): ?>
        <tr>
            <td class="f-tit"><?= $ref->company_name ?></td>
            <td><?= $ref->phone ?></td>
            <td><?= $ref->email ?></td>
            <td>
                <?php if (!empty($ref->contact_person)): ?>
                    <?= $ref->contact_person ?>
                <?php else: ?>
                    &mdash;
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
