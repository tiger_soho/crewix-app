<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Vacancies;
use app\models\Countries;
use app\models\Ranks;
use app\models\Docs;
use app\models\Langs;
use app\models\Languages;
use app\models\Education;
use app\models\TravelDocs;
use app\models\VesselTypes;
use app\models\Competency;
use app\models\Proficiency;
use app\models\ProficiencyDocs;
use app\models\Service;
use app\models\Engines;
use app\models\References;


?>

<div class="bt">
	<h2>Family</h2>
	<?php if(\app\models\User::allowForCurrentUser($data->user_id)): ?>
		<span class="edit family">Edit</span>
	<?php endif; ?>
</div>
<table>
	<tr>
		<td class="f-tit">Marital status</td>
		<td><?=\app\models\MaritalStatus::find()->where(['id'=>$data->family->marital_status_id])->one()->name?></td>
		<td class="f-tit">Children</td>
		<td>
			<?= $data->family->children ?>
		</td>
	</tr>
	<tr>
		<td class="f-tit">Mother’s name</td>
		<td>
			<?php if(\app\models\User::allowForCurrentUser($data->user_id)||Yii::$app->user->can('admin')): ?>
				<?=$data->family->mother?>
			<?php else: ?>
				*****
			<?php endif; ?>
		</td>
		<td class="f-tit">Father’s name</td>
		<td>
			<?php if(\app\models\User::allowForCurrentUser($data->user_id)||Yii::$app->user->can('admin')): ?>
				<?=$data->family->father?>
			<?php else: ?>
				*****
			<?php endif; ?>
		</td>
	</tr>
</table>