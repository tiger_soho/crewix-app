<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use kartik\file\FileInput;

?>

<div class="bt">
    <h2>Family</h2>
    <span class="cancel family pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'sfedit',
        'options' => ['class'=>'family_form'],
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>

<?php
    $model->marital_status_id = $data->family->marital_status_id;
    $model->children = $data->family->children;
    $model->mother = $data->family->mother;
    $model->father = $data->family->father;
 ?>
    <table>
        <tr>
            <td class="f-tit"><span class="req">*</span> Marital status</td>
            <td>
                <?= $form->field($model, 'marital_status_id')->dropDownList(ArrayHelper::map(\app\models\MaritalStatus::find()->all(),'id','name'), ['prompt'=>'Marital status'],['placeholder'=>'Marital status'])->label(false);?>
            </td>
            <td class="f-tit"><span class="req">*</span> Children</td>
            <td>
                <?= $form->field($model, 'children')->dropDownList(['0'=>'No','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'], ['prompt'=>'Do you have children?'],['placeholder'=>'Do you have children?'])->label(false);?>
            </td>
        </tr>
        <tr>
            <td class="f-tit">Mother’s name</td>
            <td>
                <?= $form->field($model, 'mother')->textInput(['placeholder'=>'Enter full name'])->label(false); ?>
            </td>
            <td class="f-tit">Father’s name</td>
            <td>
                <?= $form->field($model, 'father')->textInput(['placeholder'=>'Enter full name'])->label(false); ?>
            </td>
        </tr>
    </table>

<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script>
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
          _renderItem: function( ul, item ) {
            var li = $( "<li>", { text: item.label } );
     
            if ( item.disabled ) {
              li.addClass( "ui-state-disabled" );
            }
     
            $( "<span>", {
              style: item.element.attr( "data-style" ),
              "class": "ui-icon " + item.element.attr( "data-class" )
            })
              .appendTo( li );
     
            return li.appendTo( ul );
          }
        });
    $('select').each(function(i,el){
        if(!$(el).parent().hasClass('countries')){
            $(el).selectmenu({
                width: '100%'
            });
        }else{
            $(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
        }
    });
    $('.date input').datepicker({
            format: 'dd.mm.yyyy',
        });
</script>