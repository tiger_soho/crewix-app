<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Seaman */

/*'template' => '{input}<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>{error}{hint}'*/

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Ranks;
use app\models\Users;
use kartik\file\FileInput;
use dosamigos\datepicker\DatePicker;

//$this->params['breadcrumbs'][] = $this->title;
?>

<?php 
    $countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
    $options = array();
    foreach ($countries as $key => $value) {
        $options[$key] = ['data-style'=>'background:url(/images/sys/flags/'.Countries::find()->where(['id'=>$key])->one()->code_2.'.png) 0 center no-repeat'];
    }
?>
<?php $form = ActiveForm::begin(['id' => 'smedit',
    'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>

<?php
//Set values

    $bd = explode('-', $data->date_of_birth);
    $bd = $bd[2].'.'.$bd[1].'.'.$bd[0];
    if(!empty($data->availability_date)&&$data->availability_date!='0000-00-00'){
        $ad = explode('-', $data->availability_date);
        $ad = $ad[2].'.'.$ad[1].'.'.$ad[0];
    }


    $model->first_name = $data->first_name;
    $model->last_name = $data->last_name;
    $model->middle_name = $data->middle_name;
    $model->primary_rank_id = $data->primary_rank_id;
    $model->secondary_rank_id = $data->secondary_rank_id;
    $model->salary = $data->salary;
    $model->status = $data->status;
if (isset($ad)) {
    $model->availability_date = $ad;
}
    $model->salary_currency_id = $data->salary_currency_id;
    $model->date_of_birth = $bd;
    $model->place_of_birth_id = $data->place_of_birth_id;
    $model->city_of_residence = $data->city_of_residence;
    $model->country_of_residence_id = $data->country_of_residence_id;
    $model->address = $data->address;
    $model->citizenship_id = $data->citizenship_id;
    $model->closest_airport = $data->closest_airport;
    $model->primary_phone = $data->primary_phone;
    $model->secondary_phone = $data->secondary_phone;
    $model->skype = $data->skype;
 ?>
<div class="bt">
    <h2>Main information</h2>
    <span class="cancel scont pull-right">Cancel</span>
</div>
    <table>
        <tr>
            <td class="f-tit">Profile picture</td>
            <td colspan="3">
                <input type="hidden" id="csrf" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                <div class="avatar ph">
                    <div id="images"><img src="/images/sys/profile2.png" alt=""></div>
                    <div class="file-desc">
                        <p><strong>Your profile picture.</strong> It will be used on the Crewix website.</p>
                        <p class="tip">We support JPG files. The minimum image size is 300x300 pixels.</p>
                        <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                            <div class="upload-btn__txt">Select file</div>
                            <?= $form->field($model,'profile_picture')->fileInput(['accept'=>'image/jpg'])->label(false); ?>
                        </div>
                        <span class="btn remove">Remove</span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="f-tit">CV picture</td>
            <td colspan="3">
                <div class="avatar cv">
                    <div id="imagess"><img src="/images/seaman/nocv.jpg" alt=""></div>
                    <div class="file-desc">
                        <p><strong>Your 3.5x4.5 cm photo.</strong> It will be used in the PDF version of your CV.</p>
                        <p class="tip">We support JPG files. The minimum image size is 350x450 pixels.</p>
                        <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                            <div class="upload-btn__txt">Select file</div>
                            <?= $form->field($model,'cv_picture')->fileInput(['accept'=>'image/jpg'])->label(false); ?>
                        </div>
                        <span class="btn remove">Remove</span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Name</td>
            <td>

                <?= $form->field($model, 'first_name', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'Name'])->label(false); ?><!--
             --><?= $form->field($model, 'last_name', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'Surname'])->label(false); ?>
            </td>
            <td class="f-tit">Middle name</td>
            <td>
                <?= $form->field($model, 'middle_name')->textInput(['placeholder'=>'Middle name'])->label(false); ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Rank</td>
            <td>
                <?= $form->field($model, 'primary_rank_id',
                    ['options' => ['class' => 'half']])->dropDownList(ArrayHelper::map(Ranks::find()->all(), 'id',
                    'name'), ['prompt' => 'Primary rank'])->label(false); ?><!--
             --><?= $form->field($model, 'secondary_rank_id',
                    ['options' => ['class' => 'half']])->dropDownList(ArrayHelper::map(Ranks::find()->all(), 'id',
                    'name'), ['prompt' => 'Additional rank'])->label(false); ?>
            </td>
            <td class="f-tit"><span class="req">*</span> Availability</td>
            <td>
                <?= $form->field($model, 'status', ['options' => ['class' => 'half']])->dropDownList([
                    '0' => 'Available from...',
                    '1' => 'Not available'
                ], ['prompt' => 'Status'])->label(false); ?><!--
             --><?= $form->field($model, 'availability_date', ['options'=>['class'=>'half date']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Desired salary per month</td>
            <td>
                <?= $form->field($model, 'salary', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'Enter number'])->label(false); ?><!--
             --><?= $form->field($model, 'salary_currency_id', ['options'=>['class'=>'half']])->dropDownList(ArrayHelper::map(\app\models\Currency::find()->all(),'id','code'))->label(false);?>
            </td>
            <td class="f-tit"><span class="req">*</span> Date and place of birth</td>
            <td>
                <?= $form->field($model, 'date_of_birth', ['options'=>['class'=>'half date','enableAjaxValidation' => true]])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?><!--
             --><?= $form->field($model, 'place_of_birth_id', ['options'=>['class'=>'half countries']])->dropDownList($countries, ['prompt'=>'Select country','options'=>$options])->label(false);?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> City and country of residence</td>
            <td>
                <?= $form->field($model, 'city_of_residence', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'City'])->label(false); ?><!--
             --><?= $form->field($model, 'country_of_residence_id', ['options'=>['class'=>'half countries']])->dropDownList($countries, ['prompt'=>'Select country','options'=>$options])->label(false);?>
            </td>
            <td class="f-tit"><span class="req">*</span> Home address</td>
            <td>
                <?= $form->field($model, 'address')->textInput(['placeholder'=>'Street and house'])->label(false); ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Citizenship</td>
            <td>
                <?= $form->field($model, 'citizenship_id', ['options'=>['class'=>'countries']])->dropDownList($countries, ['prompt'=>'Select country','options'=>$options])->label(false); ?>
            </td>
            <td class="f-tit"><span class="req">*</span> Closest airport</td>
            <td>
                <?= $form->field($model, 'closest_airport')->textInput(['placeholder'=>'City'])->label(false); ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Primary phone</td>
            <td>
                <?= $form->field($model, 'primary_phone')->textInput()->label(false); ?>
            </td>
            <td class="f-tit">Additional phone</td>
            <td>
                <?= $form->field($model, 'secondary_phone')->textInput()->label(false); ?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Skype</td>
            <td>
                <?= $form->field($model, 'skype')->textInput(['placeholder'=>'Username'])->label(false); ?>
            </td>
            <td class="f-tit">Email</td>
            <td><?=Yii::$app->user->getIdentity()->email?></td>
        </tr>
    </table>

<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 's_edit']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<script>
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
          _renderItem: function( ul, item ) {
            var li = $( "<li>", { text: item.label } );
     
            if ( item.disabled ) {
              li.addClass( "ui-state-disabled" );
            }
     
            $( "<span>", {
              style: item.element.attr( "data-style" ),
              "class": "ui-icon " + item.element.attr( "data-class" )
            })
              .appendTo( li );
     
            return li.appendTo( ul );
          }
        });
    $('select').each(function(i,el){
        if(!$(el).parent().hasClass('countries')){
            $(el).selectmenu({
                width: '100%'
            });
        }else{
            $(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
        }
    });
    $('.date input').datepicker({
            format: 'dd.mm.yyyy',
        });
</script>
<script>
    window.FileAPI = {
        html5: !!1,
        staticPath: '/js/filapi/',
        flashUrl: '/js/filapi/FileAPI.flash.swf',
        flashImageUrl: '/js/filapi/FileAPI.flash.image.swf'
    };
</script>
<script src="/js/fileapi/FileAPI.min.js"></script>
<script>
    var choose = document.getElementById('seaman-profile_picture');
    FileAPI.event.on(choose, 'change', function (evt){
        var files = FileAPI.getFiles(evt); // Retrieve file list

        FileAPI.filterFiles(files, function (file, info/**Object*/){
            if( /^image/.test(file.type) ){
                return  info.width >= 300 && info.height >= 300;
            }
            return  false;
        }, function (files/**Array*/, rejected/**Array*/){
            if( files.length ){
                // Make preview 100x100
                FileAPI.each(files, function (file){
                    FileAPI.Image(file).preview(100).get(function (err, img){
                        if (images.hasChildNodes()) {
                            images.removeChild(images.childNodes[0]);
                        }
                        images.appendChild(img);

                    });
                });

                // Uploading Files
                FileAPI.upload({
                    url: '/signup/avatarupload',
                    data: { '_csrf': $('#csrf').val() },
                    files: { images: files },
                    imageTransform: {
                        'huge': { width: 300, height: 300, preview: true },
                        // crop & resize
                        'medium': { width: 100, height: 100, preview: true },
                    },
                    progress: function (evt){ /* ... */ },
                    complete: function (err, xhr){},
                    filecomplete: function (err, xhr, file){
                        $('.field-seaman-profile_picture input[type=hidden]').val(file.name);
                    }
                });
            }
        });
    });
</script>
<script>
    var ch = document.getElementById('seaman-cv_picture');
    FileAPI.event.on(ch, 'change', function (evt){
        var files = FileAPI.getFiles(evt); // Retrieve file list

        FileAPI.filterFiles(files, function (file, info/**Object*/){
            if( /^image/.test(file.type) ){
                return  info.width >= 350 && info.height >= 450;
            }
            return  false;
        }, function (files/**Array*/, rejected/**Array*/){
            if( files.length ){
                // Make preview 100x100
                FileAPI.each(files, function (file){
                    FileAPI.Image(file).preview(100,128).get(function (err, img){
                        if (imagess.hasChildNodes()) {
                            imagess.removeChild(imagess.childNodes[0]);
                        }
                        imagess.appendChild(img);

                    });
                });

                // Uploading Files
                FileAPI.upload({
                    url: '/signup/cvupload',
                    data: { '_csrf': $('#csrf').val() },
                    files: { images: files },
                    imageTransform: {
                        width: 350,
                        height: 450,
                        preview: true
                    },
                    progress: function (evt){ /* ... */ },
                    complete: function (err, xhr){},
                    filecomplete: function (err, xhr, file){
                        $('.field-seaman-cv_picture input[type=hidden]').val(file.name);
                    }
                });
            }
        });
    });
    $('#seaman-primary_phone').intlTelInput();
    $('#seaman-secondary_phone').intlTelInput();
</script>