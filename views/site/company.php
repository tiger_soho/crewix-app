<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $data \app\models\Companies */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Vacancies;
use app\models\Countries;
use app\models\Ranks;
use app\models\Currency;
use app\models\Engines;
use app\models\vesselTypes;
use vova07\imperavi\Widget;

$this->title = $data->name;
?>
<div class="profile">
	<?php if (\app\models\User::allowForCurrentUser($data->user_id)&&!(Yii::$app->user->getIdentity()->verified)): ?>
		<div class="notice clearfix">
			<div class="msg pull-left">
				Please confirm your email address: <strong><?=Yii::$app->user->getIdentity()->email?></strong>
			</div>
			<div class="send pull-right">
				<?=Html::button('Resend',['type'=>'button','class'=>'btn btn-primary'])?>
			</div>
		</div>
	<?php endif; ?>
	<div class="wrapper">
		<div class="img">
			<?php if(!empty($data->profile_picture)): ?>
				<img src="/images/companies/logos/medium/<?=$data->profile_picture?>" alt="<?=$data->name?>" />
			<?php else: ?>
				<img src="/images/companies/logos/noimage.jpg" alt="" />
			<?php endif; ?>
		</div>
		<div class="info">
			<div class="top">
				<h1><?=$data->name?></h1>
				<div class="type"><?=$data->companyType->name?></div>
			</div>
			<div class="bot">
				<div class="stat time">
					<div class="digit"><?=Users::find()->where(['id'=>$data->user_id])->one()->signup_date?></div>
					days with Crewix
				</div><!-- 
			 --><div class="stat active">
					<div class="digit"><?=Vacancies::find()->where(['company_id'=>$data->id,'status'=>1])->count()?></div>
					vacancies open
				</div><!-- 
			 --><div class="stat opened-vac">
					<div class="digit"><?=Vacancies::find()->where(['company_id'=>$data->id])->count()?></div>
					vacancies posted
				</div>
			</div>
		</div>
	</div>
	<ul class="tabs">
		<li class="tab active" id="prof"><i class="fa fa-newspaper-o"></i> Profile</li>
		<li class="tab" id="vac"><i class="fa fa-briefcase"></i> Vacancies</li>
		<?php if(!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->id == $data->user_id): ?>
			<li class="tab" id="vess"><i class="fa fa-ship"></i> My vessels</li>
		<?php endif; ?>
	</ul>
</div>
<div class="wrapper company">
	<div class="tab_cont prof active">
		<div class="contacts">
			<div class="bt">
				<h2>Contacts</h2>
				<?php if(!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->id == $data->user_id): ?>
					<span class="edit cont">Edit</span>
                    <span class="cont-form pull-right cancel company-contacts">Cancel</span>
				<?php endif; ?>
			</div>
			<div class="cont">
				<?php if(!empty($data->companyContact->website)): ?>
	    			<div class="line">
	    				<p><strong>Website</strong></p>
	    				<p><a href="http://<?=$data->companyContact->website?>"><?=$data->companyContact->website?></a></p>
	    			</div>
	    		<?php endif; ?>
	    		<?php if(!empty($data->companyContact->email)): ?>
	    			<div class="line">
	    				<p><strong>Email</strong></p>
	    				<p><a href="mailto:<?=$data->companyContact->email?>"><?=$data->companyContact->email?></a></p>
	    			</div>
	    		<?php endif; ?>
	    		<?php if(!empty($data->companyContact->phone)): ?>
	    			<div class="line">
	    				<p><strong>Phone</strong></p>
	    				<p><?=$data->companyContact->phone?></p>
	    			</div>
	    		<?php endif; ?>
				<div class="line">
					<p><strong>HQ address</strong></p>
					<p><?=$data->companyContact->hq_address?>, <?=$data->companyContact->hq_city?>, <?=Countries::find()->where(['id'=>$data->companyContact->hq_country_id])->one()->name?></p>
				</div>
			</div>
			<?php if(!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->id == $data->user_id): ?>
				<div class="cont-form">
					<?php $form = ActiveForm::begin(['id' => 'contacts',
	                    'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
	                    'validateOnBlur' => false,
	                    'validateOnChange' => false,
	                ]); ?>
                    <div class="line unb">
	    				<?php $model->website = $data->companyContact->website; ?>
	    				<?=$form->field($model, 'website')->textInput(['placeholder'=>''])->label('Website');?>
	    			</div>
                    <div class="line unb">
	    				<?php $model->email = $data->companyContact->email; ?>
	    				<?=$form->field($model, 'email')->textInput(['placeholder'=>''])->label('Email');?>
	    			</div>
                    <div class="line unb">
	    				<?php $model->phone = $data->companyContact->phone; ?>
	    				<?=$form->field($model, 'phone')->textInput(['placeholder'=>''])->label('Phone');?>
	    			</div>
                    <div class="line unb">
						<?php $model->hq_country_id = $data->companyContact->hq_country_id; ?>
						<?php $model->hq_city = $data->companyContact->hq_city; ?>
						<?php $model->hq_address = $data->companyContact->hq_address; ?>
						<p><strong>Headquarters address <span class="req">*</span></strong></p>
						<?= $form->field($model, 'hq_country_id')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country'])->label(false); ?>
                        <?= $form->field($model, 'hq_city')->textInput(['placeholder'=>'City'])->label(false); ?>
                        <?= $form->field($model, 'hq_address')->textInput(['placeholder'=>'Street and hous/building number'])->label(false); ?>
					</div>
					<div class="bot">
                        <div class="form-group clearfix">
                            <?= Html::submitButton('Save changes', [
                                'class' => 'btn btn-primary col-lg-5 col-xs-12 pull-left',
                                'name' => 'signup-button',
                                'id' => 'c_sign'
                            ]) ?>
                            <?= Html::Button('Cancel',
                                ['class' => 'btn cancel company-contacts col-lg-5 col-xs-12 pull-right']) ?>
		                </div>
		            </div>
		            <?php ActiveForm::end(); ?>
				</div>
			<?php endif; ?>
		</div>
	    <div class="com_cont">
	    	<div class="bt">
	    		<h2>About</h2>
	    		<?php if(!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->id == $data->user_id): ?>
					<span class="edit about">Edit</span>
                    <span class="about-form pull-right cancel company-about">Cancel</span>
				<?php endif; ?>
	    	</div>
            <div class="about">
	    		<?php if(empty($data->about) && !Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->id == $data->user_id): ?>
					<div class="empty text-center"><p>Tell everyone what your company is about.</p>
						<span class="edit about">Add information &raquo;</span></div>
	    		<?php elseif(empty($data->about)): ?>
				<div class="empty text-center"><p>There is no company description</p></div>
	    		<?php else: ?>
	    			<?=$data->about?>
	    		<?php endif; ?>
	    	</div>
	    	<?php if(!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->id == $data->user_id): ?>
				<div class="about-form">
					<?php $form = ActiveForm::begin(['id' => 'about-form',
	                    'options' => ['class'=>'company_form','enctype'=>'multipart/form-data'],
	                    'validateOnBlur' => false,
	                    'validateOnChange' => false,
	                ]); ?>
	                <?php $model->about = $data->about; ?>
					<?=$form->field($model, 'about')->widget(Widget::className(), [
					    'settings' => [
					        'minHeight' => 200,
					        'buttons' => ['format', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link']
					    ]
					])->label(false);?>
	                <div class="bot">
		                <div class="form-group">
		                    <?= Html::submitButton('Save changes', ['class' => 'btn btn-primary save-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
                            <?= Html::submitButton('Cancel',
                                ['class' => 'btn cancel company-about', 'name' => 'signup-button', 'id' => 'c_sign']) ?>
		                </div>
		            </div>
		            <?php ActiveForm::end(); ?>
				</div>
	    	<?php endif; ?>
	    </div>
	</div>
    <div class="tab_cont vac">
    	<div class="vacancies">
    		<div class="bt">
				<h2>Vacancies</h2>
			</div>
			<?php if(Yii::$app->user->isGuest): ?>
				<div class="empty">
					<p>Please <a href="<?=Url::to('login')?>">log in</a> or <a href="<?=Url::to('signup')?>">sign up</a> to view <?=$data->name?> vacancies.</p>
				</div>
    		<?php elseif(empty($vacancies) && Yii::$app->user->id == $data->user_id): ?>
				<div class="empty">
					<p>You haven't added any vacancies yet.</p>
					<a href="<?=Url::to('vac')?>" class="add">New vacancy &raquo;</a>
				</div>
    		<?php elseif(empty($vacancies)): ?>
    			<div class="empty">
    				<p><img src="" alt=""></p>
					<p>Dropbox haven’t added any vacancies yet.</p>
				</div>
			<?php else: ?>
				<div class="list">
					<?php foreach ($vacancies as $vacancy): ?>
						<div class="item">
					    	<div class="img"><img src="images/companies/logos/noimage.jpg" alt=""></div>
					    	<div class="info">
					    		<h2><a href="<?=Url::to(['/vacancy','id'=>$vacancy->id])?>"><?=$vacancy->ranks->name?> on <?=$vacancy->vesselTypes->name?></a></h2>
						    	<div class="detales">
						    		<div class="date"><i class="fa fa-calendar-o"></i> <?=Yii::$app->formatter->asDate($vacancy->joining_date_from, "php:d.m.Y")?></div>
						    		<div class="duration"><i class="fa fa-clock-o"></i> <?=$vacancy->duration_of_contract?> &plusmn; <?=$vacancy->duration_of_contract_offset?></div>
						    		<div class="vessel_type"><i class="fa fa-ship"></i> <?=Yii::$app->formatter->asInteger($vacancy->vessel_deadweight)?> MT</div>
						    	</div>
					    	</div>
					    	<div class="sallary">
								<?php if(!empty($vacancy->salary_from)): ?>
									<div class="summ">
										<?=Currency::find()->where(['id'=>$vacancy->salary_currency_id])->one()->symbol?>&nbsp;<?=Yii::$app->formatter->asInteger($vacancy->salary_from)?>
									</div>
									<div class="period">
										<?php if($vacancy->payroll_period==1): ?>
											per day
										<?php elseif($vacancy->payroll_period==7): ?>
											per week
										<?php elseif($vacancy->payroll_period==30): ?>
											per month
										<?php endif; ?>
									</div>
								<?php else: ?>
									<div class="nego">Negotiable</div>
								<?php endif; ?>
					    	</div>
					    </div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
    	</div>
    </div>
	<?php if(!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->id == $data->user_id): ?>
		<div class="tab_cont vess">
			<div class="vessels">
				<div class="bt">
					<h2>My vessels</h2>
					<span class="edit bvess">Edit</span>
				</div>
				<?php if(!empty($vessels)): ?>
					<table>
						<thead>
							<tr>
								<td>Vessel name</td>
								<td>IMO / Vessel flag</td>
								<td>Vessel type / DWT</td>
								<td>ME type / ME power</td>
								<td>Year built / Shipowner</td>
							</tr>
						</thead>
						<?php foreach($vessels as $vessel): ?>
							<tr>
								<td class="f-tit"><?=$vessel->vessel_name?></td>
								<td>
									<?php if(!empty($vessel->imo_number)): ?>
										<?=$vessel->imo_number?>
									<?php else: ?>
										&mdash;
									<?php endif; ?>
									&nbsp;/ <?=Countries::find()->where(['id'=>$vessel->vessel_flag_id])->one()->name?></td>
								<td><?=vesselTypes::find()->where(['id'=>$vessel->vessel_type_id])->one()->name?> / <?=$vessel->deadweight?></td>
								<td>
									<?php if(!empty($vessel->engine_type_id)): ?>
										<?=Engines::find()->where(['id'=>$vessel->engine_type_id])->one()->name?>
									<?php else: ?>
										&mdash;
									<?php endif; ?>
									&nbsp;/
									<?php if(!empty($vessel->engine_power)): ?>
										<?php if($vessel->measure=='kW'): ?>
											<?=$vessel->engine_power?>
										<?php else: ?>
											<?=($vessel->engine_power*1.43)?>
										<?php endif; ?>
										kW
									<?php else: ?>
										&mdash;
									<?php endif; ?>
								</td>
								<td>
									<?php if(!empty($vessel->year_built)): ?>
										<?=$vessel->year_built?>
									<?php else: ?>
										&mdash;
									<?php endif; ?>
									&nbsp;/
									<?php if(!empty($vessel->shipowning_company_name)): ?>
										<?=$vessel->shipowning_company_name?>
									<?php else: ?>
										&mdash;
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
				<?php else: ?>
					<div class="empty">
						<p>Add all of your vessels here and use them to post vacancies with ease.</p>
						<a href="#" class="edit bvess">New vessel »</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php
$this->registerJs('
	$(\'.edit.cont\').on(\'click\', function(){
		$(\'.contacts .cont\').fadeOut(400);
		setTimeout(function(){
			$(\'.contacts .cont-form\').fadeIn();
		},400);
	});
	$(\'.cancel.company-contacts\').on(\'click\',function(){
		$(\'.contacts .cont-form\').fadeOut(400);
		setTimeout(function(){
			$(\'.contacts .cont\').fadeIn();
		},400);
	});
	$(\'.edit.about\').on(\'click\', function(){
		$(\'.wrapper.company .com_cont .about\').fadeOut(400);
		setTimeout(function(){
			$(\'.wrapper.company .com_cont .about-form\').fadeIn();
		},400);
	});

	$(\'.cancel.company-about\').on(\'click\',function(){
		$(\'.wrapper.company .com_cont .about-form\').fadeOut(400);
		setTimeout(function(){
			$(\'.wrapper.company .com_cont .about\').fadeIn();
		},400);
	});
', \yii\web\View::POS_READY);
?>