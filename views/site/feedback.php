<?php
use yii\bootstrap\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\web\View;



/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $feedback app\models\Feedback */

$this->title = 'Feedback';
?>

<div class="wrapper vacancies">
    <div class="vac_list">
        <div class="bt">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
            <?= $this->render('feedback/_form', ['model' => $model]); ?>

        <?php Pjax::begin(['id' => 'feedbacks', 'enablePushState' => false]); ?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'summary' => '',
            'itemView' => 'feedback/_item',
        ]) ?>
        <?php Pjax::end(); ?>
    </div>
    <div class="filters">
        Looking for help? Check out our <?= Html::a('Help', ['#']); ?>
          </div>
    <div class="clear">
    </div>
</div>

<script type="text/template" id="tmp_feedback">
    <?= $this->render('feedback/_del_item'); ?>
</script>
<script type="text/template" id="tmp_edit_feedback">
    <?= $this->render('feedback/_edit_item'); ?>
</script>
<?php
$this->registerJs(
    '$("document").ready(function(){
            $("#edit_feedback").on("pjax:end", function() {
            $.pjax.reload({container:"#feedbacks"});  //Reload list of feedbacks
        });
    });'
);
?>
<?php
$this->registerJs("
$(document).on('mouseover', '.item',function() {
    $(this).find('.feedback-btn').removeClass('hide').addClass('show');
});
$(document).on('mouseout', '.item',function() {
    $(this).find('.feedback-btn').removeClass('show').addClass('hide');
});;

$(document).on('click', '.edit-feedback', function(e) {
    var el = $(this);
    var el_id = el.data('feedback-id');
    var tmp = $('#tmp_edit_feedback').html();
    var el_info = el.parent().parent();
    tmp = tmp.replace('%text%',el_info.find('.feedback-text').text().trim());
    tmp = tmp.replace('%id%',el_id);
    el_info.addClass('hide');
    el_info.parent().append(tmp);
});

$(document).on('click', '.remove-feedback', function(e) {
    var el = $(this);
    var el_id = el.data('feedback-id');
    var tmp = $('#tmp_feedback').html();

    var el_info = el.parent().parent().parent();
    el_info.html(tmp);
    $.post( '/feedbackdelete', { id: el_id } );

});

$(document).on('click', '#btn-feedback-cancel', function() {
    $('#edit-info').parent().find('.info').removeClass('hide');
    $('#edit-info').remove();
});
", View::POS_READY, 'feedback-btn');
?>


