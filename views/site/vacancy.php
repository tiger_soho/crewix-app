<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $data \app\models\Vacancies */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Companies;
use app\models\Countries;
use app\models\Ranks;
use app\models\vesselTypes;
use app\models\LanguageLevel;

$this->title = 'Vacancies';
?>
<div class="wrapper vacancy">
    <div class="block">
    	<div class="com_head">
    		<div class="img">
    			<img src="/images/companies/logos/noimage.jpg" alt="" />
    		</div>
    		<div class="info">
    			<h1><?=$data->ranks->name?> on <?=$data->vesselTypes->name?></h1>
    			<div class="company">
    				<a href="<?=Url::to(['profile', 'id'=>\app\models\Profile::findOne(['user_id'=>Companies::findOne($data->company_id)->user_id])->id])?>"><?=Companies::find()->where(['id'=>$data->company_id])->one()->name?></a>
    			</div>
    			<div class="opened"><i class="fa fa-clock-o"></i> <span><?=$data->created_at?></span></div>
    			<div class="views"><i class="fa fa-eye"></i> <?=$data->views_count?></div>
    		</div>
    	</div>
    	<div class="bt">
    		<h2>Main information</h2>
    	</div>
    	<div class="data">
    		<table>
    			<tr>
    				<td>Rank</td>
    				<td><?=$data->ranks->name?></td>
    			</tr>
    			<tr>
    				<td>Salary</td>
    				<td>
						<?php if($data->negotiable=='1'): ?>
							Negotiable
						<?php else: ?>
							<?=\app\models\Currency::findOne($data->salary_currency_id)->symbol?><?=$data->salary_from*$data->payroll_period?>
							Per
							<?php if($data->payroll_period==1): ?>
								day
							<?php elseif($data->payroll_period==7): ?>
								week
							<?php elseif($data->payroll_period==30): ?>
								month
							<?php endif; ?>
						<?php endif; ?>
					</td>
    			</tr>
    			<tr>
    				<td>Duration of contract</td>
    				<td><?=$data->duration_of_contract?> </td>
    			</tr>
    			<tr>
    				<td>Joining date</td>
    				<td><?=Yii::$app->formatter->asDate($data->joining_date_from, "php:d.m.Y")?></td>
    			</tr>
    		</table>
    	</div>
    	<div class="bt">
    		<h2>Vessel</h2>
    	</div>
    	<div class="data">
    		<table>
    			<tr>
    				<td>Type</td>
    				<td><?=$data->vesselTypes->name?></td>
    			</tr>
    			<tr>
    				<td>Flag</td>
    				<td><?=$data->countries->name?></td>
    			</tr>
    			<tr>
    				<td>DWT</td>
    				<td><?=Yii::$app->formatter->asInteger($data->vessel_deadweight)?> MT</td>
    			</tr>
    			<tr>
    				<td>Year of built</td>
    				<td><?=$data->vessel_year_built?></td>
    			</tr>
    			<tr>
    				<td>ME type / ME power</td>
    				<td><?=$data->engines->name?></td>
    			</tr>
    		</table>
    	</div>
    	<div class="bt">
    		<h2>Requirements</h2>
    	</div>
    	<div class="data">
    		<table>
    			<tr>
    				<td>Age</td>
    				<td>
    					<?php if(!empty($data->required_age_from)&&(!empty($data->required_age_to))): ?>
							<?=$data->required_age_from?>&nbsp;&mdash;&nbsp;<?=$data->required_age_to?>
						<?php elseif(!empty($data->required_age_from)&&(empty($data->required_age_to))): ?>
							<?=$data->required_age_from?>+
						<?php elseif(empty($data->age_from)&&(!empty($data->required_age_to))): ?>
							18&nbsp;&mdash;&nbsp;<?=$data->required_age_to?>
						<?php else: ?>
							18+
						<?php endif; ?>
    				</td>
    			</tr>
    			<tr>
    				<td>Citizenship</td>
    				<td>
                        <?php 
                            $arr = array();
                            $countries = explode(',', $data->required_citizenship);
                            foreach ($countries as $country) {
                                $arr[] = Countries::find()->where(['id'=>$country])->one()->name;
                            }
                            $countries = implode(', ', $arr);
                            echo $countries;
                        ?>            
                    </td>
    			</tr>
    			<tr>
    				<td>English level</td>
    				<td><?=LanguageLevel::find()->where(['id'=>$data->required_language])->one()->name?></td>
    			</tr>
    			<tr>
    				<td>US visa</td>
    				<td>
    					<?php if($data->us_visa_required==1): ?>
    						Required: <?=$data->us_visa_required_type_id?>
    					<?php else: ?>
    						Not required
    					<?php endif; ?>
    				</td>
    			</tr>
    		</table>
    	</div>
    	<?php if(!empty($data->notes)): ?>
			<div class="bt">
	    		<h2>Notes</h2>
	    	</div>
			<div class="data notes">
				<?=$data->notes?>
			</div>
		<?php endif; ?>
    </div>
</div>