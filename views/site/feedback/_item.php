<?php
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use app\models\User;

use app\models\Feedback;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = 'Feedback';
?>


<div class="img">
    <img src="/images/sys/profile.png" alt=""/>
</div>
<div class="info">
    <?php $username = $model->user->account_type_id == Feedback::ACCOUNT_TYPE_COMPANY ? $model->company->name : $model->seamen->last_name ?>
    <?= Html::a($username, ['#']); ?>
    <div class="pull-right hide feedback-btn">
        <?php if (User::allowForCurrentUser($model->user_id)): ?>
            <?php if (time() < strtotime("+10 minutes", strtotime($model->created_at))): ?>
                <span class="glyphicon glyphicon-pencil edit-feedback" data-feedback-id="<?= $model->id ?>"></span>
            <?php endif; ?>
        <span class="glyphicon glyphicon-remove remove-feedback" data-feedback-id="<?= $model->id ?>"></span>

        <?php endif; ?>
    </div>
    <div class="feedback-text">
        <?= $model->text ?>
    </div>
    <div class="">
        <h2><?= $model->created_at ?></h2>
    </div>
</div>
