<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

?>

<?php
$this->registerJs(
    '$("document").ready(function(){
            $("#new_feedback").on("pjax:end", function() {
            $.pjax.reload({container:"#feedbacks"});  //Reload list of feedbacks
        });
    });'
);
?>

<div class="item">
    <div class="img">
        <img src="/images/sys/profile.png" alt="" />
    </div>
    <div class="info">
        <div class="feedback-form">
            <?php Pjax::begin(['id' => 'new_feedback']) ?>
            <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true],'id' => 'feedback-form']); ?>
            <?= $form->field($model, 'text')->textArea(['rows' => 3, 'class'=>'form-control']) ?>
            <?= $form->errorSummary($model); ?>
            <div class="form-group">
                <?= Html::submitButton('Publish', ['class' => 'btn btn-primary', 'name' => 'feedback-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <?php Pjax::end(); ?>

        </div>
    </div>
    <div class="vac_count">
        <div class="qty">

        </div>
    </div>
</div>



