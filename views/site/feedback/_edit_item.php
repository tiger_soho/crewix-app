<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

?>
    <div class="info" id="edit-info">
        <div class="feedback-form">
            <?php Pjax::begin(['id' => 'edit_feedback']) ?>
            <?= Html::beginForm('/feedbackedit', 'post', ['id' => 'feedback-edit-form', 'data-pjax' => true]); ?>
            <?= Html::hiddenInput('edit_form', 'true'); ?>
            <?= Html::hiddenInput('id', '%id%'); ?>
            <?= Html::textarea('text', '%text%', ['rows' => 3, 'class'=>'form-control']); ?>
            <div class="form-group">
                <?= Html::submitButton('Publish', ['class' => 'btn btn-primary', 'name' => 'feedback-button']) ?>
                <?= Html::button('Cancel',['class' => 'btn btn-default', 'name' => 'feedback-button-cancel', 'id' => 'btn-feedback-cancel'] ) ?>
            </div>
            <?= Html::endForm(); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
