<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */



?>
<div class="bt">
    <h2>Languages</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
<span class="edit lang">Edit</span>
<?php endif; ?>
</div>
<table class="langs">
    <?php foreach ($data->languages as $lang) { ?>
        <tr>
            <td class="f-tit"><?= \app\models\Langs::find()->where(['id' => $lang->language_id])->one()->name ?></td>
            <td colspan="3"><?= \app\models\LanguageLevel::find()->where(['id' => $lang->language_level_id])->one()->name ?></td>
        </tr>
    <?php } ?>
</table>