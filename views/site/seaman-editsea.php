<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
?>
<div class="bt">
    <h2>Sea service</h2>
    <span class="cancel sevice pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'st-serv',
    'options' => ['class'=>'serv_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
<table>
    <thead>
    <tr>
        <td>Rank</td>
        <td>Vessel name / Flag</td>
        <td>Vessel type / DWT</td>
        <td>ME type / ME power</td>
        <td>Shipowner / Crewing...</td>
        <td>From / To</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($data->service as $item): ?>
        <tr>
            <td>
                <?= $form->field($model,'id[]')->hiddenInput(['value'=>$item['id']])->label(false) ?>
                <?= $form->field($model, 'rank_id[]')->dropDownList(ArrayHelper::map(\app\models\Ranks::find()->all(), 'id', 'name'), ['prompt'=>'Select rank','options'=>[$item['rank_id']=>['Selected'=>'selected']]])->label(false);?>
            </td>
            <td>
                <?=$form->field($model,'vessel_name[]')->textInput(['placeholder'=>'Vessel name','value'=>$item['vessel_name']])->label(false);?>
                <?= $form->field($model, 'vessel_flag_id[]')->dropDownList(ArrayHelper::map(Countries::find()->all(), 'id', 'name'), ['prompt'=>'Select country','options'=>[$item['vessel_flag_id']=>['Selected'=>'selected']]])->label(false);?>
            </td>
            <td>
                <?= $form->field($model, 'vessel_type_id[]')->dropDownList(ArrayHelper::map(\app\models\VesselTypes::find()->all(), 'id', 'name'),['prompt'=>'Vessel type','options'=>[$item['vessel_type_id']=>['Selected'=>'selected']]])->label(false);?>
                <?=$form->field($model,'vessel_deadweight[]')->textInput(['placeholder'=>'Enter DWT','value'=>$item['vessel_deadweight']])->label(false);?>
            </td>
            <td>
                <?= $form->field($model, 'engine_type_id[]')->dropDownList(ArrayHelper::map(\app\models\Engines::find()->all(), 'id', 'name'), ['prompt'=>'ME type','options'=>[$item['engine_type_id']=>['Selected'=>'selected']]])->label(false);?>
                <div class="f_blc">
                    <?=$form->field($model,'engine_power[]')->textInput(['placeholder'=>'ME power','value'=>$item['engine_power']])->label(false);?><?= $form->field($model, 'measure[]')->dropDownList(['kW'=>'kW','BHP'=>'BHP'])->label(false);?>
                </div>
            </td>
            <td>
                <?=$form->field($model,'shipowning_company_name[]')->textInput(['placeholder'=>'Shipowner','value'=>$item['shipowning_company_name']])->label(false);?>
                <?=$form->field($model,'crewing_company_name[]')->textInput(['placeholder'=>'Crewing agency','value'=>$item['crewing_company_name']])->label(false);?>
            </td>
            <td>
                <?= $form->field($model, 'date_from[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy','value'=>Yii::$app->formatter->asDate($item['date_from'],'dd.MM.y')])->label(false) ?>
                <?= $form->field($model, 'date_to[]', ['options'=>['class'=>'date']])->textInput(['placeholder'=>'dd.mm.yyyy','value'=>Yii::$app->formatter->asDate($item['date_to'],'dd.MM.y')])->label(false) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Row</span>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'm_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
    <script type="text/template" id="new-sea-row">
        <?= $this->render('/signup/rowserv', [
            'model' => $model,
        ]); ?>
    </script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-sea-row\').html();

    $(\'#st-serv table tbody\').append(tpl);
    $(\'#st-serv select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'prof-add');
?>