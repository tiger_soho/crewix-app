<?php

?>
<div class="bt">
    <h2>Education</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit edu">Edit</span>
    <?php endif; ?>
</div>
<table class="education">
    <thead>
    <tr>
        <td>Institution</td>
        <td>Degree</td>
        <td>From</td>
        <td>To</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data->education as $edu) { ?>
        <tr>
            <td class="f-tit">
                <?php if (\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin')): ?>
                <?= $edu->institution_name ?></td>
            <?php else: ?>
                *****
            <?php endif; ?>
            <td><?= $edu->degree ?></td>
            <td><?= $edu->year_from ?></td>
            <td><?= $edu->year_to ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
