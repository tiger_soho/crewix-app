<?php
use app\models\Countries;
    use app\models\TravelDocument;
use app\models\SeamanTravelDocument;
use app\models\SeamanCertificateOfCompetency;

$travel_scans = SeamanTravelDocument::find()->where(['!=', 'scan_id', ''])->andWhere(['seaman_id' => $data->id])->all();
$coc_scans = SeamanCertificateOfCompetency::find()->where([
    '!=',
    'scan_id',
    ''
])->andWhere(['seaman_id' => $data->id])->all();
?>

<div class="bt">
    <h2>Document scans</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit add-scan">Edit</span>
    <?php endif; ?>
</div>
<div class="docs">
    <?php if (!empty($travel_scans)): ?>
        <div class="bt">
            <h2>Travel documents</h2>
        </div>
        <?php $base = __DIR__ . '/../../../'; ?>

        <?php foreach ($travel_scans as $scan): ?>
            <?php $filedata = exif_read_data(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src) ?>
            <?php $size = getimagesize(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src) ?>
            <div class="item">
                <div class="img">
                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                        <img src="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src ?>" alt="">
                    <?php else: ?>
                        <div class="hide-img"><img src="/images/sys/cloud-ico.svg" alt=""/></div>
                    <?php endif; ?>
                </div>
                <div class="info">
                    <div class="docname">
                        <h3><?= TravelDocument::find()->where(['id' => $scan->document_id])->one()->name ?></h3></div>
                    <div class="additional">Issued
                        in <?= Countries::find()->where(['id' => $scan->place_of_issue_id])->one()->name ?>
                        / <?= round($filedata['FileSize'] / 1024 / 1024, 2) ?> MB / <?= $size[0] ?>
                        x<?= $size[1] ?></div>
                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                        <div class="buttons">
                            <a class="view_doc btn btn-primary" href="/images/seaman/docs/scans/<?= \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src ?>"><i class="fa fa-search-plus"></i> View
                                document</a> <a href="/images/seaman/docs/scans/<?= \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src ?>"
                                                class="dl btn btn-primary" target="_blank"><i
                                    class="fa fa-download"></i> Download</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (!empty($coc_scans)): ?>
        <div class="bt">
            <h2>Certificate of Competency</h2>
        </div>
        <?php $base = __DIR__ . '/../../';
        ?>

        <?php foreach ($coc_scans as $sc): ?>
            <?php $filedata = exif_read_data(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src) ?>
            <?php $size = getimagesize(Yii::$app->basePath . '/web/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src) ?>
            <div class="item">
                <div class="img">
                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                        <img
                            src="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src ?>"
                            alt="">
                    <?php else: ?>
                        <div class="hide-img"><img src="/images/sys/cloud-ico.svg" alt=""/></div>
                    <?php endif; ?>
                </div>
                <div class="info">
                    <div class="docname">
                        <h3><?=\app\models\CertificateOfCompetencyType::findOne($sc->document_type_id)->name?></h3></div>
                    <div class="additional">Issued
                        in <?= Countries::find()->where(['id' => $sc->place_of_issue_id])->one()->name ?>
                        / <?= round($filedata['FileSize'] / 1024 / 1024, 2) ?> MB / <?= $size[0] ?>
                        x<?= $size[1] ?></div>
                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                        <div class="buttons">
                            <a class="view_doc btn btn-primary" href="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src ?>"><i class="fa fa-search-plus"></i> View
                                document</a> <a
                                href="<?= '/images/seaman/docs/scans/' . \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src ?>"
                                class="dl btn btn-primary" target="_blank"><i
                                    class="fa fa-download"></i> Download</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (empty($travel_scans) && empty($coc_scans)): ?>
        <div class="empty">
            <p>Upload document scans to increase the chances of being hired dramatically.</p>
            <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
                <a class="add-scan">Upload scans &raquo;</a>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>