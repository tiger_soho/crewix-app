<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */
/* @var $prof \app\models\SeamanCertificateOfProficiency */

use app\models\Countries;
use app\models\Ranks;
use app\models\Competency;

?>

    <div class="bt">
        <h2>Certificates of proficiency</h2>
        <?php if(\app\models\User::allowForCurrentUser($data->user_id)): ?>
            <span class="edit prof">Edit</span>
        <?php endif; ?>
    </div>
    <table class="five">
        <thead>
        <tr>
            <td>Document</td>
            <td>Number</td>
            <td>Place of issue</td>
            <td>Date of issue</td>
            <td>Date of expiry</td>
        </tr>
        </thead>
        <?php foreach ($data->proficiency as $prof) { ?>
            <tr>
                <td class="f-tit"><?= \app\models\CertificateOfProficiency::find()->where(['id' => $prof->document_id])->one()->name ?></td>
                <td>
                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                    <?= $prof->number ?></td>
                <?php else: ?>
                    *****
                <?php endif; ?>
                </td>
                <td><?= Countries::find()->where(['id' => $prof->place_of_issue_id])->one()->name ?></td>
                <td class="date"><?= Yii::$app->formatter->asDate($prof->date_of_issue, "php:d.m.Y") ?></td>
                <td class="date"><?= Yii::$app->formatter->asDate($prof->date_of_expiry, "php:d.m.Y") ?></td>
            </tr>
        <?php } ?>
    </table>
