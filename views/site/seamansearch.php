<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Ranks;
use app\models\Profile;
use app\models\Currency;

$this->title = 'Seamen';
?>
    <div class="vac_list">
        <div class="bt">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <?php if(!empty($models)): ?>
            <?php foreach ($models as $model) { ?>
                <div class="item">
                    <div class="img"><img src="images/companies/logos/noimage.jpg" alt=""></div>
                    <div class="info">
                        <?php $id = Profile::find()->select('id')->where(['user_id'=>$model->user_id])->one()->id; ?>
                        <h2><a href="<?=Url::to(['seaman','id'=>$id])?>"><?=$model->first_name?> &mdash; <?=Ranks::find()->where(['id'=>$model->primary_rank_id])->one()->name?> / <?=Ranks::find()->where(['id'=>$model->secondary_rank_id])->one()->name?></a></h2>
                        <div class="detales">
                            <div class="bdate"><i class="fa fa-clock-o"></i> <span><?=$model->date_of_birth?></span></div>
                            <div class="duration"><i class="fa fa-globe"></i> <?=\app\models\Countries::find()->where(['id'=>$model->citizenship_id])->one()->name?></div>
                            <div class="lang"><i class="fa fa-commenting-o"></i> <?=\app\models\SeamanLanguage::find()->where(['seaman_id'=>$model->id])->andWhere(['language_id'=>'1'])->one()->language_level_id?></div>
                        </div>
                    </div>
                    <div class="sallary">
                        <div class="summ"><?=Currency::find()->where(['id'=>$model->salary_currency_id])->one()->symbol?>&nbsp;<?=Yii::$app->formatter->asInteger($model->salary)?></div>
                        <div class="period">per month</div>
                    </div>
                </div>
            <?php } ?>
        <?php else: ?>
            <div class="empty">There is no result</div>
        <?php endif; ?>
    </div>


    <div class="clear"></div>
<?php echo LinkPager::widget([
    'pagination' => $pages,
    'maxButtonCount' => 5
]); ?>