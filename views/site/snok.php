<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Vacancies;
use app\models\Countries;
use app\models\Ranks;
use app\models\Docs;
use app\models\Langs;
use app\models\Languages;
use app\models\Education;
use app\models\TravelDocs;
use app\models\VesselTypes;
use app\models\Competency;
use app\models\Proficiency;
use app\models\ProficiencyDocs;
use app\models\Service;
use app\models\Engines;
use app\models\References;


?>

<div class="block">
	<div class="bt">
		<h2>Next of kin</h2>
		<?php if(\app\models\User::allowForCurrentUser($data->user_id)): ?>
			<span class="edit kin">Edit</span>
		<?php endif; ?>
	</div>
	<table>
		<tr>
			<td class="f-tit">Full name</td>
			<td><?=$data->nok->name?></td>
			<td class="f-tit">Relationship</td>
			<td><?=\app\models\NextOfKinRelationship::find()->where(['id'=>$data->nok->relationship_id])->one()->name?></td>
		</tr>
		<tr>
			<td class="f-tit">Phone number</td>
			<td><?=$data->nok->phone?></td>
			<td class="f-tit">Home address</td>
			<td><?=$data->nok->address?></td>
		</tr>
	</table>
</div>