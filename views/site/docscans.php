<?php


/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \app\models\LoginForm */


use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use app\models\Ranks;

use app\models\Countries;

use app\models\CertificateOfCompetencyType;

$id = \app\models\Seaman::findOne(['user_id' => Yii::$app->user->id])->id;
$all_tr = \app\models\SeamanTravelDocument::find()->where(['seaman_id' => $id])->all();
$all_coc = \app\models\SeamanCertificateOfCompetency::find()->where(['seaman_id' => $id])->all();
?>
<div class="docs">
    <?php $form = ActiveForm::begin([
        'id' => 'scans',
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>
    <?php if (!empty($travel_scans)): ?>
        <div class="bt">
            <h2>Travel documents</h2>
        </div>

        <?php foreach ($travel_scans as $scan): ?>
            <?php $file = \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->src ?>
            <div class="item">
                <div class="img">
                    <div class="images"><img src="/images/companies/logos/noimage.jpg" alt=""></div>
                </div>
                <div class="info">
                    <div class="doc-type">
                        <select name="UserFile[doc_id][]" id="">
                            <?php foreach ($all_tr as $sc): ?>
                                <option
                                    value="1-<?= $sc->id ?>" <?php if ($scan->id == $sc->id): ?> selected<?php endif; ?>><?= \app\models\TravelDocument::find()->where(['id' => $sc->document_id])->one()->name ?>
                                    (<?= Countries::find()->where(['id' => $sc->place_of_issue_id])->one()->name ?>)
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="file-desc">
                        <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                            <div class="upload-btn__txt">Select file</div>
                            <?= $form->field($model, 'src[]',
                                ['options' => ['class' => 'scan']])->fileInput(['accept' => 'image/jpg'])->label(false); ?>
                            <?= $form->field($model, 'size[]',
                                ['options' => ['class' => 'file-info']])->hiddenInput()->label(false) ?>
                            <?= $form->field($model,
                                'id[]')->hiddenInput(['value' => \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($scan->scan_id)->scan_file_id)->id])->label(false) ?>
                        </div>
                        <span class="btn remove">Remove</span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if (!empty($coc_scans)): ?>
        <div class="bt">
            <h2>Travel documents</h2>
        </div>
        <?php foreach ($coc_scans as $sc): ?>
            <?php $file = \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->src ?>
            <div class="item">
                <div class="img">
                    <div class="images"><img src="/images/companies/logos/noimage.jpg" alt=""></div>
                </div>
                <div class="info">
                    <div class="doc-type">
                        <select name="UserFile[doc_id][]" id="">
                            <?php foreach ($all_coc as $coc): ?>
                                <option
                                    value="2-<?= $coc->id ?>" <?php if ($coc->id == $sc->id): ?> selected<?php endif; ?>><?= \app\models\CertificateOfCompetencyType::findOne($coc->document_type_id)->name ?>
                                    : <?= Ranks::find()->where(['id' => $coc->rank_id])->one()->name ?>
                                    (<?= Countries::find()->where(['id' => $coc->place_of_issue_id])->one()->name ?>)
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="file-desc">
                        <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                            <div class="upload-btn__txt">Select file</div>
                            <?= $form->field($model, 'src[]',
                                ['options' => ['class' => 'scan']])->fileInput(['accept' => 'image/jpg'])->label(false); ?>
                            <?= $form->field($model, 'size[]',
                                ['options' => ['class' => 'file-info']])->hiddenInput()->label(false) ?>
                            <?= $form->field($model,
                                'id[]')->hiddenInput(['value' => \app\models\UserFile::findOne(\app\models\SeamanDocumentScan::findOne($sc->scan_id)->scan_file_id)->id])->label(false) ?>
                        </div>
                        <span class="btn remove">Remove</span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="bt">
            <h2>New document</h2>
        </div>
        <div class="item">
            <div class="img">
                <div class="images">
                    <svg class="loader" viewBox="0 0 100 100">
                        <circle class="one" r="40" cx="50" cy="50"/>
                        <circle class="two" r="40" cx="50" cy="50"/>
                    </svg>
                </div>
            </div>
            <div class="info">
                <div class="doc-type">
                    <select name="UserFile[doc_id][]" id="">
                        <?php foreach ($all_tr as $tr): ?>
                            <option
                                value="1-<?= $tr->id ?>"><?= \app\models\TravelDocument::find()->where(['id' => $tr->document_id])->one()->name ?>
                                (<?= Countries::find()->where(['id' => $tr->place_of_issue_id])->one()->name ?>)
                            </option>
                        <?php endforeach; ?>
                        <?php foreach ($all_coc as $coc): ?>
                            <option
                                value="2-<?= $coc->id ?>"><?= \app\models\CertificateOfCompetencyType::find()->where(['id' => $coc->document_type_id])->one()->name ?>
                                : <?= Ranks::find()->select('name')->where(['id' => $coc->rank_id])->one()->name ?>
                                (<?= Countries::find()->where(['id' => $coc->place_of_issue_id])->one()->name ?>)
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="file-desc">
                    <div class="js-fileapi-wrapper upload-btn btn btn-primary">
                        <div class="upload-btn__txt">Select file</div>
                        <?= $form->field($model, 'src[]',
                            ['options' => ['class' => 'scan']])->fileInput(['accept' => 'image/jpg'])->label(false); ?>
                        <?= $form->field($model, 'size[]',
                            ['options' => ['class' => 'file-info']])->hiddenInput()->label(false) ?>
                        <?= $form->field($model,'id[]')->hiddenInput()->label(false); ?>
                    </div>
                    <span class="btn remove">Remove</span>
                </div>
                <div class="size"></div>
            </div>
        </div>
    <?php endif; ?>
    <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Scan</span>
    <div class="bot">
        <div class="form-group">
            <?= Html::submitButton('Save changes',
                ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'save']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script>
    $("select").selectmenu({
        width: '100%'
    });
    window.FileAPI = {
        html5: !!1,
        staticPath: '/js/filapi/',
        flashUrl: '/js/filapi/FileAPI.flash.swf',
        flashImageUrl: '/js/filapi/FileAPI.flash.image.swf'
    };
</script>
<script src="/js/fileapi/FileAPI.min.js"></script>
<script>
    $('.scan').on('change', function (evt) {
        var f = evt.target.files[0];
        var reader = new FileReader();
        reader.onload = (function (theFile) {
            return function (e) {
                if (theFile.size > 2000000) {
                    alert('Images size more then 2MB');
                    return false;
                }
            };
        });
        var i = $(this);

        FileAPI.upload({
            url: '/docupload',
            files: {src: f},
            data: {'_csrf': '<?=Yii::$app->request->getCsrfToken()?>'},
            imageTransform: {
                width: 350,
                height: 450,
                preview: true
            },
            progress: function (evt) {
                var a = (evt.loaded / evt.total) * 360;
                i.parents('.item').find('.images').find('svg').find('.two').css({'stroke-dasharray': '' + a + ' 300'});
            },
            complete: function (err, xhr) {
                console.log(xhr.responseText);
            },
            filecomplete: function (err, xhr, file) {
                FileAPI.Image(f).preview(100, 100).get(function (err, img) {
                    setTimeout(function () {
                        i.parents('.item').find('.images').html(img);
                    }, 300);
                    var fileinfo = '{"name":"' + xhr.responseText + '","size":"' + file.size + '"}';
                    i.parents('.item').find('.file-info').find('input').val(fileinfo);
                });
            }
        });
        reader.readAsDataURL;
    });
</script>