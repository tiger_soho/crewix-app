<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */
/* @var $com \app\models\SeamanCertificateOfCompetency */

use app\models\Countries;
use app\models\Ranks;
use app\models\CertificateOfCompetencyType;

?>

    <div class="bt">
        <h2>Certificates of competency</h2>
        <?php if(\app\models\User::allowForCurrentUser($data->user_id)): ?>
            <span class="edit comp">Edit</span>
        <?php endif; ?>
    </div>
    <table class="five">
        <thead>
        <tr>
            <td>Document</td>
            <td>Number</td>
            <td>Place of issue</td>
            <td>Date of issue</td>
            <td>Date of expiry</td>
        </tr>
        </thead>
        <?php foreach ($data->competency as $com) { ?>
            <tr>
                <td class="f-tit">Rank</td>
                <td colspan="4"><?= Ranks::find()->where(['id' => $com->rank_id])->one()->name ?></td>
            </tr>
            <tr>
                <td class="f-tit"><?= CertificateOfCompetencyType::find()->where(['id' => $com->document_type_id])->one()->name ?></td>
                <td>
                    <?php if ((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
                    <?= $com->number ?></td>
                <?php else: ?>
                    *****
                <?php endif; ?>
                </td>
                <td><?= Countries::find()->where(['id' => $com->place_of_issue_id])->one()->name ?></td>
                <td class="date"><?= Yii::$app->formatter->asDate($com->date_of_issue, "php:d.m.Y") ?></td>
                <td class="date"><?= Yii::$app->formatter->asDate($com->date_of_expiry, "php:d.m.Y") ?></td>
            </tr>
        <?php } ?>
    </table>
