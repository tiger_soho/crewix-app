<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use kartik\file\FileInput;
?>
<div class="bt">
    <h2>Next of kin</h2>
    <span class="cancel kin pull-right">Cancel</span>
</div>
    <?php $form = ActiveForm::begin(['id' => 'skedit',
        'options' => ['class'=>'family_form'],
        'validateOnBlur' => false,
    ]); ?>

    <?php
        $model->name = $data->nok->name;
        $model->relationship_id = $data->nok->relationship_id;
        $model->phone = $data->nok->phone;
        $model->address = $data->nok->address;
    ?>
    <table>
        <tr>
            <td class="f-tit"><span class="req">*</span> Full name</td>
            <td>
                <?= $form->field($model, 'name')->textInput(['placeholder'=>'Enter name and surname'])->label(false); ?>
            </td>
            <td class="f-tit"><span class="req">*</span> Relationship</td>
            <td>
                <?= $form->field($model, 'relationship_id')->dropDownList(ArrayHelper::map(\app\models\NextOfKinRelationship::find()->all(),'id','name'), ['prompt'=>'Select relationship'],['placeholder'=>'Select relationship'])->label(false);?>
            </td>
        </tr>
        <tr>
            <td class="f-tit"><span class="req">*</span> Phone number</td>
            <td>
                <?= $form->field($model, 'phone')->textInput(['placeholder'=>'+1 555 555 5555'])->label(false); ?>
            </td>
            <td class="f-tit"><span class="req">*</span> Home address</td>
            <td>
                <?= $form->field($model, 'address')->textInput(['placeholder'=>'Street, house, city and country'])->label(false); ?>
            </td>
        </tr>
    </table>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>
<script>
    $('#family-kin_phone').intlTelInput();
</script>