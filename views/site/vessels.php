<?php

use app\models\Countries;
use app\models\Engines;
use app\models\VesselTypes;
?>
<div class="bt">
    <h2>My vessels</h2>
    <span class="edit bvess">Edit</span>
</div>
<?php if(!empty($data->companyVessel)): ?>
    <table>
        <thead>
        <tr>
            <td>Vessel name</td>
            <td>IMO / Vessel flag</td>
            <td>Vessel type / DWT</td>
            <td>ME type / ME power</td>
            <td>Year built / Shipowner</td>
        </tr>
        </thead>
        <?php foreach($data->companyVessel as $vessel): ?>
            <tr>
                <td class="f-tit"><?=$vessel->vessel_name?></td>
                <td>
                    <?php if(!empty($vessel->imo_number)): ?>
                        <?=$vessel->imo_number?>
                    <?php else: ?>
                        &mdash;
                    <?php endif; ?>
                    &nbsp;/ <?=Countries::find()->where(['id'=>$vessel->vessel_flag_id])->one()->name?></td>
                <td><?=VesselTypes::find()->where(['id'=>$vessel->vessel_type_id])->one()->name?> / <?=$vessel->deadweight?></td>
                <td>
                    <?php if(!empty($vessel->engine_type_id)): ?>
                        <?=Engines::find()->where(['id'=>$vessel->engine_type_id])->one()->name?>
                    <?php else: ?>
                        &mdash;
                    <?php endif; ?>
                    &nbsp;/
                    <?php if(!empty($vessel->engine_power)): ?>
                        <?php if($vessel->measure=='kW'): ?>
                            <?=$vessel->engine_power?>
                        <?php else: ?>
                            <?=($vessel->engine_power*1.43)?>
                        <?php endif; ?>
                        kW
                    <?php else: ?>
                        &mdash;
                    <?php endif; ?>
                </td>
                <td>
                    <?php if(!empty($vessel->year_built)): ?>
                        <?=$vessel->year_built?>
                    <?php else: ?>
                        &mdash;
                    <?php endif; ?>
                    &nbsp;/
                    <?php if(!empty($vessel->shipowning_company_name)): ?>
                        <?=$vessel->shipowning_company_name?>
                    <?php else: ?>
                        &mdash;
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <div class="empty">
        <p>Add all of your vessels here and use them to post vacancies with ease.</p>
        <a href="#" class="edit bvess">New vessel »</a>
    </div>
<?php endif; ?>
