<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\VesselTypes;
use app\models\Engines;
?>

<div class="bt">
	<h2>My vessels</h2>
    <span class="cancel vess">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'vessels',
    'options' => ['class'=>'vessels'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>

<table>
    <thead>
    <tr>
        <td>Vessel name</td>
        <td>IMO / Vessel flag</td>
        <td>Vessel type / DWT</td>
        <td>ME type / ME power</td>
        <td>Year built / Shipowner</td>
    </tr>
    </thead>
    <?php if(!empty($data)): ?>
        <?php foreach($data as $d): ?>
            <tr>
                <td>
                    <?=$form->field($model,'id[]')->hiddenInput(['value'=>$d['id']])?>
                    <?= $form->field($model, 'vessel_name[]')->textInput(['placeholder'=>'Enter vessel name','value'=>$d['vessel_name']])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($model, 'imo_number[]')->textInput(['placeholder'=>'Enter IMO number','value'=>$d['imo_number']])->label(false); ?>
                    <?php
                    $countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
                    $options = array();
                    foreach ($countries as $key => $value) {
                        $options[$key] = ['data-style'=>'background:url(/images/sys/flags/'.Countries::find()->where(['id'=>$key])->one()->code_2.'.png) 0 center no-repeat','value'=>$d['vessel_flag_id']];
                    }
                    ?>
                    <?= $form->field($model, 'vessel_flag_id[]', ['options'=>['class'=>'countries']])->dropDownList($countries, ['prompt'=>'Select country','options'=>$options])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($model, 'vessel_type_id[]')->dropDownList(ArrayHelper::map(VesselTypes::find()->all(),'id','name'), ['prompt'=>'Select type', 'options'=>['value'=>$d['vessel_type_id']]])->label(false); ?>
                    <?= $form->field($model, 'deadweight[]')->textInput(['placeholder'=>'Enter DWT','value'=>$d['deadweight']])->label(false); ?>
                </td>
                <td>
                    <?= $form->field($model, 'engine_type_id[]')->dropDownList(ArrayHelper::map(Engines::find()->all(),'id','name'), ['prompt'=>'Select ME type', 'options'=>['value'=>$d['engine_type_id']]])->label(false); ?>
                    <div class="power">
                        <?= $form->field($model, 'engine_power[]')->textInput(['placeholder'=>'Number','value'=>$d['engine_power']])->label(false); ?><?= $form->field($model, 'measure')->dropDownList(['kW'=>'kW','BHP'=>'BHP'], ['prompt'=>'Select type'])->label(false); ?>
                    </div>
                </td>
                <td>
                    <?php
                    $to = date('Y');
                    $range = range('1940', $to, 1);
                    foreach($range as $r){
                        $arr[]=['k'=>$r,'y'=>$r];
                    }
                    ?>
                    <?=$form->field($model, 'year_built[]')->dropDownList(ArrayHelper::map($arr,'k','y'),['prompt'=>'Select year of built','value'=>$d['year_built']])->label(false);?>
                    <?=$form->field($model, 'shipowning_company_name[]')->textInput(['placeholder'=>'Enter company name','value'=>$d['shipowning_company_name']])->label(false); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td>
                <?= $form->field($model, 'vessel_name[]')->textInput(['placeholder'=>'Enter vessel name'])->label(false); ?>
            </td>
            <td>
                <?= $form->field($model, 'imo_number[]')->textInput(['placeholder'=>'Enter IMO number'])->label(false); ?>
                <?php
                $countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
                $options = array();
                foreach ($countries as $key => $value) {
                    $options[$key] = ['data-style'=>'background:url(/images/sys/flags/'.Countries::find()->where(['id'=>$key])->one()->code_2.'.png) 0 center no-repeat'];
                }
                ?>
                <?= $form->field($model, 'vessel_flag_id[]', ['options'=>['class'=>'countries']])->dropDownList($countries, ['prompt'=>'Select country','options'=>$options])->label(false); ?>
            </td>
            <td>
                <?= $form->field($model, 'vessel_type_id[]')->dropDownList(ArrayHelper::map(VesselTypes::find()->all(),'id','name'), ['prompt'=>'Select type'])->label(false); ?>
                <?= $form->field($model, 'deadweight[]')->textInput(['placeholder'=>'Enter DWT'])->label(false); ?>
            </td>
            <td>
                <?= $form->field($model, 'engine_type_id[]')->dropDownList(ArrayHelper::map(Engines::find()->all(),'id','name'), ['prompt'=>'Select ME type'])->label(false); ?>
                <div class="power">
                    <?= $form->field($model, 'engine_power[]')->textInput(['placeholder'=>'Number'])->label(false); ?><?= $form->field($model, 'measure')->dropDownList(['kW'=>'kW','BHP'=>'BHP'], ['prompt'=>'Select type'])->label(false); ?>
                </div>
            </td>
            <td>
                <?php
                    $to = date('Y');
                    $range = range('1940', $to, 1);
                    foreach($range as $r){
                        $arr[]=['k'=>$r,'y'=>$r];
                    }
                ?>
                <?=$form->field($model, 'year_built[]')->dropDownList(ArrayHelper::map($arr,'k','y'),['prompt'=>'Select year of built'])->label(false);?>
                <?=$form->field($model, 'shipowning_company_name[]')->textInput(['placeholder'=>'Enter company name'])->label(false); ?>
            </td>
        </tr>
    <?php endif; ?>
</table>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save changes', ['class' => 'btn btn-primary', 'name' => 'save', 'id' => 'save-vess']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
