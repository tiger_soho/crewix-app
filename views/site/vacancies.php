<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Ranks;
use app\models\VesselTypes;
use app\models\Currency;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = 'Vacancies';
?>


	<div class="wrapper vacancies">
    <div class="filters" id="vac_filter">
		<?php Pjax::begin(['id' => 'filters']) ?>
		<?= Html::beginForm('/filter-form-vacancies', 'GET', ['id' => 'filter-form-vacancies']); ?>
    		<div class="line">
    			<label for="rank">Rank</label>
				<?=Html::dropDownList('rank', null, ArrayHelper::map(Ranks::find()->all(), 'id', 'name'),['id'=>'rank', 'class'=>'form-control','prompt'=>'Any rank']);?>
    		</div>
	    	<div class="line">
	    		<label for="sallary">Sallary in USD</label>
	    		<?=Html::textInput('salary', null, ['id'=>'sallary', 'class'=>'form-control'])?>
				<?= Html::dropDownList('period', '1', ['30' => 'Per month', '7' => 'Per week', '1' => 'Per day'],
					['id' => 'period', 'class' => 'form-control']); ?>
	    	</div>
	    	<div class="line">
	    		<label for="vessel">Vessel type</label>
				<?= Html::dropDownList('vessel', null, ArrayHelper::map(VesselTypes::find()->all(), 'id', 'name'),
					['id' => 'vessel', 'class' => 'form-control', 'prompt' => 'Any type']); ?>
	    	</div>
	    	<div class="line">
	    		<?=Html::button('Clear all',['class'=>'btn','id'=>'clear'])?>
	    	</div>
    	<?=Html::endForm();?>
		<?php Pjax::end(); ?>
    </div>
    <div class="vac_list">
    	<div class="bt">
			<h1><?= Html::encode($this->title) ?></h1>
			<?php if(!\Yii::$app->user->isGuest&&Yii::$app->user->can('comany')): ?>
				<span class="new">New vacancy</span>
			<?php endif; ?>
		</div>
		<?php Pjax::begin(['id' => 'vacancies', 'enablePushState' => false]); ?>
		<?= ListView::widget([
			'dataProvider' => $dataProvider,
			'id' => 'sran',
			'itemOptions' => ['class' => 'item'],
			'summary' => '',
			'itemView' => 'vacancies/_item',
			'pager' => [

				'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
				'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
				'maxButtonCount' => 3,

				// Customzing options for pager container tag
				'options' => [
					'tag' => 'div',
					'class' => 'pagination page-pag',
					'id' => 'pager-container',
				],

				// Customzing CSS class for pager link
				'linkOptions' => ['class' => 'admin-pag-a'],
				'activePageCssClass' => 'active',
				'disabledPageCssClass' => 'disable',

				// Customzing CSS class for navigating link
				'prevPageCssClass' => 'left',
				'nextPageCssClass' => 'right',
			],
		]) ?>
		<?php Pjax::end(); ?>
    </div>
</div>
<?php
$this->registerJs(
	'
		$(\'#vac_filter form\').on(\'change\',\'input\',function(){
			var p = $(\'#filter-form-vacancies\').serialize();
			$.pjax({
				url:\'/vacancies\',
				container: \'#vacancies\',
        		fragment: \'#sran\',
				data: p,
			});
			return false;
		});

		$(\'#vac_filter form select\').on(\'selectmenuchange\',function(){
			var p = $(\'#filter-form-vacancies\').serialize();
			$.pjax({
				url:\'/vacancies\',
				container: \'#vacancies\',
        		fragment: \'#sran\',
        		push: true,
				data: p,
			});
		});
		$(\'#clear\').on(\'click\',function(){
			$(\'#filter-form-vacancies\').trigger(\'reset\');
			var p = $(this).parent().parent().serialize();
			$.pjax({
				url:\'/vacancies\',
				container: \'#vacancies\',
        		fragment: \'#sran\',
				data: p,
				push: true,
				success:function(data){
					$(\'.vac_list\').remove();
					$(\'#vac_filter\').after(data);
				}
			});
		});

 //   });', \yii\web\View::POS_READY, 'vacancies-list-pjax');
?>