<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
?>

<div class="bt">
    <h2>Certificates of competency</h2><span class="cancel comp pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'st-com',
    'options' => ['class'=>'travel_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
    <table>
        <thead>
        <tr>
            <td>Document</td>
            <td>Number</td>
            <td>Place of issue</td>
            <td>Date of issue</td>
            <td>Date of expiry</td>
        </tr>
        </thead>

        <?php foreach($data->competency as $item): ?>
            <tr>
                <td>Rank</td>
                <td colspan="2">
                    <?=$form->field($model,'id[]')->hiddenInput(['value'=>$item['id']])->label(false)?>
                    <?=$form->field($model,'rank_id[]')->dropDownList(ArrayHelper::map(\app\models\Ranks::find()->all(),'id','name'),['options'=> [$item['rank_id']=>['Selected'=>'selected']]])->label(false)?>
                </td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td><?=$form->field($model,'document_type_id[]')->dropDownList(ArrayHelper::map(\app\models\CertificateOfCompetencyType::find()->all(),'id','name'),['options'=> [$item['document_type_id']=>['Selected'=>'selected']]])->label(false)?></td>
                <td>
                    <?=$form->field($model,'number[]')->textInput(['placeholder'=>'Enter number','value'=>$item['number']])->label(false)?>
                </td>
                <td>
                    <?=$form->field($model,'place_of_issue_id[]')->
                    dropDownList(ArrayHelper::map(Countries::find()->all(),'id','name'),['options'=>[$item['place_of_issue_id']=>['Selected'=>'selected']]])->label(false)?>
                </td>
                <td>
                    <div class="date"><?=$form->field($model,'date_of_issue[]')->textInput(['placeholder'=>'dd.mm.yyyy','value'=>Yii::$app->formatter->asDate($item['date_of_issue'],'dd.MM.y')])->label(false)?></div>
                </td>
                <td><div class="date"><?=$form->field($model,'date_of_expiry[]')->textInput(['placeholder'=>'dd.mm.yyyy','value'=>Yii::$app->formatter->asDate($item['date_of_expiry'],'dd.MM.y')])->label(false)?></div></td>
            </tr>

        <?php endforeach; ?>

    </table>
    <span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Certificate</span>
    <span class="add-endor btn btn-primary" data-id=""><i class="fa fa-plus"></i> Flag endorsement</span>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script>
    $('.date input').datepicker({
        format: 'dd.mm.yyyy',
    });
</script>


    <script type="text/template" id="new-coc-row">
        <?= $this->render('/signup/cocrow', [
            'model' => $model,
        ]); ?>
    </script>

    <script type="text/template" id="new-endor-row">
        <?= $this->render('/signup/endor', [
            'model' => $model,
        ]); ?>
    </script>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add\',function(){
    var tpl = $(\'#new-coc-row\').html();

    $(\'#st-com table tbody\').append(tpl);
    $(\'#st-com select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'coc-add');
?>

<?php
$this->registerJs('
$(\'body\').on(\'click\',\'.add-endor\',function(){
    var tpl = $(\'#new-endor-row\').html();

    $(\'#st-com table tbody\').append(tpl);
    $(\'#st-com select\').each(function(i,el){
        $(el).selectmenu({
            width: \'100%\'
        });
        $(\'.date input\').datepicker({
            format: \'dd.mm.yyyy\',
            autoclose:true
        });
    });
});
', \yii\web\View::POS_READY, 'endor-add');
?>