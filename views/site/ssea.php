<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Seaman */

use app\models\Countries;
use app\models\Ranks;
use app\models\VesselTypes;
use app\models\Competency;
use app\models\Engines;


?>

<div class="bt">
    <h2>Sea service</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit sea">Edit</span>
    <?php endif; ?>
</div>
<table class="services">
    <thead>
    <tr>
        <td>Rank</td>
        <td>Vessel name / Flag</td>
        <td>Vessel type / DWT</td>
        <td>ME type / ME power</td>
        <td>Shipowner / Crewing agency</td>
        <td>From / To</td>
    </tr>
    </thead>
    <?php foreach ($data->service as $serv): ?>
        <tr>
            <td class="f-tit"><?= Ranks::find()->where(['id' => $serv->rank_id])->one()->name ?></td>
            <td><?= $serv->vessel_name ?>
                &nbsp;/ <?= Countries::find()->where(['id' => $serv->vessel_flag_id])->one()->name ?></td>
            <td><?= VesselTypes::find()->where(['id' => $serv->vessel_type_id])->one()->name ?>
                &nbsp;/ <?= Yii::$app->formatter->asInteger($serv->vessel_deadweight) ?></td>
            <td><?= Engines::find()->where(['id' => $serv->engine_type_id])->one()->name ?>&nbsp;/
                <?php if ($serv->measure == 'kW'): ?>
                    <?= Yii::$app->formatter->asInteger($serv->engine_power) ?>
                <?php else: ?>
                    <?= Yii::$app->formatter->asInteger($serv->engine_power * 1.34) ?>
                <?php endif; ?>&nbsp;kW
            </td>
            <td>
                <?= $serv->shipowning_company_name ?>
                <?php if (!empty($serv->crewing_company_name)): ?>
                    &nbsp;- <?= $serv->crewing_company_name ?>
                <?php endif; ?>
            </td>
            <td><?= Yii::$app->formatter->asDate($serv->date_from, "php:d.m.Y") ?>
                &nbsp;/ <?= Yii::$app->formatter->asDate($serv->date_to, "php:d.m.Y") ?></td>
        </tr>
    <?php endforeach; ?>
</table>
