<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <div class="bt">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
    <div class="cont">
        <p>Please fill out your email. A link to reset password will be sent there.</p>
        <div class="row">
            <div class="col-lg-5">
                <?= $form->field($model, 'email')->textInput(['placeholder'=>'Enter your email'])->label(false); ?>
            </div>
        </div>
    </div>

    <div class="bot form-group">
        <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>


<?php if(Yii::$app->session->hasFlash('success')): ?>
    <?php $this->registerJs('
        $.notify("'.Yii::$app->session->getFlash('success').'", "success");
    ',\yii\web\View::POS_LOAD) ?>
<?php endif; ?>
<?php if(Yii::$app->session->hasFlash('error')): ?>
    <?php $this->registerJs('
        $.notify("'.Yii::$app->session->getFlash('error').'", "warn");
    ',\yii\web\View::POS_LOAD) ?>
<?php endif; ?>