<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Seaman */
/* @var $com \app\models\SeamanCertificateOfCompetency */

?>
<div class="bt">
    <h2>Notes</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit notes pull-right">Edit</span>
    <?php endif; ?>
</div>
<?php if (!empty($data->notes)): ?>
    <div class="text">
        <?= $data->notes ?>
    </div>
<?php else: ?>
    <div class="empty text-center">
        <p>You can add notes for your employers here.</p>
        <span class="edit notes">Add notes &raquo;</span>
    </div>
<?php endif; ?>
</div>