<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanTextForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;

?>

    <div class="bt">
        <h2>CV</h2>
        <span class="cancel cv pull-right">Cancel</span>
    </div>
<?php $form = ActiveForm::begin([
    'id' => 'scv',
    'options' => ['class' => 'cv_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
    <div class="col-lg-12 col-md-12 col-xs-12">
        <?php $model->cv = $data->cv ?>
        <?= $form->field($model, 'cv')->widget(Widget::className(), [
            'settings' => [
                'minHeight' => 200,
                'buttons' => ['format', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'link'],
            ]
        ])->label(false); ?>
    </div>
    <div class="bot">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'cv-button', 'id' => 'f_cv']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>