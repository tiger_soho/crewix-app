<?php
use yii\web\View;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    $current = date('Y');
    $from = $current-70;
    $to = $current+20;
    $range = range($from, $to, 1);
    foreach($range as $r){
        $arr[]=['k'=>$r,'y'=>$r];
    }
?>

<div class="bt">
    <h2>Education</h2>
    <span class="cancel edu pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'sedu',
    'options' => ['class'=>'edu_form'],
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>
    <table>
        <thead>
        <tr>
            <td>Institution</td>
            <td>Degree</td>
            <td>From</td>
            <td>To</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data->education as $item): ?>
            <?php $model->year_from = $item['year_from']; ?>
            <tr>
                <td>
                    <?= $form->field($model,'id[]')->hiddenInput(['value'=>$item['id']])->label(false); ?>
                    <?=$form->field($model,'institution_name[]')->textInput(['placeholder'=>'Enter college / university name','value'=>$item['institution_name']])->label(false);?>
                </td>
                <td>
                    <?=$form->field($model,'degree[]')->textInput(['placeholder'=>'Enter your degree','value'=>$item['degree']])->label(false);?>
                </td>
                <td>
                    <?= $form->field($model, 'year_from[]')->dropDownList(\yii\helpers\ArrayHelper::map($arr,'k','y'), ['options'=> [$item['year_from']=>['Selected'=>'selected']]],['placeholder'=>'Select year'])->label(false);?>
                </td>
                <td>
                    <?= $form->field($model, 'year_to[]',
                        ['options' => ['enableAjaxValidation' => true]])->dropDownList(\yii\helpers\ArrayHelper::map($arr,
                        'k', 'y'), ['options' => [$item['year_to'] => ['Selected' => 'selected']]],
                        ['placeholder' => 'Select year'])->label(false); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <span class="add institute btn btn-primary" data-id=""><i class="fa fa-plus"></i> Education</span>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

    <script type="text/template" id="new-education-row">
        <?= $this->render('/signup/rowed', [
            'model' => $model,
        ]); ?>
    </script>

<?php
$this->registerJs('

', View::POS_READY, 'asdasasdasdsad');
?>