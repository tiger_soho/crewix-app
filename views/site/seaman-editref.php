<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SeamanReference */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;

$current = date('Y');
$from = $current-70;
$to = $current+20;
$range = range($from, $to, 1);
foreach($range as $r){
    $arr[]=['k'=>$r,'y'=>$r];
}
?>

<div class="bt">
    <h2>References</h2>
    <span class="cancel reference pull-right">Cancel</span>
</div>
<?php $form = ActiveForm::begin(['id' => 'st-ref',
    'options' => ['class'=>'ref_form'],
    'validateOnBlur' => false,
]); ?>
<table>
    <thead>
        <tr>
            <td>Company</td>
            <td>Phone number</td>
            <td>Email</td>
            <td>Contact person</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data->references as $ref): ?>
            <tr>
                <td>
                    <?=$form->field($model,'id[]')->hiddenInput(['value'=>$ref['id']])->label(false)?>
                    <?=$form->field($model,'company_name[]')->textInput(['placeholder'=>'Enter company name', 'value'=>$ref['company_name']])->label(false);?>
                </td>
                <td>
                    <?=$form->field($model,'phone[]', ['options'=>['class'=>'phone']])->textInput(['placeholder'=>'+1 555 555 5555', 'value'=>$ref['phone']])->label(false);?>
                </td>
                <td>
                    <?=$form->field($model,'email[]')->textInput(['placeholder'=>'Enter email', 'value'=>$ref['email']])->label(false);?>
                </td>
                <td>
                    <?=$form->field($model,'contact_person[]')->textInput(['placeholder'=>'Enter full name', 'value'=>$ref['contact_person']])->label(false);?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<span class="add btn btn-primary" data-id=""><i class="fa fa-plus"></i> Reference</span>
<div class="bot">
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 'f_sign']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script type="text/template" id="new-ref-row">
    <?= $this->render('/signup/rowref', [
        'model' => $model,
    ]); ?>
</script>

<?php
$this->registerJs('
    $(\'body\').on(\'click\',\'.add\',function(){
        var tpl = $(\'#new-ref-row\').html();

        $(\'#st-ref table tbody\').append(tpl);
        $(\'#st-ref select\').each(function(i,el){
            $(el).selectmenu({
                width: \'100%\'
            });
            $(\'.date input\').datepicker({
                format: \'dd.mm.yyyy\',
                autoclose:true
            });
            $(\'.phone input\').intlTelInput();
        });
    });
    ', \yii\web\View::POS_READY, 'ref-add');
?>