<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Vacancies */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\Ranks;
use app\models\VesselTypes;
use app\models\Engines;
use app\models\CompanyVessel;
use app\models\Currency;
use app\models\Companies;
use kartik\file\FileInput;
use dosamigos\datepicker\DatePicker;
use app\models\LanguageLevel;

$this->title = 'New vacancy';
$to = date('Y');
$range = range('1940', $to, 1);
foreach($range as $r){
    $arr[]=['k'=>$r,'y'=>$r];
}
$countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
$options = array();
foreach ($countries as $key => $value) {
    $options[$key] = ['data-style'=>'background:url(/images/sys/flags/'.Countries::find()->where(['id'=>$key])->one()->code_2.'.png) 0 center no-repeat'];
}
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seamansign">
    <div class="row">
        <div class="vac-block">
            <div class="form-tit">
                <h2>New vacancy<span class="clear">Clear all</span></h2>

            </div>
            <div class="bt">
                <h2>Main information</h2>
            </div>
                <?php $form = ActiveForm::begin(['id' => 'vacancy',
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                ]); ?>
            <div class="col-lg-12">
                <table>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Rank</td>
                        <td class="vac-position">
                            <?= $form->field($model, 'rank_id', ['options'=>['class'=>'half']])->dropDownList(ArrayHelper::map(Ranks::find()->all(), 'id', 'name'), ['prompt'=>'Select rank'])->label(false);?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Salary</td>
                        <td colspan="3">
                            <div class="col-lg-6 vac-no-padding">
                                <?= $form->field($model, 'salary_from')->textInput(['placeholder'=>'From'])->label(false); ?><!--
                             --><?= $form->field($model, 'salary_to')->textInput(['placeholder'=>'To'])->label(false); ?><!--
                             --><?= $form->field($model, 'salary_currency_id')->dropDownList(ArrayHelper::map(Currency::find()->all(), 'id', 'code'))->label(false); ?><!--
                             --><?= $form->field($model, 'payroll_period')->dropDownList(['1'=>'Per day','7'=>'Per week','30'=>'Per month'])->label(false); ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($model, 'negotiable')->checkbox(['value' => '1'])->label('Negotiable'); ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Duration of contract</td>
                        <td>
                            <div class="duration">
                                <?= $form->field($model, 'duration_of_contract', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'0'])->label(false); ?><!--
                             --><div class="plus-minus">+<br>-</div><!--
                             --><?= $form->field($model, 'duration_of_contract_offset', ['options'=>['class'=>'half']])->textInput(['placeholder'=>'0'])->label(false); ?>
                            </div>
                            <?= $form->field($model, 'dur_period')->dropDownList(['1'=>'Days','7'=>'Weeks','30'=>'Month'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit "><span class="req">*</span> Joining date</td>
                        <td class="vac-join-padding">
                            <?= $form->field($model, 'joining_date_from', ['options'=>['class'=>'date half']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                            <?= $form->field($model, 'joining_date_to', ['options'=>['class'=>'date half']])->textInput(['placeholder'=>'dd.mm.yyyy'])->label(false) ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="bt">
                <h2>Vessel</h2>
            </div>
            <div class="col-lg-12">
                <table class="vessel">
                    <tr>
                        <td class="f-tit v-fit"><span class="req">*</span> Vessel</td>
                        <td>
                            <?= $form->field($model, 'company_vessel_id', ['options'=>['class'=>'half']])->dropDownList(ArrayHelper::map(CompanyVessel::find()->where(['company_id'=>Companies::find()->where(['user_id'=>Yii::$app->user->id])->one()->id])->all(),'id','vessel_name'), ['prompt'=>'New vessel'])->label(false);?>
                            <div class="save"><?= $form->field($model, 'save_vessel')->checkbox(['value' => '1'])->label('Save to my vessels') ?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Name</td>
                        <td>
                            <?= $form->field($model, 'vessel_name')->textInput(['placeholder'=>'Enter vessel name'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Type</td>
                        <td>
                            <?= $form->field($model, 'vessel_type_id')->dropDownList(ArrayHelper::map(VesselTypes::find()->all(), 'id','name'), ['prompt'=>'Select vessel type'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> Flag</td>
                        <td>
                            <?= $form->field($model, 'vessel_flag_id', ['options'=>['class'=>'countries']])->dropDownList($countries, ['prompt'=>'Select vessel flag','options'=>$options])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit"><span class="req">*</span> DWT</td>
                        <td>
                            <?=$form->field($model,'vessel_deadweight')->textInput(['placeholder'=>'Enter vessel DWT'])->label(false);?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Year of built</td>
                        <td>
                            <?=$form->field($model, 'vessel_year_built')->dropDownList(ArrayHelper::map($arr,'k','y'),['prompt'=>'Select year of built'])->label(false);?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">ME type / ME power</td>
                        <td class="vac-me">
                            <?=$form->field($model, 'vessel_engine_type_id')->dropDownList(ArrayHelper::map(Engines::find()->all(), 'id', 'name'),['prompt'=>'Select ME type'])->label(false);?>
                            <div class="power">
                                <?=$form->field($model, 'vessel_engine_power')->textInput(['placeholder'=>'Number'])->label(false);?>
                                <?=$form->field($model, 'power_mesure')->dropDownList(['kW'=>'kW','BHP'=>'BHP'])->label(false);?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Shipowner</td>
                        <td>
                            <?=$form->field($model,'vessel_shipowning_company_name')->textInput(['placeholder'=>'Enter company name'])->label(false);?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="bt">
                <h2>Vessel</h2>
            </div>
            <div class="col-lg-12">
                <table>
                    <tr>
                        <td class="f-tit v-fit" >Age</td>
                        <td>
                            <?=$form->field($model,'required_age_from')->textInput(['placeholder'=>'From'])->label(false);?>
                            <?=$form->field($model,'required_age_to')->textInput(['placeholder'=>'To'])->label(false);?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Citizenship</td>
                        <td class="vac-citi">
                        	<?=$form->field($model, 'required_citizenship', ['options'=>['class'=>'citizenship']])->label(false)?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">English level</td>
                        <td>
                            <?= $form->field($model,'required_language')->dropDownList(ArrayHelper::map(LanguageLevel::find()->all(),'id','name'),['prompt'=>'Any level'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Document scans</td>
                        <td>
                            <?= $form->field($model,'document_scans_required')->checkbox(['value'=>'1'])->label('Required');; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">US visa</td>
                        <td>
                            <?= $form->field($model,'us_visa_required')->checkbox(['value'=>'1'])->label('Required');; ?>
                            <?= $form->field($model,'us_visa_required_type_id')->dropDownList(['1'=>'Type1','2'=>'Type2'])->label(false); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="f-tit">Schengen visa</td>
                        <td>
                            <?= $form->field($model,'schengen_visa_required')->checkbox(['value'=>'1'])->label('Required');; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="bt">
                <h2>Notes</h2>
            </div>
            <div class="col-lg-12 vac-text-area">
                <?= $form->field($model,'notes')->textArea()->label(false); ?>
            </div>
            <div class="bt">
                <h2>Notes</h2>
            </div>
            <div class="col-lg-12 summary">
                <p>Your vacancy will be active for <strong>2 weeks</strong>.</p>
                <p>You have <strong>no free vacancies</strong> until <strong>22.04.2015</strong>. We’re sorry but you’ll have to wait <strong>13 days</strong>.</p>
            </div>
            <div class="bot">
                <div class="form-group">
                    <?= Html::submitButton('Save & continue', ['class' => 'btn btn-primary', 'name' => 'signup-button', 'id' => 's_sign']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    

</div><!-- seamansign -->


<?php
	$arr = [];
	$countries = ArrayHelper::map(Countries::find()->all(),'id','name');
	foreach ($countries as $key => $value) {
		$arr[] = ['value'=>$key,'text'=>$value];
	}
	$countries = json_encode($arr);
?>


<script>
	var countries = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: <?=$countries?>
	});
	countries.initialize();

	var elt = $('.citizenship input');
	elt.tagsinput({
		splitOn:',',
		itemValue: 'value',
		itemText: 'text',
		typeaheadjs: {
			name: 'countries',
			displayKey: 'text',
			source: countries.ttAdapter()
		}
	});
</script>
<?php
$this->registerJs('
        $(\'#vacancies-company_vessel_id\').on(\'selectmenuchange\',function(){
			var p = $(this).val();
			if(p){
                $.ajax({
                    url:\'/loadvessel\',
                    data:{id:p},
                    type:\'GET\',
                    success:function(data){
                        $.each(data, function(key,val){
                            $(\'#vacancies-\'+key).val(val);
                        });
                        $(\'table.vessel select\').each(function(i,el){
                            if(!$(el).parent().hasClass(\'countries\')){
                                $(el).selectmenu();
                                $(el).selectmenu(\'refresh\', true);
                            }else{
                                $(\'#vacancies-vessel_flag_id\').iconselectmenu({width: \'100%\'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
                                $(\'#vacancies-vessel_flag_id\').iconselectmenu(\'refresh\', true);
                            }
                        });
                    }
                });
            }else{
                $(\'table.vessel input,table.vessel select\').val(\'\');
                $(\'table.vessel select\').each(function(i,el){
                    if(!$(el).parent().hasClass(\'countries\')){
                        $(el).selectmenu();
                        $(el).selectmenu(\'refresh\', true);
                    }else{
                        $(\'#vacancies-vessel_flag_id\').iconselectmenu({width: \'100%\'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
                        $(\'#vacancies-vessel_flag_id\').iconselectmenu(\'refresh\', true);
                    }
                });
            }
		});
    ', 4, 'vessel_load')
?>