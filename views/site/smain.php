<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\i18n\Formatter;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Vacancies;
use app\models\Countries;
use app\models\Ranks;
use app\models\Docs;
use app\models\Langs;
use app\models\Languages;
use app\models\Education;
use app\models\TravelDocs;
use app\models\VesselTypes;
use app\models\Competency;
use app\models\Proficiency;
use app\models\ProficiencyDocs;
use app\models\Service;
use app\models\Engines;
use app\models\References;



?>

<div class="bt">
	<h2>Main information</h2>
	<?php if(!Yii::$app->user->isGuest && \app\models\User::allowForCurrentUser($data->user_id)): ?>
		<span class="edit scont">Edit</span>
	<?php endif; ?>
</div>
<table>
	<tr class="first">
		<td>
			<?php if (!empty($data->cv_picture)): ?>
				<img src="/images/seaman/docs/<?= $data->cv_picture ?>" alt="">
			<?php else: ?>
				<img src="/images/seaman/nocv.jpg" alt="">
			<?php endif; ?>
		</td>
		<td colspan="2">
			<h2><?=$data->first_name?><?php if(!empty($data->middle_name)):?> <?=$data->middle_name?><?php endif ?> <?=$data->last_name?></h2>
			<div class="rank"><?= Ranks::find()->where(['id' => $data->primary_rank_id])->one()->name ?>
				<?php if (!empty($data->secondary_rank_id)): ?>
					/ <?= Ranks::find()->where(['id' => $data->secondary_rank_id])->one()->name ?>
				<?php endif; ?>
			</div>
			<a href="<?= Yii::$app->urlManager->createAbsoluteUrl([
				'site/profile',
				'id' => \app\models\Profile::findOne(['user_id'=>$data->user_id])->id
			]) ?>" class="link"><?= Yii::$app->urlManager->createAbsoluteUrl([
					'site/profile',
					'id' => \app\models\Profile::findOne(['user_id'=>$data->user_id])->id
				]) ?></a>
		</td>
		<td></td>
	</tr>
	<tr>
		<td class="f-tit">Rank</td>
		<td><?=Ranks::find()->where(['id'=>$data->primary_rank_id])->one()->name?></td>
		<td class="f-tit">Availability</td>
		<td>
			<?php if($data->status==0): ?>
				Available from
				<?php if($data->availability_date!='0000-00-00'): ?>
					<?=Yii::$app->formatter->asDate($data->availability_date,'dd.MM.yyyy')?>
				<?php endif; ?>
			<?php else: ?>
				Not available
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td class="f-tit">Desired salary per month</td>
		<td><?= \app\models\Currency::findOne(['id' => $data->salary_currency_id])->symbol ?>
			&nbsp;<?= Yii::$app->formatter->asInteger($data->salary) ?></td>
		<td class="f-tit">Date and place of birth</td>
		<td>
			<?php if(\app\models\User::allowForCurrentUser($data->user_id)): ?>
				<?=Yii::$app->formatter->asDate($data->date_of_birth, "php:d.m.Y")?>
			<?php else: ?>
				<?=Yii::$app->formatter->asDate($data->date_of_birth, "php:Y")?>
			<?php endif; ?>
			&nbsp;/&nbsp;<?=Countries::find()->where(['id'=>$data->place_of_birth_id])->one()->name?>
		</td>
	</tr>
	<tr>
		<td class="f-tit">City and country of residence</td>
		<td><?=$data->city_of_residence?>, <?=Countries::find()->where(['id'=>$data->country_of_residence_id])->one()->name?></td>
		<td class="f-tit">Home address</td>
		<td>
			<?php if(\app\models\User::allowForCurrentUser($data->user_id)||Yii::$app->user->can('admin')): ?>
				<?=$data->address?>
			<?php else: ?>
				*****
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td class="f-tit">Citizenship</td>
		<td><?=Countries::find()->where(['id'=>$data->citizenship_id])->one()->name?></td>
		<td class="f-tit">Closest airport</td>
		<td><?=$data->closest_airport?></td>
	</tr>
	<?php if((\app\models\User::allowForCurrentUser($data->user_id) || Yii::$app->user->can('admin'))): ?>
		<tr>
			<td class="f-tit">Primary phone</td>
			<td><?=$data->primary_phone?></td>
			<td class="f-tit">Additional phone</td>
			<td><?=$data->secondary_phone?></td>
		</tr>
		<tr>
			<td class="f-tit">Email</td>
			<td><?=Users::find()->where(['id'=>$data->user_id])->asArray()->one()['email']?></td>
			<td class="f-tit">Skype</td>
			<td><?=$data->skype?></td>
		</tr>
	<?php endif; ?>
</table>