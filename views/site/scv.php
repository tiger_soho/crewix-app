<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Seaman */
/* @var $com \app\models\SeamanCertificateOfCompetency */

?>

<div class="bt">
    <h2>CV</h2>
    <?php if (\app\models\User::allowForCurrentUser($data->user_id)): ?>
        <span class="edit cv pull-right">Edit</span>
    <?php endif; ?>
</div>
<?php if (!empty($data->cv)): ?>
    <div class="text">
        <?= $data->cv ?>
    </div>
<?php else: ?>
    <div class="empty text-center">
        <p>Offshore CVs are not the same as onshore ones. To solve this, we made this section. You can add your offshore
            CV here.</p>
        <span class="edit cv">Add CV &raquo;</span>
    </div>
<?php endif; ?>
