<?php
/* @var $this yii\web\View */
use app\models\Countries;
use app\models\Ranks;
use yii\helpers\Html;
$id = \app\models\Seaman::findOne(['user_id'=>Yii::$app->user->id])->id;
$all_tr = \app\models\SeamanTravelDocument::find()->where(['seaman_id'=>$id])->all();
$all_coc = \app\models\SeamanCertificateOfCompetency::find()->where(['seaman_id'=>$id])->all();
?>
<div class="item">
		<div class="img">
			<div class="images">
				<svg class="loader" viewBox="0 0 100 100">
					<circle class="one" r="40" cx="50" cy="50" />
					<circle class="two" r="40" cx="50" cy="50" />
				</svg>
			</div>
		</div>
		<div class="info">
			<div class="doc-type">
				<select name="UserFile[doc_id][]" id="">
					<?php foreach ($all_tr as $tr): ?>
    <option
        value="1-<?= $tr->id ?>"><?= \app\models\TravelDocument::find()->where(['id' => $tr->document_id])->one()->name ?>
        (<?= Countries::find()->where(['id' => $tr->place_of_issue_id])->one()->name ?>)
    </option>
<?php endforeach; ?>
<?php foreach ($all_coc as $coc): ?>
    <option
        value="2-<?= $coc->id ?>"><?= \app\models\CertificateOfCompetencyType::find()->where(['id' => $coc->document_type_id])->one()->name ?>
        : <?= Ranks::find()->select('name')->where(['id' => $coc->rank_id])->one()->name ?>
        (<?= Countries::find()->where(['id' => $coc->place_of_issue_id])->one()->name ?>)
    </option>
<?php endforeach; ?>
</select>
</div>
<div class="file-desc">
    <div class="js-fileapi-wrapper upload-btn btn btn-primary">
        <div class="upload-btn__txt">Select file</div>
        <?=Html::activeFileInput($model,'src[]',['accept'=>'image/jpg','class'=>'scan'])?>
        <div class="file-info">
            <?=Html::activeHiddenInput($model,'size[]')?>
            <?=Html::activeHiddenInput($model,'id[]')?>
        </div>

    </div>
    <span class="btn remove">Remove</span>
</div>
<div class="size"></div>
</div>
</div>

<?php $this->registerJs('
    $(\'.scan\').on(\'change\', function(evt) {
			var f = evt.target.files[0];

			var reader = new FileReader();
			reader.onload = (function(theFile) {
				return function(e){
					if(theFile.size > 2000000){
						alert(\'Images size more then 2MB\');
						return false;
					}

				};

			});
			var i = $(this);

			FileAPI.upload({
				url: \'/docupload\',
				files: { src: f },
				data: { \'_csrf\': \''.Yii::$app->request->getCsrfToken().'\' },
				imageTransform: {
					width: 350,
					height: 450,
					preview: true
				},
				progress: function (evt){
					var a = (evt.loaded/evt.total)*360;
					i.parents(\'.item\').find(\'.images\').find(\'svg\').find(\'.two\').css({\'stroke-dasharray\': \'\'+a+\' 300\'});
				},
				complete: function (err, xhr) {
					console.log(xhr.responseText);
				},
				filecomplete: function (err, xhr, file){
                    console.log(i.parents(\'.item\'));
					FileAPI.Image(f).preview(100,100).get(function (err, img){
					setTimeout(function(){
						i.parents(\'.item\').find(\'.images\').html(img);
					},300);

						var fileinfo = \'{"name":"\' + xhr.responseText + \'","size":"\' + file.size + \'"}\';
                    i.parents(\'.item\').find(\'.file-info\').find(\'input\').val(fileinfo);

			});
				}
			});
			reader.readAsDataURL;
		});
') ?>