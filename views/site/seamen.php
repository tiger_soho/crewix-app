<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;
use app\models\Ranks;
use app\models\Profile;
use app\models\Countries;
use app\models\vesselTypes;
use app\models\SeamanLanguage;
use app\models\Currency;
use app\models\LanguageLevel;
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = 'Seamen';
?>
<div class="wrapper seamen">
	<div class="filters" id="seamen_filter">
		<?php if(Yii::$app->user->can('seaman')): ?>
			<?=Html::beginForm();?>
			<div class="line">
				<label for="rank">Rank</label>
				<?=Html::dropDownList('rank', null, ArrayHelper::map(Ranks::find()->all(), 'id', 'name'),['id'=>'rank', 'class'=>'form-control','prompt'=>'Any rank']);?>
			</div>
			<?=Html::endForm();?>
		<?php elseif (Yii::$app->user->can('partner')): ?>
			<?=Html::beginForm();?>
			<div class="info">
				<div class="icon">
					<img src="/images/sys/magnifyingglass.svg" alt="search" />
				</div>
				<p>Find skilled seamen instantly with our search. Hire the ones you like with the press of a button.</p>
				<p class="grey">Advanced search is available only to our <a href="#">Partners</a> at the moment.</p>
			</div>
			<div class="line">
				<label for="rank">Rank</label>
				<?=Html::dropDownList('rank', null, ArrayHelper::map(Ranks::find()->all(), 'id', 'name'),['id'=>'rank', 'class'=>'form-control','prompt'=>'Any rank']);?>
			</div>
			<div class="line">
				<label for="cityzenship">Cityzenship</label>
				<?=Html::dropDownList('cityzenship', null, ArrayHelper::map(Countries::find()->all(), 'id', 'name'),['id'=>'cityzenship', 'class'=>'form-control','prompt'=>'Any cityzenship']);?>
			</div>
			<div class="line">
				<label for="avail">Availability</label>
				<?=Html::dropDownList('avail', null, ['1' => 'Available','0' => 'Not available'],['id'=>'avail', 'avail'=>'form-control','prompt'=>'Any availability']);?>
			</div>
			<div class="line">
				<label>Age</label>
				<?=Html::textInput('age_from', null, ['id'=>'age_from', 'class'=>'form-control half','placeholder'=>'From any age']);?>
				<?=Html::textInput('age_to', null, ['id'=>'age_to', 'class'=>'form-control half','placeholder'=>'To any age']);?>
			</div>
			<div class="line">
				<label>Salary in USD</label>
				<?=Html::textInput('sal', null, ['id'=>'sal', 'class'=>'form-control half','placeholder'=>'Any age']);?>
				<?=Html::dropDownList('period', '1', ['30'=>'Per month','7'=>'Per week', '1'=>'Per day'],['id'=>'period', 'class'=>'form-control half']);?>
			</div>
			<div class="line">
				<label for="vessel">Experienced on</label>
				<?=Html::dropDownList('vessel', null, ArrayHelper::map(app\models\VesselTypes::find()->all(), 'id', 'name'),['id'=>'vessel', 'class'=>'form-control','prompt'=>'Any vessel']);?>
			</div>
			<div class="line">
				<label for="lang">English level</label>
				<?=Html::dropDownList('lang', null, ArrayHelper::map(LanguageLevel::find()->all(), 'id', 'name'),['id'=>'lang', 'class'=>'form-control','prompt'=>'Any vessel']);?>
			</div>
			<div class="line">
				<label for="offshore">DP for offshore</label>
				<?=Html::dropDownList('offshore', null, [],['id'=>'offshore', 'class'=>'form-control','prompt'=>'Doesn’t matter']);?>
			</div>
			<div class="line disabled">
				<label for="">Miscellaneous</label>
				<?=Html::checkBox('us_visa', false, ['label'=>'Has US visa']);?>
				<?=Html::checkBox('schengen', false, ['label'=>'Has Schengen visa']);?>
				<?=Html::checkBox('scans', false, ['label'=>'Has document scans']);?>
			</div>
			<?=Html::endForm();?>
		<?php elseif (Yii::$app->user->isGuest || Yii::$app->user->can('company')): ?>
			<?=Html::beginForm();?>
			<div class="info">
				<div class="icon">
					<img src="/images/sys/magnifyingglass.svg" alt="search" />
				</div>
				<p>Find skilled seamen instantly with our search. Hire the ones you like with the press of a button.</p>
				<p class="grey">Advanced search is available only to our <a href="#">Partners</a> at the moment.</p>
			</div>
			<div class="line">
				<label for="rank">Rank</label>
				<?=Html::dropDownList('rank', null, ArrayHelper::map(Ranks::find()->all(), 'id', 'name'),['id'=>'rank', 'class'=>'form-control','prompt'=>'Any rank']);?>
			</div>
			<div class="line">
				<label for="cityzenship">Cityzenship</label>
				<?=Html::dropDownList('cityzenship', null, [],['id'=>'cityzenship', 'class'=>'form-control','prompt'=>'Any cityzenship', 'disabled'=>'disabled']);?>
			</div>
			<div class="line">
				<label for="avail">Availability</label>
				<?=Html::dropDownList('avail', null, [],['id'=>'avail', 'avail'=>'form-control','prompt'=>'Any availability', 'disabled'=>'disabled']);?>
			</div>
			<div class="line">
				<label>Age</label>
				<?=Html::textInput('age_from', null, ['id'=>'age_from', 'class'=>'form-control half','placeholder'=>'From any age', 'disabled'=>'disabled']);?>
				<?=Html::textInput('age_to', null, ['id'=>'age_to', 'class'=>'form-control half','placeholder'=>'To any age', 'disabled'=>'disabled']);?>
			</div>
			<div class="line">
				<label>Salary in USD</label>
				<?=Html::textInput('sal', null, ['id'=>'sal', 'class'=>'form-control half','placeholder'=>'Any salary', 'disabled'=>'disabled']);?>
				<?=Html::dropDownList('period', '1', ['30'=>'Per month','7'=>'Per week', 'Per day'=>'1'],['id'=>'period', 'class'=>'form-control half', 'disabled'=>'disabled']);?>
			</div>
			<div class="line">
				<label for="vessel">Experienced on</label>
				<?=Html::dropDownList('vessel', null, [],['id'=>'vessel', 'class'=>'form-control','prompt'=>'Any vessel', 'disabled'=>'disabled']);?>
			</div>
			<div class="line">
				<label for="lang">English level</label>
				<?=Html::dropDownList('lang', null, [],['id'=>'lang', 'class'=>'form-control','prompt'=>'Any vessel', 'disabled'=>'disabled']);?>
			</div>
			<div class="line">
				<label for="offshore">DP for offshore</label>
				<?=Html::dropDownList('offshore', null, [],['id'=>'offshore', 'class'=>'form-control','prompt'=>'Doesn’t matter', 'disabled'=>'disabled']);?>
			</div>
			<div class="line disabled">
				<label for="">Miscellaneous</label>
				<?=Html::checkBox('us_visa', false, ['disabled'=>'disabled','label'=>'Has US visa']);?>
				<?=Html::checkBox('schengen', false, ['disabled'=>'disabled','label'=>'Has Schengen visa']);?>
				<?=Html::checkBox('scans', false, ['disabled'=>'disabled','label'=>'Has document scans']);?>
			</div>
			<?=Html::endForm();?>
		<?php endif; ?>
	</div>
    <div class="vac_list">
    	<div class="bt">
			<h1><?= Html::encode($this->title) ?></h1>
		</div>
		<?php Pjax::begin(['id' => 'seamen', 'enablePushState' => true]); ?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'seamen-list',
            'itemOptions' => ['class' => 'item'],
            'summary' => '',
            'itemView' => 'seamen/_item',
			'pager' => [

				'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
				'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
				'maxButtonCount' => 3,

				// Customzing options for pager container tag
				'options' => [
					'tag' => 'div',
					'class' => 'pagination page-pag',
					'id' => 'pager-container',
				],

				// Customzing CSS class for pager link
				'linkOptions' => ['class' => 'admin-pag-a'],
				'activePageCssClass' => 'active',
				'disabledPageCssClass' => 'disable',

				// Customzing CSS class for navigating link
				'prevPageCssClass' => 'left',
				'nextPageCssClass' => 'right',
			],
        ]) ?>
        <?php Pjax::end(); ?>
    </div>
</div>
<?php
$this->registerJs(
    '
		$(\'#seamen_filter form\').on(\'change\',\'input\',function(){
		    console.log(\'adfsd\');
			var p = $(\'#seamen_filter form\').serialize();
			$.pjax({
				url:\'/seamen\',
				container: \'#seamen\',
        		fragment: \'#seamen-list\',
				data: p,
			}).done(function() {
				$(\'.bdate\').each(function(i,el){
                    var d = $(el).find(\'span\').text();
                    var age = moment().diff(d,\'years\');
                    $(el).find(\'span\').text(age+\' years\')
                });
			});
			return false;
		});

		$(\'#seamen_filter form select\').on(\'selectmenuchange\',function(){
			var p = $(\'#seamen_filter form\').serialize();
			$.pjax({
				url:\'/seamen\',
				container: \'#seamen\',
        		fragment: \'#seamen-list\',

				data: p,
			}).done(function() {
				$(\'.bdate\').each(function(i,el){
                    var d = $(el).find(\'span\').text();
                    var age = moment().diff(d,\'years\');
                    $(el).find(\'span\').text(age+\' years\')
                });
			});
		});
		$(\'#clear\').on(\'click\',function(){
			$(\'#seamen_filter form\').trigger(\'reset\');
			var p = $(\'#seamen_filter form\').serialize();
			$.pjax({
				url:\'/seamen\',
				container: \'#seamen\',
        		fragment: \'#seamen-list\',
				data: p,

			}).done(function() {
				$(\'.bdate\').each(function(i,el){
                    var d = $(el).find(\'span\').text();
                    var age = moment().diff(d,\'years\');
                    $(el).find(\'span\').text(age+\' years\')
                });
			});
		//});

    });', \yii\web\View::POS_READY, 'vacancies-list-pjax');
?>