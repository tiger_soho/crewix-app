<?php
namespace app\commands;
 
use Yii;
use yii\console\Controller;
use app\components\rbac\GroupRule;
use yii\rbac\DbManager;
 
/**
 * RBAC console controller.
 */
class RbacController extends Controller
{
    /**
     * Initial RBAC action
     * @param integer $id Superadmin ID
     */
    public function actionInit($id = null)
    {
        $auth = new DbManager;
        $auth->init();
 
        $auth->removeAll(); //удаляем старые данные
        // Rules

 
        // Roles
        $user = $auth->createRole('admin');
        $user->description = 'Administrator';
        $auth->add($user);
 
        $moderator = $auth->createRole('seaman');
        $moderator ->description = 'Seaman';
        $auth->add($moderator);
 
        $admin = $auth->createRole('company');
        $admin->description = 'Company';
        $auth->add($admin);
 
 
        // Superadmin assignments

    }
}