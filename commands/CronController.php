<?php
namespace app\commands;

use app\models\User;
use app\models\Vacancies;
use yii\console\Controller;
use app\models\Companies;
use app\models\Users;

class CronController extends Controller
{

    public function actionVacanciesReset()
    {
        $model = new Companies();

        $companies = $model->find()->joinWith('user')->all();

        foreach ($companies as $item) {
            $reg_date = explode('-', date('m-d', $item->user->signup_date));
            $now = explode('-', date('m-d'));

            if ($reg_date[1] == $now[1] && $reg_date[0] < $now[0]) {
                /* @var $update Companies */
                $update = Companies::findOne($item->id);
                $update->posted_vacancies = 0;
                $update->save(false);
            }
        }
    }

    public function actionInactiveDelete()
    {
        $model = new Users;
        $users = $model->findAll(['verified' => 0]);
        foreach ($users as $user) {
            /* @var $user Users */
            $registered = strtotime($user->signup_date);
            if ($registered < (time() - 7 * 24 * 60 * 60)) {
                $del = Users::findOne($user->id);
                $del->delete();
            }
        }
    }

    public function actionVacanciesExpired()
    {
        /* @var $item Vacancies */
        $model = new Vacancies();
        $vacancies = $model->find()->where(['status' => 1])->all();
        $now = time();
        foreach ($vacancies as $item) {
            if (strtotime($item->created_at) >= ($now + 14 * 24 * 60 * 60)) {
                $update = Vacancies::findOne($item->id);
                $update->status = 0;
                $update->save(false);
            }
        }
    }

    public function actionRemoveFiles()
    {

    }

    public function actionDocExpires()
    {

    }
}

?>