/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='/images/sys/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "horizontal",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'sun',
                            type: 'image',
                            rect: ['381px', '311px', '238px', '238px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"sun.svg",'0px','0px']
                        },
                        {
                            id: 'cld1',
                            symbolName: 'cld1',
                            type: 'rect',
                            rect: ['63px', '245', '403', '332', 'auto', 'auto'],
                            opacity: 0.5
                        },
                        {
                            id: 'cld2',
                            symbolName: 'cld2',
                            type: 'rect',
                            rect: ['482px', '215', '476', '392', 'auto', 'auto'],
                            opacity: 0.5
                        },
                        {
                            id: 'city',
                            type: 'image',
                            rect: ['-500px', '170px', '2000px', '312px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"city.png",'0px','0px']
                        },
                        {
                            id: 'sea2',
                            symbolName: 'sea2',
                            type: 'rect',
                            rect: ['-500', '430', '1999', '58', 'auto', 'auto']
                        },
                        {
                            id: 'ship2',
                            symbolName: 'ship',
                            type: 'rect',
                            rect: ['38', '348', '307', '110', 'auto', 'auto']
                        },
                        {
                            id: 'sea1',
                            symbolName: 'sea1',
                            type: 'rect',
                            rect: ['-500', '450px', '2000', '58', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1000px', '500px', 'auto', 'auto'],
                            overflow: 'visible',
                            fill: ["rgba(255,255,255,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 240000,
                    autoPlay: true,
                    data: [
                        [
                            "eid78",
                            "top",
                            0,
                            3000,
                            "easeOutQuad",
                            "${sun}",
                            '311px',
                            '139px'
                        ],
                        [
                            "eid87",
                            "left",
                            0,
                            3500,
                            "easeOutQuad",
                            "${cld2}",
                            '482px',
                            '412px'
                        ],
                        [
                            "eid89",
                            "opacity",
                            0,
                            3000,
                            "easeOutQuad",
                            "${cld1}",
                            '0.5',
                            '1'
                        ],
                        [
                            "eid91",
                            "opacity",
                            0,
                            3000,
                            "easeOutQuad",
                            "${cld2}",
                            '0.5',
                            '1'
                        ],
                        [
                            "eid85",
                            "left",
                            0,
                            4000,
                            "easeOutQuad",
                            "${cld1}",
                            '63px',
                            '133px'
                        ]
                    ]
                }
            },
            "sea1": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '2000px', '58px', 'auto', 'auto'],
                            id: 'sea1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', '/images/sys/sea1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '2000px', '58px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: true,
                    data: [
                        [
                            "eid34",
                            "top",
                            0,
                            2000,
                            "easeInOutQuad",
                            "${sea1}",
                            '0px',
                            '-7px'
                        ],
                        [
                            "eid37",
                            "top",
                            2000,
                            2000,
                            "easeInOutQuad",
                            "${sea1}",
                            '-7px',
                            '0px'
                        ],
                        [
                            "eid35",
                            "left",
                            0,
                            2000,
                            "easeInOutQuad",
                            "${sea1}",
                            '0px',
                            '10px'
                        ],
                        [
                            "eid45",
                            "left",
                            2000,
                            2000,
                            "easeInOutQuad",
                            "${sea1}",
                            '10px',
                            '0px'
                        ]
                    ]
                }
            },
            "sea2": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '1999px', '58px', 'auto', 'auto'],
                            id: 'sea2',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', '/images/sys/sea2.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '1999px', '58px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid41",
                            "left",
                            0,
                            1500,
                            "easeInOutQuad",
                            "${sea2}",
                            '0px',
                            '10px'
                        ],
                        [
                            "eid42",
                            "left",
                            1500,
                            1500,
                            "easeInOutQuad",
                            "${sea2}",
                            '10px',
                            '0px'
                        ],
                        [
                            "eid40",
                            "top",
                            0,
                            1500,
                            "easeInOutQuad",
                            "${sea2}",
                            '0px',
                            '-5px'
                        ],
                        [
                            "eid43",
                            "top",
                            1500,
                            1500,
                            "easeInOutQuad",
                            "${sea2}",
                            '-5px',
                            '0px'
                        ]
                    ]
                }
            },
            "ship": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'ship',
                            rect: ['1462px', '5px', '307px', '110px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', '/images/sys/ship.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'shipCopy',
                            rect: ['-846px', '0px', '307px', '110px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', '/images/sys/ship.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '307px', '110px']
                        }
                    }
                },
                timeline: {
                    duration: 240000,
                    autoPlay: true,
                    data: [
                        [
                            "eid75",
                            "left",
                            150000,
                            90000,
                            "linear",
                            "${shipCopy}",
                            '-846px',
                            '0px'
                        ],
                        [
                            "eid66",
                            "top",
                            0,
                            150000,
                            "linear",
                            "${ship}",
                            '0px',
                            '5px'
                        ],
                        [
                            "eid67",
                            "left",
                            0,
                            150000,
                            "linear",
                            "${ship}",
                            '0px',
                            '1462px'
                        ]
                    ]
                }
            },
            "cld1": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '403px', '332px', 'auto', 'auto'],
                            id: 'cloudCopy',
                            opacity: 0.2,
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', '/images/sys/cloud.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '403px', '332px']
                        }
                    }
                },
                timeline: {
                    duration: 90000,
                    autoPlay: true,
                    data: [
                        [
                            "eid79",
                            "rotateZ",
                            0,
                            45000,
                            "linear",
                            "${cloudCopy}",
                            '0deg',
                            '180deg'
                        ],
                        [
                            "eid80",
                            "rotateZ",
                            45000,
                            45000,
                            "linear",
                            "${cloudCopy}",
                            '180deg',
                            '360deg'
                        ]
                    ]
                }
            },
            "cld2": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            transform: [[], ['-360'], [0, 0, 0], [1, 1, 1]],
                            rect: ['0px', '0px', '476px', '392px', 'auto', 'auto'],
                            id: 'cloud',
                            opacity: 0.2,
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', '/images/sys/cloud.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '476px', '392px']
                        }
                    }
                },
                timeline: {
                    duration: 90000,
                    autoPlay: true,
                    data: [
                        [
                            "eid81",
                            "rotateZ",
                            0,
                            45000,
                            "linear",
                            "${cloud}",
                            '0deg',
                            '-180deg'
                        ],
                        [
                            "eid83",
                            "rotateZ",
                            45000,
                            45000,
                            "linear",
                            "${cloud}",
                            '-180deg',
                            '-360deg'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("/js/homeslide_edgeActions.js");
})("EDGE-1455169207");
