/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'sea1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         
         sym.stop();
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         // insert code here
         sym.play();

      });
      //Edge binding end

   })("sea1");
   //Edge symbol end:'sea1'

   //=========================================================
   
   //Edge symbol: 'sea2'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         // insert code here
         sym.play();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

   })("sea2");
   //Edge symbol end:'sea2'

   //=========================================================
   
   //Edge symbol: 'ship'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 240000, function(sym, e) {
         // insert code here
         sym.play();

      });
      //Edge binding end

   })("ship");
   //Edge symbol end:'ship'

   //=========================================================
   
   //Edge symbol: 'cld1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 90000, function(sym, e) {
         // insert code here
         sym.play();

      });
      //Edge binding end

   })("cld1");
   //Edge symbol end:'cld1'

   //=========================================================
   
   //Edge symbol: 'cld2'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 90000, function(sym, e) {
         // insert code here
         sym.play();

      });
      //Edge binding end

   })("cld2");
   //Edge symbol end:'cld2'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-1455169207");