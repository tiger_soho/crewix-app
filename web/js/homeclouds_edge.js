/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='/images/sys/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "horizontal",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'clouds12',
                            symbolName: 'clouds1',
                            type: 'rect',
                            rect: ['-899px', '12px', '3843', '305', 'auto', 'auto'],
                            transform: [[],[],[],['0.73269']]
                        },
                        {
                            id: 'clouds22',
                            symbolName: 'clouds2',
                            type: 'rect',
                            rect: ['-391px', '335px', '3843', '156', 'auto', 'auto']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1000px', '500px', 'auto', 'auto'],
                            overflow: 'visible',
                            fill: ["rgba(255,255,255,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 600000,
                    autoPlay: true,
                    data: [
                        [
                            "eid12",
                            "left",
                            0,
                            0,
                            "linear",
                            "${clouds12}",
                            '-899px',
                            '-899px'
                        ],
                        [
                            "eid14",
                            "top",
                            0,
                            0,
                            "linear",
                            "${clouds22}",
                            '335px',
                            '335px'
                        ],
                        [
                            "eid15",
                            "top",
                            0,
                            0,
                            "linear",
                            "${clouds12}",
                            '12px',
                            '12px'
                        ],
                        [
                            "eid9",
                            "scaleX",
                            0,
                            0,
                            "linear",
                            "${clouds12}",
                            '0.73269',
                            '0.73269'
                        ],
                        [
                            "eid13",
                            "left",
                            0,
                            0,
                            "linear",
                            "${clouds22}",
                            '-391px',
                            '-391px'
                        ]
                    ]
                }
            },
            "clouds1": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'clouds2',
                            rect: ['-2000px', '0px', '1843px', '305px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', '/images/sys/clouds2.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'clouds2Copy',
                            rect: ['0px', '0px', '1843px', '305px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', '/images/sys/clouds2.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '3843px', '305px']
                        }
                    }
                },
                timeline: {
                    duration: 300000,
                    autoPlay: true,
                    data: [
                        [
                            "eid5",
                            "left",
                            0,
                            300000,
                            "linear",
                            "${clouds2}",
                            '0px',
                            '-2000px'
                        ],
                        [
                            "eid4",
                            "left",
                            0,
                            300000,
                            "linear",
                            "${clouds2Copy}",
                            '2000px',
                            '0px'
                        ]
                    ]
                }
            },
            "clouds2": {
                version: "5.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.0.375",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'clouds1',
                            rect: ['-2393px', '0px', '1908px', '156px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', '/images/sys/clouds1.png', '0px', '0px']
                        },
                        {
                            type: 'image',
                            id: 'clouds1Copy',
                            rect: ['-458px', '0px', '1908px', '156px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', '/images/sys/clouds1.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '3843px', '156px']
                        }
                    }
                },
                timeline: {
                    duration: 600000,
                    autoPlay: true,
                    data: [
                        [
                            "eid3",
                            "left",
                            0,
                            600000,
                            "linear",
                            "${clouds1}",
                            '0px',
                            '-2393px'
                        ],
                        [
                            "eid2",
                            "left",
                            0,
                            600000,
                            "linear",
                            "${clouds1Copy}",
                            '1935px',
                            '-458px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("/js/homeclouds_edgeActions.js");
})("EDGE-1462813254");
