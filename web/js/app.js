$(document).ready(function() {
	var ww = $(window).width();
	var wh = $(window).height();
	$('.date input').datepicker({
		format: 'dd.mm.yyyy',
		autoclose:true
	});
	$('.sign-block').on('click', '.next', function(){
		$(this).parent().parent().removeClass('vis');
		var a = $(this).parent().parent().next();
		setTimeout(function(){
			a.addClass('vis');
		},400)
		
	});
	if ($('select').length) {
			$.widget( "custom.iconselectmenu", $.ui.selectmenu, {
			  _renderItem: function( ul, item ) {
				var li = $( "<li>", { text: item.label } );
		 
				if ( item.disabled ) {
				  li.addClass( "ui-state-disabled" );
				}
		 
				$( "<span>", {
				  style: item.element.attr( "data-style" ),
				  "class": "ui-icon " + item.element.attr( "data-class" )
				})
				  .appendTo( li );
		 
				return li.appendTo( ul );
			  }
			});
		$('select').each(function(i,el){
			if(!$(el).parent().hasClass('countries')){
				$(el).selectmenu({
					width: '100%'
				});
			}else{
				$(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
			}
		});
		$( "#biometrics-sex_id" ).on( "selectmenuchange", function( event, ui ){
			$.post("shoes?id="+ui.item.value, function(data){
				if (data == "<option disabled=\"disabled\" selected>Select shoe size</option>empty") {

					$("select#biometrics-shoe_size_id").prop("disabled", "disabled");
					$("select#biometrics-shoe_size_id").selectmenu("refresh");
				}else{
					$("select#biometrics-shoe_size_id").html(data);
					$("select#biometrics-shoe_size_id").removeAttr("disabled");
					$("select#biometrics-shoe_size_id").selectmenu( "refresh" );

				}
			});
			$.post("cloth?id="+ui.item.value, function(data){
				if (data == "<option disabled=\"disabled\" selected>Select clothing size</option>empty") {
					$("select#biometrics-clothing_size_id").prop("disabled", "disabled");
					$("select#biometrics-clothing_size_id").selectmenu("refresh");
				}else{
					$("select#biometrics-clothing_size_id").html(data);
					$("select#biometrics-clothing_size_id").removeAttr("disabled");
					$("select#biometrics-clothing_size_id").selectmenu( "refresh" );
				}
			});
		});
			
	};

	$('body').on('click','#scans .add', function(){
		$.ajax({
			url:'/row-scan',
			type:'GET',
			data:{'add':'1'},
			success:function(data){
				$('#scans .item').last().after(data);
				$('#scans select').each(function(i,el){
					$(el).selectmenu({
						width: '100%'
					});
				});
			}
		});
	});

	if($('.opened').length){
		var t = $('.opened span').text();
		var m = moment(t,'YYYY-MM-D H:m:s').fromNow();
		$('.opened span').text(m);
	}


	if ($('.profile').length) {
		var d = $('.profile .stat.time .digit').text();
		var a = moment().diff(d,'years');
		$('.profile .stat.time .digit').text(a);

		$('ul.tabs li').on('click', function () {
			if($(this).hasClass('active')){
				return false;
			}else{
				$('ul.tabs li').removeClass('active');
				$(this).addClass('active');
				var tab = $(this).attr('id');
				$('.company .tab_cont').removeClass('active');
				$('.company').find('.'+tab).addClass('active');
			}
		})
	}
	
	$('input[type=radio]').each(function(i,el){
		if($(el).is(':checked')){
			$(el).parent().addClass('active');
		}
	});

	$('input[type=radio]').on('change',function(){
		var id = $(this).attr('name');
		$('.radio input[name="' + id + '"]').parent().removeClass('active');
		if($(this).is(':checked')){
			$(this).parent().addClass('active');
		}
	});

	$('input[type=checkbox]').each(function (i, el) {
		if ($(el).is(':checked')) {
			$(el).parent().addClass('active');
		}
	});
	/*$('input[type=checkbox]').parent().on('click', function () {

		if (!$(this).children('input[type=checkbox]').is(':checked')) {
			$(this).children('input[type=checkbox]').attr('checked', 'checked');
			$(this).addClass('active');
		} else {
			$(this).children('input[type=checkbox]').removeAttr('checked');
			$(this).removeClass('active');
		}
		return false;
	 });*/
	$('input[type=checkbox]').on('change', function () {
		if ($(this).is(':checked')) {
			$(this).parent().addClass('active');
		} else {
			$(this).parent().removeClass('active');
		}
	});

	if ($('#edge').length){
		$('#edge').height(wh-70+'px');




	};
	$('.user-menu').on('click',function(){
		if($('.user-menu ul').is(':visible')){
			$('.user-menu ul').slideUp(400);
		}else{
			$('.user-menu ul').slideDown(400);
		}
	});

	$('body').on('click','.edit.bvess', function(){
		$.ajax({
			url:'/formvessel',
			success: function(data) {
				$('.vessels').html(data);
				$('.vessels select').each(function(i,el){
					if(!$(el).parent().hasClass('countries')){
						$(el).selectmenu({
							width: '100%'
						});
					}else{
						$(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
					}
				});
			}
		});
		return false;
	});

	$('body').on('submit','form#vessels',function(){
		var data = $(this).serialize();
		$.ajax({
			url:'/formvessel',
			type:'POST',
			data: data,
			complete:function(){
				$.ajax({
					url:'/vessels',
					success:function(data){
						$('.tab_cont.vess .vessels').html(data);
					}
				});
			}
		});
		return false;
	});

	$('.cont-form form#contacts').on('submit', function(){
		var p = $('.cont-form form#contacts').serialize();
		$.ajax({
			url:'/editcont',
			type:'POST',
			data: p,
			dataType:'json',
			success: function(data){
				$('.contacts .cont-form').fadeOut(400);
				setTimeout(function(){
					$('.contacts .cont').fadeIn();
					$('.contacts .cont .line').first().find('a').attr('href', 'http://'+data['website']).text(data['site']);
					$('.contacts .cont .line').eq(1).find('a').attr('href', 'mailto:'+data['email']).text(data['email']);
					$('.contacts .cont .line').eq(2).find('p+p').text(data['phone']);
					$('.contacts .cont .line').eq(3).find('p+p').text(data['hq_address']+', '+data['hq_city_id']+', '+data['country']);
				},400);
			}
		});
		return false;
	});

	$('.about-form form#about-form').on('submit', function(){
		var p = $('.about-form form#about-form').serialize();
		$.ajax({
			url:'/editabout',
			type:'POST',
			data: p,
			success: function(data){
				$('.wrapper.company .com_cont .about-form').fadeOut(400);
				setTimeout(function(){
					$('.wrapper.company .com_cont .cont').fadeIn().html(data);
				},400);
			}
		});
		return false;
	});

	$('body').on('click','.finish',function(){
		window.location.href = '/';
	});

	$('body').on('click', '.add-scan', function () {
		$.ajax({
			url:'/docscans',
			success:function(data){
				$('.tab_cont.scans .block').html(data);
			}
		})
	});

	$('body').on('submit', 'form#smedit', function(){
		var data = $(this).serialize();
		$.ajax({
			url: '/smedit',
			type: 'POST',
			async: false,
			data: data,
            complete: function (data) {
                console.log(data.responseText);
                if (data.responseText != '') {
                    var response = $.parseJSON(data.responseText);
                    $.each(response.errors, function (i, el) {
                        var inp = $('.field-seaman-' + i);
                        inp.addClass('has-error');
                        $.notify(el, 'error');
                    });
                } else {
                    $.ajax({
                        url: '/smain',
                        success: function (data) {
                            $('.tab_cont.prof .block.main-info').html(data);
                        }
                    });
                }
			},
		});
		return false;
	});

	$('body').on('click', '.edit.scont', function () {
		$.ajax({
			url:'/smedit',
			success:function(data){
				$('.tab_cont.prof .block.main-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.scont', function () {
		$.ajax({
			url: '/smain',
			success: function (data) {
				$('.tab_cont.prof .block.main-info').html(data);
			}
		});
	});
	

	$('body').on('submit', 'form#sfedit', function(){
		var data = $(this).serialize();
		$.ajax({
			url: '/sfedit',
			type: 'POST',
			async: false,
			data: data,
			complete:function(){
				$.ajax({
					url:'/sfam',
					success:function(data){
						$('.tab_cont.prof .block.family-info').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click', '.edit.family', function () {
		$.ajax({
			url: '/sfedit',
			success: function (data) {
				$('.tab_cont.prof .block.family-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.family', function () {
		$.ajax({
			url: '/sfam',
			success: function (data) {
				$('.tab_cont.prof .block.family-info').html(data);
			}
		});
	});
	

	$('body').on('submit', 'form#skedit', function(){
		var data = $(this).serialize();
		$.ajax({
			url: '/skedit',
			type: 'POST',
			async: false,
			data: data,
			complete:function(){
				$.ajax({
					url:'/snok',
					success:function(data){
						$('.tab_cont.prof .block.nok-info').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click', '.edit.kin', function () {
		$.ajax({
			url: '/skedit',
			success: function (data) {
				$('.tab_cont.prof .block.nok-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.kin', function () {
		$.ajax({
			url: '/snok',
			success: function (data) {
				$('.tab_cont.prof .block.nok-info').html(data);
			}
		});
	});

	$('body').on('submit', 'form#sbedit', function(){
		var data = $(this).serialize();
		$.ajax({
			url: '/sbedit',
			type: 'POST',
			async: false,
			data: data,
			complete:function(){
				$.ajax({
					url:'/sbio',
					success:function(data){
						$('.tab_cont.prof .block.biometrics').html(data);
					}
				});
			},
		});
		return false;
	});


	$('body').on('click', '.edit.bio', function () {
		var id = $(this).attr('data-id');
		$.ajax({
			url: '/sbedit',
			data: {id: id},
			type: 'GET',
			success: function (data) {
				$('.tab_cont.prof .block.biometrics').html(data);
				$('#sbedit select').each(function (i, el) {
					$(el).selectmenu({
						width: '100%'
					});

				});

			}
		});
	});

	$('body').on('click', '.cancel.bio', function () {
		var id = $(this).attr('data-id');
		$.ajax({
			url: '/sbio',
			success: function (data) {
				$('.tab_cont.prof .block.biometrics').html(data);
			}
		});
	});

	$('body').on('submit', 'form#sledit', function() {
		var form = $(this);
		$.ajax({
			url: '/sledit',
			type: 'POST',
			async: false,
			data: form.serialize(),
			complete: function () {
				$.ajax({
					url: '/slang',
					success: function (data) {
						$('.tab_cont.prof .block.lang-info').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click', '.edit.lang', function () {
		$.ajax({
			url: '/sledit',
			success: function (data) {
				$('.tab_cont.prof .block.lang-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.languages', function () {
		$.ajax({
			url: '/slang',
			success: function (data) {
				$('.tab_cont.prof .block.lang-info').html(data);
			}
		});
	});


	$('body').on('submit', 'form#sedu', function() {
		var data = $(this).serialize();
		$.ajax({
			url: '/seduedit',
			type: 'POST',
			async: false,
			data: data,
            complete: function (data) {
                if (data.responseText != '') {
                    var response = $.parseJSON(data.responseText);
                    console.log(response);
                    $.each(response.errors, function (i, el) {
                        var inp = $('.field-education-' + i);
                        inp.addClass('has-error');
                        $.notify(el, 'error');
                    });
                } else {
                    $.ajax({
                        url: '/sedu',
                        success: function (data) {
                            $('.tab_cont.prof .block.education-info').html(data);
                        }
                    });
                }
			},
		});
		return false;
	});

	$('body').on('click', '.edit.edu', function () {
		$.ajax({
			url: '/seduedit',
			success: function (data) {
				$('.tab_cont.prof .block.education-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.edu', function () {
		$.ajax({
			url: '/sedu',
			success: function (data) {
				$('.tab_cont.prof .block.education-info').html(data);
			}
		});
	});

	$('body').on('submit', 'form#scans', function(){
		var data = $(this).serialize();
		$.ajax({
			url: '/docscans',
			type: 'POST',
			async: false,
			data: data,
            complete: function () {
				$.ajax({
                    url: '/seaman-scans',
                    success: function (data) {
                        $('.tab_cont.scans .block').html(data);
                    }
				 });
			},
			error: function (data) {
			}
		});
		return false;
	});



	$('body').on('click','.edit.tr',function(){
		$.ajax({
			url:'/stredit',
			success:function(data){
				$('.tab_cont.prof .block.travel-info').html(data);
                $('#stravel select').each(function (i, el) {
                    $(el).selectmenu({
                        width: '100%'
                    });
                });
                $('.date input').datepicker({
                    format: 'dd.mm.yyyy',
                    autoclose: true
                });
			}
		});
	});

	$('body').on('click', '.cancel.travel', function () {
		$.ajax({
			url: '/stravel',
			success: function (data) {
				$('.tab_cont.prof .block.travel-info').html(data);
			}
		});
	});

	$('body').on('submit', 'form#stravel', function() {
		var data = $(this).serialize();
		$.ajax({
			url: '/stredit',
			type: 'POST',
			async: false,
			data: data,
            complete: function (data) {
                if (data.responseText != '') {
                    var response = $.parseJSON(data.responseText);
                    console.log(response);
                    $.each(response.errors, function (i, el) {
                        var inp = $('.field-seamantraveldocument-' + i);
                        inp.addClass('has-error');
                        $.notify(el, 'error');
                    });
                } else {
                    $.ajax({
                        url: '/stravel',
                        success: function (data) {
                            $('.tab_cont.prof .block.travel-info').html(data);
                        }
                    });
                }
			},
		});
		return false;
	});

	$('body').on('click','.edit.comp',function(){
		$.ajax({
			url:'/scomedit',
			success:function(data){
				$('.tab_cont.prof .block.competency-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.comp', function () {
		$.ajax({
			url: '/scom',
			success: function (data) {
				$('.tab_cont.prof .block.competency-info').html(data);
			}
		});
	});

	$('body').on('submit', 'form#st-com', function() {
		var data = $(this).serialize();
		$.ajax({
			url: '/scomedit',
			type: 'POST',
			async: false,
			data: data,
			complete: function () {
				$.ajax({
					url: '/scom',
					success: function (data) {
						$('.tab_cont.prof .block.competency-info').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click','.edit.sea',function(){
		$.ajax({
			url: '/sseaedit',
			success: function(data){
				$('.tab_cont .sea-services').html(data);
				$('#st-serv select').each(function(i,el){
					$(el).selectmenu({
						width: '100%'
					});
				});
			}
		});
	});

	$('body').on('submit', 'form#st-serv', function() {
		var data = $(this).serialize();
		$.ajax({
			url: '/sseaedit',
			type: 'POST',
			async: false,
			data: data,
			complete: function () {
				$.ajax({
					url: '/ssea',
					success: function (data) {
						$('.tab_cont .sea-services').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click','.edit.ref',function(){
		$.ajax({
			url: '/srefedit',
			success: function(data){
				$('.tab_cont .references').html(data);
				$('.phone input').intlTelInput();
			}
		});
	});

	$('body').on('click', '.cancel.sevice', function () {
		$.ajax({
			url: '/ssea',
			success: function (data) {
				$('.tab_cont .sea-services').html(data);
			}
		});
	});

	$('body').on('submit', 'form#st-ref', function() {
		var data = $(this).serialize();
		$.ajax({
			url: '/srefedit',
			type: 'POST',
			async: false,
			data: data,
			complete: function () {
				$.ajax({
					url: '/sref',
					success: function (data) {
						$('.tab_cont .references').html(data);
					}
				});
			},
		});
		return false;
	});


	$('body').on('click','.edit.prof',function(){
		$.ajax({
			url: '/sprofedit',
			success: function(data){
				$('.tab_cont .proficiency').html(data);
				$.widget( "custom.iconselectmenu", $.ui.selectmenu, {
					_renderItem: function( ul, item ) {
						var li = $( "<li>", { text: item.label } );

						if ( item.disabled ) {
							li.addClass( "ui-state-disabled" );
						}

						$( "<span>", {
							style: item.element.attr( "data-style" ),
							"class": "ui-icon " + item.element.attr( "data-class" )
						})
							.appendTo( li );

						return li.appendTo( ul );
					}
				});
				$('#st-prof select').each(function(i,el){
					if(!$(el).parent().hasClass('countries')){
						$(el).selectmenu({
							width: '100%'
						});
					}else{
						$(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
					}
				});
			}
		});
	});

	$('body').on('submit', 'form#st-prof', function() {
		var data = $(this).serialize();
		$.ajax({
			url: '/sprofedit',
			type: 'POST',
			async: false,
			data: data,
			complete: function () {
				$.ajax({
					url: '/sprof',
					success: function (data) {
						$('.tab_cont .proficiency').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click', '.cancel.prof', function () {
		$.ajax({
			url: '/sprof',
			success: function (data) {
				$('.tab_cont .proficiency').html(data);
			}
		});
	});


	$('body').on('click','.edit.med',function(){
		$.ajax({
			url: '/smededit',
			success: function(data){
				$('.tab_cont .medical').html(data);
				$.widget( "custom.iconselectmenu", $.ui.selectmenu, {
					_renderItem: function( ul, item ) {
						var li = $( "<li>", { text: item.label } );

						if ( item.disabled ) {
							li.addClass( "ui-state-disabled" );
						}

						$( "<span>", {
							style: item.element.attr( "data-style" ),
							"class": "ui-icon " + item.element.attr( "data-class" )
						})
							.appendTo( li );

						return li.appendTo( ul );
					}
				});
				$('#st-med select').each(function(i,el){
					if(!$(el).parent().hasClass('countries')){
						$(el).selectmenu({
							width: '100%'
						});
					}else{
						$(el).iconselectmenu({width: '100%'}).iconselectmenu( "menuWidget").addClass( "ui-menu-icons avatar" );
					}
				});
			}
		});
	});

	$('body').on('submit', 'form#st-med', function() {
		var data = $(this).serialize();
		$.ajax({
			url: '/smededit',
			type: 'POST',
			async: false,
			data: data,
			complete: function (data) {
				if(data.responseText !=''){
					var response=$.parseJSON(data.responseText);
					$.each(response.errors, function(i,el){
						var inp = $('.field-seamanmedicaldocument-'+i);
						inp.addClass('has-error');
						$.notify(el,'error');
					});
				}else{
					$.ajax({
						url: '/smed',
						success: function (data) {
							$('.tab_cont .medical').html(data);
						}
					});
				}
			},
		});
		return false;
	});

	$('body').on('click', '.cancel.med', function () {
		$.ajax({
			url: '/smed',
			success: function (data) {
				$('.tab_cont .medical').html(data);
			}
		});
	});

	$('body').on('submit', 'form#scv', function () {
		var data = $(this).serialize();
		$.ajax({
			url: '/scvedit',
			type: 'POST',
			async: false,
			data: data,
			complete: function () {
				$.ajax({
					url: '/scv',
					success: function (data) {
						$('.tab_cont.prof .block.cv-info').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click', '.edit.cv', function () {
		$.ajax({
			url: '/scvedit',
			success: function (data) {
				$('.tab_cont.prof .block.cv-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.cv', function () {
		$.ajax({
			url: '/scv',
			success: function (data) {
				$('.tab_cont.prof .block.cv-info').html(data);
			}
		});
	});

	$('body').on('submit', 'form#snotes', function () {
		var data = $(this).serialize();
		$.ajax({
			url: '/snotesedit',
			type: 'POST',
			async: false,
			data: data,
			complete: function () {
				$.ajax({
					url: '/snotes',
					success: function (data) {
						$('.tab_cont.prof .block.notes-info').html(data);
					}
				});
			},
		});
		return false;
	});

	$('body').on('click', '.edit.notes', function () {
		$.ajax({
			url: '/snotesedit',
			success: function (data) {
				$('.tab_cont.prof .block.notes-info').html(data);
			}
		});
	});

	$('body').on('click', '.cancel.notes', function () {
		$.ajax({
			url: '/snotes',
			success: function (data) {
				$('.tab_cont.prof .block.notes-info').html(data);
			}
		});
	});



	if($('[data-toggle="popover"]').length) {
		$('[data-toggle="popover"]').popover();
	}

	$('#new_mail').on('submit',function(){

		var data = $(this).serialize();
		$.ajax({
			url: '/settings/security',
			type: 'POST',
			async: true,
			data: data,
			success:function(data){
			}
		});
		return false;
	});

	$('#shareseamanform-link').on('change',function(){
        var id = $(this).val();
        $.ajax({
            url:'/dashboard/loadseaman',
            data:{id:id},
            type:'GET',
            success:function(data){
                $('select#shareseamanform-scans').append(data);
                $('select#shareseamanform-scans').selectmenu( "refresh" );
            }
        });
    });

	$('body').on('click', '.remove', function () {
		$(this).parents('tr').remove();
	});


//VLAD
	if ($('.nav-follower').length) {
		var eheight = $(".nav-follower").offset().top;

		$(window).scroll(function () {
			if ($(window).scrollTop() >= eheight) {
				$('.nav-follower').addClass('nav-follower-fixed');
			}
			if ($(window).scrollTop() <= eheight) {
				$('.nav-follower').removeClass('nav-follower-fixed');
			}
		});
	}

	$('.admin-search-btn').on('click', function (event) {
		$(this).parent().toggleClass('open');
	});

	$( ".a-open-c" ).click(function() {
		$( ".admin-s-p" ).removeClass('open');
	});



	//vlad end

	$(window).resize(function(){
		ww = $(window).width()
		wh = $(window).height();
		if ($('#edge').length){
			$('#edge').height(wh-70+'px');
		};
	});

});
$(window).load(function(){
	if($('#edge').length){
		AdobeEdge.loadComposition('/js/homeslide', 'EDGE-1455169207', {
			scaleToFit: "none",
			centerStage: "horizontal",
			minW: "0",
			maxW: "undefined",
			width: "1000px",
			height: "500px"
		}, {dom: [ ]}, {dom: [ ]});
		AdobeEdge.loadComposition('/js/homeclouds', 'EDGE-1462813254', {
			scaleToFit: "none",
			centerStage: "horizontal",
			minW: "0",
			maxW: "undefined",
			width: "1000px",
			height: "500px"
		}, {dom: [ ]}, {dom: [ ]});
	}

})
