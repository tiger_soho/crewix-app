/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'clouds1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 300000, function(sym, e) {
         // insert code here
         sym.play();

      });
      //Edge binding end

   })("clouds1");
   //Edge symbol end:'clouds1'

   //=========================================================
   
   //Edge symbol: 'clouds2'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 600000, function(sym, e) {
         // insert code here
         sym.play();

      });
      //Edge binding end

   })("clouds2");
   //Edge symbol end:'clouds2'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-1462813254");