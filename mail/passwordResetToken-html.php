<?php
/**
 * Created by PhpStorm.
 * User: tiger
 * Date: 14.02.2016
 * Time: 16:09
 */
?>

<h4>Hi there <?=$name?>.</h4>

<p>We received a request to reset the password for your account.</p>

        <p>If this was you, you can set a new password here:</p>
        <p><a href="<?=Yii::$app->urlManager->createAbsoluteUrl(['do/reset-password','id'=>$user->id,'key'=>$token]);?>"><?=Yii::$app->urlManager->createAbsoluteUrl(['do/reset-password','id'=>$user->id,'key'=>$token]);?></a></p>


<p>If you don't want to change your password or didn't request this, just ignore and delete this message.</p>

    <p>Best regards,<br>
    Crewix team</p>
