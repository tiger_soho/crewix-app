<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/intlTelInput.css',
        'css/datepicker.css',
        '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
        'css/bootstrap-tagsinput.css',
        '//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        'css/site.css'
    ];
    public $js = [
        'js/browser.js',
        'js/moment-with-locales.min.js',
        '//code.jquery.com/ui/1.11.4/jquery-ui.js',
        '//animate.adobe.com/runtime/5.0.1/edge.5.0.1.min.js',
        'js/jquery.scrollTo.min.js',
        'js/sidenav.min.js',
        'js/intlTelInput.min.js',
        //'js/bootstrap.min.js',
        'js/bootstrap-datepicker.js',
        'js/typehead.js',
        '//twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js',
        'js/bootstrap-tagsinput.min.js',
        'js/notify.min.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
