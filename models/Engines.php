<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "engine_type".
 *
 * @property integer $id
 * @property string $name
 */
class Engines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'engine_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['name'], 'safe'],
            [['name'], 'string', 'max' => 32, 'message' => 'Engine name should contain at most 32 characters'],
            [['name'], 'unique', 'message' => 'Engine with the same name already exists'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Engine name'),
        ];
    }
}
