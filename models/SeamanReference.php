<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_reference".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $company_id
 * @property string $company_name
 * @property string $phone
 * @property string $email
 * @property string $contact_person
 */
class SeamanReference extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_reference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name'], 'string', 'max' => 128],
            ['phone', 'string', 'max' => 32],
            [['email'], 'email'],
            [['email','contact_person'], 'string', 'max' => 255],
        ];
    }

    public function saveRef($uid,$post)
    {
        $arr = [
            'seaman_id'=>$uid,
            'company_name'=>$post['company_name'],
            'phone'=>$post['phone'],
            'email'=>$post['email'],
            'contact_person'=>$post['contact_person'],
        ];
        return Yii::$app->db->createCommand()->insert('seaman_reference',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'company_name' => Yii::t('app', 'Company Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'contact_person' => Yii::t('app', 'Contact Person'),
        ];
    }
}
