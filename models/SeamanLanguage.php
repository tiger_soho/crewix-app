<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_language".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $language_id
 * @property integer $language_level_id
 */
class SeamanLanguage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id'], 'required'],
            [['language_level_id'], 'required'],
        ];
    }

    public function saveLang($uid,$post)
    {
        $arr = ['seaman_id'=>$uid,
                'language_id'=>$post['language_id'],
                'language_level_id'=>$post['language_level_id']];
        return Yii::$app->db->createCommand()->insert('seaman_language',$arr)->execute();
    }

    public function editLangs($id,$post)
    {
        if($post['id']){
            $arr = ['language_id'=>$post['language_id'],
                'language_level_id'=>$post['language_level_id']];
            return Yii::$app->db->createCommand()->update('seaman_language',$arr,['id'=>$post['id']])->execute();
        }else{
            $arr = ['seaman_id'=>$id,
                'language_id'=>$post['language_id'],
                'language_level_id'=>$post['language_level_id']];
            return Yii::$app->db->createCommand()->insert('seaman_language',$arr)->execute();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'language_level_id' => Yii::t('app', 'Language Level ID'),
        ];
    }
}
