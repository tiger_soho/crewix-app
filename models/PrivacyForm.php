<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "terms_of_service".
 *
 * @property string $id
 * @property string $section_name
 * @property string $text
 * @property string $updated_at
 * @property string $position
 */
class TermsForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'privacy_policy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['updated_at'], 'safe'],
            [['position'], 'integer'],
            [['section_name'], 'string', 'max' => 64],
        ];
    }

    public function updatePosition($data)
    {
        return Yii::$app->db->createCommand()->update('privacy_policy',['position'=>$data['position']],['id'=>$data['id']])->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_name' => 'Section Name',
            'text' => 'Text',
            'updated_at' => 'Updated At',
            'position' => 'Position',
        ];
    }
}
