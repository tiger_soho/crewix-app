<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shoe_size".
 *
 * @property integer $id
 * @property integer $sex_id
 * @property string $name
 */
class Shoes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shoe_size';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sex_id', 'name'], 'required'],
            [['sex_id'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['name'], 'unique'],
        ];
    }

    public function getRow($sex)
    {
        return static::find()->where(['sex_id'=>strtolower($sex)])->asArray()->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sex_id' => Yii::t('app', 'Sex ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}
