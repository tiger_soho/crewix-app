<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;
use yii\validators\Validator;

/**
 * This is the model class for table "languages".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $lang
 * @property string $level
 */
class Languages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang','level'],'required'],
        ];
    }

    public function saveLang($uid,$post)
    {
        $arr = ['uid'=>$uid,
                'lang'=>$post['lang'],
                'level'=>$post['level']];
        return Yii::$app->db->createCommand()->insert('languages',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'lang' => Yii::t('app', 'Lang'),
            'level' => Yii::t('app', 'Level'),
        ];
    }
}
