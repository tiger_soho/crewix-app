<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "travel_docs".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $doc_id
 * @property string $number
 * @property integer $country_id
 * @property string $issue
 * @property string $expiry
 */
class TravelDocs extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'travel_docs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['uid', 'doc_id', 'country_id'], 'integer'],
            [['issue', 'expiry'], 'safe'],
            [['number'], 'string', 'max' => 255],
        ];
    }

    public function saveDoc($uid,$post)
    {   
        $issue = explode('.', $post['issue']);
        $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
        if(!empty($post['expiry'])){
            $expiry = explode('.', $post['expiry']);
            $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
        }else{
            $expiry = '0000-00-00';
        }
        
        $arr = ['uid'=>$uid,
                'doc_id'=>$post['doc_id'],
                'number'=>$post['number'],
                'country_id'=>$post['country_id'],
                'issue'=>$issue,
                'expiry'=>$expiry,
                ];
        return Yii::$app->db->createCommand()->insert('travel_docs',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'doc_id' => Yii::t('app', 'Doc ID'),
            'number' => Yii::t('app', 'Number'),
            'country_id' => Yii::t('app', 'Country ID'),
            'issue' => Yii::t('app', 'Issue'),
            'expiry' => Yii::t('app', 'Expiry'),
        ];
    }
}
