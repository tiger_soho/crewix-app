<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class ChangeEmail extends Model
{
    public $id;
    public $current;
    public $new;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id', 'new'], 'required'],
            [['new'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'current' => 'Current e-mail',
            'new' => 'New e-mail',
        ];
    }

    public function compareEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!($this->new === $this->repeat)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function change()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->setTmpEmail($this->new);
            $key = $this->createToken();
            if ($key) {
                if ($user->save()) {
                    return $key;
                }
            }
            else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne($this->id);
        }

        return $this->_user;
    }

    public function createToken()
    {
        $token = new Token();
        $token_type = TokenType::findOne(TokenType::BIND_EMAIL);
        $token->user_id = $this->_user->id;
        $token->type_id = $token_type->id;
        $token->value = md5($this->_user->auth_key . time());
        $token->expires_at = gmdate("Y-m-d H:i:s", (time() + $token_type->lifetime));
        if($token->save()) {
            return $token->value;
        }
        else{
            return false;
        }
    }
}