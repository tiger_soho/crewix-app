<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "medical".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $name
 * @property string $number
 * @property integer $country
 * @property string $issue
 * @property string $expiry
 */
class Medical extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'medical';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['number[]', 'each', 'rule' => ['required']],
            [['uid', 'name', 'country', 'issue', 'expiry'], 'required'],
            [['uid', 'country'], 'integer'],
            [['issue', 'expiry'], 'safe'],
            [['name', 'number'], 'string', 'max' => 255],
        ];
    }

    public function saveDoc($uid,$post)
    {
        $issue = explode('.', $post['issue']);
        $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
        $expiry = explode('.', $post['expiry']);
        $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
        $arr = [
            'uid'=>$uid,
            'name'=>$post['name'],
            'number'=>$post['number'],
            'country'=>$post['country'],
            'issue'=>$issue,
            'expiry'=>$expiry,
        ];
        return Yii::$app->db->createCommand()->insert('medical',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'name' => Yii::t('app', 'Name'),
            'number' => Yii::t('app', 'Number'),
            'country' => Yii::t('app', 'Country'),
            'issue' => Yii::t('app', 'Issue'),
            'expiry' => Yii::t('app', 'Expiry'),
        ];
    }
}
