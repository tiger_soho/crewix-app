<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "c_info".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $mail
 * @property string $phone
 * @property string $skype
 * @property string $site
 */
class CInfo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'c_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid'], 'integer'],
            [['mail', 'phone', 'skype', 'site'], 'string', 'max' => 255],
        ];
    }

    public function saveContacts($uid,$post)
    {
        $arr = ['uid'=>$uid,
                'mail'=>$post['mail'],
                'phone'=>$post['phone'],
                'skype'=>$post['skype'],
                'site'=>$post['site'],
                ];
        return Yii::$app->db->createCommand()->insert('c_info',$arr)->execute();
    }

    public function editInfo($uid,$post)
    {
    	$arr = [];
    	if(!empty($post['mail'])){
    		$arr['mail'] = $post['mail'];
    	}
    	if(!empty($post['phone'])){
    		$arr['phone'] = $post['phone'];
    	}
    	if(!empty($post['site'])){
    		$arr['site'] = $post['site'];
    	}
    	return Yii::$app->db->createCommand()->update('c_info',$arr,['uid'=>$uid])->execute();
    }

    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'mail' => Yii::t('app', 'Mail'),
            'phone' => Yii::t('app', 'Phone'),
            'skype' => Yii::t('app', 'Skype'),
            'site' => Yii::t('app', 'Site'),
        ];
    }
}
