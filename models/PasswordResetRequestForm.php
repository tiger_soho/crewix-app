<?php
namespace app\models;

use app\models\Users;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    const RESET_TOKEN = 3;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\models\Users',
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = Users::findOne([
            'email' => $this->email,
        ]);


        if ($user) {
                $lifetime = TokenType::findOne(3)->lifetime;
                $token = md5($user->auth_key.time());
                $token_model = new Token();
                $token_model->user_id = $user->id;
                $token_model->type_id = 3;
                $token_model->value = $token;
                $token_model->expires_at = date('Y-m-d H:i:s', time()+$lifetime);

                if($user->account_type_id=='1'){
                    $name = Seaman::findOne(['user_id'=>$user->id])->first_name;
                }else{
                    $name = Companies::findOne(['user_id'=>$user->id])->name;
                }

            if ($token_model->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html'], ['user' => $user,'token'=>$token,'name'=>$name])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
                    ->setTo($this->email)
                    ->setSubject('Password reset for ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
