<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_sea_service".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $rank_id
 * @property string $vessel_name
 * @property integer $vessel_flag_id
 * @property integer $vessel_type_id
 * @property integer $vessel_deadweight
 * @property integer $engine_type_id
 * @property integer $engine_power
 * @property integer $shipowning_company_id
 * @property string $shipowning_company_name
 * @property integer $crewing_company_id
 * @property string $crewing_company_name
 * @property string $date_from
 * @property string $date_to
 */
class SeamanSeaService extends \yii\db\ActiveRecord
{

    public $measure;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_sea_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'seaman_id',
                    'rank_id',
                    'vessel_name',
                    'vessel_flag_id',
                    'vessel_type_id',
                    'engine_type_id',
                    'vessel_deadweight',
                    'date_from',
                    'date_to'
                ],
                'required',
                'on' => ['update']
            ],
            [['seaman_id', 'rank_id', 'vessel_flag_id', 'vessel_type_id', 'vessel_deadweight', 'engine_type_id', 'engine_power', 'shipowning_company_id', 'crewing_company_id'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [
                ['date_to'],
                function ($model) {
                    return $model->date_to > $model->date_from ? true : false;
                }
            ],
            [['vessel_name'], 'string', 'max' => 64],
            [['vessel_deadweight','engine_power'], 'integer'],
            [['vessel_deadweight','engine_power'], 'number', 'max'=>99999999],
            [['shipowning_company_name', 'crewing_company_name'], 'string', 'max' => 128],
        ];
    }

    public function saveServ($uid,$post)
    {
        $from = explode('.', $post['date_from']);
        $from = $from[2].'-'.$from[1].'-'.$from[0];
        $to = explode('.', $post['date_to']);
        $to = $to[2].'-'.$to[1].'-'.$to[0];
        $arr = [
            'seaman_id'=>$uid,
            'rank_id'=>$post['rank_id'],
            'vessel_name'=>$post['vessel_name'],
            'vessel_flag_id'=>$post['vessel_flag_id'],
            'vessel_type_id'=>$post['vessel_type_id'],
            'vessel_deadweight'=>$post['vessel_deadweight'],
            'engine_type_id'=>$post['engine_type_id'],
            'engine_power'=>$post['engine_power'],
            'shipowning_company_name'=>$post['shipowning_company_name'],
            'crewing_company_name'=>$post['crewing_company_name'],
            'date_from'=>$from,
            'date_to'=>$to,
        ];
        return Yii::$app->db->createCommand()->insert('seaman_sea_service',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'rank_id' => Yii::t('app', 'Rank ID'),
            'vessel_name' => Yii::t('app', 'Vessel Name'),
            'vessel_flag_id' => Yii::t('app', 'Vessel Flag ID'),
            'vessel_type_id' => Yii::t('app', 'Vessel Type ID'),
            'vessel_deadweight' => Yii::t('app', 'Vessel Deadweight'),
            'engine_type_id' => Yii::t('app', 'Engine Type ID'),
            'engine_power' => Yii::t('app', 'Engine Power'),
            'shipowning_company_id' => Yii::t('app', 'Shipowning Company ID'),
            'shipowning_company_name' => Yii::t('app', 'Shipowning Company Name'),
            'crewing_company_id' => Yii::t('app', 'Crewing Company ID'),
            'crewing_company_name' => Yii::t('app', 'Crewing Company Name'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
        ];
    }
}
