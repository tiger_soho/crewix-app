<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_family".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $marital_status_id
 * @property integer $children
 * @property string $mother
 * @property string $father
 */
class Family extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_family';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seaman_id', 'marital_status_id', 'children'], 'required'],
            [['seaman_id', 'marital_status_id', 'children'], 'integer'],
            [['mother', 'father'], 'string', 'max' => 255],
        ];
    }

    public function saveFamily($uid,$post)
    {
        $arr = ['seaman_id'=>$uid,
                'marital_status_id'=>$post['marital_status_id'],
                'children'=>$post['children'],
                ];
        if ($post['mother']&&!empty($post['mother'])) {
            $arr['mother'] = $post['mother'];
        }
        if ($post['father']&&!empty($post['father'])) {
            $arr['father'] = $post['father'];
        }
        return Yii::$app->db->createCommand()->insert('seaman_family',$arr)->execute();
    }

    public function editFamily($uid,$post)
    {
        $arr = ['marital_status_id'=>$post['marital_status_id'],
                'children'=>$post['children'],
                ];
        if ($post['mother']&&!empty($post['mother'])) {
            $arr['mother'] = $post['mother'];
        }
        if ($post['father']&&!empty($post['father'])) {
            $arr['father'] = $post['father'];
        }
        return Yii::$app->db->createCommand()->update('seaman_family',$arr,['seaman_id'=>$uid])->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'marital_status_id' => Yii::t('app', 'Marital Status ID'),
            'children' => Yii::t('app', 'Children'),
            'mother' => Yii::t('app', 'Mother'),
            'father' => Yii::t('app', 'Father'),
        ];
    }
}
