<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property string $id
 * @property string $payment_type_id
 * @property string $payment_system_id
 * @property string $user_id
 * @property string $amount
 * @property string $comment
 * @property string $paypal_transaction_id
 * @property string $paypal_payer_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $status_id
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_type_id', 'payment_system_id', 'amount', 'status_id'], 'required'],
            [['payment_type_id', 'payment_system_id', 'user_id', 'status_id'], 'integer'],
            [['amount'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['paypal_transaction_id'], 'string', 'max' => 128],
            [['paypal_payer_id'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'payment_type_id' => Yii::t('app', 'Payment Type ID'),
            'payment_system_id' => Yii::t('app', 'Payment System ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'amount' => Yii::t('app', 'Amount'),
            'comment' => Yii::t('app', 'Comment'),
            'paypal_transaction_id' => Yii::t('app', 'Paypal Transaction ID'),
            'paypal_payer_id' => Yii::t('app', 'Paypal Payer ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status_id' => Yii::t('app', 'Status ID'),
        ];
    }
}
