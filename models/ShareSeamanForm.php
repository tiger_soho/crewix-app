<?php
/**
 * Created by PhpStorm.
 * User: Кирилл
 * Date: 03.02.2016
 * Time: 12:38
 */

namespace app\models;
use Yii;
use yii\base\Model;

class ShareSeamanForm extends Model
{
    public $link;
    public $to;
    public $primary_rank_id;
    public $vessel_type_id;
    public $view;
    public $power;
    public $scans;
    public $photo;
    public $documents;

    public function rules()
    {
        return [
            [['to', 'link'], 'required'],
            // rememberMe must be a boolean value
            ['to', 'email'],
        ];
    }
}