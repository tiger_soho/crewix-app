<?php

namespace app\models;

use Yii;
use yii\base\Model;


class ComposeForm extends Model
{

    public $to;
    public $from;
    public $message;
    public $subject;
    public $files;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['from', 'to', 'subject', 'message'], 'required'],
            // email has to be a valid email address
            [['from'], 'email'],
            ['subject', 'string', 'max' => 64],
            ['message', 'string', 'max' => 2560],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from' => 'From',
            'to' => 'To',
            'subject' => 'Subject',
            'message' => 'Message',
        ];
    }
}