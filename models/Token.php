<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;
use app\models\TokenType;

/**
 * This is the model class for table "token".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $user_id
 * @property string $value
 * @property string $created_at
 * @property string $expires_at
 */
class Token extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'user_id', 'value'], 'required'],
            [['type_id', 'user_id'], 'integer'],
            [['created_at', 'expires_at'], 'safe'],
            [['value'], 'string', 'max' => 255],
            [['value'], 'unique'],
        ];
    }

    public function createToken($key,$type,$uid)
    {
        $life = TokenType::findOne(['id'=>$type])->lifetime;
        $expiry = time()+$life;
        $expiry = gmdate("Y-m-d H:i:s", $expiry);
        $arr = ['user_id'=>$uid,
                'type_id'=>$type,
                'value'=>$key,
                'expires_at'=>$expiry,];
        return Yii::$app->db->createCommand()->insert('token', $arr)->execute();
    }

    public static function findByPasswordResetToken($token,$id,$type)
    {
        if (!static::isPasswordResetTokenValid($token,$id,$type)) {

            return null;
        }

        return static::findOne([
            'value' => $token,
        ]);
    }

    public static function isPasswordResetTokenValid($token, $id, $type)
    {
        if (empty($token)) {

            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = strtotime(static::findOne(['user_id'=>$id,'type_id'=>$type])->expires_at);
        return $expire >= time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->value = null;
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'value' => Yii::t('app', 'Value'),
            'created_at' => Yii::t('app', 'Created At'),
            'expires_at' => Yii::t('app', 'Expires At'),
        ];
    }
}
