<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "docs".
 *
 * @property integer $id
 * @property string $name
 */
class Docs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'docs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }
    public function getDocs()
    {
        return static::find()->where(['!=','id','1'])->andWhere(['!=','id','2'])->andWhere(['!=','id','4'])->andWhere(['!=','id','5'])->asArray->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}
