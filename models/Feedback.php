<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\User;
use app\models\Seaman;
use app\models\Companies;


/**
 * This is the model class for table "feedback".
 *
 * @property string $id
 * @property string $user_id
 * @property string $text
 * @property string $likes
 * @property string $created_at
 *
 * @property User $user
 * @property Companies $company
 * @property Seaman $seamen
 */
class Feedback extends \yii\db\ActiveRecord
{
    const ACCOUNT_TYPE_SEAMEN = 1;
    const ACCOUNT_TYPE_COMPANY = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback%}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'text', 'likes'], 'required'],
            [['user_id'], 'integer'],
            [['likes'], 'string'],
            [['text'], 'string', 'max' => 2500 , 'message' => 'Your feedback was over 2500 characters. You\'ll have to be more clever.'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'text' => 'Text',
            'likes' => 'Likes',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeamen()
    {
        return $this->hasOne(Seaman::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return FeedbackQuery the active query used by this AR class.
     */
    public static function find()
    {
       return new FeedbackQuery(get_called_class());
    }
}
