<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "references".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $company
 * @property string $phone
 * @property string $mail
 * @property string $cont_name
 */
class References extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'references';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail'], 'email'],
            [['company', 'phone', 'mail', 'cont_name'], 'string', 'max' => 255],
        ];
    }

    public function saveRef($uid,$post)
    {
        $arr = [
            'uid'=>$uid,
            'company'=>$post['company'],
            'phone'=>$post['phone'],
            'mail'=>$post['mail'],
            'cont_name'=>$post['cont_name'],
        ];
        return Yii::$app->db->createCommand()->insert('references',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'company' => Yii::t('app', 'Company'),
            'phone' => Yii::t('app', 'Phone'),
            'mail' => Yii::t('app', 'Mail'),
            'cont_name' => Yii::t('app', 'Cont Name'),
        ];
    }
}
