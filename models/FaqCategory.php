<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faq_category".
 *
 * @property string $id
 * @property string $name
 * @property string $icon
 * @property string $url
 */
class FaqCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'icon', 'url'], 'required'],
            [['name', 'icon', 'url'], 'string', 'max' => 64],
            [['url'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'icon' => Yii::t('app', 'Icon'),
            'url' => Yii::t('app', 'Url'),
        ];
    }
}
