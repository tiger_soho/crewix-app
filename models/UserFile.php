<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_file".
 *
 * @property string $id
 * @property string $owner_id
 * @property string $size
 * @property string $src
 * @property string $created_at
 */
class UserFile extends \yii\db\ActiveRecord
{
    const IMAGE_SEAMAN_PATH = 'images/seaman/docs';
    public $doc_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'size', 'src'], 'required'],
            [['owner_id', 'size'], 'integer'],
            [['created_at'], 'safe'],
            [['src'], 'string', 'max' => 255],
        ];
    }

    public function addScan($id,$post)
    {
        $arr = [
            'owner_id'=>$id,
            'src'=>$post['src'],
            'size'=>0
        ];
        return Yii::$app->db->createCommand()->insert('user_file',$arr)->execute();
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'doc_id';

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'size' => 'Size',
            'src' => 'Src',
            'created_at' => 'Created At',
        ];
    }
}
