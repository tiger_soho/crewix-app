<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clothing_size".
 *
 * @property integer $id
 * @property integer $sex_id
 * @property string $size
 */
class Cloth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clothing_size';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sex_id', 'size'], 'required'],
            [['sex_id'], 'integer'],
            [['size'], 'string', 'max' => 20],
        ];
    }

    public function getRow($sex)
    {
        return static::find()->where(['sex_id'=>strtolower($sex)])->asArray()->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sex_id' => Yii::t('app', 'Sex ID'),
            'size' => Yii::t('app', 'Size'),
        ];
    }
}
