<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $alias
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['alias'], 'string', 'max' => 255],
        ];
    }
    public function saveData($id)
    {
        return Yii::$app->db->createCommand()->insert('profile',['user_id'=>$id])->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'alias' => Yii::t('app', 'Alias'),
        ];
    }
}
