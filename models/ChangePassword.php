<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class ChangePassword extends Model
{
    public $id;
    public $current;
    public $new;
    public $new2;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id','current', 'new'], 'required'],
            ['new', 'string', 'min'=>6],
            ['new', 'string', 'max'=>12],
            ['new2', 'compare', 'compareAttribute' => 'new', 'message' => "Passwords doesn't match"],
            // password is validated by validatePassword()
            ['current', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'current' => 'Current password',
            'new' => 'New password',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->current)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    public function comparePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!($this->new === $this->repeat)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function change()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->setPassword($this->new);
           // $user->removePasswordResetToken();
            return $user->save();
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne($this->id);
        }

        return $this->_user;
    }
}