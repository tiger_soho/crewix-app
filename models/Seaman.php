<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "seaman".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property integer $primary_rank_id
 * @property integer $secondary_rank_id
 * @property integer $status
 * @property string $availability_date
 * @property integer $salary
 * @property integer $salary_currency_id
 * @property string $date_of_birth
 * @property integer $place_of_birth_id
 * @property string $city_of_residence
 * @property integer $country_of_residence_id
 * @property string $address
 * @property integer $citizenship_id
 * @property string $closest_airport
 * @property string $primary_phone
 * @property string $secondary_phone
 * @property string $skype
 * @property string $notes
 * @property integer $profile_picture
 * @property integer $profile_picture_thumb
 * @property integer $cv_picture
 */
class Seaman extends ActiveRecord
{
    public $link;
    public $to;
    public $vessel_type_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'user_id',
                    'primary_rank_id',
                    'status',
                    'salary',
                    'place_of_birth_id',
                    'date_of_birth',
                    'country_of_residence_id',
                    'city_of_residence',
                    'first_name',
                    'last_name',
                    'address',
                    'closest_airport',
                    'citizenship_id',
                    'primary_phone'
                ],
                'required'
            ],
            [
                ['date_of_birth'],
                function ($model) {

                    $bd = strtotime($this->date_of_birth);
                    if (strtotime('+16 year', $bd) > time()) {
                        $this->addError('date_of_birth', 'You must be 16 years old at least');
                    }

                }
            ],
            [['first_name', 'middle_name', 'last_name'], 'match', 'pattern' => '/^[a-zA-Z.]+$/'],
            [['user_id', 'primary_rank_id', 'secondary_rank_id', 'status', 'salary', 'salary_currency_id',
                'place_of_birth_id', 'country_of_residence_id', 'citizenship_id'], 'integer'],
            [['availability_date', 'date_of_birth'], 'safe'],
            [['first_name', 'middle_name', 'last_name', 'city_of_residence', 'closest_airport'], 'string', 'max' => 128],
            [['address'], 'string', 'max' => 255],
            [['primary_phone', 'secondary_phone'], 'string', 'max' => 32],
            [['skype'], 'string', 'max' => 64],
            [['notes'], 'string', 'max' => 512],
        ];
    }

    public function saveData($uid,$post,$photo,$cv)
    {
        $bdate = explode('.', $post['date_of_birth']);
        $bdate = $bdate[2].'-'.$bdate[1].'-'.$bdate[0];
        $arr = ['user_id'=>$uid,
                'first_name'=>$post['first_name'],
                'last_name'=>$post['last_name'],
                'address'=>$post['address'],
                'primary_rank_id'=>$post['primary_rank_id'],
                'secondary_rank_id'=>$post['secondary_rank_id'],
                'status'=>$post['status'],
                'salary'=>$post['salary']*30,
                'salary_currency_id'=>$post['salary_currency_id'],
                'date_of_birth'=>$bdate,
                'place_of_birth_id'=>$post['place_of_birth_id'],
                'country_of_residence_id'=>$post['country_of_residence_id'],
                'city_of_residence'=>$post['city_of_residence'],
                'citizenship_id'=>$post['citizenship_id'],
                'closest_airport'=>$post['closest_airport'],
                'primary_phone'=>$post['primary_phone'],
                'skype'=>$post['skype'],
                ];
        if($post['middle_name']&&!empty($post['middle_name'])){
            $arr['middle_name'] = $post['middle_name'];
        }
        if($post['secondary_phone']&&!empty($post['secondary_phone'])){
            $arr['secondary_phone'] = $post['secondary_phone'];
        }
        if (!empty($post['availability_date'])) {
            $adate = explode('.', $post['availability_date']);
            $adate = $adate[2].'-'.$adate[1].'-'.$adate[0];
            $arr['availability_date'] = $adate;
        }
        if(!empty($photo)){
            $arr['profile_picture'] = $photo;
        }
        if(!empty($cv)){
            $arr['cv_picture'] = $cv;
        }
        return Yii::$app->db->createCommand()->insert('seaman',$arr)->execute();
    }

    public function editData($uid,$post,$photo,$cv)
    {
        $bdate = explode('.', $post['date_of_birth']);
        $bdate = $bdate[2].'-'.$bdate[1].'-'.$bdate[0];
        $arr = ['first_name'=>$post['first_name'],
                'last_name'=>$post['last_name'],
                'address'=>$post['address'],
                'primary_rank_id'=>$post['primary_rank_id'],
                'secondary_rank_id'=>$post['secondary_rank_id'],
                'status'=>$post['status'],
                'salary'=>$post['salary'],
                'salary_currency_id'=>$post['salary_currency_id'],
                'date_of_birth'=>$bdate,
                'place_of_birth_id'=>$post['place_of_birth_id'],
                'country_of_residence_id'=>$post['country_of_residence_id'],
                'city_of_residence'=>$post['city_of_residence'],
                'citizenship_id'=>$post['citizenship_id'],
                'closest_airport'=>$post['closest_airport'],
                'primary_phone'=>$post['primary_phone'],
                'skype'=>$post['skype'],
                ];
        if($post['middle_name']&&!empty($post['middle_name'])){
            $arr['middle_name'] = $post['middle_name'];
        }
        if($post['secondary_phone']&&!empty($post['secondary_phone'])){
            $arr['secondary_phone'] = $post['secondary_phone'];
        }
        if(!empty($post['avail_date'])){
            $adate = explode('.', $post['avail_date']);
            $adate = $adate[2].'-'.$adate[1].'-'.$adate[0];
            $arr['avail_date'] = $adate;
        }
        if(!empty($photo)){
            $arr['profile_picture'] = $photo;
        }
        if(!empty($cv)){
            $arr['cv_picture'] = $cv;
        }

        return Yii::$app->db->createCommand()->update('seaman',$arr,['user_id'=>$uid])->execute();
    }



    public function getAll()
    {
        return static::find()->joinWith('user')->where(['user.registration_step'=>null])->orderBy(['user.signup_date'=>SORT_DESC]);
    }

    public function getUser()
    {
        return $this->hasOne('app\models\Users', ['id' => 'user_id']);
    }

    public function getBiometrics()
    {
        return $this->hasOne('app\models\Biometrics', ['seaman_id' => 'id']);
    }

    public function getCompetency()
    {
        return $this->hasMany('app\models\SeamanCertificateOfCompetency', ['seaman_id' => 'id']);
    }

    public function getEducation()
    {
        return $this->hasMany('app\models\Education', ['seaman_id' => 'id']);
    }

    public function getFamily()
    {
        return $this->hasOne('app\models\Family', ['seaman_id' => 'id']);
    }

    public function getLanguages()
    {
        return $this->hasMany(SeamanLanguage::className(), ['seaman_id' => 'id']);
    }

    public function getMedical()
    {
        return $this->hasMany('app\models\SeamanMedicalDocument', ['seaman_id' => 'id']);
    }

    public function getProficiency()
    {
        return $this->hasMany('app\models\SeamanCertificateOfProficiency', ['seaman_id' => 'id']);
    }

    public function getReferences()
    {
        return $this->hasMany('app\models\SeamanReference', ['seaman_id' => 'id']);
    }

    public function getService()
    {
        return $this->hasMany('app\models\SeamanSeaService', ['seaman_id' => 'id']);
    }

    public function getTravelDocs()
    {
        return $this->hasMany('app\models\SeamanTravelDocument', ['seaman_id' => 'id']);
    }

    public function getNok()
    {
        return $this->hasOne('app\models\SeamanNextOfKin', ['seaman_id' => 'id']);
    }

    public function getPrimaryRank()
    {
        return $this->hasOne(Ranks::className(), ['id' => 'primary_rank_id']);
    }

    public function getSecondaryRank()
    {
        return $this->hasOne(Ranks::className(), ['id' => 'secondary_rank_id']);
    }

    public function Filter($post)
    {
        $is_where = false;
        $arr="";
        //var_dump($post);exit();

        if (isset($post['rank']) && !empty($post['rank'])) {
            $arr .= $is_where == false ? '' : '';
            $is_where = $is_where == false ? true : false;
            $arr .= " `seaman`.`primary_rank_id`=".$post['rank'];
        }
        if (isset($post['citizenship_id']) && !empty($post['citizenship_id'])) {
            $arr .= $is_where == false ? '' : ' AND';
            $is_where = $is_where == false ? true : false;
            $arr .= " `seaman`.`citizenship_id`=" . $post['citizenship_id'];
        }
        if (isset($post['avail']) && !empty($post['avail'])) {
            $arr .= $is_where == false ? '' : ' AND';
            $is_where = $is_where == false ? true : false;
            $arr .= " `seaman`.`status`=".$post['avail'];
        }
        if (isset($post['age_from']) && !empty($post['age_from'])) {
            $arr .= $is_where == false ? '' : ' AND';
            $is_where = $is_where == false ? true : false;

            $time = time() - ($post['age_from']*366*24*60*60);
            $date = date("Y-m-d",$time);

            $arr .= " `seaman`.`date_of_birth` < '{$date}'";
        }
        if (isset($post['age_to']) && !empty($post['age_to'])) {
            $arr .= $is_where == false ? '' : ' AND';
            $is_where = $is_where == false ? true : false;
            $time = time() - ($post['age_to']*366*24*60*60);
            $date = date("Y-m-d",$time);
            $arr .= " `seaman`.`date_of_birth` > '{$date}'";
        }


        if (isset($post['sal']) && !empty($post['sal'])) {
            $arr .= $is_where == false ? '' : ' AND';
            $is_where = $is_where == false ? true : false;
            $arr .= " `seaman`.`salary`=".($post['sal']/$post['period']*30)." AND `seaman`.`salary_currency_id` = '1'";
        }
        if (isset($post['vessel']) && !empty($post['vessel'])) {
            $arr .= $is_where == false ? '' : ' AND';
            $is_where = $is_where == false ? true : false;
            $arr .= " `service`.`vessel_type_id`=".$post['vessel'];
        }
        if (isset($post['lang']) && !empty($post['lang'])) {
            $arr .= $is_where == false ? '' : ' AND';
            $is_where = $is_where == false ? true : false;
            $arr .= " `languages`.`language_level_id`=".$post['lang'];
        }


        return static::find()->joinWith('service')->joinWith(['languages'])->where($arr);
    }


    public static function getOne($id)
    {
        return static::find()->joinWith('biometrics')->joinWith('family')->joinWith('competency')->joinWith('travelDocs')
            ->joinWith('education')->joinWith('languages')->joinWith('medical')->joinWith('references')->joinWith('service')
            ->joinWith('nok')->joinWith('proficiency')->where(['seaman.id'=>$id])->one();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'primary_rank_id' => Yii::t('app', 'Primary Rank ID'),
            'secondary_rank_id' => Yii::t('app', 'Secondary Rank ID'),
            'status' => Yii::t('app', 'Status'),
            'availability_date' => Yii::t('app', 'Availability Date'),
            'salary' => Yii::t('app', 'Salary'),
            'salary_currency_id' => Yii::t('app', 'Salary Currency ID'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'place_of_birth_id' => Yii::t('app', 'Place Of Birth ID'),
            'city_of_residence' => Yii::t('app', 'City Of Residence'),
            'country_of_residence_id' => Yii::t('app', 'Country Of Residence ID'),
            'address' => Yii::t('app', 'Address'),
            'citizenship_id' => Yii::t('app', 'Citizenship ID'),
            'closest_airport' => Yii::t('app', 'Closest Airport'),
            'primary_phone' => Yii::t('app', 'Primary Phone'),
            'secondary_phone' => Yii::t('app', 'Secondary Phone'),
            'skype' => Yii::t('app', 'Skype'),
            'notes' => Yii::t('app', 'Notes'),
            'profile_picture' => Yii::t('app', 'Profile Picture'),
            'profile_picture_thumb' => Yii::t('app', 'Profile Picture Thumb'),
            'cv_picture' => Yii::t('app', 'Cv Picture'),
        ];
    }
}
