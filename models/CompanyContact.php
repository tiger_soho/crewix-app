<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_contact".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $hq_country_id
 * @property string $hq_city
 * @property string $hq_address
 * @property string $email
 * @property string $phone
 * @property string $website
 * @property string $skype
 */
class CompanyContact extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'company_contact';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['company_id', 'hq_country_id', 'hq_city', 'hq_address'], 'required'],
			[['company_id', 'hq_country_id'], 'integer'],
			[['hq_city', 'website'], 'string', 'max' => 128],
			[['hq_address', 'email'], 'string', 'max' => 255],
			[['phone'], 'string', 'max' => 32],
			[['skype'], 'string', 'max' => 64],
		];
	}

	public function getCountries()
	{
		return $this->hasOne('app\models\Countries', ['id' => 'hq_country_id']);
	}

	public function saveData($uid,$post)
	{
		$arr = ['company_id'=>$uid,
				'hq_country_id'=>$post['hq_country_id'],];
		if(!empty($post['hq_city'])){
			$arr['hq_city']=$post['hq_city'];
		}
		if(!empty($post['hq_address'])){
			$arr['hq_address']=$post['hq_address'];
		}
		return Yii::$app->db->createCommand()->insert('company_contact',$arr)->execute();
	}

	public function saveContacts($uid,$post)
	{
		$arr = [];
		if(!empty($post['email'])){
			$arr['email'] = $post['email'];
		}
		if(!empty($post['phone'])){
			$arr['phone'] = $post['phone'];
		}
		if(!empty($post['skype'])){
			$arr['skype'] = $post['skype'];
		}
		if(!empty($post['website'])){
			$arr['website'] = $post['website'];
		}
		if(!empty($arr)){
			return Yii::$app->db->createCommand()->update('company_contact',$arr,['company_id'=>$uid])->execute();
		}
	}

	public function editContacts($uid,$post)
	{
		//var_dump($post);exit;
		$arr = ['hq_country_id'=>$post['hq_country_id'],];
		if(!empty($post['hq_city'])){
			$arr['hq_city']=$post['hq_city'];
		}
		if(!empty($post['hq_address'])){
			$arr['hq_address']=$post['hq_address'];
		}
		if(!empty($post['email'])){
			$arr['email'] = $post['email'];
		}
		if(!empty($post['phone'])){
			$arr['phone'] = $post['phone'];
		}
		if(!empty($post['skype'])){
			$arr['skype'] = $post['skype'];
		}
		if(!empty($post['website'])){
			$arr['website'] = $post['website'];
		}
		return Yii::$app->db->createCommand()->update('company_contact',$arr,['company_id'=>$uid])->execute();
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'company_id' => Yii::t('app', 'Company ID'),
			'hq_country_id' => Yii::t('app', 'Hq Country ID'),
			'hq_city' => Yii::t('app', 'Hq City'),
			'hq_address' => Yii::t('app', 'Hq Address'),
			'email' => Yii::t('app', 'Email'),
			'phone' => Yii::t('app', 'Phone'),
			'website' => Yii::t('app', 'Website'),
			'skype' => Yii::t('app', 'Skype'),
		];
	}
}
