<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "seaman_travel_document".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $document_id
 * @property string $number
 * @property integer $place_of_issue_id
 * @property string $date_of_issue
 * @property string $date_of_expiry
 * @property integer $scan_id
 */
class SeamanTravelDocument extends \yii\db\ActiveRecord
{
    public $scan;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_travel_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_id', 'place_of_issue_id', 'scan_id'], 'integer'],
            // [['date_of_issue', 'date_of_expiry'], 'safe'],
            [
                'date_of_expiry',
                function ($model) {
                    if (strtotime($this->date_of_expiry) < strtotime($this->date_of_issue)) {
                        $this->addError('date_of_expiry', 'Please give correct Dates');
                    }
                }
            ],
            [['number'], 'match', 'pattern' => '/^[a-zA-Z0-9_\-\/.]+$/'],
            [['number'], 'string', 'max' => 128],
        ];
    }

    /* public function validateDate($attribute, $params){
         VarDumper::dump($this);die;
         if(strtotime($this->date_of_expiry) <= strtotime($this->date_of_issue)){
             $this->addError('date_of_expiry','Please give correct Dates');
             return false;
         }
         return true;
     }*/
    public function saveDoc($uid,$post)
    {   
        $issue = explode('.', $post['date_of_issue']);
        $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
        if(!empty($post['date_of_expiry'])){
            $expiry = explode('.', $post['date_of_expiry']);
            $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
        }else{
            $expiry = '0000-00-00';
        }
        
        $arr = ['seaman_id'=>$uid,
                'document_id'=>$post['document_id'],
                'number'=>$post['number'],
                'place_of_issue_id'=>$post['place_of_issue_id'],
                'date_of_issue'=>$issue,
                'date_of_expiry'=>$expiry,
                ];
        return Yii::$app->db->createCommand()->insert('seaman_travel_document',$arr)->execute();
    }

    public function getDocType()
    {
        return $this->hasOne(TravelDocument::className(),['id'=>'document_id']);
    }

    public static function getAllByScan($id)
    {
        return static::find()->joinWith('docType')->where(['seaman_travel_document.scan_id'=>$id])->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'document_id' => Yii::t('app', 'Document ID'),
            'number' => Yii::t('app', 'Number'),
            'place_of_issue_id' => Yii::t('app', 'Place Of Issue ID'),
            'date_of_issue' => Yii::t('app', 'Date Of Issue'),
            'date_of_expiry' => Yii::t('app', 'Date Of Expiry'),
            'scan_id' => Yii::t('app', 'Scan ID'),
        ];
    }
}
