<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "documents".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $doc_id
 * @property integer $number
 * @property integer $country_id
 * @property string $issue
 * @property string $expiry
 */
class Documents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'doc_id', 'number', 'country_id', 'issue', 'expiry'], 'required'],
            [['uid', 'doc_id', 'number', 'country_id'], 'integer'],
            [['issue', 'expiry'], 'safe'],
        ];
    }

    public function saveDics($uid, $post)
    {
        $arr = ['uid'=>$uid,
                'doc_id'=>$post['doc_id'],
                'number'=>$post['number'],
                'country_id'=>$post['country_id'],
                'issue'=>$post['issue'],
                'expiry'=>$post['expiry'],
                ];
        return Yii::$app->db->createCommand()->insert('documents',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'doc_id' => Yii::t('app', 'Doc ID'),
            'number' => Yii::t('app', 'Number'),
            'country_id' => Yii::t('app', 'Country ID'),
            'issue' => Yii::t('app', 'Issue'),
            'expiry' => Yii::t('app', 'Expiry'),
        ];
    }
}
