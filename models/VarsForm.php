<?php
/**
 * Created by PhpStorm.
 * User: Кирилл
 * Date: 02.03.2016
 * Time: 16:49
 */

namespace app\models;

use Yii;

class VarsForm extends \yii\db\ActiveRecord
{
    public $from_currency_id;
    public $to_currency_id;
    public $value;
    public $updated_at;
    public $is_indexed;
    public $name;
    public $url;
    public $meta_description;
    public $type;

    public function rules()
    {
        return [
            [['from_currency_id', 'to_currency_id'], 'integer'],
            [['from_currency_id', 'to_currency_id', 'value'], 'required'],
            [['value'], 'number'],
            [['updated_at'], 'safe'],
            [['is_indexed'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['url', 'title'], 'string', 'max' => 128],
            [['meta_description'], 'string', 'max' => 150]
        ];
    }


}