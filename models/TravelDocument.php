<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "travel_document".
 *
 * @property integer $id
 * @property string $name
 * @property integer $always_visible
 * @property integer $locked
 * @property integer $position
 */
class TravelDocument extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'travel_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['name'], 'required'],
            //['name', 'each', 'rule' => ['unique']],
            [['always_visible', 'locked', 'position'], 'integer'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'always_visible' => Yii::t('app', 'Always Visible'),
            'locked' => Yii::t('app', 'Locked'),
            'position' => Yii::t('app', 'Position'),
        ];
    }
}
