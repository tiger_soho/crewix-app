<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vessels".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $name
 * @property integer $type
 * @property integer $me
 * @property integer $power
 * @property string $measure
 * @property integer $flag
 * @property integer $dwt
 * @property integer $year
 */
class Vessels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vessels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'name', 'type', 'me', 'power', 'measure', 'flag', 'dwt', 'year'], 'required'],
            [['uid', 'type', 'me', 'power', 'flag', 'dwt', 'year'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['measure'], 'string', 'max' => 5],
        ];
    }

    public function getVessels($id)
    {
        return static::find()->where(['uid'=>$id])->orderBy(['name'=>SORT_ASC])->all();
    }

    public function saveVessel($uid,$post)
    {
        $arr = [
            'uid' => $uid,
            'name'=>$post['vessel_name'],
            'type'=>$post['vessel_type'],
            'flag'=>$post['vessel_flag'],
            'dwt'=>$post['dwt'],
        ];
        if(!empty($post['vessel_year'])){
            $arr['year'] = $post['vessel_year'];
        }
        if(!empty($post['me_type'])){
            $arr['me'] = $post['me_type'];
        }
        if(!empty($post['me_power'])){
            $arr['power'] = $post['me_power'];
        }
        if(!empty($post['pow_mesure'])){
            $arr['measure'] = $post['pow_mesure'];
        }
        if(!empty($post['shipowner'])){
            $arr['shipowner'] = $post['shipowner'];
        }

        return Yii::$app->db->createCommand()->insert('vessels',$arr)->execute(); 
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'me' => Yii::t('app', 'Me'),
            'power' => Yii::t('app', 'Power'),
            'measure' => Yii::t('app', 'Measure'),
            'flag' => Yii::t('app', 'Flag'),
            'dwt' => Yii::t('app', 'Dwt'),
            'year' => Yii::t('app', 'Year'),
        ];
    }
}
