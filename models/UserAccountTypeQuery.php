<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserAccountType]].
 *
 * @see UserAccountType
 */
class UserAccountTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserAccountType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserAccountType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
