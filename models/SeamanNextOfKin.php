<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_next_of_kin".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property string $name
 * @property integer $relationship_id
 * @property string $phone
 * @property string $address
 */
class SeamanNextOfKin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_next_of_kin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seaman_id', 'name', 'relationship_id', 'address','phone'], 'required'],
            [['seaman_id', 'relationship_id'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 32],
        ];
    }

    public function saveKin($uid,$post)
    {
        $arr = ['seaman_id'=>$uid,
                'name'=>$post['name'],
                'phone'=>$post['phone'],
                'relationship_id'=>$post['relationship_id'],
                'address'=>$post['address'],
                ];
        return Yii::$app->db->createCommand()->insert('seaman_next_of_kin',$arr)->execute();
    }

    public function editKin($uid,$post)
    {
        $arr = ['name'=>$post['name'],
            'phone'=>$post['phone'],
            'relationship_id'=>$post['relationship_id'],
            'address'=>$post['address'],
        ];
        return Yii::$app->db->createCommand()->update('seaman_next_of_kin',$arr,['seaman_id'=>$uid])->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'name' => Yii::t('app', 'Name'),
            'relationship_id' => Yii::t('app', 'Relationship ID'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
        ];
    }
}
