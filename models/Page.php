<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property string $id
 * @property string $name
 * @property string $url
 * @property string $title
 * @property string $meta_description
 * @property integer $is_indexed
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['is_indexed'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['url', 'title'], 'string', 'max' => 128],
            [['meta_description'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'title' => Yii::t('app', 'Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'is_indexed' => Yii::t('app', 'Is Indexed'),
        ];
    }
}
