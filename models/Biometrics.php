<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "seaman_biometrics".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $sex_id
 * @property integer $eyes_color_id
 * @property integer $height
 * @property integer $weight
 * @property integer $clothing_size_id
 * @property integer $shoe_size_id
 */
class Biometrics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_biometrics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sex_id', 'eyes_color_id', 'height', 'weight', 'clothing_size_id', 'shoe_size_id'], 'required'],
            [['seaman_id', 'sex_id', 'eyes_color_id', 'height', 'weight', 'clothing_size_id', 'shoe_size_id'], 'integer'],
            [['height'] , 'number', 'min' => 50, 'max'=>254],
            [['weight'] , 'number', 'min' => 5, 'max'=>254],
        ];
    }

    public function saveBio($uid,$post)
    {
        $arr = ['seaman_id'=>$uid,
                'sex_id'=>$post['sex_id'],
                'eyes_color_id'=>$post['eyes_color_id'],
                'height'=>$post['height'],
                'weight'=>$post['weight'],
                'clothing_size_id'=>$post['clothing_size_id'],
                'shoe_size_id'=>$post['shoe_size_id'],
                ];
        return Yii::$app->db->createCommand()->insert('seaman_biometrics',$arr)->execute();
    }

    public function editBio($uid,$post)
    {
        $arr = ['sex_id'=>$post['sex_id'],
                'eyes_color_id'=>$post['eyes_color_id'],
                'height'=>$post['height'],
                'weight'=>$post['weight'],
                'clothing_size_id'=>$post['clothing_size_id'],
                'shoe_size_id'=>$post['shoe_size_id'],
                ];
        return Yii::$app->db->createCommand()->update('seaman_biometrics',$arr,['seaman_id'=>$uid])->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'sex_id' => Yii::t('app', 'Sex ID'),
            'eyes_color_id' => Yii::t('app', 'Eyes Color ID'),
            'height' => Yii::t('app', 'Height'),
            'weight' => Yii::t('app', 'Weight'),
            'clothing_size_id' => Yii::t('app', 'Clothing Size ID'),
            'shoe_size_id' => Yii::t('app', 'Shoe Size ID'),
        ];
    }
}
