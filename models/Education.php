<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_education".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property string $institution_name
 * @property string $degree
 * @property string $year_from
 * @property string $year_to
 */
class Education extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_education';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seaman_id'], 'integer'],
            [
                ['year_to'],
                function ($model) {
                    if ($this->year_to < $this->year_from) {
                        $this->addError('year_to', 'Please give correct Dates');
                    }
                }
            ],
            [['year_from', 'year_to','institution_name','degree'], 'safe'],
            [['institution_name'], 'string', 'max' => 128],
            [['degree'], 'string', 'max' => 255],
        ];
    }

    public function saveEdu($uid,$post)
    {
        $arr = ['seaman_id'=>$uid,
                'institution_name'=>$post['institution_name'],
                'degree'=>$post['degree'],
                'year_from'=>$post['year_from'],
                'year_to'=>$post['year_to'],
                ];
        return Yii::$app->db->createCommand()->insert('seaman_education',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'degree' => Yii::t('app', 'Degree'),
            'year_from' => Yii::t('app', 'Year From'),
            'year_to' => Yii::t('app', 'Year To'),
        ];
    }
}
