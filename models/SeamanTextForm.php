<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "seaman".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property integer $primary_rank_id
 * @property integer $secondary_rank_id
 * @property integer $status
 * @property string $availability_date
 * @property integer $salary
 * @property integer $salary_currency_id
 * @property string $date_of_birth
 * @property integer $place_of_birth_id
 * @property string $city_of_residence
 * @property integer $country_of_residence_id
 * @property string $address
 * @property integer $citizenship_id
 * @property string $closest_airport
 * @property string $primary_phone
 * @property string $secondary_phone
 * @property string $skype
 * @property string $notes
 * @property integer $profile_picture
 * @property integer $profile_picture_thumb
 * @property integer $cv_picture
 */
class SeamanTextForm extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cv'], 'string', 'max' => 2048],
            [['notes'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'primary_rank_id' => Yii::t('app', 'Primary Rank'),
            'secondary_rank_id' => Yii::t('app', 'Secondary Rank'),
            'status' => Yii::t('app', 'Status'),
            'availability_date' => Yii::t('app', 'Availability Date'),
            'salary' => Yii::t('app', 'Salary'),
            'salary_currency_id' => Yii::t('app', 'Salary Currency'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'place_of_birth_id' => Yii::t('app', 'Place Of Birth'),
            'city_of_residence' => Yii::t('app', 'City Of Residence'),
            'country_of_residence_id' => Yii::t('app', 'Country Of Residence'),
            'address' => Yii::t('app', 'Address'),
            'citizenship_id' => Yii::t('app', 'Citizenship'),
            'closest_airport' => Yii::t('app', 'Closest Airport'),
            'primary_phone' => Yii::t('app', 'Primary Phone'),
            'secondary_phone' => Yii::t('app', 'Secondary Phone'),
            'skype' => Yii::t('app', 'Skype'),
            'notes' => Yii::t('app', 'Notes'),
            'profile_picture' => Yii::t('app', 'Profile Picture'),
            'profile_picture_thumb' => Yii::t('app', 'Profile Picture Thumb'),
            'cv_picture' => Yii::t('app', 'Cv Picture'),
        ];
    }
}
