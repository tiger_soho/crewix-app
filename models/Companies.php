<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property integer $type_id
 * @property string $about
 * @property integer $profile_picture
 * @property integer $profile_picture_thumb
 */
class Companies extends \yii\db\ActiveRecord
{

    public $hq_country_id;
    public $hq_city;
    public $hq_address;
    public $website;
    public $email;
    public $phone;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','type_id','hq_country_id','hq_city','hq_address'], 'required'],
            [['user_id', 'type_id'], 'integer'],
            [['about'], 'string'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    

    public function getCompanyContact()
    {
        return $this->hasOne('app\models\CompanyContact', ['company_id' => 'id']);
    }
    public function getCompanyType()
    {
        return $this->hasOne('app\models\CompanyType', ['id' => 'type_id']);
    }
    public function getCompanyVessel()
    {
        return $this->hasMany('app\models\CompanyVessel', ['company_id' => 'id']);
    }
    public function getUser()
    {
        return $this->hasOne('app\models\Users', ['id' => 'user_id']);
    }

    public function saveData($uid, $post, $logo)
    {
        $arr = ['user_id'=>$uid,
                'name'=>$post['name'],
                'type_id'=>$post['type_id'],
                ];
        if(!empty($post['about'])){
            $arr['about'] = $post['about'];
        }
        if(!empty($logo)){
            $arr['profile_picture'] = $logo;
        }

        return Yii::$app->db->createCommand()->insert('company',$arr)->execute();
    }

    public function getAll()
    {
        return static::find()->joinWith('companyContact')->joinWith('user')->where(['user.registration_step'=>null])->orderBy(['company.id'=>SORT_DESC]);
    }

    public static function getOne($id)
    {
        return static::find()->joinWith('companyContact')->joinWith('companyType')->joinWith('companyVessel')->where(['company.user_id'=>$id])->one();
    }

    public function editAbout($uid,$post)
    {
        if(!empty($post['about'])){
            return Yii::$app->db->createCommand()->update('company',['about'=>$post['about']],['user_id'=>$uid])->execute();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'type_id' => Yii::t('app', 'Type ID'),
            'about' => Yii::t('app', 'About'),
            'profile_picture' => Yii::t('app', 'Profile Picture'),
            'profile_picture_thumb' => Yii::t('app', 'Profile Picture Thumb'),
        ];
    }
}
