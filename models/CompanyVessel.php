<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_vessel".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $vessel_name
 * @property integer $imo_number
 * @property integer $vessel_flag_id
 * @property integer $vessel_type_id
 * @property string $deadweight
 * @property integer $engine_type_id
 * @property integer $engine_power
 * @property string $year_built
 * @property integer $shipowning_company_id
 * @property integer $shipowning_company_name
 */
class CompanyVessel extends \yii\db\ActiveRecord
{
    public $measure;
    public $vessel_engine_power;
    public $vessel_deadweight;
    public $vessel_engine_type_id;
    public $vessel_shipowning_company_name;
    public $vessel_year_built;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_vessel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'vessel_name', 'vessel_flag_id', 'vessel_type_id', 'deadweight', 'engine_type_id'], 'required'],
            [['company_id', 'imo_number', 'vessel_flag_id', 'vessel_type_id', 'deadweight', 'engine_type_id', 'engine_power', 'shipowning_company_id'], 'integer'],
            [['year_built', 'vessel_engine_power'], 'safe'],
            [['vessel_name'], 'string', 'max' => 64],
        ];
    }

    public function getVessels($id)
    {
        return static::find()->where(['company_id'=>$id])->orderBy(['vessel_name'=>SORT_ASC])->all();
    }

    public function saveVessel($uid,$post)
    {
        $arr = [
            'company_id' => $uid,
            'vessel_name'=>$post['vessel_name'],
            'vessel_type_id'=>$post['vessel_type_id'],
            'vessel_flag_id'=>$post['vessel_flag_id'],
            'deadweight'=>$post['vessel_deadweight'],
        ];
        if(!empty($post['year_built'])){
            $arr['year_built'] = $post['year_built'];
        }
        if(!empty($post['vessel_engine_type_id'])){
            $arr['engine_type_id'] = $post['vessel_engine_type_id'];
        }
        if(!empty($post['me_power'])){
            $arr['engine_power'] = $post['engine_power'];
        }
        if(!empty($post['shipowning_company_id'])){
            $arr['shipowning_company_id'] = $post['shipowning_company_id'];
        }
        if(!empty($post['shipowning_company_name'])){
            $arr['shipowning_company_name'] = $post['shipowning_company_name'];
        }

        return Yii::$app->db->createCommand()->insert('company_vessel',$arr)->execute(); 
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'vessel_name' => Yii::t('app', 'Vessel Name'),
            'imo_number' => Yii::t('app', 'Imo Number'),
            'vessel_flag_id' => Yii::t('app', 'Vessel Flag ID'),
            'vessel_type_id' => Yii::t('app', 'Vessel Type ID'),
            'deadweight' => Yii::t('app', 'Deadweight'),
            'engine_type_id' => Yii::t('app', 'Engine Type ID'),
            'engine_power' => Yii::t('app', 'Engine Power'),
            'vessel_engine_power' => Yii::t('app', 'Engine Power'),
            'year_built' => Yii::t('app', 'Year Built'),
            'shipowning_company_id' => Yii::t('app', 'Shipowning Company ID'),
            'shipowning_company_name' => Yii::t('app', 'Shipowning Company Name'),
        ];
    }

    public function afterFind()
    {
        $this->vessel_engine_power = $this->engine_power;
        $this->vessel_deadweight = $this->deadweight;
        $this->vessel_engine_type_id = $this->engine_type_id;
        $this->vessel_shipowning_company_name = $this->shipowning_company_name;
        $this->vessel_year_built = $this->year_built;
        parent::afterFind();
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'vessel_engine_power';
        $fields[] = 'vessel_deadweight';
        $fields[] = 'vessel_engine_type_id';
        $fields[] = 'vessel_shipowning_company_name';
        $fields[] = 'vessel_year_built';

        return $fields;
    }
}
