<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_certificate_of_proficiency".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $document_id
 * @property string $number
 * @property integer $place_of_issue_id
 * @property string $date_of_issue
 * @property string $date_of_expiry
 * @property integer $scan_id
 */
class SeamanCertificateOfProficiency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_certificate_of_proficiency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['seaman_id', 'document_id', 'number', 'place_of_issue_id', 'date_of_issue'], 'required'],
            [['seaman_id', 'document_id', 'place_of_issue_id', 'scan_id'], 'integer'],
            [['date_of_issue', 'date_of_expiry'], 'safe'],
            [
                ['date_of_expiry'],
                function () {
                    if (strtotime($this->date_of_expiry) <= strtotime($this->date_of_issue)) {
                        $this->addError('date_of_expiry', 'Please give correct Dates');
                        $this->addError('date_of_issue', 'Please give correct Dates');
                        return false;
                    }
                }
            ],
            [['number'], 'match', 'pattern' => '/^[a-zA-Z0-9._\-\/]+$/'],
            [['number'], 'string', 'max' => 128],
        ];
    }

    public function saveDoc($uid,$post)
    {
        $issue = explode('.', $post['date_of_issue']);
        $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
        $expiry = explode('.', $post['date_of_expiry']);
        $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
        $arr = [
            'seaman_id'=>$uid,
            'document_id'=>$post['document_id'],
            'number'=>$post['number'],
            'place_of_issue_id'=>$post['place_of_issue_id'],
            'date_of_issue'=>$issue,
            'date_of_expiry'=>$expiry,
        ];
        return Yii::$app->db->createCommand()->insert('seaman_certificate_of_proficiency',$arr)->execute();
    }

    public function getDocType()
    {
        return $this->hasOne(CertificateOfProficiency::className(),['id'=>'document_id']);
    }

    public static function getAllByScan($id)
    {
        return static::find()->joinWith('docType')->where(['seaman_certificate_of_proficiency.scan_id'=>$id])->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'document_id' => Yii::t('app', 'Document ID'),
            'number' => Yii::t('app', 'Number'),
            'place_of_issue_id' => Yii::t('app', 'Place Of Issue ID'),
            'date_of_issue' => Yii::t('app', 'Date Of Issue'),
            'date_of_expiry' => Yii::t('app', 'Date Of Expiry'),
            'scan_id' => Yii::t('app', 'Scan ID'),
        ];
    }
}
