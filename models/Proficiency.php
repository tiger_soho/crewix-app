<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "proficiency".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $doc_id
 * @property string $number
 * @property integer $country
 * @property string $issue
 * @property string $expiry
 */
class Proficiency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proficiency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['doc_id', 'each', 'rule' => ['required']],
            ['number', 'each', 'rule' => ['required']],
            ['country', 'each', 'rule' => ['required']],
            ['issue', 'each', 'rule' => ['required']],
            ['expiry', 'each', 'rule' => ['required']],
            ['doc_id', 'each', 'rule' => ['integer']],
            ['country', 'each', 'rule' => ['integer']],
            [['issue', 'expiry'], 'safe'],
            [['number'], 'string', 'max' => 64],
        ];
    }

    public function saveDoc($uid,$post)
    {
        $issue = explode('.', $post['issue']);
        $issue = $issue[2].'-'.$issue[1].'-'.$issue[0];
        $expiry = explode('.', $post['expiry']);
        $expiry = $expiry[2].'-'.$expiry[1].'-'.$expiry[0];
        $arr = [
            'uid'=>$uid,
            'doc_id'=>$post['doc_id'],
            'number'=>$post['number'],
            'country'=>$post['country'],
            'issue'=>$issue,
            'expiry'=>$expiry,
        ];
        return Yii::$app->db->createCommand()->insert('proficiency',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'doc_id' => Yii::t('app', 'Doc ID'),
            'number' => Yii::t('app', 'Number'),
            'country' => Yii::t('app', 'Country'),
            'issue' => Yii::t('app', 'Issue'),
            'expiry' => Yii::t('app', 'Expiry'),
        ];
    }
}
