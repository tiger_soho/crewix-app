<?php

namespace app\models;

use Yii;
use app\models\Companies;
/**
 * This is the model class for table "vacancy".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $rank_id
 * @property integer $salary_from
 * @property integer $salary_to
 * @property integer $salary_currency_id
 * @property integer $payroll_period
 * @property integer $duration_of_contract
 * @property integer $duration_of_contract_offset
 * @property string $joining_date_from
 * @property string $joining_date_to
 * @property integer $company_vessel_id
 * @property string $vessel_name
 * @property integer $vessel_flag_id
 * @property integer $vessel_type_id
 * @property integer $vessel_deadweight
 * @property integer $vessel_engine_type_id
 * @property integer $vessel_engine_power
 * @property string $vessel_year_built
 * @property integer $vessel_shipowning_company_id
 * @property string $vessel_shipowning_company_name
 * @property integer $required_age_from
 * @property integer $required_age_to
 * @property string $required_citizenship
 * @property string $required_language
 * @property integer $document_scans_required
 * @property integer $us_visa_required
 * @property integer $us_visa_required_type_id
 * @property integer $schengen_visa_required
 * @property string $views_count
 * @property string $created_at
 */
class Vacancies extends \yii\db\ActiveRecord
{

    public $dur_period;
    public $save_vessel;
    public $power_mesure;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vacancy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'rank_id',
                    'salary_currency_id',
                    'payroll_period',
                    'duration_of_contract',
                    'duration_of_contract_offset',
                    'joining_date_from',
                    'vessel_name',
                    'vessel_flag_id',
                    'vessel_type_id',
                    'vessel_deadweight',
                    'required_citizenship',
                    'required_language'
                ],
                'required'
            ],
            ['negotiable', 'boolean'],
            ['salary_from', 'required', 'when' => function ($model) {
                return $model->negotiable==false;
            }, 'whenClient' => "function (attribute, value) {
                return !$('#vacancies-negotiable').is(':checked');
            }"],
            ['salary_to', 'required', 'when' => function($model) {
                return $model->negotiable==false;
            }, 'whenClient' => "function (attribute, value) {
                return !$('#vacancies-negotiable').is(':checked');
            }"],
            [['company_id', 'rank_id', 'salary_from', 'salary_to', 'salary_currency_id', 'payroll_period', 'duration_of_contract', 'duration_of_contract_offset', 'company_vessel_id', 'vessel_flag_id', 'vessel_type_id', 'vessel_deadweight',
             'vessel_engine_type_id', 'vessel_engine_power', 'vessel_shipowning_company_id', 'required_age_from', 'required_age_to', 'document_scans_required', 'us_visa_required', 'us_visa_required_type_id', 'schengen_visa_required', 'views_count'], 'integer'],
            [['duration_of_contract'], 'integer', 'min' => 1, 'max' => 250],
            [['duration_of_contract_offset'], 'integer', 'min' => 1, 'max' => 250],
            [['joining_date_from', 'joining_date_to', 'vessel_year_built', 'created_at'], 'safe'],
            [['required_citizenship', 'required_language'], 'string'],
            [['vessel_name'], 'string', 'max' => 64],
            [['vessel_shipowning_company_name'], 'string', 'max' => 128],
        ];
    }


    /**
     * @save vacancy
     */

    public function saveVacancy($uid,$post)
    {
        $from = explode('.', $post['joining_date_from']);
        $from = $from[2].'-'.$from[1].'-'.$from[0];
        $to = explode('.', $post['joining_date_from']);
        $to = $to[2].'-'.$to[1].'-'.$to[0];
        $arr = [
            'company_id' => $uid,
            'rank_id'=>$post['rank_id'],
            'salary_currency_id'=>$post['salary_currency_id'],
            'payroll_period'=>$post['payroll_period'],
            'duration_of_contract'=>$post['duration_of_contract'],
            'duration_of_contract_offset'=>$post['duration_of_contract_offset'],
            'joining_date_from'=>$from,
            'joining_date_to'=>$to,
            'vessel_name'=>$post['vessel_name'],
            'vessel_type_id'=>$post['vessel_type_id'],
            'vessel_flag_id'=>$post['vessel_flag_id'],
            'vessel_deadweight'=>$post['vessel_deadweight'],
        ];
        if(!empty($post['vessel_year_built'])){
            $arr['vessel_year_built'] = $post['vessel_year_built'];
        }
        if(!empty($post['vessel_engine_type_id'])){
            $arr['vessel_engine_type_id'] = $post['vessel_engine_type_id'];
        }
        if(!empty($post['vessel_engine_power'])){
            $arr['vessel_engine_power'] = $post['vessel_engine_power'];
        }
        if(!empty($post['vessel_shipowning_company_name'])){
            $arr['vessel_shipowning_company_name'] = $post['vessel_shipowning_company_name'];
        }
        if(!empty($post['vessel_shipowning_company_name'])){
            $arr['vessel_shipowning_company_name'] = $post['vessel_shipowning_company_name'];
        }
        if(!empty($post['required_age_from'])){
            $arr['required_age_from'] = $post['required_age_from'];
        }
        if(!empty($post['required_age_to'])){
            $arr['required_age_to'] = $post['required_age_to'];
        }
        if(!empty($post['required_citizenship'])){
            $arr['required_citizenship'] = $post['required_citizenship'];
        }
        if(!empty($post['required_language'])){
            $arr['required_language'] = $post['required_language'];
        }
        if(!empty($post['document_scans_required'])){
            $arr['document_scans_required'] = $post['document_scans_required'];
        }
        if(!empty($post['us_visa_required'])){
            $arr['us_visa_required'] = $post['us_visa_required'];
        }
        if(!empty($post['us_visa_required_type_id'])){
            $arr['us_visa_required_type_id'] = $post['us_visa_required_type_id'];
        }
        if(!empty($post['schengen_visa_required'])){
            $arr['schengen_visa_required'] = $post['schengen_visa_required'];
        }
        if(!empty($post['notes'])){
            $arr['notes'] = $post['notes'];
        }
        if(!empty($post['negotiable'])){
            $arr['negotiable'] = 1;
        }
        if(!empty($post['salary_from'])){
            $arr['salary_from'] = $post['salary_from'] * $post['payroll_period'];
        }
        if(!empty($post['salary_to'])){
            $arr['salary_to'] = $post['salary_from'] * $post['payroll_period'];
        }

        return Yii::$app->db->createCommand()->insert('vacancy',$arr)->execute();
    }

    public function getRanks()
    {
        return $this->hasOne('app\models\Ranks', ['id' => 'rank_id']);
    }

    public function getCountries()
    {
        return $this->hasOne('app\models\Countries', ['id' => 'vessel_flag_id']);
    }

    public function getVesselTypes()
    {
        return $this->hasOne('app\models\VesselTypes', ['id' => 'vessel_type_id']);
    }

    public function getEngines(){
        return $this->hasOne('app\models\Engines',['id'=>'vessel_engine_type_id']);
    }

    public function getAll()
    {
        return static::find()->joinWith(['ranks'])->joinWith(['vesselTypes'])->where(['vacancy.status' => 1])->orderBy(['created_at' => SORT_DESC]);
    }

    public function addCount($id)
    {
        $current = static::find()->select('views_count')->where(['id'=>$id])->one()->views_count;
        $v = $current+1;
        return Yii::$app->db->createCommand()->update('vacancy',['views_count'=>$v])->execute();
    }

    public function getOne($id)
    {
        return static::find()->joinWith(['ranks'])->joinWith(['vesselTypes'])->joinWith(['countries'])->joinWith(['engines'])->andWhere(['vacancy.id' => $id])->one();
    }

    public function Filter($post)
    {
        $arr = "`vacancy`.`status` = 1";
        if(!empty($post['rank'])){
            $arr .= " AND `vacancy`.`rank_id`=".$post['rank'];
        }
        if(!empty($post['salary'])){
            if($post['period']=='Per month'){
                $arr .= " AND ((`vacancy`.`salary_from` >= ".$post['salary']." AND `vacancy`.`payroll_period`='30') OR (`vacancy`.`salary_from` >= ".($post['salary']/30*4)." AND `vacancy`.`payroll_period`='7') OR (`vacancy`.`salary_from` >= ".($post['salary']/30)." AND `vacancy`.`payroll_period`='1'))";
            }elseif($post['period']=='Per week'){
                $arr .= " AND ((`vacancy`.`salary_from` >= ".($post['salary']/7*30)." AND `vacancy`.`payroll_period`='30') OR (`vacancy`.`salary_from` >= ".$post['salary']." AND `vacancy`.`payroll_period`='7') OR (`vacancy`.`salary_from` >= ".($post['salary']/7)." AND `vacancy`.`payroll_period`='1'))";
            }elseif($post['period']=='Per day'){
                $arr .= " AND ((`vacancy`.`salary_from` >= ".($post['salary']*30)." AND `vacancy`.`payroll_period`='30') OR (`vacancy`.`salary_from` >= ".($post['salary']*7)." AND `vacancy`.`payroll_period`='7') OR (`vacancy`.`salary_from` >= ".$post['salary']." AND `vacancy`.`payroll_period`='1'))";
            }
            
        }
        if(!empty($post['vessel'])){
            $arr .= " AND `vacancy`.`vessel_type_id` = ".$post['vessel'];
        }
        return static::find()->joinWith(['ranks'])->joinWith(['vesselTypes'])->where($arr)->orderBy(['created_at' => SORT_DESC]);
    }

    public function getByCompany($id)
    {
        return static::find()->joinWith(['ranks'])->joinWith(['vesselTypes'])->where(['company_id'=>$id])->orderBy(['created_at' => SORT_DESC])->all();
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'rank_id' => Yii::t('app', 'Rank ID'),
            'salary_from' => Yii::t('app', 'Salary From'),
            'salary_to' => Yii::t('app', 'Salary To'),
            'salary_currency_id' => Yii::t('app', 'Salary Currency ID'),
            'payroll_period' => Yii::t('app', 'Payroll Period'),
            'duration_of_contract' => Yii::t('app', 'Duration Of Contract'),
            'duration_of_contract_offset' => Yii::t('app', 'Duration Of Contract Offset'),
            'joining_date_from' => Yii::t('app', 'Joining Date From'),
            'joining_date_to' => Yii::t('app', 'Joining Date To'),
            'company_vessel_id' => Yii::t('app', 'Company Vessel ID'),
            'vessel_name' => Yii::t('app', 'Vessel Name'),
            'vessel_flag_id' => Yii::t('app', 'Vessel Flag ID'),
            'vessel_type_id' => Yii::t('app', 'Vessel Type ID'),
            'vessel_deadweight' => Yii::t('app', 'Vessel Deadweight'),
            'vessel_engine_type_id' => Yii::t('app', 'Vessel Engine Type ID'),
            'vessel_engine_power' => Yii::t('app', 'Vessel Engine Power'),
            'vessel_year_built' => Yii::t('app', 'Vessel Year Built'),
            'vessel_shipowning_company_id' => Yii::t('app', 'Vessel Shipowning Company ID'),
            'vessel_shipowning_company_name' => Yii::t('app', 'Vessel Shipowning Company Name'),
            'required_age_from' => Yii::t('app', 'Required Age From'),
            'required_age_to' => Yii::t('app', 'Required Age To'),
            'required_citizenship' => Yii::t('app', 'Required Citizenship'),
            'required_language' => Yii::t('app', 'Required Language'),
            'document_scans_required' => Yii::t('app', 'Document Scans Required'),
            'us_visa_required' => Yii::t('app', 'Us Visa Required'),
            'us_visa_required_type_id' => Yii::t('app', 'Us Visa Required Type ID'),
            'schengen_visa_required' => Yii::t('app', 'Schengen Visa Required'),
            'views_count' => Yii::t('app', 'Views Count'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
