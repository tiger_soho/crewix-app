<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property integer $role
 * @property string $date
 */
class Users extends ActiveRecord
{
    public $mail;
    public $next;
    public $name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'account_type_id'], 'required'],
            [['account_type_id'], 'integer'],
            [['account_type_id'], 'in', 'range' => [1, 2], 'message' => 'You\'re trying to break the form'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['registration_step'], 'integer'],
            [['email', 'password'], 'string', 'max' => 255],
        ];
    }

    public function saveUser($post,$auth_key){
        $pass = Yii::$app->getSecurity()->generatePasswordHash($post['password'], 13);
        $arr = ['email'=>$post['email'],
                'password'=>$pass,
                'account_type_id'=>$post['account_type_id'],
                'auth_key'=>$auth_key,];

        return Yii::$app->db->createCommand()->insert('user',$arr)->execute();
    }

    public function saveRole($role,$id)
    {
        return Yii::$app->db->createCommand()->update('user',['role'=>$role],['id'=>$id])->execute();
    }

    public function getByMail($mail)
    {
        return static::find()->select('id')->where(['email'=>$mail])->one();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
        return $this->auth_key;
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function getChartDataYear()
    {
        return Yii::$app->db->createCommand('SELECT count(*) as count_month, DATE_FORMAT(signup_date,\'%M %Y\') as pick  FROM crewix.user WHERE DATE_FORMAT(signup_date,\'%Y\') = DATE_FORMAT(now(),\'%Y\')  group by date_format(signup_date,\'%Y-%m\') ORDER BY signup_date ASC')->queryAll();
    }

    public static function getChartDataWeek()
    {
        return Yii::$app->db->createCommand('SELECT count(*) as count_month, DATE_FORMAT(signup_date,\'%W\') as pick  FROM crewix.user WHERE yearweek(`signup_date`) = yearweek(curdate())  group by date_format(signup_date,\'%W\') ORDER BY signup_date ASC')->queryAll();
    }

    public static function getChartDataMonth()
    {
        return Yii::$app->db->createCommand('SELECT
            count(*) as count_month,
            DATE_FORMAT(signup_date,\'%d.%m\') as pick
        FROM
            crewix.user
        WHERE
            DATE_FORMAT(signup_date,\'%m %Y\') = DATE_FORMAT(now(),\'%m %Y\')
        group by date_format(signup_date,\'%d %M\') ORDER BY signup_date ASC')->queryAll();
    }

    public static function getChartDataAll()
    {
        return Yii::$app->db->createCommand('SELECT
            count(*) as count_month,
            DATE_FORMAT(signup_date,\'%m.%Y\') as pick
        FROM
            crewix.user
        group by date_format(signup_date,\'%m %Y\') ORDER BY signup_date ASC')->queryAll();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Mail'),
            'password' => Yii::t('app', 'Password'),
            'account_type_id' => Yii::t('app', 'U Type'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    public function getSeaman()
    {
        return $this->hasOne('app\models\Seaman', ['user_id' => 'id']);
    }

    public function getCompany()
    {
        return $this->hasOne('app\models\Companies', ['user_id' => 'id']);
    }

    public function getUserAccountType()
    {
        return $this->hasOne(UserAccountType::className(), ['id' => 'user_account_type_id']);
    }

}
