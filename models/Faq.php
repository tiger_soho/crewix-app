<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property string $id
 * @property string $faq_category_id
 * @property string $question
 * @property string $answer
 * @property string $position
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['faq_category_id', 'question', 'answer'], 'required'],
            [['faq_category_id', 'position'], 'integer'],
            [['answer'], 'string'],
            [['question'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'faq_category_id' => Yii::t('app', 'Faq Category ID'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'position' => Yii::t('app', 'Position'),
        ];
    }
}
