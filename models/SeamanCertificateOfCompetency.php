<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "seaman_certificate_of_competency".
 *
 * @property integer $id
 * @property integer $seaman_id
 * @property integer $rank_id
 * @property integer $document_type_id
 * @property string $number
 * @property integer $place_of_issue_id
 * @property string $date_of_issue
 * @property string $date_of_expiry
 * @property integer $parent_document_id
 * @property integer $scan_id
 */
class SeamanCertificateOfCompetency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_certificate_of_competency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seaman_id', 'rank_id', 'document_type_id', 'number', 'place_of_issue_id', 'date_of_issue'], 'required'],
            [['rank_id', 'document_type_id', 'place_of_issue_id', 'parent_document_id', 'scan_id'], 'integer'],

            [
                'date_of_expiry',
                function () {
                    if (strtotime($this->date_of_expiry) <= strtotime($this->date_of_issue)) {
                        $this->addError('date_of_expiry', 'Please give correct Dates');
                        $this->addError('date_of_issue', 'Please give correct Dates');
                        return false;
                    }
                }
            ],
            [['number'], 'match', 'pattern' => '/^[a-zA-Z0-9_\-\/.]+$/'],
            [['number'], 'string', 'max' => 128],
        ];
    }

    public function validateDate(){
        VarDumper::dump($this->date_of_expiry);die;


    }

    public function saveDoc($uid,$post)
    {
        $doc_issue = explode('.', $post['doc_issue']);
        $doc_issue = $doc_issue[2].'-'.$doc_issue[1].'-'.$doc_issue[0];
        $arr['doc_issue'] = $doc_issue;
        $arr = [
            'seaman_id'=>$uid,
            'rank_id'=>$post['rank_id'],
            'document_type_id'=>$post['document_type_id'],
            'number'=>$post['number'],
            'place_of_issue_id'=>$post['place_of_issue_id'],
            'date_of_issue'=>$doc_issue,
        ];
        if(!empty($post['parent_document_id'])){
            $arr['parent_document_id'] = $post['parent_document_id'];
        }
        if(!empty($post['date_of_expiry'])){
            $doc_expiry = explode('.', $post['date_of_expiry']);
            $doc_expiry = $doc_expiry[2].'-'.$doc_expiry[1].'-'.$doc_expiry[0];
            $arr['date_of_expiry'] = $doc_expiry;
        }

        return Yii::$app->db->createCommand()->insert('seaman_certificate_of_competency',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seaman_id' => Yii::t('app', 'Seaman ID'),
            'rank_id' => Yii::t('app', 'Rank ID'),
            'document_type_id' => Yii::t('app', 'Document Type ID'),
            'number' => Yii::t('app', 'Number'),
            'place_of_issue_id' => Yii::t('app', 'Place Of Issue ID'),
            'date_of_issue' => Yii::t('app', 'Date Of Issue'),
            'date_of_expiry' => Yii::t('app', 'Date Of Expiry'),
            'parent_document_id' => Yii::t('app', 'Parent Document ID'),
            'scan_id' => Yii::t('app', 'Scan ID'),
        ];
    }

    public function getDocType()
    {
        return $this->hasOne(CertificateOfCompetencyType::className(),['id'=>'document_type_id']);
    }

    public static function getAllByScan($id)
    {
        return static::find()->joinWith('docType')->where(['seaman_certificate_of_competency.scan_id'=>$id])->all();
    }
}
