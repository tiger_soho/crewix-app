<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seaman_document_scan".
 *
 * @property string $id
 * @property string $seaman_id
 * @property string $scan_file_id
 * @property string $scan_thumb_file_id
 * @property integer $img_width
 * @property integer $img_height
 */
class SeamanDocumentScan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seaman_document_scan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seaman_id', 'scan_file_id', 'scan_thumb_file_id', 'img_width', 'img_height'], 'required'],
            [['seaman_id', 'scan_file_id', 'scan_thumb_file_id', 'img_width', 'img_height'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seaman_id' => 'Seaman ID',
            'scan_file_id' => 'Scan File ID',
            'scan_thumb_file_id' => 'Scan Thumb File ID',
            'img_width' => 'Img Width',
            'img_height' => 'Img Height',
        ];
    }
}
