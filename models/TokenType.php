<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "token_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $lifetime
 */
class TokenType extends \yii\db\ActiveRecord
{
    const BIND_EMAIL = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'token_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lifetime'], 'required'],
            [['lifetime'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'lifetime' => Yii::t('app', 'Lifetime'),
        ];
    }
}
