<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property string $id
 * @property string $user_id
 * @property string $notification_type_id
 * @property integer $value
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'notification_type_id', 'value'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'notification_type_id' => Yii::t('app', 'Notification Type ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }
}
