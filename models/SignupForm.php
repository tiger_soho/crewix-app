<?php
namespace app\models;

use app\models\User;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $mail;
    public $password;
    public $u_type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail', 'password', 'u_type',], 'required'],
            [['u_type'], 'integer'],
            [['mail'], 'email'],
            [['mail', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
