<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "privacy_policy".
 *
 * @property string $id
 * @property string $section_name
 * @property string $text
 * @property string $updated_at
 * @property string $position
 */
class PrivacyPolicy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'privacy_policy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_name', 'text'], 'required'],
            [['text'], 'string'],
            [['updated_at'], 'safe'],
            [['position'], 'integer'],
            [['section_name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_name' => 'Section Name',
            'text' => 'Text',
            'updated_at' => 'Updated At',
            'position' => 'Position',
        ];
    }
}
