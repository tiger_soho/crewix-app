<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vessel_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $group_id
 */
class VesselTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vessel_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['name', 'group_id'], 'required'],
            [['group_id'], 'integer'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'group_id' => Yii::t('app', 'Group ID'),
        ];
    }
}
