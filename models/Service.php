<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryBuilder;
use yii\db\Connection;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $rank
 * @property string $vessel_name
 * @property integer $country
 * @property integer $vessel_type
 * @property string $dwt
 * @property integer $me_type
 * @property string $me_power
 * @property string $measure
 * @property string $shipowner
 * @property string $agency
 * @property string $from_date
 * @property string $to_date
 */
class Service extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rank', 'country', 'vessel_type', 'me_type'], 'integer'],
            [['from_date', 'to_date'], 'safe'],
            [['vessel_name', 'dwt', 'shipowner', 'agency'], 'string', 'max' => 255],
            [['me_power'], 'string', 'max' => 20],
            [['measure'], 'string', 'max' => 5],
        ];
    }

    public function saveServ($uid,$post)
    {
        $from = explode('.', $post['from_date']);
        $from = $from[2].'-'.$from[1].'-'.$from[0];
        $to = explode('.', $post['to_date']);
        $to = $to[2].'-'.$to[1].'-'.$to[0];
        $arr = [
            'uid'=>$uid,
            'rank'=>$post['rank'],
            'vessel_name'=>$post['vessel_name'],
            'country'=>$post['country'],
            'vessel_type'=>$post['vessel_type'],
            'dwt'=>$post['dwt'],
            'me_type'=>$post['me_type'],
            'me_power'=>$post['me_power'],
            'measure'=>$post['measure'],
            'shipowner'=>$post['shipowner'],
            'agency'=>$post['agency'],
            'from_date'=>$from,
            'to_date'=>$to,
        ];
        return Yii::$app->db->createCommand()->insert('service',$arr)->execute();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'rank' => Yii::t('app', 'Rank'),
            'vessel_name' => Yii::t('app', 'Vessel Name'),
            'country' => Yii::t('app', 'Country'),
            'vessel_type' => Yii::t('app', 'Vessel Type'),
            'dwt' => Yii::t('app', 'Dwt'),
            'me_type' => Yii::t('app', 'Me Type'),
            'me_power' => Yii::t('app', 'Me Power'),
            'measure' => Yii::t('app', 'Measure'),
            'shipowner' => Yii::t('app', 'Shipowner'),
            'agency' => Yii::t('app', 'Agency'),
            'from_date' => Yii::t('app', 'From Date'),
            'to_date' => Yii::t('app', 'To Date'),
        ];
    }
}
