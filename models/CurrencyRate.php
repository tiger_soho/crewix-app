<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency_rate".
 *
 * @property string $id
 * @property string $from_currency_id
 * @property string $to_currency_id
 * @property string $value
 * @property string $updated_at
 */
class CurrencyRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_currency_id', 'to_currency_id'], 'integer'],
            [['from_currency_id', 'to_currency_id', 'value'], 'required'],
            [['value'], 'number'],
            [['updated_at'], 'safe']
        ];
    }

    public function getCurrencyFrom()
    {
        return $this->hasOne(Currency::className(), ['id' => 'from_currency_id']);
    }

    public function getCurrencyTo()
    {
        return $this->hasOne(Currency::className(), ['id' => 'to_currency_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_currency_id' => Yii::t('app', 'From Currency ID'),
            'to_currency_id' => Yii::t('app', 'To Currency ID'),
            'value' => Yii::t('app', 'Value'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
